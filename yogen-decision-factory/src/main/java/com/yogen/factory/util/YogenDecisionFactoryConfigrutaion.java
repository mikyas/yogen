package com.yogen.factory.util;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import com.yogen.log.util.YogenLogConfigruation;

@Configuration
@ComponentScan("com.yogen.factory")
@Import(YogenLogConfigruation.class)
public class YogenDecisionFactoryConfigrutaion {
    public YogenDecisionFactoryConfigrutaion() {
    }
}
