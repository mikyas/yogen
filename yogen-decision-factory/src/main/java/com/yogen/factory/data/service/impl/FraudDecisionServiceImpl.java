package com.yogen.factory.data.service.impl;

import com.yogen.factory.data.service.DefinedRuleService;
import com.yogen.factory.data.service.FraudDecisionService;
import com.yogen.factory.filter.YogenRuleExecutor;
import com.yogen.factory.model.DecisionDetail;
import com.yogen.factory.model.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class FraudDecisionServiceImpl implements FraudDecisionService {

    @Autowired
    YogenRuleExecutor yogenRuleExecutor;

    @Autowired
    DefinedRuleService definedRuleService;

    @Override
    public DecisionDetailDTO getRuleExecutorResult(TransactionInfoDTO transactionInfoDTO) throws Exception {
        List<DefinedRuleDTO> merchantDefinedRules = definedRuleService.getDefinedRuleDTOListByIntegratorId(transactionInfoDTO.getIntegeratorId());
        RuleExecutorTraceDTO ruleTrace = yogenRuleExecutor.checkTransactionForFraudByDefinedRules(transactionInfoDTO, merchantDefinedRules, false);

        DecisionDetailDTO decisionDetailDTO = new DecisionDetailDTO();
        decisionDetailDTO.setTransactionVersionId(transactionInfoDTO.getTransactionVersionId());
        decisionDetailDTO.setTransactionUID(transactionInfoDTO.getTransactionUID());
        decisionDetailDTO.setDecisionDate(new Date());

        decisionDetailDTO.setResult(false);
//        decisionDetailDTO.setNotes();
        StringBuilder notes = new StringBuilder();
        Integer riskScore = 0;
        boolean riskScoreExceeded = false;
        for (RuleExecuteResult ruleResult : ruleTrace.getRuleResults()) {
            if (ruleResult.isFraud()) {
                DefinedRuleDTO definedRuleDTO = merchantDefinedRules.stream().filter(rule -> ruleResult.getSystemRuleCode() == rule.getSystemRuleCode()).findAny().orElse(null);
                riskScore += definedRuleDTO != null ? definedRuleDTO.getRiskScore() : 0;
                notes.append(ruleResult.getMessage());
                if (riskScore > transactionInfoDTO.getRiskScoreLimit()) {
                    riskScoreExceeded = true;
                    break;
                }
            }
        }
        if (riskScoreExceeded) {
            decisionDetailDTO.setNotes(notes.toString());
            decisionDetailDTO.setReason("Risk skoru aşıldığı için fraud tespit edildi.");
        }
        createDecisionDetailByDTO(decisionDetailDTO);
        return decisionDetailDTO;
    }

    private void createDecisionDetailByDTO(DecisionDetailDTO decisionDetailDTO) {
        DecisionDetail decisionDetail = new DecisionDetail();
        decisionDetail.setTransactionVersionId(decisionDetailDTO.getTransactionVersionId());
        decisionDetail.setResult(decisionDetailDTO.getResult());
        decisionDetail.setReason(decisionDetailDTO.getReason());
        decisionDetail.setNotes(decisionDetailDTO.getNotes());


    }
}
