package com.yogen.factory.data.dao.impl;

import com.yogen.factory.data.dao.DecisionDetailDao;
import com.yogen.factory.model.DecisionDetail;
import org.springframework.stereotype.Repository;

@Repository
public class DecisionDetailDaoImpl extends BaseDecisionFactoryHibernateDaoSupport<DecisionDetail, Long> implements DecisionDetailDao {
}
