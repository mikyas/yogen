package com.yogen.factory.data.service.impl;

import com.yogen.factory.data.dao.DefinedRuleDao;
import com.yogen.factory.data.service.DefinedRuleService;
import com.yogen.factory.model.DefinedRule;
import com.yogen.factory.model.dto.DefinedRuleDTO;
import com.yogen.util.EnumUtil;
import com.yogen.util.enumtype.TimePeriod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class DefinedRuleServiceImpl implements DefinedRuleService {

    @Autowired
    DefinedRuleDao definedRuleDao;

    @Override
    public void createDefinedRulesByDTOs(List<DefinedRuleDTO> definedRuleDTOs) {
        for (DefinedRuleDTO definedRuleDTO : definedRuleDTOs) {
            DefinedRule definedRule = initializeDefinedRule(definedRuleDTO);
            definedRuleDao.persist(definedRule);
        }

    }

    @Override
    public List<DefinedRuleDTO> getDefinedRuleDTOListByIntegratorId(Long integratorId) {
        List<DefinedRule> rules;
        List<DefinedRuleDTO> rulesDTO = new ArrayList<>();
        rules = definedRuleDao.getDefinedRuleListByIntegratorId(integratorId);
        for (DefinedRule rule : rules) {
            DefinedRuleDTO definedRuleDTO = new DefinedRuleDTO();
            definedRuleDTO.setIntegratorId(rule.getIntegratorId());
            definedRuleDTO.setId(rule.getId());
            definedRuleDTO.setSystemRuleCode(rule.getSystemRuleCode());
            definedRuleDTO.setLimit(rule.getLimit());
            definedRuleDTO.setTimePeriod(EnumUtil.safeValueOf(TimePeriod.class, rule.getTimePeriod()));
            definedRuleDTO.setTimeBound(rule.getTimeBound());
            definedRuleDTO.setValue(rule.getValue());
            definedRuleDTO.setRiskScore(rule.getRiskScore());
            rulesDTO.add(definedRuleDTO);
        }
        return rulesDTO;
    }

    @Override
    public void updateDefinedRuleByDTO(DefinedRuleDTO definedRuleDTO) {
        DefinedRule definedRule = initializeDefinedRule(definedRuleDTO);
        definedRuleDao.update(definedRule);
    }

    @Override
    public void deleteDefinedRuleByDTO(Long deletedDefinedRuleId) {
        definedRuleDao.delete(definedRuleDao.get(deletedDefinedRuleId));
    }

    private DefinedRule initializeDefinedRule(DefinedRuleDTO definedRuleDTO) {
        DefinedRule definedRule = new DefinedRule();
        definedRule.setIntegratorId(definedRuleDTO.getIntegratorId());
        definedRule.setId(definedRuleDTO.getId());
        definedRule.setSystemRuleCode(definedRuleDTO.getSystemRuleCode());
        definedRule.setLimit(definedRuleDTO.getLimit());
        definedRule.setTimePeriod(definedRuleDTO.getTimePeriod() != null ? definedRuleDTO.getTimePeriod().getValue() : null);
        definedRule.setTimeBound(definedRuleDTO.getTimeBound());
        definedRule.setValue(definedRuleDTO.getValue());
        definedRule.setRiskScore(definedRuleDTO.getRiskScore());
        return definedRule;
    }

}
