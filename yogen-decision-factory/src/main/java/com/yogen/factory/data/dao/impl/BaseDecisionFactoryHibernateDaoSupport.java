package com.yogen.factory.data.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import com.yogen.factory.data.dao.IBaseDecisionFactoryHibernateDao;
import com.yogen.factory.model.AbstractBaseFactoryEntity;

/**
 * @param <T>
 * @param <ID>
 * @author Tarik.Mikyas
 */
public abstract class BaseDecisionFactoryHibernateDaoSupport<T extends AbstractBaseFactoryEntity, ID extends Serializable> extends HibernateDaoSupport implements IBaseDecisionFactoryHibernateDao<T, ID> {
    private Class<T> type;

    public BaseDecisionFactoryHibernateDaoSupport() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Autowired
    public void bindSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    public Class<T> getType() {
        return type;
    }

    public void setType(Class<T> type) {
        this.type = type;
    }

    protected Session getCurrentSession() {
        return getHibernateTemplate().getSessionFactory().getCurrentSession();
    }

    public ID persist(T newInstance) {
        return (ID) getHibernateTemplate().save(newInstance);
    }

    public void update(T transientObject) {
        getHibernateTemplate().update(transientObject);
    }

    public void delete(T persistentObject) {
        getHibernateTemplate().delete(persistentObject);
    }

    public T get(ID id) {
        return (T) getHibernateTemplate().get(type, id);
    }

    public T load(ID id) {
        return (T) getHibernateTemplate().load(type, id);
    }

    public List<T> findAll() {
        return (List<T>) getHibernateTemplate().find("from " + type.getName());
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    public void clear() {
        getHibernateTemplate().clear();
    }

    public ID getIdentifier(T transientObject) {
        return (ID) getCurrentSession().getIdentifier(transientObject);
    }

}
