package com.yogen.factory.data.service;

import com.yogen.factory.model.dto.DecisionDetailDTO;
import com.yogen.factory.model.dto.TransactionInfoDTO;

public interface FraudDecisionService {

    DecisionDetailDTO getRuleExecutorResult(TransactionInfoDTO transactionInfoDTO) throws Exception;

}
