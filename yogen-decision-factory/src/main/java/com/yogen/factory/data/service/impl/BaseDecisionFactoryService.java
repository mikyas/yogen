package com.yogen.factory.data.service.impl;

import java.io.Serializable;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import com.yogen.factory.data.dao.IBaseDecisionFactoryHibernateDao;
import com.yogen.factory.data.service.IBaseDecisionFactoryService;
import com.yogen.factory.model.AbstractBaseFactoryEntity;
import com.yogen.util.exception.YogenDatabaseException;


/**
 * @param <T>
 * @param <ID>
 * @author Tarik.Mikyas<br>
 */
@Transactional
public abstract class BaseDecisionFactoryService<T extends AbstractBaseFactoryEntity, ID extends Serializable> implements IBaseDecisionFactoryService<T, ID> {

    protected IBaseDecisionFactoryHibernateDao<T, ID> getDaoTemplate;

    @Override
    public ID persist(T newInstance) {
        return getDaoTemplate.persist(newInstance);
    }

    @Override
    public void update(T transientObject) {
        getDaoTemplate.update(transientObject);
    }

    @Override
    public void delete(T persistentObject) throws YogenDatabaseException {
        getDaoTemplate.delete(persistentObject);
    }

    @Override
    public T get(ID id) {
        return getDaoTemplate.get(id);
    }

    @Override
    public T load(ID id) {
        return getDaoTemplate.load(id);
    }

    @Override
    public List<T> findAll() {
        return getDaoTemplate.findAll();
    }

    @Override
    public ID getIdentifier(T transientObject) {
        return getDaoTemplate.getIdentifier(transientObject);
    }
}
