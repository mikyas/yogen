package com.yogen.factory.data.dao;

import com.yogen.factory.model.DecisionDetail;

public interface DecisionDetailDao extends IBaseDecisionFactoryHibernateDao<DecisionDetail, Long> {
}
