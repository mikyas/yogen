package com.yogen.factory.data.dao;

import com.yogen.factory.model.DefinedRule;

import java.util.List;

public interface DefinedRuleDao extends IBaseDecisionFactoryHibernateDao<DefinedRule,Long>{
    List<DefinedRule> getDefinedRuleListByIntegratorId(Long integratorId);
}
