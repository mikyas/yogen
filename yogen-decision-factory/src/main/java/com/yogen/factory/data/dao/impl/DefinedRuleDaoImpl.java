package com.yogen.factory.data.dao.impl;

import com.yogen.factory.data.dao.DefinedRuleDao;
import com.yogen.factory.model.DefinedRule;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.List;

@Repository
public class DefinedRuleDaoImpl extends BaseDecisionFactoryHibernateDaoSupport<DefinedRule,Long> implements DefinedRuleDao {
    @Override
    public List<DefinedRule> getDefinedRuleListByIntegratorId(Long integratorId) {
        CriteriaBuilder builder = getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<DefinedRule> criteriaQuery = builder.createQuery(DefinedRule.class);
        Root<DefinedRule> root = criteriaQuery.from(DefinedRule.class);

        criteriaQuery.select(root);
        criteriaQuery.where( builder.equal( root.get("integratorId"), integratorId) );
        return getCurrentSession().createQuery( criteriaQuery ).getResultList();

    }
}
