package com.yogen.factory.data.service;

import com.yogen.factory.model.dto.DefinedRuleDTO;

import java.util.List;

public interface DefinedRuleService {

    void createDefinedRulesByDTOs(List<DefinedRuleDTO> definedRuleDTOs);

    List<DefinedRuleDTO> getDefinedRuleDTOListByIntegratorId(Long integratorId);

    void updateDefinedRuleByDTO(DefinedRuleDTO definedRuleDTO);

    void deleteDefinedRuleByDTO(Long deletedDefinedRuleId);
}
