package com.yogen.factory.model.dto;

import java.util.List;

public class RuleExecutorTraceDTO {
    private String transactionUID;
    private List<RuleExecuteResult> ruleResults;

    public String getTransactionUID() {
        return transactionUID;
    }

    public void setTransactionUID(String transactionUID) {
        this.transactionUID = transactionUID;
    }

    public List<RuleExecuteResult> getRuleResults() {
        return ruleResults;
    }

    public void setRuleResults(List<RuleExecuteResult> ruleResults) {
        this.ruleResults = ruleResults;
    }

}
