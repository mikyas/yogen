package com.yogen.factory.model.dto;

public class RuleExecuteResult {
    private int systemRuleCode;
    private boolean fraud;
    private String message;

    public RuleExecuteResult(boolean decision, String message) {
        this.fraud = decision;
        this.message = message;
    }

    public RuleExecuteResult(int systemRuleCode, boolean fraud, String message) {
        this.systemRuleCode = systemRuleCode;
        this.fraud = fraud;
        this.message = message;
    }

    public int getSystemRuleCode() {
        return systemRuleCode;
    }

    public void setSystemRuleCode(int systemRuleCode) {
        this.systemRuleCode = systemRuleCode;
    }

    public boolean isFraud() {
        return fraud;
    }

    public void setFraud(boolean fraud) {
        this.fraud = fraud;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
