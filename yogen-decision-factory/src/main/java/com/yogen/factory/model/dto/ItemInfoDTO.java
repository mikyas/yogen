package com.yogen.factory.model.dto;

import com.yogen.util.enumtype.ItemType;

public class ItemInfoDTO {

    private ItemType itemType;
    private Double itemPrice;
    private Integer itemQuantity;
    private String sellerName;
    private AddressInfoDTO sellerAddress;

    public ItemInfoDTO() {
    }

    public ItemInfoDTO(ItemType itemType, Double itemPrice, Integer itemQuantity, String sellerName, AddressInfoDTO sellerAddress) {
        this.itemType = itemType;
        this.itemPrice = itemPrice;
        this.itemQuantity = itemQuantity;
        this.sellerName = sellerName;
        this.sellerAddress = sellerAddress;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public Double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(Double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public Integer getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(Integer itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public AddressInfoDTO getSellerAddress() {
        return sellerAddress;
    }

    public void setSellerAddress(AddressInfoDTO sellerAddress) {
        this.sellerAddress = sellerAddress;
    }
}
