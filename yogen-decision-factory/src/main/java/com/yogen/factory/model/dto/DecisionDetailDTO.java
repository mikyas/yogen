package com.yogen.factory.model.dto;

import java.util.Date;

public class DecisionDetailDTO {

    private String transactionUID;
    private Long transactionVersionId;
    private Date decisionDate;
    private Boolean result;
    private String notes;
    private String reason;

    public String getTransactionUID() {
        return transactionUID;
    }

    public void setTransactionUID(String transactionUID) {
        this.transactionUID = transactionUID;
    }

    public Long getTransactionVersionId() {
        return transactionVersionId;
    }

    public void setTransactionVersionId(Long transactionVersionId) {
        this.transactionVersionId = transactionVersionId;
    }

    public Date getDecisionDate() {
        return decisionDate;
    }

    public void setDecisionDate(Date decisionDate) {
        this.decisionDate = decisionDate;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
