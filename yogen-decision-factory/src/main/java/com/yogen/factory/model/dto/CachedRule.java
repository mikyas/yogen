package com.yogen.factory.model.dto;

import java.util.List;

public class CachedRule {
    private long validityPeriod;
    private int count;
    private List<String> valueList;

    public CachedRule(long validityPeriod, int count) {
        this.validityPeriod = validityPeriod;
        this.count = count;
    }

    public CachedRule(long validityPeriod, int count, List<String> valueList) {
        this.validityPeriod = validityPeriod;
        this.count = count;
        this.valueList = valueList;
    }

    public long getValidityPeriod() {
        return validityPeriod;
    }

    public void setValidityPeriod(long validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<String> getValueList() {
        return valueList;
    }

    public void setValueList(List<String> valueList) {
        this.valueList = valueList;
    }
}
