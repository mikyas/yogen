package com.yogen.factory.model;


import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * @author Tarik.Mikyas<br>
 * <p>
 * Tum entitylerin extend edildigi Base sinif
 */
@MappedSuperclass
public abstract class AbstractBaseFactoryEntity implements IGenericFactoryEntity, Serializable, Cloneable {


    @Override
    public abstract Long getId();

    @Override
    public abstract void setId(Long id);

    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;

    /**
     * @author Tarik.Mikyas <br>
     * Tum entitylerin olusturulma asamasinda date degiskenlerinin setlendigi yordamdir.
     */
    private void init() {
//		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        this.createdDate = Calendar.getInstance().getTime();
        this.updatedDate = null;
    }

    public AbstractBaseFactoryEntity() {
        init();
    }


    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println("" + e);
            return null;
        }
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    /**
     * @author Tarik.Mikyas
     * @return<br> varsa transient alanlarin kontrolu icin
     */
    @Transient
    public String getTransitVariableDescription() {
        return "";
    }

}