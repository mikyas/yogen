package com.yogen.factory.model.dto;

import java.util.Date;
import java.util.List;

public class TransactionInfoDTO {

    private Long integeratorId;
    private Integer riskScoreLimit;
    private Date transactionDate;
    private String transactionUID;
    private long transactionVersionId;
    private List<ItemInfoDTO> items;
    private String shipmentDeliveryCompany;
    private AddressInfoDTO shipmentAddress;
    private String billingSerialNo;
    private AddressInfoDTO billingAddress;
    private String purchaserUID;
    private String purchaserFirstName;
    private String purchaserLastName;
    private String purchaserPhone;
    private String purchaserMail;
    private Integer paymentInstallment;
    private Double totalPrice;
    private Double totalPaidPrice;
    private String paymentCurrency;
    private String creditCardNo;
    private String deviceIpAddress;

    public TransactionInfoDTO() {
    }

    public TransactionInfoDTO(String transactionUID, List<ItemInfoDTO> items, String shipmentDeliveryCompany, AddressInfoDTO shipmentAddress, String billingSerialNo, AddressInfoDTO billingAddress, String purchaserUID, String purchaserFirstName, String purchaserLastName, String purchaserPhone, String purchaserMail, Integer paymentInstallment, Double totalPrice, Double totalPaidPrice, String paymentCurrency, String creditCardNo, String deviceIpAddress) {
        this.transactionUID = transactionUID;
        this.items = items;
        this.shipmentDeliveryCompany = shipmentDeliveryCompany;
        this.shipmentAddress = shipmentAddress;
        this.billingSerialNo = billingSerialNo;
        this.billingAddress = billingAddress;
        this.purchaserFirstName = purchaserFirstName;
        this.purchaserLastName = purchaserLastName;
        this.purchaserPhone = purchaserPhone;
        this.purchaserMail = purchaserMail;
        this.purchaserUID = purchaserUID;
        this.paymentInstallment = paymentInstallment;
        this.totalPrice = totalPrice;
        this.totalPaidPrice = totalPaidPrice;
        this.paymentCurrency = paymentCurrency;
        this.creditCardNo = creditCardNo;
        this.deviceIpAddress = deviceIpAddress;
    }

    public Long getIntegeratorId() {
        return integeratorId;
    }

    public void setIntegeratorId(Long integeratorId) {
        this.integeratorId = integeratorId;
    }

    public Integer getRiskScoreLimit() {
        return riskScoreLimit;
    }

    public void setRiskScoreLimit(Integer riskScoreLimit) {
        this.riskScoreLimit = riskScoreLimit;
    }

    public long getTransactionVersionId() {
        return transactionVersionId;
    }

    public void setTransactionVersionId(long transactionVersionId) {
        this.transactionVersionId = transactionVersionId;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionUID() {
        return transactionUID;
    }

    public void setTransactionUID(String transactionUID) {
        this.transactionUID = transactionUID;
    }

    public List<ItemInfoDTO> getItems() {
        return items;
    }

    public void setItems(List<ItemInfoDTO> items) {
        this.items = items;
    }

    public String getShipmentDeliveryCompany() {
        return shipmentDeliveryCompany;
    }

    public void setShipmentDeliveryCompany(String shipmentDeliveryCompany) {
        this.shipmentDeliveryCompany = shipmentDeliveryCompany;
    }

    public AddressInfoDTO getShipmentAddress() {
        return shipmentAddress;
    }

    public void setShipmentAddress(AddressInfoDTO shipmentAddress) {
        this.shipmentAddress = shipmentAddress;
    }

    public String getBillingSerialNo() {
        return billingSerialNo;
    }

    public void setBillingSerialNo(String billingSerialNo) {
        this.billingSerialNo = billingSerialNo;
    }

    public AddressInfoDTO getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(AddressInfoDTO billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getPurchaserUID() {
        return purchaserUID;
    }

    public void setPurchaserUID(String purchaserUID) {
        this.purchaserUID = purchaserUID;
    }

    public String getPurchaserFirstName() {
        return purchaserFirstName;
    }

    public void setPurchaserFirstName(String purchaserFirstName) {
        this.purchaserFirstName = purchaserFirstName;
    }

    public String getPurchaserLastName() {
        return purchaserLastName;
    }

    public void setPurchaserLastName(String purchaserLastName) {
        this.purchaserLastName = purchaserLastName;
    }

    public String getPurchaserPhone() {
        return purchaserPhone;
    }

    public void setPurchaserPhone(String purchaserPhone) {
        this.purchaserPhone = purchaserPhone;
    }

    public String getPurchaserMail() {
        return purchaserMail;
    }

    public void setPurchaserMail(String purchaserMail) {
        this.purchaserMail = purchaserMail;
    }

    public Integer getPaymentInstallment() {
        return paymentInstallment;
    }

    public void setPaymentInstallment(Integer paymentInstallment) {
        this.paymentInstallment = paymentInstallment;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Double getTotalPaidPrice() {
        return totalPaidPrice;
    }

    public void setTotalPaidPrice(Double totalPaidPrice) {
        this.totalPaidPrice = totalPaidPrice;
    }

    public String getPaymentCurrency() {
        return paymentCurrency;
    }

    public void setPaymentCurrency(String paymentCurrency) {
        this.paymentCurrency = paymentCurrency;
    }

    public String getCreditCardNo() {
        return creditCardNo;
    }

    public void setCreditCardNo(String creditCardNo) {
        this.creditCardNo = creditCardNo;
    }

    public String getDeviceIpAddress() {
        return deviceIpAddress;
    }

    public void setDeviceIpAddress(String deviceIpAddress) {
        this.deviceIpAddress = deviceIpAddress;
    }
}
