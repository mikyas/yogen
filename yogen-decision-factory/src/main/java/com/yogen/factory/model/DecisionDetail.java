package com.yogen.factory.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "decision_detail", catalog = "yogen_decisionfactory", schema = "dbo")
public class DecisionDetail extends AbstractBaseFactoryEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "TRANSACTION_VERSION_ID")
    private Long transactionVersionId;

    @Column(name = "DECIDED_AT", nullable = false)
    private Date decidedAt;

    @Column(name = "RESULT", nullable = false)
    private Boolean result;

    @Column(name = "REASON")
    private String reason;

    @Column(name = "NOTES")
    private String notes;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Long getTransactionVersionId() {
        return transactionVersionId;
    }

    public void setTransactionVersionId(Long transactionVersionId) {
        this.transactionVersionId = transactionVersionId;
    }

    public Date getDecidedAt() {
        return decidedAt;
    }

    public void setDecidedAt(Date decidedAt) {
        this.decidedAt = decidedAt;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
