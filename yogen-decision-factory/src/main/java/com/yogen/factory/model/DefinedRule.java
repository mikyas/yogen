package com.yogen.factory.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "defined_rule", catalog = "yogen_decisionfactory", schema = "dbo")
public class DefinedRule extends AbstractBaseFactoryEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "SYSTEM_RULE_CODE")
    private Integer systemRuleCode;

    @Column(name = "INTEGRATOR_ID", nullable = false)
    private Long integratorId;

    @Column(name = "LIMIT")
    private Integer limit;

    @Column(name = "TIME_PERIOD")
    private Integer timePeriod;

    @Column(name = "TIME_BOUND")
    private Integer timeBound;

    @Column(name = "VALUE")
    private String value;

    @Column(name = "RISK_SCORE")
    private Integer riskScore;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSystemRuleCode() {
        return systemRuleCode;
    }

    public void setSystemRuleCode(Integer systemRuleCode) {
        this.systemRuleCode = systemRuleCode;
    }

    public Long getIntegratorId() {
        return integratorId;
    }

    public void setIntegratorId(Long integratorId) {
        this.integratorId = integratorId;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(Integer timePeriod) {
        this.timePeriod = timePeriod;
    }

    public Integer getTimeBound() {
        return timeBound;
    }

    public void setTimeBound(Integer timeBound) {
        this.timeBound = timeBound;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getRiskScore() {
        return riskScore;
    }

    public void setRiskScore(Integer riskScore) {
        this.riskScore = riskScore;
    }
}
