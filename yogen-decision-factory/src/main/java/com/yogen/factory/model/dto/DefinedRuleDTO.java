package com.yogen.factory.model.dto;

import com.yogen.util.enumtype.RuleType;
import com.yogen.util.enumtype.TimePeriod;

public class DefinedRuleDTO {

    private Long id;
    private Long integratorId;
    private RuleType ruleType;
    private Integer systemRuleCode;
    private String description;
    private TimePeriod timePeriod;
    private Integer timeBound;
    private Integer limit;
    private String value;
    private Integer riskScore;

    public DefinedRuleDTO() {
    }

    public DefinedRuleDTO(Integer systemRuleCode, TimePeriod timePeriod, Integer timeBound, Integer limit, String value) {
        this.systemRuleCode = systemRuleCode;
        this.timePeriod = timePeriod;
        this.timeBound = timeBound;
        this.limit = limit;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIntegratorId() {
        return integratorId;
    }

    public void setIntegratorId(Long integratorId) {
        this.integratorId = integratorId;
    }

    public RuleType getRuleType() {
        return ruleType;
    }

    public void setRuleType(RuleType ruleType) {
        this.ruleType = ruleType;
    }

    public Integer getSystemRuleCode() {
        return systemRuleCode;
    }

    public void setSystemRuleCode(Integer systemRuleCode) {
        this.systemRuleCode = systemRuleCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TimePeriod getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(TimePeriod timePeriod) {
        this.timePeriod = timePeriod;
    }

    public Integer getTimeBound() {
        return timeBound;
    }

    public void setTimeBound(Integer timeBound) {
        this.timeBound = timeBound;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getRiskScore() {
        return riskScore;
    }

    public void setRiskScore(Integer riskScore) {
        this.riskScore = riskScore;
    }
}
