package com.yogen.factory.model.dto;

public class AddressInfoDTO {
    private String address1;
    private String address2;
    private String neighbourhood;
    private String city;
    private String region;
    private String country;
    private String poBoxNumber;
    private String postalCode;

    public AddressInfoDTO() {
    }

    public AddressInfoDTO(String address1, String address2, String neighbourhood, String city, String region, String country, String poBoxNumber, String postalCode) {
        this.address1 = address1;
        this.address2 = address2;
        this.neighbourhood = neighbourhood;
        this.city = city;
        this.region = region;
        this.country = country;
        this.poBoxNumber = poBoxNumber;
        this.postalCode = postalCode;
    }

    public String getFullAddress() {
        return address1 + " " + address2 + " " + neighbourhood + " " + city + " " + region + " " + country + " " + postalCode + " " + poBoxNumber;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getNeighbourhood() {
        return neighbourhood;
    }

    public void setNeighbourhood(String neighbourhood) {
        this.neighbourhood = neighbourhood;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPoBoxNumber() {
        return poBoxNumber;
    }

    public void setPoBoxNumber(String poBoxNumber) {
        this.poBoxNumber = poBoxNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
}
