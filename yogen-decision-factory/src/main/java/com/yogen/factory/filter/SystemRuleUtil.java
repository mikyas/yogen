package com.yogen.factory.filter;

import com.yogen.factory.model.dto.CachedRule;
import com.yogen.factory.model.dto.RuleExecuteResult;
import com.yogen.util.enumtype.SystemRule;
import com.yogen.util.enumtype.TimePeriod;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

class SystemRuleUtil {


    /**
     * if ip is forbidden
     */
    RuleExecuteResult executeRule1001(String value, String forbiddenIp) {

        RuleExecuteResult ruleResult = new RuleExecuteResult(1001, false, "1001 kodlu kural icin sahtekarlik suphesine rastlanmadi.");
        if (!Objects.equals(forbiddenIp, value)) {
            return ruleResult;
        } else {
            ruleResult.setFraud(true);
            ruleResult.setMessage("islemin geldiği ip adresi : " + forbiddenIp + " yasak ip : " + value + " olduğu icin islemde sahtekarlik tespit edildi.");
            return ruleResult;
        }
    }

    /**
     * if mail address contains forbidden mail extensions
     */
    RuleExecuteResult executeRule1002(String mail, String forbiddenMailExtension) {
        RuleExecuteResult ruleResult = new RuleExecuteResult(SystemRule.RULE_1002.getRuleCode(), false, "1002 kodlu kural icin sahtekarlik suphesine rastlanmadi.");
        String mailExtension = mail.substring(mail.indexOf('@'));
        if (!Objects.equals(mailExtension, forbiddenMailExtension)) {
            return ruleResult;
        } else {
            ruleResult.setFraud(true);
            ruleResult.setMessage("Mail uzantısı için yasak mail uzantısı filtresi uygulanmasında sahtekarlik tespit edildi.");
            return ruleResult;
        }
    }

    RuleExecuteResult executeRule1003(String purchaserName, String forbiddenChar) {
        return executeForbiddenCharRule(purchaserName, forbiddenChar, SystemRule.RULE_1003);
    }

    RuleExecuteResult executeRule1004(String purchaserLastName, String forbiddenChar) {
        return executeForbiddenCharRule(purchaserLastName, forbiddenChar, SystemRule.RULE_1004);

    }

    RuleExecuteResult executeRule1005(String purchaserAddress, String forbiddenChar) {
        return executeForbiddenCharRule(purchaserAddress, forbiddenChar, SystemRule.RULE_1005);
    }

    /**
     * if amount > value return false else return true
     */
    RuleExecuteResult executeRule1006(String value, Double amount) {
        RuleExecuteResult ruleResult = new RuleExecuteResult(SystemRule.RULE_1006.getRuleCode(), false, "1006 kodlu kural icin sahtekarlik suphesine rastlanmadi.");
        if (Double.compare(amount, Double.parseDouble(value)) <= 0) {
            return ruleResult;
        } else {
            ruleResult.setFraud(true);
            ruleResult.setMessage("isleme ait tutar değeri : " + amount + " belirlenen tutar değerinden : " + value + "'den buyuk olduğu icin islemde sahtekarlik tespit edildi.");
            return ruleResult;
        }
    }

    /**
     * if amount < value return false else return true
     */
    RuleExecuteResult executeRule1007(String value, Double amount) {
        RuleExecuteResult ruleResult = new RuleExecuteResult(SystemRule.RULE_1007.getRuleCode(), false, "1007 kodlu kural icin sahtekarlik suphesine rastlanmadi.");
        if (Double.compare(amount, Double.parseDouble(value)) >= 0) {
            return ruleResult;
        } else {
            ruleResult.setFraud(true);
            ruleResult.setMessage("isleme ait tutar değeri : " + amount + " belirlenen tutar değerinden : " + value + "'den kucuk olduğu icin islemde sahtekarlik tespit edildi.");
            return ruleResult;
        }
    }


    /**
     * execute Rule_2001
     */
    RuleExecuteResult executeRule2001(TimePeriod timePeriod, int timeBound, int limit, Date transactionDate, String variable, Map<String, CachedRule> cachedRuleMap) throws Exception {
        return executeControlledOneDimensionRule(SystemRule.RULE_2001, timePeriod, timeBound, limit, transactionDate, variable, "Kart numarasi", cachedRuleMap);
    }

    /**
     * execute Rule_2002
     */
    RuleExecuteResult executeRule2002(TimePeriod timePeriod, int timeBound, int limit, Date transactionDate, String variable, Map<String, CachedRule> cachedRuleMap) throws Exception {
        return executeControlledOneDimensionRule(SystemRule.RULE_2002, timePeriod, timeBound, limit, transactionDate, variable, "Mail Adresi", cachedRuleMap);
    }

    /**
     * execute Rule_2003
     */
    RuleExecuteResult executeRule2003(TimePeriod timePeriod, int timeBound, int limit, Date transactionDate, String variable, Map<String, CachedRule> cachedRuleMap) throws Exception {
        return executeControlledOneDimensionRule(SystemRule.RULE_2003, timePeriod, timeBound, limit, transactionDate, variable, "IP Adresi", cachedRuleMap);
    }

    /**
     * execute Rule_2004
     */
    RuleExecuteResult executeRule2004(TimePeriod timePeriod, int timeBound, int limit, Date transactionDate, String variable, Map<String, CachedRule> cachedRuleMap) throws Exception {
        return executeControlledOneDimensionRule(SystemRule.RULE_2004, timePeriod, timeBound, limit, transactionDate, variable, "Müşteri Numarası", cachedRuleMap);
    }

    /**
     * execute Rule_3001
     */
    RuleExecuteResult executeRule3001(TimePeriod timePeriod, int timeBound, int limit, Date transactionDate, String constant, String variable, Map<String, CachedRule> cachedRuleMap) throws Exception {
        return executeControlledTwoLevelDimension(SystemRule.RULE_3001, timePeriod, timeBound, limit, transactionDate, constant, "Kart Numarası", variable, "IP Adresi", cachedRuleMap);
    }

    /**
     * execute Rule_3101
     */
    RuleExecuteResult executeRule3101(TimePeriod timePeriod, int timeBound, int limit, Date transactionDate, String constant, String variable, Map<String, CachedRule> cachedRuleMap) throws Exception {
        return executeControlledTwoLevelDimension(SystemRule.RULE_3101, timePeriod, timeBound, limit, transactionDate, constant, "IP Adresi", variable, "Kart Numarası", cachedRuleMap);
    }

    /**
     * execute Rule_3201
     */
    RuleExecuteResult executeRule3201(TimePeriod timePeriod, int timeBound, int limit, Date transactionDate, String constant, String variable, Map<String, CachedRule> cachedRuleMap) throws Exception {
        return executeControlledTwoLevelDimension(SystemRule.RULE_3201, timePeriod, timeBound, limit, transactionDate, constant, "Mail Adresi", variable, "IP Adresi", cachedRuleMap);
    }

    /**
     * execute RULE_3302
     */
    RuleExecuteResult executeRule3302(TimePeriod timePeriod, int timeBound, int limit, Date transactionDate, String constant, String variable, Map<String, CachedRule> cachedRuleMap) throws Exception {
        return executeControlledTwoLevelDimension(SystemRule.RULE_3302, timePeriod, timeBound, limit, transactionDate, constant, "Müşteri Numarası", variable, "Kart Numarası", cachedRuleMap);
    }

    /**
     * if string contains forbidden characters
     *
     * @param checkString kontrol edilecek string
     * @param rule        execute edilen kural
     */
    private RuleExecuteResult executeForbiddenCharRule(String checkString, String forbiddenChar, SystemRule rule) {
        RuleExecuteResult ruleResult = new RuleExecuteResult(rule.getRuleCode(), false, rule.getRuleCode() + " kodlu kural icin sahtekarlik suphesine rastlanmadi.");
//        String[] forbiddenChars = new String[]{"$", "#", "?", "!"};
//        for (String forbiddenChar : forbiddenChars) {
        if (Pattern.compile(Pattern.quote(forbiddenChar), Pattern.CASE_INSENSITIVE).matcher(checkString).find()) {
            ruleResult.setFraud(true);
            ruleResult.setMessage(rule.getDescription() + "nda sahtekarlik tespit edildi.");
            return ruleResult;
        }
//        }
        return ruleResult;
    }


    /**
     * @param systemRule      kural
     * @param timeBound       zaman siniri
     * @param limit           üst sinir
     * @param transactionDate islemin geldizi zaman
     * @param variable        degisken degeri
     * @param variableName    degisken adi
     * @param cachedRuleMap   kurala ait kisit ve degisken degerlerinin limit ve zaman sinirlari degerlerleri ile tutuldugu hafizadaki map
     *                        <p>
     *                        Gelen degisken degeri kural kodu ile mapde anahtar olarak tutulur.
     *                        Gelen her yeni cagirim için map'te tutulan anahtar degerine bagli olarak limit artıtılır.
     *                        Limit üst sinirina ulasmadan zaman kisiti biterse anahtara bagli deger sifirlanir.
     * @return RuleExecuteResult
     */
    private RuleExecuteResult executeControlledOneDimensionRule(SystemRule systemRule, TimePeriod timePeriod, int timeBound, int limit, Date transactionDate, String variable, String variableName, Map<String, CachedRule> cachedRuleMap) throws Exception {
//        System.out.println(systemRule.getRuleCode() + " kodlu kural inceleniyor. " + timeBound + " icerisinde ozel bir " + variableName + " icin " + limit + " farkli odeme isteği gelirse");
        RuleExecuteResult ruleResult = new RuleExecuteResult(systemRule.getRuleCode(), false, systemRule.getRuleCode() + " kodlu kural icin sahtekarlik suphesine rastlanmadi.");
        String keyPreCode = systemRule.getRuleCode() + "-";
        CachedRule cachedRule = cachedRuleMap.get(keyPreCode + variable);
        long timeLimit = getTimeLimitByPeriodAndBound(timePeriod, timeBound);
        if (cachedRule == null) { // eğer cache de bu kart numarasi icin bir değer tanimlanmamissa kural icin cahce'e eklenir
            cachedRuleMap.put(keyPreCode + variable, new CachedRule((transactionDate.getTime() + timeLimit), 1));
        } else { // eğer bu kart numarasi icin bir kayit varsa kontrol geceklestirilir
            if (transactionDate.getTime() > cachedRule.getValidityPeriod()) { // Eger islem zamani gecerlilik suresini astiysa kart numarasi icin senaryo baslagica doner
                cachedRuleMap.put(keyPreCode + variable, new CachedRule((transactionDate.getTime() + timeLimit), 1));
            } else { // kuralin gecerlilik suresi icerisinde limit asilirsa fraud vardir.
                if (cachedRule.getCount() + 1 > limit) {
                    ruleResult.setFraud(true);
                    ruleResult.setMessage(systemRule.getRuleCode() + " kodlu kural için " + timeLimit + " milliseconds içerisinde " + variableName + " : " +
                            variable + " için gelen ödeme isteği sayısı: " + (cachedRule.getCount() + 1) + ", üst sınır istek sayısı olan: " +
                            limit + " aşıldığı için sahtekarlik tespit edildi");
                }
                cachedRule.setCount(cachedRule.getCount() + 1);
                cachedRuleMap.put(keyPreCode + variable, cachedRule);
            }
        }
        return ruleResult;
    }


    /**
     * @param systemRule      kural
     * @param timeBound       zaman siniri
     * @param limit           üst sinir
     * @param transactionDate islemin geldizi zaman
     * @param constant        kisit degeri
     * @param constantName    kisit adi
     * @param variable        degisken degeri
     * @param variableName    degisken adi
     * @param cachedRuleMap   kurala ait kisit ve degisken degerlerinin limit ve zaman sinirlari degerlerleri ile tutuldugu hafizadaki map
     *                        <p>
     *                        Gelen kural kisit degeri kural kodu ile mapde anahtar olarak tutulur.
     *                        Gelen her farklı degisken degeri için map'te tutulan anahtar degerine bagli olarak limit artıtılır.
     *                        Limit üst sinirina ulasmadan zaman kisiti biterse anahtara bagli deger sifirlanir.
     * @return RuleExecuteResult
     */
    private RuleExecuteResult executeControlledTwoLevelDimension(SystemRule systemRule, TimePeriod timePeriod, int timeBound, int limit, Date transactionDate, String constant, String constantName, String variable, String variableName, Map<String, CachedRule> cachedRuleMap) throws Exception {
//        System.out.println(systemRule.getRuleCode() + " kodlu kural inceleniyor. Belirli bir zaman içerisinde bir " + constantName + " için N farklı " + variableName + "'ndan ödeme isteği gelirse");
        RuleExecuteResult ruleResult = new RuleExecuteResult(systemRule.getRuleCode(), false, systemRule.getRuleCode() + " kodlu kural icin sahtekarlik suphesine rastlanmadi.");
        String keyPreCode = systemRule.getRuleCode() + "-";
        CachedRule cachedRule = cachedRuleMap.get(keyPreCode + constant);
        List<String> variableList = new ArrayList<>();
        variableList.add(variable);
        long timeLimit = getTimeLimitByPeriodAndBound(timePeriod, timeBound);
        if (cachedRule == null) { // eğer cache de bu constant icin bir değer tanimlanmamissa kural icin cahce'e eklenir
            cachedRuleMap.put(keyPreCode + constant, new CachedRule((transactionDate.getTime() + timeLimit), 1, variableList));
        } else { // eğer bu constant icin bir kayit varsa kontrol geceklestirilir
            if (transactionDate.getTime() > cachedRule.getValidityPeriod()) { // Eger islem zamani gecerlilik suresini astiysa kart numarasi icin senaryo baslangica doner
                cachedRuleMap.put(keyPreCode + constant, new CachedRule((transactionDate.getTime() + timeLimit), 1, variableList));
            } else { // kuralin gecerlilik suresi icerisinde n+1 farkli variable'den istek gelirse fraud vardir.
                int distinctCount = (int) cachedRule.getValueList().stream().distinct().count(); // o kurala ait birbirinden farkli degerler sayisi
                String distinctVariables = cachedRule.getValueList().stream().distinct().collect(Collectors.joining("', '"));
                boolean newVariableExist = Pattern.compile(Pattern.quote(variable), Pattern.CASE_INSENSITIVE).matcher(distinctVariables).find(); // yeni gelen deger eski degerler icerisinde mevcut mu?
                if ((!newVariableExist && distinctCount + 1 > limit) || (newVariableExist && distinctCount > limit)) {
                    ruleResult.setFraud(true);
                    ruleResult.setMessage(systemRule.getRuleCode() + " kodlu kural için " + timeLimit + " milliseconds içerisinde " + constantName + " : " +
                            constant + " için farklı " + variableName + ": '" + distinctVariables + "', '" + variable +
                            "' gelen farklı ödeme isteği sayısı: " + (newVariableExist ? distinctCount : (distinctCount + 1)) + ", üst sınır olan: " +
                            limit + " aşıldığı için sahtekarlik tespit edildi");
                }
                if (!newVariableExist) {
                    cachedRule.setCount(distinctCount + 1);
                }
                cachedRule.getValueList().add(variable);
                cachedRuleMap.put(keyPreCode + constant, cachedRule);
            }
        }
        return ruleResult;
    }

    RuleExecuteResult executeRule3003() {
        return new RuleExecuteResult(3003, false, "3003 kodlu kural icin sahtekarlik suphesine rastlanmadi.");
    }

    private long getTimeLimitByPeriodAndBound(TimePeriod timePeriod, int timeBound) throws Exception {
        if (Objects.equals(TimePeriod.MINUTE, timePeriod)) {
            return TimeUnit.MINUTES.toMillis(timeBound);
        } else if (Objects.equals(TimePeriod.HOUR, timePeriod)) {
            return TimeUnit.HOURS.toMillis(timeBound);
        } else if (Objects.equals(TimePeriod.DAY, timePeriod)) {
            return TimeUnit.DAYS.toMillis(timeBound);
        } else if (Objects.equals(TimePeriod.WEEK, timePeriod)) {
            return (7 * (TimeUnit.DAYS.toMillis(timeBound)));
        } else if (Objects.equals(TimePeriod.MONTH, timePeriod)) {
            return (30 * (TimeUnit.DAYS.toMillis(timeBound)));
        } else if (Objects.equals(TimePeriod.YEAR, timePeriod)) {
            return (365 * (TimeUnit.DAYS.toMillis(timeBound)));
        } else {
            throw new Exception("Tanimlanmamis Zaman Periodu");
        }
    }
}
