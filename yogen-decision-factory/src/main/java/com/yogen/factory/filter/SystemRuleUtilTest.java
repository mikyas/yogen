package com.yogen.factory.filter;

import com.yogen.factory.model.dto.CachedRule;
import com.yogen.factory.model.dto.RuleExecuteResult;
import com.yogen.util.enumtype.SystemRule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Date;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class SystemRuleUtilTest {

    private SystemRuleUtil systemRuleUtil = new SystemRuleUtil();
    @BeforeEach
    void setUp() {

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void executeRule1001() {

        RuleExecuteResult ruleResult = new RuleExecuteResult(1001, false, "1001 kodlu kural icin sahtekarlik suphesine rastlanmadi.");

        assertEquals(systemRuleUtil.executeRule1001("1.1.1.1", "1.1.1.2").getMessage(),ruleResult.getMessage());
        assertEquals(systemRuleUtil.executeRule1001("1.1.1.1", "1.1.1.2").isFraud(),ruleResult.isFraud());
        assertNotEquals(systemRuleUtil.executeRule1001("1.1.1.1", "1.1.1.1").getMessage(),ruleResult.getMessage());
        ruleResult.setFraud(true);
        ruleResult.setMessage("islemin geldiği ip adresi : " + "1.1.1.1" + " yasak ip : " + "1.1.1.1" + " olduğu icin islemde sahtekarlik tespit edildi.");
        assertEquals(systemRuleUtil.executeRule1001("1.1.1.1", "1.1.1.1").getMessage(),ruleResult.getMessage());
        assertEquals(systemRuleUtil.executeRule1001("1.1.1.1", "1.1.1.1").isFraud(),ruleResult.isFraud());
        ruleResult.setFraud(false);
        assertNotEquals(systemRuleUtil.executeRule1001("1.1.1.1", "1.1.1.1").isFraud(),ruleResult.isFraud());
    }

    @Test
    void executeRule1002() {
        RuleExecuteResult ruleResult = new RuleExecuteResult(Integer.valueOf(SystemRule.RULE_1002.getRuleCode()), false, "1002 kodlu kural icin sahtekarlik suphesine rastlanmadi.");
        assertEquals(systemRuleUtil.executeRule1002("mail@mail.com", "@hotmail.com").getMessage(),ruleResult.getMessage());
        assertEquals(systemRuleUtil.executeRule1002("mail@mail.com", "@hotmail.com").isFraud(),ruleResult.isFraud());
        assertNotEquals(systemRuleUtil.executeRule1002("mail@mail.com", "@mail.com").getMessage(),ruleResult.getMessage());
        ruleResult.setFraud(true);
        ruleResult.setMessage("Mail uzantısı için yasak mail uzantısı filtresi uygulanmasında sahtekarlik tespit edildi.");
        assertEquals(systemRuleUtil.executeRule1002("mail@mail.com", "@mail.com").getMessage(),ruleResult.getMessage());
        assertEquals(systemRuleUtil.executeRule1002("mail@mail.com", "@mail.com").isFraud(),ruleResult.isFraud());
        ruleResult.setFraud(false);
        assertNotEquals(systemRuleUtil.executeRule1002("mail@mail.com", "@mail.com").isFraud(),ruleResult.isFraud());
    }

    @Test
    void executeRule1003() {
        RuleExecuteResult ruleResult = new RuleExecuteResult(1003, false, "1003 kodlu kural icin sahtekarlik suphesine rastlanmadi.");
        assertEquals(systemRuleUtil.executeRule1003("Ali", "@").getMessage(),ruleResult.getMessage());
        assertEquals(systemRuleUtil.executeRule1003("Ali", "@").isFraud(),ruleResult.isFraud());
        assertNotEquals(systemRuleUtil.executeRule1003("@li", "@").isFraud(),ruleResult.isFraud());
        ruleResult.setFraud(true);
        ruleResult.setMessage("Alıcı Adı alanı için yasaklı karakter filtresi uygulanmasında sahtekarlik tespit edildi.");
        assertEquals(systemRuleUtil.executeRule1003("@li", "@").getMessage(),ruleResult.getMessage());
        assertEquals(systemRuleUtil.executeRule1003("@li", "@").isFraud(),ruleResult.isFraud());
        ruleResult.setFraud(false);
        assertNotEquals(systemRuleUtil.executeRule1003("@li", "@").isFraud(),ruleResult.isFraud());
    }

    @ParameterizedTest
    @CsvSource({ "Veli, #", "Şükrü, @", "lastname, %" })
    void executeRule1004True(String purchaserLastName, String forbiddenChar) {
        RuleExecuteResult ruleResult = new RuleExecuteResult(1004, false, "1004 kodlu kural icin sahtekarlik suphesine rastlanmadi.");
        assertEquals(systemRuleUtil.executeRule1004(purchaserLastName,forbiddenChar).getMessage(),ruleResult.getMessage());
        assertEquals(systemRuleUtil.executeRule1004(purchaserLastName,forbiddenChar).isFraud(),ruleResult.isFraud());
    }

    @ParameterizedTest
    @CsvSource({ "V#li, #", "Şükrü@@@, @", "l@stn%me, %" })
    void executeRule1004False(String purchaserLastName, String forbiddenChar) {
        RuleExecuteResult ruleResult = new RuleExecuteResult(1004, false, "1004 kodlu kural icin sahtekarlik suphesine rastlanmadi.");
        assertNotEquals(systemRuleUtil.executeRule1004(purchaserLastName,forbiddenChar).getMessage(),ruleResult.getMessage());
        assertNotEquals(systemRuleUtil.executeRule1004(purchaserLastName,forbiddenChar).isFraud(),ruleResult.isFraud());
    }

    @ParameterizedTest
    @CsvSource({ "196.05, 200.0", "196, 2000", "196.0000005, 200.00000005" })
    void executeRule1006True(String value, Double amount) {
        RuleExecuteResult ruleResult = new RuleExecuteResult(1006, false, "1006 kodlu kural icin sahtekarlik suphesine rastlanmadi.");
        assertEquals(systemRuleUtil.executeRule1006(value,amount).getMessage(),ruleResult.getMessage());
    }

    @ParameterizedTest
    @CsvSource({ "196.05, 190.0", "196, -5", "196.0000005, 192.5" })
    void executeRule1006False(String value, Double amount) {
        RuleExecuteResult ruleResult = new RuleExecuteResult(1006, false, "1006 kodlu kural icin sahtekarlik suphesine rastlanmadi.");
        ruleResult.setFraud(true);
        ruleResult.setMessage("isleme ait tutar değeri : " + amount + " belirlenen tutar değerinden : " + value + "'den buyuk olduğu icin islemde sahtekarlik tespit edildi.");
        assertNotEquals(systemRuleUtil.executeRule1006(value,amount).getMessage(),ruleResult.getMessage());
        assertNotEquals(systemRuleUtil.executeRule1006(value,amount).isFraud(),ruleResult.isFraud());
    }

    @ParameterizedTest
    @CsvSource({ "2001, 3000006", "2002, 3000006", "2003, 3000006", "2004, 3000006" })
    void executeControlledOneDimensionRule(int ruleCode, long timeBound, int limit, Date transactionDate, String variable, String variableName, Map<String, CachedRule> cachedRuleMap) {
    }

    @Test
    void executeControlledTwoLevelDimension() {
    }
}