package com.yogen.factory.filter;

import com.yogen.factory.model.dto.*;
import com.yogen.util.enumtype.SystemRule;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class YogenRuleExecutor {

    private Map<String, CachedRule> cachedRuleMap;

    //    public   void main(String[] args) throws ParseException {
//
//        initializeCacheRuleMap();
//        startProcess(getTransactionList(), getMerchantDefinedRules(), false);
//    }


    public List<RuleExecutorTraceDTO> testTransactionsForFraudByDefinedRules(List<TransactionInfoDTO> transactions, List<DefinedRuleDTO> merchantDefinedRules, boolean breakIfFraud) throws Exception {
        List<RuleExecutorTraceDTO> ruleTraceDTOs = new ArrayList<>();
        for (TransactionInfoDTO transaction : transactions) { // ilk islemi al ve mağaza icin tanimlanmis kurallari isletmeye basla
            RuleExecutorTraceDTO ruleTraceDTO = checkTransactionForFraudByDefinedRules(transaction, merchantDefinedRules, breakIfFraud);
            ruleTraceDTOs.add(ruleTraceDTO);
        }
        return ruleTraceDTOs;
    }

    public RuleExecutorTraceDTO checkTransactionForFraudByDefinedRules(TransactionInfoDTO transaction, List<DefinedRuleDTO> merchantDefinedRules, boolean breakIfFraud) throws Exception {
        initializeCacheRuleMapIfNotInitialized();
        RuleExecutorTraceDTO ruleTraceDTO = new RuleExecutorTraceDTO();
        SystemRuleUtil systemRuleUtil = new SystemRuleUtil();
        List<RuleExecuteResult> ruleResultList = new ArrayList<>();
        ruleTraceDTO.setTransactionUID(transaction.getTransactionUID());
        System.out.println(transaction.getTransactionVersionId() + " id'li islem icin kontrollere baslaniyor.");

        for (DefinedRuleDTO definedRule : merchantDefinedRules) {
            if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_1001.getRuleCode())) {
                RuleExecuteResult ruleResult = systemRuleUtil.executeRule1001(definedRule.getValue(), transaction.getDeviceIpAddress());
                System.out.println(ruleResult.getMessage());
                ruleResultList.add(ruleResult);
                if (breakIfFraud && ruleResult.isFraud()) {
                    break;
                }
            }
            if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_1002.getRuleCode())) {
                RuleExecuteResult ruleResult = systemRuleUtil.executeRule1002(transaction.getPurchaserMail(), definedRule.getValue());
                System.out.println(ruleResult.getMessage());
                ruleResultList.add(ruleResult);
                if (breakIfFraud && ruleResult.isFraud()) {
                    break;
                }
            }
            if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_1003.getRuleCode())) {
                RuleExecuteResult ruleResult = systemRuleUtil.executeRule1003(transaction.getPurchaserFirstName(), definedRule.getValue());
                System.out.println(ruleResult.getMessage());
                ruleResultList.add(ruleResult);
                if (breakIfFraud && ruleResult.isFraud()) {
                    break;
                }
            }
            if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_1004.getRuleCode())) {
                RuleExecuteResult ruleResult = systemRuleUtil.executeRule1004(transaction.getPurchaserLastName(), definedRule.getValue());
                System.out.println(ruleResult.getMessage());
                ruleResultList.add(ruleResult);
                if (breakIfFraud && ruleResult.isFraud()) {
                    break;
                }
            }
            if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_1005.getRuleCode())) {
                RuleExecuteResult ruleResult = systemRuleUtil.executeRule1005(transaction.getShipmentAddress().getFullAddress(), definedRule.getValue());
                System.out.println(ruleResult.getMessage());
                ruleResultList.add(ruleResult);
                if (breakIfFraud && ruleResult.isFraud()) {
                    break;
                }
            }
            if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_1006.getRuleCode())) {
                RuleExecuteResult ruleResult = systemRuleUtil.executeRule1006(definedRule.getValue(), transaction.getTotalPaidPrice());
                System.out.println(ruleResult.getMessage());
                ruleResultList.add(ruleResult);
                if (breakIfFraud && ruleResult.isFraud()) {
                    break;
                }
            }
            if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_1007.getRuleCode())) {
                RuleExecuteResult ruleResult = systemRuleUtil.executeRule1007(definedRule.getValue(), transaction.getTotalPaidPrice());
                System.out.println(ruleResult.getMessage());
                ruleResultList.add(ruleResult);
                if (breakIfFraud && ruleResult.isFraud()) {
                    break;
                }
            }
//                if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_1008.getRuleCode())) {
//                    RuleExecuteResult ruleResult = systemRuleUtil.executeRule1006(definedRule.getValue(), transaction.getTotalPaidPrice());
//                    System.out.println(ruleResult.getMessage());
//                    ruleResultList.add(ruleResult);
//                    if (ruleResult.isFraud()) {
//                        break;
//                    }
//                }
//                if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_1009.getRuleCode())) {
//                    RuleExecuteResult ruleResult = systemRuleUtil.executeRule1007(definedRule.getValue(), transaction.getTotalPaidPrice());
//                    System.out.println(ruleResult.getMessage());
//                    ruleResultList.add(ruleResult);
//                    if (ruleResult.isFraud()) {
//                        break;
//                    }
//                }
//                if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_1010.getRuleCode())) {
//                    RuleExecuteResult ruleResult = systemRuleUtil.executeRule1010(definedRule.getValue(), transaction.getTotalPaidPrice());
//                    System.out.println(ruleResult.getMessage());
//                    ruleResultList.add(ruleResult);
//                    if (ruleResult.isFraud()) {
//                        break;
//                    }
//                }
//                if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_1011.getRuleCode())) {
//                    RuleExecuteResult ruleResult = systemRuleUtil.executeRule1011(definedRule.getValue(), transaction.getTotalPaidPrice());
//                    System.out.println(ruleResult.getMessage());
//                    ruleResultList.add(ruleResult);
//                    if (ruleResult.isFraud()) {
//                        break;
//                    }
//                }
            if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_2001.getRuleCode())) {
                RuleExecuteResult result2001 = systemRuleUtil.executeRule2001(definedRule.getTimePeriod(), definedRule.getTimeBound(), definedRule.getLimit(), transaction.getTransactionDate(), transaction.getCreditCardNo(), cachedRuleMap);
                System.out.println(result2001.getMessage());
                ruleResultList.add(result2001);
                if (breakIfFraud && result2001.isFraud()) {
                    break;
                }
            }
            if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_2002.getRuleCode())) {
                RuleExecuteResult result2002 = systemRuleUtil.executeRule2002(definedRule.getTimePeriod(), definedRule.getTimeBound(), definedRule.getLimit(), transaction.getTransactionDate(), transaction.getPurchaserMail(), cachedRuleMap);
                System.out.println(result2002.getMessage());
                ruleResultList.add(result2002);
                if (breakIfFraud && result2002.isFraud()) {
                    break;
                }
            }
            if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_2003.getRuleCode())) {
                RuleExecuteResult result2003 = systemRuleUtil.executeRule2003(definedRule.getTimePeriod(), definedRule.getTimeBound(), definedRule.getLimit(), transaction.getTransactionDate(), transaction.getDeviceIpAddress(), cachedRuleMap);
                System.out.println(result2003.getMessage());
                ruleResultList.add(result2003);
                if (breakIfFraud && result2003.isFraud()) {
                    break;
                }
            }
            if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_2004.getRuleCode())) {
                RuleExecuteResult result2004 = systemRuleUtil.executeRule2004(definedRule.getTimePeriod(), definedRule.getTimeBound(), definedRule.getLimit(), transaction.getTransactionDate(), transaction.getPurchaserUID(), cachedRuleMap);
                System.out.println(result2004.getMessage());
                ruleResultList.add(result2004);
                if (breakIfFraud && result2004.isFraud()) {
                    break;
                }
            }
            if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_3001.getRuleCode())) {
                RuleExecuteResult result3001 = systemRuleUtil.executeRule3001(definedRule.getTimePeriod(), definedRule.getTimeBound(), definedRule.getLimit(), transaction.getTransactionDate(), transaction.getCreditCardNo(), transaction.getDeviceIpAddress(), cachedRuleMap);
                System.out.println(result3001.getMessage());
                ruleResultList.add(result3001);
                if (breakIfFraud && result3001.isFraud()) {
                    break;
                }
            }
//                if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_3002.getRuleCode())) {
//                    RuleExecuteResult result3002 = systemRuleUtil.executeControlledTwoLevelDimension(3002, definedRule.getTimePeriod(), definedRule.getTimeBound(), definedRule.getLimit(), transaction.getTransactionDate(), transaction.getCreditCardNo(), "Kart Numarası", transaction.getPurchaserMail(), "Mail Adresi", cachedRuleMap);
//                    System.out.println(result3002.getMessage());
//                    ruleResultList.add(result3002);
//                    if (breakIfFraud && result3002.isFraud()) {
//                        break;
//                    }
//                }
//
//                if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_3003.getRuleCode())) {
//                    RuleExecuteResult result3003 = systemRuleUtil.executeControlledTwoLevelDimension(3003, definedRule.getTimePeriod(), definedRule.getTimeBound(), definedRule.getLimit(), transaction.getTransactionDate(), transaction.getCreditCardNo(), "Kart Numarası", transaction.getPurchaserUID(), "Müşteri Numarası", cachedRuleMap);
//                    System.out.println(result3003.getMessage());
//                    ruleResultList.add(result3003);
//                    if (breakIfFraud && result3003.isFraud()) {
//                        break;
//                    }
//                }
//
            if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_3101.getRuleCode())) {
                RuleExecuteResult result3101 = systemRuleUtil.executeRule3101(definedRule.getTimePeriod(), definedRule.getTimeBound(), definedRule.getLimit(), transaction.getTransactionDate(), transaction.getDeviceIpAddress(), transaction.getCreditCardNo(), cachedRuleMap);
                System.out.println(result3101.getMessage());
                ruleResultList.add(result3101);
                if (breakIfFraud && result3101.isFraud()) {
                    break;
                }
            }

//                if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_3102.getRuleCode())) {
//                    RuleExecuteResult result3102 = systemRuleUtil.executeControlledTwoLevelDimension(3102, definedRule.getTimePeriod(), definedRule.getTimeBound(), definedRule.getLimit(), transaction.getTransactionDate(), transaction.getDeviceIpAddress(), "IP Adresi", transaction.getPurchaserMail(), "Mail Adresi", cachedRuleMap);
//                    System.out.println(result3102.getMessage());
//                    ruleResultList.add(result3102);
//                    if (breakIfFraud && result3102.isFraud()) {
//                        break;
//                    }
//                }
//                if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_3103.getRuleCode())) {
//                    RuleExecuteResult result3103 = systemRuleUtil.executeControlledTwoLevelDimension(3103, definedRule.getTimePeriod(), definedRule.getTimeBound(), definedRule.getLimit(), transaction.getTransactionDate(), transaction.getDeviceIpAddress(), "IP Adresi", transaction.getPurchaserUID(), "Müşteri Numarası", cachedRuleMap);
//                    System.out.println(result3103.getMessage());
//                    ruleResultList.add(result3103);
//                    if (breakIfFraud && result3103.isFraud()) {
//                        break;
//                    }
//                }
            if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_3201.getRuleCode())) {
                RuleExecuteResult result3201 = systemRuleUtil.executeRule3201(definedRule.getTimePeriod(), definedRule.getTimeBound(), definedRule.getLimit(), transaction.getTransactionDate(), transaction.getPurchaserMail(), transaction.getDeviceIpAddress(), cachedRuleMap);
                System.out.println(result3201.getMessage());
                ruleResultList.add(result3201);
                if (breakIfFraud && result3201.isFraud()) {
                    break;
                }
            }
//                if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_3202.getRuleCode())) {
//                    RuleExecuteResult result3202 = systemRuleUtil.executeControlledTwoLevelDimension(3202, definedRule.getTimePeriod(), definedRule.getTimeBound(), definedRule.getLimit(), transaction.getTransactionDate(), transaction.getPurchaserMail(), "Mail Adresi", transaction.getCreditCardNo(), "Kart Numarası", cachedRuleMap);
//                    System.out.println(result3202.getMessage());
//                    ruleResultList.add(result3202);
//                    if (breakIfFraud && result3202.isFraud()) {
//                        break;
//                    }
//                }
//                if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_3203.getRuleCode())) {
//                    RuleExecuteResult result3203 = systemRuleUtil.executeControlledTwoLevelDimension(3203, definedRule.getTimePeriod(), definedRule.getTimeBound(), definedRule.getLimit(), transaction.getTransactionDate(), transaction.getPurchaserMail(), "Mail Adresi", transaction.getPurchaserUID(), "Müşteri Numarası", cachedRuleMap);
//                    System.out.println(result3203.getMessage());
//                    ruleResultList.add(result3203);
//                    if (breakIfFraud && result3203.isFraud()) {
//                        break;
//                    }
//                }
//                if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_3301.getRuleCode())) {
//                    RuleExecuteResult result3301 = systemRuleUtil.executeControlledTwoLevelDimension(3301, definedRule.getTimePeriod(), definedRule.getTimeBound(), definedRule.getLimit(), transaction.getTransactionDate(), transaction.getPurchaserUID(), "Müşteri Numarası", transaction.getDeviceIpAddress(), "IP Adresi", cachedRuleMap);
//                    System.out.println(result3301.getMessage());
//                    ruleResultList.add(result3301);
//                    if (breakIfFraud && result3301.isFraud()) {
//                        break;
//                    }
//                }
            if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_3302.getRuleCode())) {
                RuleExecuteResult result3302 = systemRuleUtil.executeRule3302(definedRule.getTimePeriod(), definedRule.getTimeBound(), definedRule.getLimit(), transaction.getTransactionDate(), transaction.getPurchaserUID(), transaction.getCreditCardNo(), cachedRuleMap);
                System.out.println(result3302.getMessage());
                ruleResultList.add(result3302);
                if (breakIfFraud && result3302.isFraud()) {
                    break;
                }
            }
//                if (Objects.equals(definedRule.getSystemRuleCode(), SystemRule.RULE_3303.getRuleCode())) {
//                    RuleExecuteResult result3303 = systemRuleUtil.executeControlledTwoLevelDimension(3303, definedRule.getTimePeriod(), definedRule.getTimeBound(), definedRule.getLimit(), transaction.getTransactionDate(), transaction.getPurchaserUID(), "Müşteri Numarası", transaction.getPurchaserMail(), "Mail Adresi", cachedRuleMap);
//                    System.out.println(result3303.getMessage());
//                    ruleResultList.add(result3303);
//                    if (breakIfFraud && result3303.isFraud()) {
//                        break;
//                    }
//                }
            ruleTraceDTO.setRuleResults(ruleResultList);
        }
        return ruleTraceDTO;
    }

    private void initializeCacheRuleMapIfNotInitialized() {
        if (cachedRuleMap == null) {
            cachedRuleMap = new HashMap<>();
        }
    }

//    private List<TransactionInfoDTO> getTransactionList() throws ParseException {
//        List<TransactionInfoDTO> transactions = new ArrayList<>();
//        TransactionInfoDTO transaction1 = new TransactionInfoDTO("tx001", getItems(2), "ABC kargo", getAddress(), "faturaNo1", getAddress(),
//                "1111",
//                "Mehmet", "Yılmaz",
//                "05333333333", "abc@xyz.com", 1, 50.0D, 50.0D, "TRY",
//                "5001",
//                "1.1.2.1");
//        transaction1.setTransactionID(1L);
//        transaction1.setTransactionDate(getDate("2018-11-08 12:00:00"));
//        transactions.add(transaction1);
//        TransactionInfoDTO transaction2 = new TransactionInfoDTO("tx002", getItems(1), "KLM kargo", getAddress(), "faturaNo2", getAddress(),
//                "1111",
//                "Ahmet$#%", "Durmaz$#%",
//                "05311111111", "abc@xyz.com", 6, 10.0D, 10.0D, "TRY",
//                "5002",
//                "1.1.2.11");
//        transaction2.setTransactionID(2L);
//        transaction2.setTransactionDate(getDate("2018-11-08 12:01:00"));
//        transactions.add(transaction2);
//        AddressInfoDTO transaction3Address = getAddress();
//        transaction3Address.setAddress1(transaction3Address.getAddress1() + "$#%");
//        TransactionInfoDTO transaction3 = new TransactionInfoDTO("tx003", getItems(4), "XYZ kargo", transaction3Address, "faturaNo3", getAddress(),
//                "1111",
//                "Deniz", "Yılmaz",
//                "05322222222", "dyilmaz@cimeyil.com", 3, 650.0D, 650.0D, "TRY",
//                "5003",
//                "1.1.2.1");
//        transaction3.setTransactionID(3L);
//        transaction3.setTransactionDate(getDate("2018-11-08 12:03:00"));
//        transactions.add(transaction3);
//        TransactionInfoDTO transaction4 = new TransactionInfoDTO("tx004", getItems(1), "ABC kargo", getAddress(), "faturaNo4", getAddress(),
//                "1111",
//                "Mehmet", "Yılmaz",
//                "05333333333", "myilmaz@cimeyil.com", 4, 10.0D, 10.0D, "TRY",
//                "5004",
//                "1.1.2.1");
//        transaction4.setTransactionID(4L);
//        transaction4.setTransactionDate(getDate("2018-11-08 12:10:00"));
//        transactions.add(transaction4);
//        TransactionInfoDTO transaction5 = new TransactionInfoDTO("tx005", getItems(3), "XYZ kargo", getAddress(), "faturaNo5", getAddress(),
//                "1111",
//                "Kamil", "Yıldırım",
//                "05344444444", "abc@xyz.com", 1, 1500.0D, 1500.0D, "TRY",
//                "5001",
//                "1.1.2.5");
//        transaction5.setTransactionID(5L);
//        transaction5.setTransactionDate(getDate("2018-11-08 12:14:00"));
//        transactions.add(transaction5);
//        TransactionInfoDTO transaction6 = new TransactionInfoDTO("tx006", getItems(2), "ABC kargo", getAddress(), "faturaNo6", getAddress(),
//                "1111",
//                "Mehmet", "Dursun",
//                "05333333333", "abc@xyz.com", 1, 5.0D, 5.0D, "TRY",
//                "5002",
//                "1.1.2.6");
//        transaction6.setTransactionID(6L);
//        transaction6.setTransactionDate(getDate("2018-11-08 12:59:00"));
//        transactions.add(transaction6);
//        return transactions;
//    }

//    private List<RuleEntity> getMerchantDefinedRules() {
//        List<RuleEntity> definedRules = new ArrayList<>();
//        RuleEntity rule1001 = new RuleEntity(SystemRule.RULE_1001, null, null, "1.1.1.32");
//        rule1001.setDescription("1.1.1.32 ip den istek gelirse");
//        definedRules.add(rule1001);
//
//        RuleEntity rule1002 = new RuleEntity(SystemRule.RULE_1002, null, null, "xyz.com");
//        rule1002.setDescription("mail uzantisi xyz.com olarak gelirse");
//        definedRules.add(rule1002);
//
//        RuleEntity rule1003 = new RuleEntity(SystemRule.RULE_1003, null, null, "$");
//        rule1003.setDescription("Alıcı adı $ karaktersi içerirse");
//        definedRules.add(rule1003);
//
//        RuleEntity rule1004 = new RuleEntity(SystemRule.RULE_1004, null, null, "#");
//        rule1004.setDescription("Alıcı soyadı # karaktersi içerirse");
//        definedRules.add(rule1004);
//
//        RuleEntity rule1005 = new RuleEntity(SystemRule.RULE_1005, null, null, "%");
//        rule1005.setDescription("Alıcı adresi % karaktersi içerirse");
//        definedRules.add(rule1005);
//
//        RuleEntity rule1006 = new RuleEntity(SystemRule.RULE_1006, null, null, "1000");
//        rule1006.setDescription("Toplam tutar 1000 TL üzerinde olursa");
//        definedRules.add(rule1006);
//
//        RuleEntity rule1007 = new RuleEntity(SystemRule.RULE_1007, null, null, "10");
//        rule1007.setDescription("Toplam tutar 10 TL altında olursa");
//        definedRules.add(rule1007);
//
//        RuleEntity rule2001 = new RuleEntity(SystemRule.RULE_2001, TimeUnit.HOURS.toMillis(1), 2, null);
//        rule2001.setDescription("1 saat içerisinde bir kart numarasından gelen ödeme isteği sınırı 2");
//        definedRules.add(rule2001);
//        RuleEntity rule2002 = new RuleEntity(SystemRule.RULE_2002, TimeUnit.HOURS.toMillis(1), 2, null);
//        rule2002.setDescription("1 saat içerisinde bir müşteri mailinden gelen ödeme isteği sınırı 2");
//        definedRules.add(rule2002);
//        RuleEntity rule2003 = new RuleEntity(SystemRule.RULE_2003, TimeUnit.HOURS.toMillis(1), 2, null);
//        rule2003.setDescription("1 saat içerisinde bir IP Adresinden gelen ödeme isteği sınırı 2");
//        definedRules.add(rule2003);
//        RuleEntity rule2004 = new RuleEntity(SystemRule.RULE_2004, TimeUnit.HOURS.toMillis(1), 2, null);
//        rule2004.setDescription("1 saat içerisinde bir müşteri numarasından gelen ödeme isteği sınırı 2");
//        definedRules.add(rule2004);
//
//        RuleEntity rule3001 = new RuleEntity(SystemRule.RULE_3001, TimeUnit.HOURS.toMillis(1), 2, null);
//        rule3001.setDescription("1 saat içerisinde bir kart numarasından gelen ödemeler için farklı IP adresinden gelme sınırı 2");
//        definedRules.add(rule3001);
//        RuleEntity rule3101 = new RuleEntity(SystemRule.RULE_3101, TimeUnit.HOURS.toMillis(1), 2, null);
//        rule3101.setDescription("1 saat içerisinde bir ip adresinden gelen ödemeler için farklı kart numarasından gelme sayısı sınırı 2");
//        definedRules.add(rule3101);
//        RuleEntity rule3201 = new RuleEntity(SystemRule.RULE_3201, TimeUnit.HOURS.toMillis(1), 2, null);
//        rule3201.setDescription("1 saat içerisinde bir mail adresinden gelen ödemeler için farklı IP adresinden gelme sınırı 2");
//        definedRules.add(rule3201);
//        RuleEntity rule3302 = new RuleEntity(SystemRule.RULE_3302, TimeUnit.HOURS.toMillis(1), 2, null);
//        rule3302.setDescription("1 saat içerisinde bir müşteri numarasından gelen ödemeler için farklı kart numarasından gelme sayısı sınırı 2");
//        definedRules.add(rule3302);
//
//
//        return definedRules;
//    }

//    private List<ItemInfoDTO> getItems(int itemCount) {
//        List<ItemInfoDTO> items = new ArrayList<>();
//        ItemInfoDTO item1 = new ItemInfoDTO(ItemType.PHYSICAL, 10.0D, 2, "Erler Tek.", getAddress());
//        items.add(item1);
//        if (itemCount > 1) {
//            ItemInfoDTO item2 = new ItemInfoDTO(ItemType.PHYSICAL, 40.0D, 1, "Erler Tek.", getAddress());
//            items.add(item2);
//        }
//        if (itemCount > 2) {
//            ItemInfoDTO item3 = new ItemInfoDTO(ItemType.PHYSICAL, 100.0D, 1, "Erler Tek.", getAddress());
//            items.add(item3);
//        }
//        if (itemCount > 3) {
//            ItemInfoDTO item4 = new ItemInfoDTO(ItemType.PHYSICAL, 500.0D, 1, "Erler Tek.", getAddress());
//            items.add(item4);
//        }
//        return items;
//    }

//    private AddressInfoDTO getAddress() {
//        return new AddressInfoDTO("address1", "address2", "neighbourhood", "city", "region", "TR", "341235", "41180");
//    }

//    private Date getDate(String dateString) throws ParseException {
//        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        return sdf.parse(dateString);
//    }

}
