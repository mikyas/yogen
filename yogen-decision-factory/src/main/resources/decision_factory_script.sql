USE [master]
GO
/****** Object:  Database [yogen_decisionfactory]    Script Date: 2.11.2018 10:04:28 ******/
CREATE DATABASE [yogen_decisionfactory]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'yogen_decisionfactory', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\yogen_decisionfactory.mdf' , SIZE = 94400KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'yogen_decisionfactory_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\yogen_decisionfactory_log.ldf' , SIZE = 359424KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [yogen_decisionfactory] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [yogen_decisionfactory].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [yogen_decisionfactory] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [yogen_decisionfactory] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [yogen_decisionfactory] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [yogen_decisionfactory] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [yogen_decisionfactory] SET ARITHABORT OFF 
GO
ALTER DATABASE [yogen_decisionfactory] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [yogen_decisionfactory] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [yogen_decisionfactory] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [yogen_decisionfactory] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [yogen_decisionfactory] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [yogen_decisionfactory] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [yogen_decisionfactory] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [yogen_decisionfactory] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [yogen_decisionfactory] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [yogen_decisionfactory] SET  DISABLE_BROKER 
GO
ALTER DATABASE [yogen_decisionfactory] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [yogen_decisionfactory] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [yogen_decisionfactory] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [yogen_decisionfactory] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [yogen_decisionfactory] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [yogen_decisionfactory] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [yogen_decisionfactory] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [yogen_decisionfactory] SET RECOVERY FULL 
GO
ALTER DATABASE [yogen_decisionfactory] SET  MULTI_USER 
GO
ALTER DATABASE [yogen_decisionfactory] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [yogen_decisionfactory] SET DB_CHAINING OFF 
GO
ALTER DATABASE [yogen_decisionfactory] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [yogen_decisionfactory] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [yogen_decisionfactory] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'yogen_decisionfactory', N'ON'
GO
USE [yogen_decisionfactory]
GO
/****** Object:  Table [dbo].[decision_detail]    Script Date: 2.11.2018 10:04:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[decision_detail](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ORDER_ID] [bigint] NOT NULL,
	[DECIDED_AT] [datetime2](0) NOT NULL,
	[RESULT] [bit] NOT NULL,
	[NOTES] [nvarchar](100) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_decision_detail_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[defined_rule]    Script Date: 20.11.2018  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[defined_rule](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SYSTEM_RULE_CODE] [int] NOT NULL,
	[INTEGRATOR_ID] [bigint] NOT NULL,
	[LIMIT] [int] NULL,
	[TIME_PERIOD] [int] NULL,
  [TIME_BOUND] [int] NULL,
	[VALUE] [nvarchar](100) NULL,
  [RISK_SCORE] [int] NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_defined_rule_ID] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
USE [master]
GO
ALTER DATABASE [yogen_decisionfactory] SET  READ_WRITE
GO
