package com.yogen.factory.data.service;

import com.yogen.factory.data.dao.impl.DefinedRuleDaoImpl;
import com.yogen.factory.data.service.impl.DefinedRuleServiceImpl;
import com.yogen.factory.model.DefinedRule;
import com.yogen.factory.model.dto.DefinedRuleDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DefinedRuleServiceTest {

    @InjectMocks
    DefinedRuleServiceImpl definedRuleService; // This is the class which will be tested. Notice that it is implementation class.

    @Mock
    DefinedRuleDaoImpl definedRuleDao; // This is the class which tested class uses that as dependency

    List<DefinedRule> definedRuleList;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this); // Set all mocks
        definedRuleList = new ArrayList<>();
        DefinedRule definedRule = new DefinedRule();
        definedRule.setId(12345L);
        definedRule.setSystemRuleCode(3003);
        definedRule.setIntegratorId(5L);
        definedRule.setValue("5");
        definedRule.setTimeBound(1);
        definedRule.setTimePeriod(1);
        definedRuleList.add(definedRule);
        definedRuleList.add(definedRule);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    final void getDefinedRuleListByIntegratorId() {

        // Mocking kullanilacak durumlarda bunu kullan
        when(definedRuleDao.getDefinedRuleListByIntegratorId(any(Long.class))).thenReturn(definedRuleList);

        List<DefinedRuleDTO> definedRuleDTOList = definedRuleService.getDefinedRuleDTOListByIntegratorId(5L);
        assertNotNull(definedRuleDTOList);
        assertEquals(definedRuleDTOList.size(), definedRuleList.size());
        for (int i = 0; i < definedRuleDTOList.size(); i++) {
            DefinedRuleDTO definedRuleDTO = definedRuleDTOList.get(i);
            DefinedRule definedRule = definedRuleList.get(i);
            assertEquals(definedRuleDTO.getIntegratorId(), definedRule.getIntegratorId());
            assertEquals(definedRuleDTO.getId(), definedRule.getId());
            assertEquals(definedRuleDTO.getSystemRuleCode(), definedRule.getSystemRuleCode());
            assertEquals(definedRuleDTO.getLimit(), definedRule.getLimit());
            assertEquals(definedRuleDTO.getTimePeriod().getValue(), definedRule.getTimePeriod());
            assertEquals(definedRuleDTO.getTimeBound(), definedRule.getTimeBound());
            assertEquals(definedRuleDTO.getValue(), definedRule.getValue());
            assertEquals(definedRuleDTO.getRiskScore(), definedRule.getRiskScore());
        }
    }

    @Test
    void createDefinedRulesByDTOs() {




    }


}