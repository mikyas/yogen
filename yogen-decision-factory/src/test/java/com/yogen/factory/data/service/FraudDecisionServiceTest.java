package com.yogen.factory.data.service;

import com.yogen.factory.data.service.impl.FraudDecisionServiceImpl;
import com.yogen.factory.filter.YogenRuleExecutor;
import com.yogen.factory.model.dto.*;
import com.yogen.util.enumtype.TimePeriod;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class FraudDecisionServiceTest {

    @InjectMocks
    FraudDecisionServiceImpl fraudDecisionService;

    @Mock
    YogenRuleExecutor yogenRuleExecutor;

    @Mock
    DefinedRuleService definedRuleService;

    List<DefinedRuleDTO> definedRuleDTOList = new ArrayList<>();

    TransactionInfoDTO transactionInfoDTO = new TransactionInfoDTO();

    RuleExecutorTraceDTO ruleExecutorTraceDTO = new RuleExecutorTraceDTO();

    @BeforeEach
    void setUp(){

        DefinedRuleDTO definedRuleDTO = new DefinedRuleDTO();
        definedRuleDTO.setId(12345L);
        definedRuleDTO.setSystemRuleCode(3003);
        definedRuleDTO.setIntegratorId(5L);
        definedRuleDTO.setValue("5");
        definedRuleDTO.setTimeBound(1);
        definedRuleDTO.setRiskScore(100);
        definedRuleDTO.setTimePeriod(TimePeriod.DAY);
        definedRuleDTOList.add(definedRuleDTO);
        definedRuleDTOList.add(definedRuleDTO);

        ruleExecutorTraceDTO.setTransactionUID("TID-123456");
        List<RuleExecuteResult> ruleExecuteResultList = new ArrayList<>();
        RuleExecuteResult ruleExecuteResult1 = new RuleExecuteResult(3101,true,"Sahte Ulan");
        RuleExecuteResult ruleExecuteResult2 = new RuleExecuteResult(2101,true,"Sahte Ulan");

        ruleExecutorTraceDTO.setRuleResults(ruleExecuteResultList);
        transactionInfoDTO.setIntegeratorId(5L);
        transactionInfoDTO.setRiskScoreLimit(300);
    }

    @Test
    void testFraudServiceDetails() throws Exception {
        when(definedRuleService.getDefinedRuleDTOListByIntegratorId(any())).thenReturn(definedRuleDTOList);
        when(yogenRuleExecutor.checkTransactionForFraudByDefinedRules(transactionInfoDTO,definedRuleDTOList,false)).thenReturn(ruleExecutorTraceDTO);

        DecisionDetailDTO decisionDetailDTO = fraudDecisionService.getRuleExecutorResult(transactionInfoDTO);
        int riskScore = 0;
        assertNotEquals(null,decisionDetailDTO);
        for (RuleExecuteResult ruleExecuteResult : ruleExecutorTraceDTO.getRuleResults()){
            DefinedRuleDTO definedRuleDTO = definedRuleDTOList.stream().filter(rule -> ruleExecuteResult.getSystemRuleCode() == rule.getSystemRuleCode()).findAny().orElse(null);
            riskScore += definedRuleDTO != null ? definedRuleDTO.getRiskScore() : 0;
        }
        if (riskScore>transactionInfoDTO.getRiskScoreLimit())
            assertEquals("Risk skoru aşıldığı için fraud tespit edildi.", decisionDetailDTO.getReason());
        else
            assertEquals(null,decisionDetailDTO.getReason());
    }
}