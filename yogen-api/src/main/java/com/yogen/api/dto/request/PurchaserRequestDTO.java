package com.yogen.api.dto.request;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class PurchaserRequestDTO {

    private String purchaserUID;
    private String firstName;
    private String lastName;
    private String mail;
    private String verifiedMail;
    private String mailVerifiedDate;
    private String phone;
    private String verifiedPhone;
    private String phoneVerifiedDate;
    private String firstPurchaseAt;
    private String orderCount;
    private String accountType;
    private String dateOfBirth;
    private String userName;
    private String language;
    private String gender;
    private SocialDetailRequestDTO socialDetail;


    private Date mailVerifiedDateTransient;
    private Date dateOfBirthTransient;
    private Date phoneVerifiedDateTransient;
    private Date firstPurchaseAtTransient;
    private Integer orderCountTransient;

    public String getPurchaserUID() {
        return purchaserUID;
    }

    public void setPurchaserUID(String purchaserUID) {
        this.purchaserUID = purchaserUID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getVerifiedMail() {
        return verifiedMail;
    }

    public void setVerifiedMail(String verifiedMail) {
        this.verifiedMail = verifiedMail;
    }

    public String getMailVerifiedDate() {
        return mailVerifiedDate;
    }

    public void setMailVerifiedDate(String mailVerifiedDate) {
        this.mailVerifiedDate = mailVerifiedDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getVerifiedPhone() {
        return verifiedPhone;
    }

    public void setVerifiedPhone(String verifiedPhone) {
        this.verifiedPhone = verifiedPhone;
    }

    public String getPhoneVerifiedDate() {
        return phoneVerifiedDate;
    }

    public void setPhoneVerifiedDate(String phoneVerifiedDate) {
        this.phoneVerifiedDate = phoneVerifiedDate;
    }

    public String getFirstPurchaseAt() {
        return firstPurchaseAt;
    }

    public void setFirstPurchaseAt(String firstPurchaseAt) {
        this.firstPurchaseAt = firstPurchaseAt;
    }

    public String getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(String orderCount) {
        this.orderCount = orderCount;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public SocialDetailRequestDTO getSocialDetail() {
        return socialDetail;
    }

    public void setSocialDetail(SocialDetailRequestDTO socialDetail) {
        this.socialDetail = socialDetail;
    }

    @JsonIgnore
    public Date getMailVerifiedDateTransient() {
        return mailVerifiedDateTransient;
    }

    public void setMailVerifiedDateTransient(Date mailVerifiedDateTransient) {
        this.mailVerifiedDateTransient = mailVerifiedDateTransient;
    }

    @JsonIgnore
    public Date getDateOfBirthTransient() {
        return dateOfBirthTransient;
    }

    public void setDateOfBirthTransient(Date dateOfBirthTransient) {
        this.dateOfBirthTransient = dateOfBirthTransient;
    }

    @JsonIgnore
    public Date getPhoneVerifiedDateTransient() {
        return phoneVerifiedDateTransient;
    }

    public void setPhoneVerifiedDateTransient(Date phoneVerifiedDateTransient) {
        this.phoneVerifiedDateTransient = phoneVerifiedDateTransient;
    }

    @JsonIgnore
    public Date getFirstPurchaseAtTransient() {
        return firstPurchaseAtTransient;
    }

    public void setFirstPurchaseAtTransient(Date firstPurchaseAtTransient) {
        this.firstPurchaseAtTransient = firstPurchaseAtTransient;
    }

    @JsonIgnore
    public Integer getOrderCountTransient() {
        return orderCountTransient;
    }

    public void setOrderCountTransient(Integer orderCountTransient) {
        this.orderCountTransient = orderCountTransient;
    }
}
