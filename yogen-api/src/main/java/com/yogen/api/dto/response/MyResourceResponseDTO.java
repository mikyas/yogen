package com.yogen.api.dto.response;

import com.yogen.datafeed.model.dto.snapshot.OrderDTO;

public class MyResourceResponseDTO {
    OrderDTO orderDTO;
    String result;

    public OrderDTO getOrderDTO() {
        return orderDTO;
    }

    public void setOrderDTO(OrderDTO orderDTO) {
        this.orderDTO = orderDTO;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
