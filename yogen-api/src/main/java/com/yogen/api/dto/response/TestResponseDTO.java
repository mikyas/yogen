package com.yogen.api.dto.response;

import java.util.List;
import com.yogen.datafeed.model.snapshot.Transaction;
import com.yogen.util.enumtype.BaseResponseDTO;

public class TestResponseDTO extends BaseResponseDTO {


    private List<Transaction> transactions;

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }
}
