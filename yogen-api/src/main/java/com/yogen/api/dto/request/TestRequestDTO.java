package com.yogen.api.dto.request;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class TestRequestDTO extends BaseRequestDTO {
    private String id;
    private String orderId;
    private String name;
    private String code;
    private String date;
    private String price;
    private String email;
    private String currency;

    /**
     * Transient Alanlar
     */

    private Long orderIdTransient;
    private Date dateTransient;
    private double priceTransient;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonIgnore
    public Long getOrderIdTransient() {
        return orderIdTransient;
    }

    public void setOrderIdTransient(Long orderIdTransient) {
        this.orderIdTransient = orderIdTransient;
    }

    @JsonIgnore
    public Date getDateTransient() {
        return dateTransient;
    }

    public void setDateTransient(Date dateTransient) {
        this.dateTransient = dateTransient;
    }

    @JsonIgnore
    public double getPriceTransient() {
        return priceTransient;
    }

    public void setPriceTransient(double priceTransient) {
        this.priceTransient = priceTransient;
    }


}
