package com.yogen.api.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class PaymentDetailRequestDTO {

    private String paymentDetailUID;
    private String paymentType;
    private String installment;
    private String totalPrice;
    private String totalPaidPrice;
    private String currency;
    private String creditCardBin;
    private String creditCardLastFourDigit;
    private String creditCardCompany;
    private String isThreeD;
    private String threeDInquiryResult;
    private String paymentResult;
    private String eciCode;
    private String cavvCode;
    private String mdStatus;
    private String avsResultCode;
    private String cvvResultCode;

    //Transient Variables
    private Integer paymentTypeTransient;
    private Integer installmentTransient;
    private Double totalPriceTransient;
    private Double totalPaidPriceTransient;
    private Boolean isThreeDTransient;
    private Boolean threeDInquiryResultTransient;
    private Boolean paymentResultTransient;

    public String getPaymentDetailUID() {
        return paymentDetailUID;
    }

    public void setPaymentDetailUID(String paymentDetailUID) {
        this.paymentDetailUID = paymentDetailUID;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getTotalPaidPrice() {
        return totalPaidPrice;
    }

    public void setTotalPaidPrice(String totalPaidPrice) {
        this.totalPaidPrice = totalPaidPrice;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCreditCardBin() {
        return creditCardBin;
    }

    public void setCreditCardBin(String creditCardBin) {
        this.creditCardBin = creditCardBin;
    }

    public String getCreditCardLastFourDigit() {
        return creditCardLastFourDigit;
    }

    public void setCreditCardLastFourDigit(String creditCardLastFourDigit) {
        this.creditCardLastFourDigit = creditCardLastFourDigit;
    }

    public String getCreditCardCompany() {
        return creditCardCompany;
    }

    public void setCreditCardCompany(String creditCardCompany) {
        this.creditCardCompany = creditCardCompany;
    }

    public String getIsThreeD() {
        return isThreeD;
    }

    public void setIsThreeD(String isThreeD) {
        this.isThreeD = isThreeD;
    }

    public String getThreeDInquiryResult() {
        return threeDInquiryResult;
    }

    public void setThreeDInquiryResult(String threeDInquiryResult) {
        this.threeDInquiryResult = threeDInquiryResult;
    }

    public String getPaymentResult() {
        return paymentResult;
    }

    public void setPaymentResult(String paymentResult) {
        this.paymentResult = paymentResult;
    }

    public String getEciCode() {
        return eciCode;
    }

    public void setEciCode(String eciCode) {
        this.eciCode = eciCode;
    }

    public String getCavvCode() {
        return cavvCode;
    }

    public void setCavvCode(String cavvCode) {
        this.cavvCode = cavvCode;
    }

    public String getMdStatus() {
        return mdStatus;
    }

    public void setMdStatus(String mdStatus) {
        this.mdStatus = mdStatus;
    }

    public String getAvsResultCode() {
        return avsResultCode;
    }

    public void setAvsResultCode(String avsResultCode) {
        this.avsResultCode = avsResultCode;
    }

    public String getCvvResultCode() {
        return cvvResultCode;
    }

    public void setCvvResultCode(String cvvResultCode) {
        this.cvvResultCode = cvvResultCode;
    }

    @JsonIgnore
    public Integer getPaymentTypeTransient() {
        return paymentTypeTransient;
    }

    public void setPaymentTypeTransient(Integer paymentTypeTransient) {
        this.paymentTypeTransient = paymentTypeTransient;
    }

    @JsonIgnore
    public Integer getInstallmentTransient() {
        return installmentTransient;
    }

    public void setInstallmentTransient(Integer installmentTransient) {
        this.installmentTransient = installmentTransient;
    }

    @JsonIgnore
    public Double getTotalPriceTransient() {
        return totalPriceTransient;
    }

    public void setTotalPriceTransient(Double totalPriceTransient) {
        this.totalPriceTransient = totalPriceTransient;
    }

    @JsonIgnore
    public Double getTotalPaidPriceTransient() {
        return totalPaidPriceTransient;
    }

    public void setTotalPaidPriceTransient(Double totalPaidPriceTransient) {
        this.totalPaidPriceTransient = totalPaidPriceTransient;
    }

    @JsonIgnore
    public Boolean getThreeDTransient() {
        return isThreeDTransient;
    }

    public void setThreeDTransient(Boolean threeDTransient) {
        isThreeDTransient = threeDTransient;
    }

    @JsonIgnore
    public Boolean getThreeDInquiryResultTransient() {
        return threeDInquiryResultTransient;
    }

    public void setThreeDInquiryResultTransient(Boolean threeDInquiryResultTransient) {
        this.threeDInquiryResultTransient = threeDInquiryResultTransient;
    }

    @JsonIgnore
    public Boolean getPaymentResultTransient() {
        return paymentResultTransient;
    }

    public void setPaymentResultTransient(Boolean paymentResultTransient) {
        this.paymentResultTransient = paymentResultTransient;
    }
}
