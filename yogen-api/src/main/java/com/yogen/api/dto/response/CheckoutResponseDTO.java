package com.yogen.api.dto.response;

import com.yogen.util.enumtype.BaseResponseDTO;

import java.util.Date;

public class CheckoutResponseDTO extends BaseResponseDTO {

    private String orderUID;
    private String transactionUID;
    private Date fraudDecisionDate;
    private int fraudDecisionResult;
    private String fraudDecisionNotes;



    public String getOrderUID() {
        return orderUID;
    }

    public void setOrderUID(String orderUID) {
        this.orderUID = orderUID;
    }

    public String getTransactionUID() {
        return transactionUID;
    }

    public void setTransactionUID(String transactionUID) {
        this.transactionUID = transactionUID;
    }

    public Date getFraudDecisionDate() {
        return fraudDecisionDate;
    }

    public void setFraudDecisionDate(Date fraudDecisionDate) {
        this.fraudDecisionDate = fraudDecisionDate;
    }

    public int getFraudDecisionResult() {
        return fraudDecisionResult;
    }

    public void setFraudDecisionResult(int fraudDecisionResult) {
        this.fraudDecisionResult = fraudDecisionResult;
    }

    public String getFraudDecisionNotes() {
        return fraudDecisionNotes;
    }

    public void setFraudDecisionNotes(String fraudDecisionNotes) {
        this.fraudDecisionNotes = fraudDecisionNotes;
    }
}
