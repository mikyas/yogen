package com.yogen.api.dto.request;

public class SellerRequestDTO {
    private String sellerUID;
    private String name;
    private LocationRequestDTO location;

    public String getSellerUID() {
        return sellerUID;
    }

    public void setSellerUID(String sellerUID) {
        this.sellerUID = sellerUID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocationRequestDTO getLocation() {
        return location;
    }

    public void setLocation(LocationRequestDTO location) {
        this.location = location;
    }
}
