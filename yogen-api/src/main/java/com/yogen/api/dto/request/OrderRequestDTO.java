package com.yogen.api.dto.request;

import java.util.Date;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class OrderRequestDTO extends BaseRequestDTO{

    private String orderUID;
    private String name;
    private String createdAt;
    private String updatedAt;
    private String closedAt;
    private List<ItemRequestDTO> items;
    private ShipmentRequestDTO shipment;
    private BillingRequestDTO billing;
    private PurchaserRequestDTO purchaser;
    private PaymentDetailRequestDTO paymentDetail;
    private DeviceRequestDTO device;
    private ContactRequestDTO contact;

    // Transient Fields
    private Date createdAtTransient;
    private Date updatedAtTransient;
    private Date closedAtTransient;

    public String getOrderUID() {
        return orderUID;
    }

    public void setOrderUID(String orderUID) {
        this.orderUID = orderUID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getClosedAt() {
        return closedAt;
    }

    public void setClosedAt(String closedAt) {
        this.closedAt = closedAt;
    }

    public List<ItemRequestDTO> getItems() {
        return items;
    }

    public void setItems(List<ItemRequestDTO> items) {
        this.items = items;
    }

    public ShipmentRequestDTO getShipment() {
        return shipment;
    }

    public void setShipment(ShipmentRequestDTO shipment) {
        this.shipment = shipment;
    }

    public BillingRequestDTO getBilling() {
        return billing;
    }

    public void setBilling(BillingRequestDTO billing) {
        this.billing = billing;
    }

    public PurchaserRequestDTO getPurchaser() {
        return purchaser;
    }

    public void setPurchaser(PurchaserRequestDTO purchaser) {
        this.purchaser = purchaser;
    }

    public PaymentDetailRequestDTO getPaymentDetail() {
        return paymentDetail;
    }

    public void setPaymentDetail(PaymentDetailRequestDTO paymentDetail) {
        this.paymentDetail = paymentDetail;
    }

    @JsonIgnore
    public Date getCreatedAtTransient() {
        return createdAtTransient;
    }

    public void setCreatedAtTransient(Date createdAtTransient) {
        this.createdAtTransient = createdAtTransient;
    }

    @JsonIgnore
    public Date getUpdatedAtTransient() {
        return updatedAtTransient;
    }

    public void setUpdatedAtTransient(Date updatedAtTransient) {
        this.updatedAtTransient = updatedAtTransient;
    }

    @JsonIgnore
    public Date getClosedAtTransient() {
        return closedAtTransient;
    }

    public void setClosedAtTransient(Date closedAtTransient) {
        this.closedAtTransient = closedAtTransient;
    }

    public DeviceRequestDTO getDevice() {
        return device;
    }

    public void setDevice(DeviceRequestDTO device) {
        this.device = device;
    }

    public ContactRequestDTO getContact() {
        return contact;
    }

    public void setContact(ContactRequestDTO contact) {
        this.contact = contact;
    }
}
