package com.yogen.api.dto.request;

public class BillingRequestDTO {

    private String billingUID;
    private String firstName;
    private String lastName;
    private String citizenNum;
    private String taxNum;
    private String serialNum;
    private LocationRequestDTO location;

    public String getBillingUID() {
        return billingUID;
    }

    public void setBillingUID(String billingUID) {
        this.billingUID = billingUID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCitizenNum() {
        return citizenNum;
    }

    public void setCitizenNum(String citizenNum) {
        this.citizenNum = citizenNum;
    }

    public String getTaxNum() {
        return taxNum;
    }

    public void setTaxNum(String taxNum) {
        this.taxNum = taxNum;
    }

    public String getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    public LocationRequestDTO getLocation() {
        return location;
    }

    public void setLocation(LocationRequestDTO location) {
        this.location = location;
    }
}
