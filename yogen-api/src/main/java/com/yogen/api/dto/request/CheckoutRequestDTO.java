package com.yogen.api.dto.request;

public class CheckoutRequestDTO extends BaseRequestDTO {

    OrderRequestDTO order;

    public OrderRequestDTO getOrder() {
        return order;
    }

    public void setOrder(OrderRequestDTO order) {
        this.order = order;
    }
}
