package com.yogen.api.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BaseRequestDTO {


    private String integratorId;
    private String integratorIp;

    private Long integratorIdTransient;

    public String getIntegratorId() {
        return integratorId;
    }

    public void setIntegratorId(String integratorId) {
        this.integratorId = integratorId;
    }

    public String getIntegratorIp() {
        return integratorIp;
    }

    public void setIntegratorIp(String integratorIp) {
        this.integratorIp = integratorIp;
    }

    @JsonIgnore
    public Long getIntegratorIdTransient() {
        return integratorIdTransient;
    }

    public void setIntegratorIdTransient(Long integratorIdTransient) {
        this.integratorIdTransient = integratorIdTransient;
    }
}
