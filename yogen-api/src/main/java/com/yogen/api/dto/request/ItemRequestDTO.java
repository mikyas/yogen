package com.yogen.api.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ItemRequestDTO {

    private String itemUID;
    private String type;
    private String upc;
    private String sku;
    private String price;
    private String paidPrice;
    private String quantity;
    private String brand;
    private String manufacturer;
    private String title;
    private SellerRequestDTO seller;


    // Transient Variables
    private Integer typeTransient;
    private Double priceTransient;
    private Double paidPriceTransient;
    private Integer quantityTransient;

    public String getItemUID() {
        return itemUID;
    }

    public void setItemUID(String itemUID) {
        this.itemUID = itemUID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPaidPrice() {
        return paidPrice;
    }

    public void setPaidPrice(String paidPrice) {
        this.paidPrice = paidPrice;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SellerRequestDTO getSeller() {
        return seller;
    }

    public void setSeller(SellerRequestDTO seller) {
        this.seller = seller;
    }

    @JsonIgnore
    public Integer getTypeTransient() {
        return typeTransient;
    }

    public void setTypeTransient(Integer typeTransient) {
        this.typeTransient = typeTransient;
    }

    @JsonIgnore
    public Double getPriceTransient() {
        return priceTransient;
    }

    public void setPriceTransient(Double priceTransient) {
        this.priceTransient = priceTransient;
    }

    @JsonIgnore
    public Double getPaidPriceTransient() {
        return paidPriceTransient;
    }

    public void setPaidPriceTransient(Double paidPriceTransient) {
        this.paidPriceTransient = paidPriceTransient;
    }

    @JsonIgnore
    public Integer getQuantityTransient() {
        return quantityTransient;
    }

    public void setQuantityTransient(Integer quantityTransient) {
        this.quantityTransient = quantityTransient;
    }
}
