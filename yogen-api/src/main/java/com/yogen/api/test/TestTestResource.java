package com.yogen.api.test;

import com.yogen.api.dto.request.TestRequestDTO;
import com.yogen.util.StringUtil;

public class TestTestResource extends BaseIntegrationTestResource {

    public static void main(String[] args) {
        try {
            testApi();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void testApi() {
        String url = "api/publichNewKeys";
        TestRequestDTO requestDTO = new TestRequestDTO();
        requestDTO.setIntegratorId("1");
        requestDTO.setIntegratorIp("192.168.248.177");

        requestDTO.setId("1");
        requestDTO.setCode("integratorcode");
        requestDTO.setCurrency("TRY");
        requestDTO.setDate("2017-10-30 15:47:05");
        requestDTO.setEmail("integrator@mail.com");
        requestDTO.setName("integratorname");
        requestDTO.setOrderId("abcd-1234-efgd-5678-hjkl");
        requestDTO.setPrice("120.45");

        String[] hashParamValues = new String[]{
                requestDTO.getOrderId(),
                requestDTO.getIntegratorIp(),
                StringUtil.isNotNullAndNotZeroLength(requestDTO.getIntegratorId()) ? requestDTO.getIntegratorId() : StringUtil.EMPTY_STRING};
        String hashedParams = prepareHashValues(hashParamValues);

        sendRequest(requestDTO, url, hashedParams);
    }


}
