package com.yogen.api.test;

import com.yogen.api.dto.request.*;

import java.util.ArrayList;
import java.util.List;

public class YogenTestResource extends BaseIntegrationTestResource {


    public static void main(String[] args) {
        try {
            String testIntegratorId = "001FjQ3VGCxs9aBcA9cWxnAOQ=="; //001FjQ3VGCxs9aBcA9cWxnAOQ==
            checkoutApi(testIntegratorId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void checkoutApi(String testIntegratorId) {
        String url = "api/checkout";
        CheckoutRequestDTO requestDTO = new CheckoutRequestDTO();
        requestDTO.setIntegratorId(testIntegratorId);
        requestDTO.setIntegratorIp("192.168.1.1");
        OrderRequestDTO orderRequestDTO = initializeOrderRequestDTO();

        requestDTO.setOrder(orderRequestDTO);

        String[] hashParamValues = new String[]{
                orderRequestDTO.getOrderUID(),
                requestDTO.getIntegratorId(),
                requestDTO.getIntegratorIp(),

        };
        String hashedParams = prepareHashValues(hashParamValues);
        sendRequest(requestDTO, url, hashedParams);

    }

    private static OrderRequestDTO initializeOrderRequestDTO() {
        OrderRequestDTO orderRequestDTO = new OrderRequestDTO();
        orderRequestDTO.setName("Test Order 1");
        orderRequestDTO.setOrderUID("OrderUID 001");
        orderRequestDTO.setCreatedAt("2017-10-30 15:47:05");
        orderRequestDTO.setClosedAt("2017-10-30 15:47:05");
        orderRequestDTO.setUpdatedAt("2017-10-30 15:47:05");
        orderRequestDTO.setItems(initializeItems());
        orderRequestDTO.setShipment(initializeShipmentRequestDTO());
        orderRequestDTO.setBilling(initializeBillingRequestDTO());
        orderRequestDTO.setPurchaser(initializePurchaserRequestDTO());
        orderRequestDTO.setPaymentDetail(initializePaymentDetailRequestDTO());
        orderRequestDTO.setDevice(initializeDeviceRequestDTO());
        orderRequestDTO.setContact(initializeContactRequestDTO());
        return orderRequestDTO;
    }

    private static PaymentDetailRequestDTO initializePaymentDetailRequestDTO() {
        PaymentDetailRequestDTO paymentDetailRequestDTO = new PaymentDetailRequestDTO();
        paymentDetailRequestDTO.setPaymentDetailUID("Payment001");
        paymentDetailRequestDTO.setPaymentType("1");
        paymentDetailRequestDTO.setInstallment("1");
        paymentDetailRequestDTO.setTotalPrice("9990");
        paymentDetailRequestDTO.setTotalPaidPrice("9990");
        paymentDetailRequestDTO.setCurrency("TRY");
        paymentDetailRequestDTO.setCreditCardCompany("VISA");
        paymentDetailRequestDTO.setCreditCardBin("123456");
        paymentDetailRequestDTO.setCreditCardLastFourDigit("1234");
        paymentDetailRequestDTO.setIsThreeD("0");
        paymentDetailRequestDTO.setThreeDInquiryResult("1");
        paymentDetailRequestDTO.setPaymentResult("1");
        paymentDetailRequestDTO.setEciCode("05");
        paymentDetailRequestDTO.setCavvCode("8");
        paymentDetailRequestDTO.setMdStatus("1");
        paymentDetailRequestDTO.setAvsResultCode("A");
        paymentDetailRequestDTO.setCvvResultCode("M");
        return paymentDetailRequestDTO;
    }

    private static PurchaserRequestDTO initializePurchaserRequestDTO() {

        PurchaserRequestDTO purchaserRequestDTO = new PurchaserRequestDTO();
        purchaserRequestDTO.setPurchaserUID("purchaser001");
        purchaserRequestDTO.setFirstName("Ferhat");
        purchaserRequestDTO.setLastName("Yaman");
        purchaserRequestDTO.setMail("email@mail.com");
        //purchaserRequestDTO.setPhone("+905012345678");
        purchaserRequestDTO.setDateOfBirth("2017-10-30 15:47:05");
        purchaserRequestDTO.setAccountType("2");
        purchaserRequestDTO.setUserName("fyaman");
        purchaserRequestDTO.setLanguage("TR");
        purchaserRequestDTO.setGender("M");
        return purchaserRequestDTO;
    }

    private static BillingRequestDTO initializeBillingRequestDTO() {
        BillingRequestDTO billingRequestDTO = new BillingRequestDTO();
        billingRequestDTO.setBillingUID("Billing 001");
        billingRequestDTO.setFirstName("Ali");
        billingRequestDTO.setLastName("Veli");
        billingRequestDTO.setCitizenNum("123456789");
        billingRequestDTO.setTaxNum("1234567890");
        billingRequestDTO.setSerialNum("SERI:001SD89");
        billingRequestDTO.setLocation(initializeLocationRequestDTO());
        return billingRequestDTO;
    }

    private static List<ItemRequestDTO> initializeItems() {
        List<ItemRequestDTO> items = new ArrayList<>();
        ItemRequestDTO itemRequestDTO = new ItemRequestDTO();
        itemRequestDTO.setItemUID("Item001");
        itemRequestDTO.setType("1");
        itemRequestDTO.setUpc("012345678901");
        itemRequestDTO.setSku("CM01R");
        itemRequestDTO.setPrice("9990");
        itemRequestDTO.setPaidPrice("9990");
        itemRequestDTO.setQuantity("1");
        itemRequestDTO.setBrand("Brand");
        itemRequestDTO.setManufacturer("Manufacturer");
        itemRequestDTO.setTitle("Product");
        SellerRequestDTO sellerRequestDTO = new SellerRequestDTO();
        sellerRequestDTO.setSellerUID("Seller001");
        sellerRequestDTO.setName("Seller 001");
        sellerRequestDTO.setLocation(initializeLocationRequestDTO());
        itemRequestDTO.setSeller(sellerRequestDTO);
        items.add(itemRequestDTO);
        return items;
    }

    private static ShipmentRequestDTO initializeShipmentRequestDTO() {
        ShipmentRequestDTO shipmentRequestDTO = new ShipmentRequestDTO();
        shipmentRequestDTO.setShipmentUID("Shipment 001");
        shipmentRequestDTO.setFirstName("Ali");
        shipmentRequestDTO.setLastName("Veli");
        shipmentRequestDTO.setCitizenNum("123456789");
        shipmentRequestDTO.setDeliveryCompany("UPS");
        shipmentRequestDTO.setTaxNum("123456789");
        shipmentRequestDTO.setLocation(initializeLocationRequestDTO());
        return shipmentRequestDTO;
    }


    private static DeviceRequestDTO initializeDeviceRequestDTO() {

        DeviceRequestDTO deviceRequestDTO = new DeviceRequestDTO();
        deviceRequestDTO.setDeviceUID("Device 001");
        deviceRequestDTO.setIpAddress("192.168.1.1");
        deviceRequestDTO.setOs("Mac OS");
        deviceRequestDTO.setModel("MacBook Pro");
        deviceRequestDTO.setManufacturer("Apple");
        deviceRequestDTO.setType("Desktop");
        return deviceRequestDTO;
    }

    private static ContactRequestDTO initializeContactRequestDTO() {
        ContactRequestDTO contactRequestDTO = new ContactRequestDTO();
        contactRequestDTO.setContactUID("Contact 001");
        contactRequestDTO.setAccountUrl("facebook.com");
        contactRequestDTO.setEmail("mail@mail.com");
        contactRequestDTO.setPhone("123456789");
        contactRequestDTO.setType("Social");
        return contactRequestDTO;
    }

    private static LocationRequestDTO initializeLocationRequestDTO() {
        LocationRequestDTO locationRequestDTO = new LocationRequestDTO();
        locationRequestDTO.setAddress1("GOSB Teknopark, Kemal Nehrozoğlu Caddesi");
        locationRequestDTO.setAddress2("1.Üretim Binası 3. Ve 4. Ünite");
        locationRequestDTO.setNeighbourhood("GOSB Teknoopark");
        locationRequestDTO.setCity("Kocaeli");
        locationRequestDTO.setRegion("Gebze");
        locationRequestDTO.setCountry("Turkey");
        locationRequestDTO.setPoBoxNumber("005");
        locationRequestDTO.setPostalCode("41480");
        locationRequestDTO.setLatitude("405055.1N");
        locationRequestDTO.setLongitude("292533.0E");
        locationRequestDTO.setGeohash("sxkbms71pd4r");
        return locationRequestDTO;
    }
}
