package com.yogen.api.test;

import java.util.Date;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import com.yogen.service.util.TLS12RestTemplate;
import com.yogen.util.DateUtil;
import com.yogen.util.StringUtil;

public class BaseIntegrationTestResource {

    static String publicKey;
    static String privateKey;
    static String apiUrl;

    private enum Environment {
        LOCAL,
        TEST,
        LIVE;
    }

    static final Environment environment = Environment.LOCAL;

    static {
        if (environment.equals(Environment.LOCAL)) {
            apiUrl = "http://localhost:8083/";
            publicKey = "AzQasdW1rsGy";// LOCAL
            privateKey = "F4rK";// LOCAL
        } else if (environment.equals(Environment.TEST)) {
//            apiUrl = "https://apitest.yogen.com/";
//            publicKey = "EILSMVQZJY2XY7R";
//            privateKey = "10HSB9DBH6M6YD00BVT37QS77";
        } else if (environment.equals(Environment.LIVE)) {
//            apiUrl = "";
//            publicKey = "";
//            privateKey = "";
        }
    }

    protected static String sendRequest(Object requestEntity, String path, String hashParams) {
        RestTemplate restTemplate = new TLS12RestTemplate();

        String url = apiUrl + path;

        String transactionDate = DateUtil.toMYSQLDateTimeFormat(new Date());

        String token = null;

        try {
            token = StringUtil.getSHA1Text(privateKey + hashParams + transactionDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String header = publicKey + ":" + token;
        System.out.println("header : " + header);

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.add("token", header);
        headers.add("transactionDate", transactionDate);
        headers.add("version", "1.0");

        RequestEntity entity = new RequestEntity<>(requestEntity, headers, HttpMethod.POST, UriComponentsBuilder.fromHttpUrl(url).build().toUri());

        ResponseEntity<String> responseEntity = restTemplate.exchange(entity, String.class);
        System.out.println(responseEntity.getBody());
        return responseEntity.getBody();
    }

    protected static String prepareHashValues(String[] hashValues) {
        String hashParams = "";
        if (hashValues != null) {
            for (String s : hashValues) {
                if (!StringUtil.isNullOrZeroLength(s)) {
                    hashParams += s;
                } else {
                    hashParams += StringUtil.EMPTY_STRING;
                }
            }
        } else {
            return StringUtil.EMPTY_STRING;
        }

        return hashParams;
    }
}