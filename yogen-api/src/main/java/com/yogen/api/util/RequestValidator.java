package com.yogen.api.util;

import com.yogen.api.dto.request.BaseRequestDTO;
import com.yogen.integrator.data.service.EncryptionService;
import com.yogen.integrator.model.dto.IntegratorInfoDTO;
import com.yogen.util.DateUtil;
import com.yogen.util.EnumUtil;
import com.yogen.util.NumberUtil;
import com.yogen.util.StringUtil;
import com.yogen.util.enumtype.*;
import com.yogen.util.validator.*;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.validator.routines.InetAddressValidator;
import org.apache.commons.validator.routines.UrlValidator;

import java.net.URLDecoder;
import java.util.Date;
import java.util.regex.Pattern;

public class RequestValidator {


    public static void validateIntegratorIdAndIntegratorIp(BaseRequestDTO baseRequestDTO, BaseResponseDTO baseResponseDTO, IntegratorInfoDTO integratorInfoDTO, EncryptionService encryptionService) throws Exception {
        baseRequestDTO.setIntegratorIp(validateIntegratorIp(baseRequestDTO.getIntegratorIp(), baseResponseDTO));
        baseRequestDTO.setIntegratorIdTransient(validateEncryptedLong(baseRequestDTO.getIntegratorId(), true, encryptionService, MessageCode.INTEGRATOR_ID_EMPTY, MessageCode.INTEGRATOR_ID_INVALID, baseResponseDTO));
    }


    public static String validateIntegratorIp(String integratorIp, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(integratorIp)) {
//            response.setErrorCode(MessageCode.CLIENT_IP_FIELD_EMPTY.getValue());
            response.setForceSystemErrorMessage(true);
            throw new Exception();
        }

//        integratorIp = integratorIp.trim();
//        if (!YogenConstants.PROFILE.equals(ProfileMode.PROD) && integratorIp.equals("0:0:0:0:0:0:0:1")) {
//            return integratorIp;
//        }

        if (!integratorIp.equals("::1") && !InetAddressValidator.getInstance().isValid(integratorIp)) {
//            response.setErrorCode(MessageCode.CLIENT_IP_FIELD_INVALID.getValue());
            response.setForceSystemErrorMessage(true);
            throw new Exception();
        }

        return integratorIp;
    }

    public static String validatePassword(String password, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        // Password
        if (StringUtil.isNullOrZeroLength(password)) {
            response.setErrorCode(emptyError.getValue());
            throw new Exception();
        }

        password = password.trim();

        if (!PasswordValidator.isPasswordValid(password)) {
            response.setErrorCode(invalidError.getValue());
            throw new Exception();
        }

        return password;
    }

    public static void validateIntegerInterval(int value, int minValue, int maxValue, MessageCode errorCode, BaseResponseDTO response) throws Exception {
        if (value < minValue || value > maxValue) {
            response.setErrorCode(errorCode.getValue());
            response.setForceSystemErrorMessage(false);
            throw new Exception();
        }
    }

    public static void validateOneMonthInterval(Date startDateTransient, Date endDateTransient, BaseResponseDTO response) throws Exception {
        if (startDateTransient != null && endDateTransient != null && DateUtil.addMonth(startDateTransient, 1).getTime() < endDateTransient.getTime()) {
//            response.setErrorCode(MessageCode.DATE_INTERVAL_EXCEEDS_30_DAY.getValue());
            throw new Exception();
        }
    }

    public static void validateThreeMonthInterval(Date startDateTransient, Date endDateTransient, BaseResponseDTO response) throws Exception {
        if (startDateTransient != null && endDateTransient != null && DateUtil.addMonth(startDateTransient, 3).getTime() < endDateTransient.getTime()) {
//            response.setErrorCode(MessageCode.DATE_INTERVAL_EXCEEDS_90_DAYS.getValue());
            throw new Exception();
        }
    }

    public static void validateOneYearInterval(Date startDateTransient, Date endDateTransient, BaseResponseDTO response) throws Exception {
        if (startDateTransient != null && endDateTransient != null && DateUtil.addYears(startDateTransient, 1).getTime() < endDateTransient.getTime()) {
//            response.setErrorCode(MessageCode.DATE_INTERVAL_EXCEEDS_ONE_YEAR.getValue());
            throw new Exception();
        }
    }

    public static void validateDateInterval(Date startDateTransient, Date endDateTransient, int dateIntervalCount, BaseResponseDTO response, MessageCode errorCode) throws Exception {
        if (startDateTransient != null && endDateTransient != null && DateUtil.addDays(startDateTransient, dateIntervalCount).getTime() < endDateTransient.getTime()) {
            response.setErrorCode(errorCode.getValue());
            throw new Exception();
        }
    }


    public static String validateIban(String iban, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(iban)) {
//            response.setErrorCode(MessageCode.IBAN_EMPTY.getValue());
            throw new Exception();
        }

        iban = iban.trim();

        if (!IbanValidator.validate(iban)) {
//            response.setErrorCode(MessageCode.IBAN_INVALID.getValue());
            throw new Exception();
        }

        return iban;
    }

    public static String validateTaxNumber(String taxNumber, boolean required, boolean cityInKKTC, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(taxNumber)) {
            if (required) {
                response.setErrorCode(MessageCode.BILLING_TAXNUM_FIELD_EMPTY.getValue());
                throw new Exception();
            }
            return null;
        }

        taxNumber = taxNumber.trim();

        if (!cityInKKTC && !TaxNumberValidator.isValid(taxNumber)) {
            response.setErrorCode(MessageCode.BILLING_TAXNUM_FIELD_INVALID.getValue());
            throw new Exception();
        }
        return taxNumber;
    }

    public static String validateRequiredString(String value, boolean decodeIfNecessary, boolean forceSystemErrorMessage, MessageCode emptyError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(value)) {
            response.setErrorCode(emptyError.getValue());
            response.setForceSystemErrorMessage(forceSystemErrorMessage);
            throw new Exception();
        }

        value = StringEscapeUtils.unescapeHtml(value).trim();

        if (decodeIfNecessary && value.contains("%")) {
            value = URLDecoder.decode(value, YogenConstants.UTF8);
        }

        return value;
    }

    public static String validateRegex(String value, boolean required, Pattern pattern, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(value)) {
            if (required) {
                response.setErrorCode(emptyError.getValue());
                throw new Exception();
            }
            return null;
        }

        value = value.trim();

        if (!ValidatorUtil.validateRegex(pattern, value)) {
            response.setErrorCode(invalidError.getValue());
            throw new Exception();
        }

        return value;
    }

    public static String validatePhoneNumber(String telephone, Boolean required, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(telephone)) {
            if (required) {
                response.setErrorCode(emptyError.getValue());
                throw new Exception();
            } else
                return null;
        }

        telephone = telephone.trim();

        if (!ValidatorUtil.validateRegex(ValidatorUtil.TELEPHONE_REGEX, telephone)) {
            response.setErrorCode(invalidError.getValue());
            throw new Exception();
        }

        return telephone;
    }

    public static Boolean validateBooleanIntegerValue(String value, boolean required, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(value)) {
            if (required) {
                response.setErrorCode(emptyError.getValue());
                throw new Exception();
            }
            return null;
        }

        value = value.trim();

        try {
            int valueInt = Integer.valueOf(value.trim());
            if (valueInt == GenericStatus.ACTIVE_POSITIVE.getValue()) {
                return true;
            } else if (valueInt == GenericStatus.PASSIVE_NEGATIVE.getValue()) {
                return false;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            response.setErrorCode(invalidError.getValue());
            throw new Exception();
        }
    }

    public static Long validateLongValue(String value, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(value)) {
            response.setErrorCode(emptyError.getValue());
            throw new Exception();
        }

        value = value.trim();

        try {
            return Long.valueOf(value.trim());
        } catch (Exception e) {
            response.setErrorCode(invalidError.getValue());
            throw new Exception();
        }
    }

    public static Integer validateEncryptedInteger(String value, boolean forceSystemErrorMessage, EncryptionService encryptionService, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(value)) {
            response.setErrorCode(emptyError.getValue());
            response.setForceSystemErrorMessage(forceSystemErrorMessage);
            throw new Exception();
        }

        value = value.trim();

        try {
            if (value.contains("%")) {
                value = URLDecoder.decode(value, YogenConstants.UTF8);
            }

            value = encryptionService.decrypt(value, EncryptionKeyType.TEMP);
            return Integer.valueOf(value.trim());
        } catch (Throwable e) {
            response.setErrorCode(invalidError.getValue());
            response.setForceSystemErrorMessage(forceSystemErrorMessage);
            throw new Exception();
        }
    }

    public static Long validateEncryptedLong(String value, boolean forceSystemErrorMessage, EncryptionService encryptionService, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(value)) {
            response.setErrorCode(emptyError.getValue());
            response.setForceSystemErrorMessage(forceSystemErrorMessage);
            throw new Exception();
        }
        value = value.trim();

        try {
            value = encryptionService.decrypt(value, EncryptionKeyType.TEMP);
            return Long.valueOf(value.trim());
        } catch (Throwable e) {
            response.setErrorCode(invalidError.getValue());
            response.setForceSystemErrorMessage(forceSystemErrorMessage);
            throw new Exception();
        }
    }

    public static String validateEncryptedString(String value, boolean forceSystemErrorMessage, EncryptionService encryptionService, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(value)) {
            response.setErrorCode(emptyError.getValue());
            response.setForceSystemErrorMessage(forceSystemErrorMessage);
            throw new Exception();
        }

        value = value.trim();

        try {
            if (value.contains("%")) {
                value = URLDecoder.decode(value, YogenConstants.UTF8);
            }
            return encryptionService.decrypt(value, EncryptionKeyType.TEMP);
        } catch (Throwable e) {
            response.setErrorCode(invalidError.getValue());
            response.setForceSystemErrorMessage(forceSystemErrorMessage);
            throw new Exception();
        }
    }

    public static Integer validateInteger(String value, boolean required, boolean forceSystemErrorMessage, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(value)) {
            if (required) {
                response.setErrorCode(emptyError.getValue());
                response.setForceSystemErrorMessage(forceSystemErrorMessage);
                throw new Exception();
            }
            return null;
        }

        value = value.trim();

        try {
            return Integer.valueOf(value.trim());
        } catch (Exception e) {
            response.setErrorCode(invalidError.getValue());
            response.setForceSystemErrorMessage(forceSystemErrorMessage);
            throw new Exception();
        }
    }

    public static Integer validateIntegerFromRegex(String value, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(value)) {
            response.setErrorCode(emptyError.getValue());
            throw new Exception();
        } else if (!ValidatorUtil.validateRegex(ValidatorUtil.INTEGER_WITHOUT_SIGNS_REGEX, value)) {
            response.setErrorCode(invalidError.getValue());
            throw new Exception();
        } else {
            try {
                return Integer.valueOf(value);
            } catch (Exception e) {
                response.setErrorCode(invalidError.getValue());
                throw new Exception();
            }
        }
    }

    public static String validateIntegerWithRegex(String value, boolean required, Pattern pattern, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(value)) {
            if (required) {
                response.setErrorCode(emptyError.getValue());
                throw new Exception();
            }
            return null;
        } else if (!ValidatorUtil.validateRegex(pattern, value)) {
            response.setErrorCode(invalidError.getValue());
            throw new Exception();
        } else {
            try {
                return value;
            } catch (Exception e) {
                response.setErrorCode(invalidError.getValue());
                throw new Exception();
            }
        }
    }

    public static String validateName(String name, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(name)) {
            response.setErrorCode(emptyError.getValue());
            throw new Exception();
        }

        name = name.trim();

        if (!ValidatorUtil.validateRegex(ValidatorUtil.NAME_SURNAME_REGEX, name)) {
            response.setErrorCode(invalidError.getValue());
            throw new Exception();
        }

        return name;
    }

    public static String validateName(String name, boolean required, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(name)) {
            if (required) {
                response.setErrorCode(emptyError.getValue());
                throw new Exception();
            } else {
                return null;
            }
        }

        name = name.trim();

        if (!ValidatorUtil.validateRegex(ValidatorUtil.NAME_SURNAME_REGEX, name)) {
            response.setErrorCode(invalidError.getValue());
            throw new Exception();
        }

        return name;
    }

    public static String validateSurname(String surname, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(surname)) {
            response.setErrorCode(emptyError.getValue());
            throw new Exception();
        }

        surname = surname.trim();

        if (!ValidatorUtil.validateRegex(ValidatorUtil.NAME_SURNAME_REGEX, surname)) {
            response.setErrorCode(invalidError.getValue());
            throw new Exception();
        }

        return surname;
    }

    public static String validateSurname(String surname, boolean required, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(surname)) {
            if (required) {
                response.setErrorCode(emptyError.getValue());
                throw new Exception();
            } else {
                return null;
            }
        }

        surname = surname.trim();

        if (!ValidatorUtil.validateRegex(ValidatorUtil.NAME_SURNAME_REGEX, surname)) {
            response.setErrorCode(invalidError.getValue());
            throw new Exception();
        }

        return surname;
    }

    public static String validateEmail(String email, boolean required, BaseResponseDTO response) throws Exception {
        // Email
        if (StringUtil.isNullOrZeroLength(email)) {
            if (required) {
//                response.setErrorCode(MessageCode.EMAIL_EMPTY.getValue());
                throw new Exception();
            }
            return null;
        }

//        initializeRequiredResponse(email, required, emptyError, response);


        email = email.trim();

        if (email.length() > 100) {
//            response.setErrorCode(MessageCode.EMAIL_INVALID.getValue());
            throw new Exception();
        }

        if (!ValidatorUtil.validateRegex(ValidatorUtil.EMAIL_REGEX, email)) {
//            response.setErrorCode(MessageCode.EMAIL_INVALID.getValue());
            throw new Exception();
        }

        return email;
    }

    public static String validateGsm(String gsm, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(gsm)) {
//            response.setErrorCode(MessageCode.CORPORATE_RESOURCE_GSM_EMPTY.getValue());
            throw new Exception();
        }

        gsm = gsm.trim();

        if (!ValidatorUtil.validateRegex(ValidatorUtil.GSM_REGEX, gsm)) {
//            response.setErrorCode(MessageCode.CORPORATE_RESOURCE_GSM_INVALID.getValue());
            throw new Exception();
        }

        return gsm;
    }

    public static String validateGsm(String gsm, boolean required, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(gsm)) {
            if (required) {
//                response.setErrorCode(MessageCode.CORPORATE_RESOURCE_GSM_EMPTY.getValue());
                throw new Exception();
            } else {
                return null;
            }
        }

        gsm = gsm.trim();

        if (!ValidatorUtil.validateRegex(ValidatorUtil.GSM_REGEX, gsm)) {
//            response.setErrorCode(MessageCode.CORPORATE_RESOURCE_GSM_INVALID.getValue());
            throw new Exception();
        }

        return gsm;
    }


    public static String validateTck(String tck, boolean required, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(tck)) {
            if (required) {
//                response.setErrorCode(MessageCode.TC_CERTIFICATE_EMPTY.getValue());
                throw new Exception();
            }
            return null;
        }

        if (!StringUtil.isNullOrZeroLength(tck)) {
            tck = tck.trim();

            if (!ValidatorUtil.validateRegex(ValidatorUtil.TC_CERTIFICATE_REGEX, tck)) {
//                response.setErrorCode(MessageCode.TC_CERTIFICATE_LENGTH_INVALID.getValue());
                throw new Exception();
            }

            if (!TcCertificateValidator.isValid(tck)) {
//                response.setErrorCode(MessageCode.TC_CERTIFICATE_LENGTH_INVALID.getValue());
                throw new Exception();
            }
            return tck;
        }

        return null;
    }

    public static Date validateBirthDate(String birthDate, boolean nullable, BaseResponseDTO response) throws Exception {
        if (!nullable && StringUtil.isNullOrZeroLength(birthDate)) {
//            response.setErrorCode(MessageCode.CORPORATE_RESOURCE_BIRTH_DATE_EMPTY.getValue());
            throw new Exception();
        }

        Date birth = null;
        if (!StringUtil.isNullOrZeroLength(birthDate)) {
            try {
                birth = DateUtil.getDateFromMySQLFormat(birthDate.trim());
                if (DateUtil.getYear(birth) < 1900) {
//                    response.setErrorCode(MessageCode.CORPORATE_RESOURCE_BIRTH_DATE_INVALID.getValue());
                    throw new Exception();
                }
            } catch (Exception e) {
//                response.setErrorCode(MessageCode.CORPORATE_RESOURCE_BIRTH_DATE_INVALID.getValue());
                throw new Exception();
            }
        }
        return birth;
    }

    public static String validateUrl(String url, Boolean required, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(url)) {
            if (required) {
                response.setErrorCode(emptyError.getValue());
                throw new Exception();
            } else
                return null;

        }

        url = url.trim();

        if (!UrlValidator.getInstance().isValid(url)) {
            response.setErrorCode(invalidError.getValue());
            throw new Exception();
        }
        return url;
    }

    public static Double validateDouble(String value, boolean required, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(value)) {
            if (required) {
                response.setErrorCode(emptyError.getValue());
                throw new Exception();
            }
            return null;
        }

        Double amount;
        try {
            amount = NumberUtil.parseDouble(value);
        } catch (Exception ex) {
            response.setErrorCode(invalidError.getValue());
            throw new Exception();
        }

        if (amount < YogenConstants.MAXIMUM_DIFFERENCE_FOR_FRACTION_DIGITS) {
            response.setErrorCode(invalidError.getValue());
            throw new Exception();
        }

        return amount;
    }


    /**
     * @param value
     * @param required
     * @param emptyError
     * @param invalidError
     * @param responseDTO
     * @return
     * @throws Exception
     */
    public static String validateUID(String value, boolean required, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO responseDTO) throws Exception {
        if (StringUtil.isNullOrZeroLength(value)) {
            if (required) {
                responseDTO.setErrorCode(emptyError.getValue());
                throw new Exception();
            }
            return null;
        }

        if (!ValidatorUtil.validateRegex(ValidatorUtil.UID_REGEX, value.trim())) {
            responseDTO.setErrorCode(invalidError.getValue());
            throw new Exception();
        }
        return value.trim();
    }

    public static <T extends Enum<T> & IValueEnum> T validateIValueEnum(String enumParam, Class<T> enumType, boolean required, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(enumParam)) {
            if (required) {
                response.setErrorCode(emptyError.getValue());
                throw new Exception();
            }
            return null;
        }

        enumParam = enumParam.trim();

        try {
            T iValueEnum = EnumUtil.safeValueOf(enumType, Integer.valueOf(enumParam));
            if (iValueEnum == null) {
                throw new Exception();
            }
            return iValueEnum;
        } catch (Throwable ex) {
            response.setErrorCode(invalidError.getValue());
            throw new Exception();
        }
    }

    public static <T extends Enum<T> & ITextEnum> T validateITextEnum(String enumParam, Class<T> enumType, boolean required, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(enumParam)) {
            if (required) {
                response.setErrorCode(emptyError.getValue());
                throw new Exception();
            }
            return null;
        }

        enumParam = enumParam.trim();

        try {
            T iTextEnum = EnumUtil.safeValueOf(enumType, enumParam);
            if (iTextEnum == null) {
                throw new Exception();
            }
            return iTextEnum;
        } catch (Throwable ex) {
            response.setErrorCode(invalidError.getValue());
            throw new Exception();
        }
    }

    public static Date validateDate(String value, boolean required, MessageCode emptyError, MessageCode errorCode, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(value)) {
            if (required) {
                response.setErrorCode(emptyError.getValue());
                throw new Exception();
            }
            return null;
        }

        value = value.trim();

        Date date = DateUtil.getDateFromMySQLFormat(value);
        if (date == null) {
            response.setErrorCode(errorCode.getValue());
            throw new Exception();
        }
        return date;
    }

    /**
     * Denetlenen Kosullar:<br>
     * <p>
     * Baslangic tarihi bosken Bitis tarihi dolu mu?
     * </p>
     * <p>
     * Bitis tarihi bosken Baslangic tarihi dolu mu?
     * </p>
     * <p>
     * <p>
     * <pre>
     * Baslangic ve Bitis tarihi dolu ise:
     * </pre>
     * <ul>
     * Baslangic tarihi Bitis tarihinden sonra mi?
     * </ul>
     * <ul>
     * Tarihler arasindaki gün farki 30 gunu geciyor mu?
     * </ul>
     *
     * @param startDate
     * @param endDate
     * @param response
     * @throws Exception
     */
    public static void validateStartDateEndDatePair(Date startDate, Date endDate, BaseResponseDTO response) throws Exception {
        if (startDate == null && endDate != null) {
//            response.setErrorCode(MessageCode.START_DATE_IS_EMPTY_WHILE_END_DATE_IS_FULL.getValue());
            throw new Exception();
        } else if (startDate != null && endDate == null) {
//            response.setErrorCode(MessageCode.START_DATE_IS_FULL_WHILE_END_DATE_IS_EMPTY.getValue());
            throw new Exception();
        } else if (startDate != null && endDate != null) {
            validateStartDateNotGreaterThanEndDate(startDate, endDate, response);
            validateOneMonthInterval(startDate, endDate, response);
        }
    }

    /**
     * Interim code product => Do null check before usage
     *
     * @param startDate
     * @param endDate
     * @param response
     * @throws Exception
     */
    public static void validateStartDateNotGreaterThanEndDate(Date startDate, Date endDate, BaseResponseDTO response) throws Exception {
        if (startDate != null && endDate != null && startDate.getTime() > endDate.getTime()) {
//            response.setErrorCode(MessageCode.START_DATE_CANNOT_BE_BEFORE_END_DATE.getValue());
            throw new Exception();
        }
    }

    /**
     * Checks whether if the input date is the same day as today. If so, current exact time is returned, else input date is echoed.
     *
     * @param date
     * @return
     */
    public static Date getExactTimeIfInTheSameDay(Date date) {
        Date now = new Date();
        if (DateUtil.isInSameDay(now, date)) {
            return now;
        } else {
            return date;
        }
    }

    /**
     * Interim code product => Do null check before usage
     *
     * @param startDate
     * @param endDate
     * @param response
     * @throws Exception
     */
    public static void validateDateIntervalDoesNotExceeds30Days(Date startDate, Date endDate, BaseResponseDTO response) throws Exception {
        if (DateUtil.dayDiff(endDate, startDate) > 31) {
//            response.setErrorCode(MessageCode.DATE_INTERVAL_EXCEEDS_30_DAY.getValue());
            throw new Exception();
        }
    }


    public static Integer validateDayOfTheWeek(String day, boolean required, MessageCode emptyError, MessageCode invalidError, MessageCode outOfRange, BaseResponseDTO response) throws Exception {
        return dayCounterValidatorFunc(day, required, emptyError, invalidError, outOfRange, response, 1, 7);
    }

    public static Integer validateDayInaYear(String day, boolean required, MessageCode emptyError, MessageCode invalidError, MessageCode outOfRange, BaseResponseDTO response) throws Exception {
        return dayCounterValidatorFunc(day, required, emptyError, invalidError, outOfRange, response, 1, 365);
    }

    private static Integer dayCounterValidatorFunc(String day, boolean required, MessageCode emptyError, MessageCode invalidError, MessageCode outOfRange, BaseResponseDTO response, int min, int max) throws Exception {
        if (required && day == null) {
            response.setErrorCode(invalidError.getValue());
            throw new Exception();
        }
        Integer value = null;
        if (day != null) {
            value = validateInteger(day, required, false, emptyError, invalidError, response);
            if ((max < value.intValue()) || (value.intValue() < min)) {
                response.setErrorCode(outOfRange.getValue());
                throw new Exception();
            }
        }
        return value;
    }

    public static void atLeastOneMustExists(MessageCode errorCode, BaseResponseDTO responseDTO, Object... values) throws Exception {
        if (values == null || (values.length == 0)) {
            responseDTO.setErrorCode(errorCode.getValue());
            throw new Exception();
        }
        boolean atLeastOneValueExists = false;
        for (Object value : values) {
            if (value != null) {
                atLeastOneValueExists = true;
                break;
            }
        }
        if (!atLeastOneValueExists) {
            responseDTO.setErrorCode(errorCode.getValue());
            throw new Exception();
        }
    }


    public static void validateObject(Object object, String message, MessageCode fieldEmpty, BaseResponseDTO responseDTO) throws Exception {
        if (object == null) {
            responseDTO.setErrorMessage(message);
            responseDTO.setErrorCode(fieldEmpty.getValue());
            throw new Exception();
        }
    }

    public static Integer validatePaymentType(String paymentTypeStr, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        try {
            return RequestValidator.validateIValueEnum(paymentTypeStr, PaymentType.class, false, emptyError, invalidError, response).getValue();
        }catch (Exception e){
            return  PaymentType.CARD.getValue();
        }
    }

    public static Integer validateInstallment(String installment, MessageCode emptyError, MessageCode invalidError, BaseResponseDTO response) throws Exception {
        if (StringUtil.isNullOrZeroLength(installment)) {
            return 1;
        }
        try {
            return Integer.valueOf(installment.trim());
        } catch (Exception e) {
            response.setErrorCode(invalidError.getValue());
            throw new Exception();
        }
    }

}