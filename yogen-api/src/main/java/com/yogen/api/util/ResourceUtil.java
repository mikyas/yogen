
package com.yogen.api.util;
import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringEscapeUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yogen.datafeed.data.service.CacheManagementService;
import com.yogen.integrator.model.dto.IntegratorInfoDTO;
import com.yogen.log.data.service.ApiLogService;
import com.yogen.util.DateUtil;
import com.yogen.util.StringUtil;
import com.yogen.util.enumtype.ApiLogModuleAndServiceName;
import com.yogen.util.enumtype.BaseResponseDTO;
import com.yogen.util.enumtype.MessageCode;
import com.yogen.util.exception.YogenBusinessException;


public class ResourceUtil {

    public static String getTokenFromHeader(HttpServletRequest httpServletRequest) {
        return StringEscapeUtils.unescapeHtml(httpServletRequest.getHeader("token"));
    }

    public static String getVersionFromHeader(HttpServletRequest httpServletRequest) {
        return StringEscapeUtils.unescapeHtml(httpServletRequest.getHeader("version"));
    }

    public static String getTransactionDateFromHeader(HttpServletRequest httpServletRequest) {
        return StringEscapeUtils.unescapeHtml(httpServletRequest.getHeader("transactionDate"));
    }

    public static String getLanguageFromHeader(HttpServletRequest httpServletRequest) {
        return StringEscapeUtils.unescapeHtml(httpServletRequest.getHeader("language"));
    }

    public static String getHashFromHeader(HttpServletRequest httpServletRequest) {
        String token = getTokenFromHeader(httpServletRequest);
        return token.split(":")[1];
    }

    public static String getPublicKeyFromRequest(HttpServletRequest httpServletRequest) {
        String token = getTokenFromHeader(httpServletRequest);
        return token.split(":")[0];
    }

    public static void controlRequestSecurity(HttpServletRequest httpServletRequest, BaseResponseDTO responseDTO, IntegratorInfoDTO integratorInfoDTO) throws YogenBusinessException { //, CacheManagementService cacheManagementService
        // header da gonderilmesi gereken bilgilerin validasyon kontrolleri
        String token = ResourceUtil.getTokenFromHeader(httpServletRequest);
        if (StringUtil.isNullOrZeroLength(token)) {
            responseDTO.setErrorCode(MessageCode.UNKNOWN_ERROR.getValue());//token hatasi
            throw new YogenBusinessException("error.payment.auth.request.security");
        } else if (!token.contains(":")) {
            responseDTO.setErrorCode(MessageCode.UNKNOWN_ERROR.getValue());
            throw new YogenBusinessException("error.payment.auth.request.security");
        }

        integratorInfoDTO.setToken(token.trim());

        String merchantPublicKey = ResourceUtil.getPublicKeyFromRequest(httpServletRequest);
        if (StringUtil.isNullOrZeroLength(merchantPublicKey)) {
            responseDTO.setErrorCode(MessageCode.UNKNOWN_ERROR.getValue());
            throw new YogenBusinessException("error.payment.auth.request.security");
        }

        integratorInfoDTO.setPublicKey(merchantPublicKey.trim());

        String hash = ResourceUtil.getHashFromHeader(httpServletRequest);
        if (StringUtil.isNullOrZeroLength(hash)) {
            responseDTO.setErrorCode(MessageCode.UNKNOWN_ERROR.getValue());
            throw new YogenBusinessException("error.payment.auth.request.security");
        }

        integratorInfoDTO.setHash(hash.trim());

        String version = ResourceUtil.getVersionFromHeader(httpServletRequest);
        if (StringUtil.isNullOrZeroLength(version)) {
            responseDTO.setErrorCode(MessageCode.UNKNOWN_ERROR.getValue());
            throw new YogenBusinessException("error.payment.auth.request.security");
        }

        if (!version.equals("1.0")) {
            responseDTO.setErrorCode(MessageCode.UNKNOWN_ERROR.getValue());
            throw new YogenBusinessException("error.payment.auth.request.security");
        }

        integratorInfoDTO.setVersion(version.trim());

        String timestamp = ResourceUtil.getTransactionDateFromHeader(httpServletRequest);
        if (StringUtil.isNullOrZeroLength(timestamp)) {
            responseDTO.setErrorCode(MessageCode.UNKNOWN_ERROR.getValue());
            throw new YogenBusinessException("error.payment.auth.request.security");
        }

        // timestamp header bilgisi kontrolu, +-10 dakika harici istekler reddedilir
        try {
            Date orderDate = DateUtil.getDateTimeFromMySQLFormat(timestamp);
            Calendar tenMinutesBeforeFromNow = Calendar.getInstance();
            tenMinutesBeforeFromNow.setTime(new Date());
            tenMinutesBeforeFromNow.add(Calendar.MINUTE, -10);
            Calendar tenMinutesAfterFromNow = Calendar.getInstance();
            tenMinutesAfterFromNow.setTime(new Date());
            tenMinutesAfterFromNow.add(Calendar.MINUTE, 10);
            if (orderDate.getTime() < tenMinutesBeforeFromNow.getTime().getTime() || orderDate.getTime() > tenMinutesAfterFromNow.getTime().getTime()) {
                responseDTO.setErrorCode(MessageCode.UNKNOWN_ERROR.getValue());
                throw new YogenBusinessException("error.payment.auth.request.security");
            }
        } catch (ParseException e) {
            responseDTO.setErrorCode(MessageCode.UNKNOWN_ERROR.getValue());
            throw new YogenBusinessException("error.payment.auth.request.security");
        }
        integratorInfoDTO.setTransactionDate(timestamp.trim());

        String language = ResourceUtil.getLanguageFromHeader(httpServletRequest);
        if (!StringUtil.isNullOrZeroLength(language)) {
//            merchantAPIInfoDTO.setLanguageId(cacheManagementService.controlGivenLanguageAndReturnLanguageId(language.trim()));
            integratorInfoDTO.setLanguageId(1);
            if (integratorInfoDTO.getLanguageId() == null) {
                responseDTO.setErrorCode(MessageCode.UNKNOWN_ERROR.getValue());
                throw new YogenBusinessException("error.payment.auth.request.security");
            }
        } else {
//            merchantAPIInfoDTO.setLanguageId(cacheManagementService.getDefaultLanguageId());
            integratorInfoDTO.setLanguageId(1);
        }
    }

    public static <T extends BaseResponseDTO> T populateErrorValuesAndReturnResponse(Integer errorCode, String displayMessage, Long merchantOrApiUserId,
                                                                                     BaseResponseDTO response,
                                                                                     ApiLogModuleAndServiceName apiLogModuleAndServiceName,
                                                                                     ApiLogService apiLogService, Integer languageId, CacheManagementService cacheManagementService) {
        return populateErrorValuesAndReturnResponse(errorCode, displayMessage, merchantOrApiUserId, response, apiLogModuleAndServiceName, null, apiLogService, languageId, cacheManagementService);
    }


    public static <T extends BaseResponseDTO> T populateErrorValuesForValidationAndReturnResponse(Integer errorCode, boolean forceSystemErrorMessage, Long merchantOrApiUserId,
                                                                                                  BaseResponseDTO response,
                                                                                                  ApiLogModuleAndServiceName apiLogModuleAndServiceName,
                                                                                                  String orderId, ApiLogService apiLogService, Integer languageId, CacheManagementService cacheManagementService) {
        if (errorCode != null) {
            response.setErrorCode(errorCode);
            String errorMessage = cacheManagementService.findMessageLookupByCode(errorCode, languageId);
            response.setErrorMessage(errorMessage);
            response.setResponseMessage(forceSystemErrorMessage ? cacheManagementService.getSystemError(languageId) : errorMessage);
        } else {
            String errorMessage = cacheManagementService.getSystemError(languageId);
            response.setErrorMessage(errorMessage);
            response.setResponseMessage(errorMessage);
        }


        apiLogService.logResponse(merchantOrApiUserId, apiLogModuleAndServiceName, response, orderId, response.getResult(), response.getErrorCode() != null ? String.valueOf(response.getErrorCode()) : null, response.getErrorMessage(), null, null);
        return (T) response;
    }

    public static <T extends BaseResponseDTO> T populateErrorValuesAndReturnResponse(Integer errorCode, String displayMessage, Long merchantOrApiUserId,
                                                                                     BaseResponseDTO response,
                                                                                     ApiLogModuleAndServiceName apiLogModuleAndServiceName,
                                                                                     String orderId, ApiLogService apiLogService, Integer languageId, CacheManagementService cacheManagementService) {
        if (errorCode != null) {
            response.setErrorCode(errorCode);
            response.setErrorMessage(cacheManagementService.findMessageLookupByCode(errorCode, languageId));
            response.setResponseMessage(!StringUtil.isNullOrZeroLength(displayMessage) ? displayMessage : response.getErrorMessage());
        }
        apiLogService.logResponse(merchantOrApiUserId, apiLogModuleAndServiceName, response, orderId, response.getResult(), response.getErrorCode() != null ? String.valueOf(response.getErrorCode()) : null, response.getErrorMessage(), null, null);
        return (T) response;
    }

    public static <T extends BaseResponseDTO> T logAndReturnResponse(Long merchantOrApiUserId, BaseResponseDTO response,
                                                                     ApiLogModuleAndServiceName apiLogModuleAndServiceName,
                                                                     ApiLogService apiLogService) {
        return logAndReturnResponse(merchantOrApiUserId, response, apiLogModuleAndServiceName, null, apiLogService);
    }

    public static <T extends BaseResponseDTO> T logAndReturnResponse(Long merchantOrApiUserId, BaseResponseDTO response,
                                                                     ApiLogModuleAndServiceName apiLogModuleAndServiceName,
                                                                     String orderId, ApiLogService apiLogService) {
        apiLogService.logResponse(merchantOrApiUserId, apiLogModuleAndServiceName, response, orderId, response.getResult(), response.getErrorCode() != null ? String.valueOf(response.getErrorCode()) : null, response.getErrorMessage(), null, null);
        return (T) response;
    }

    public static void setResponseErrorValues(Integer errorCode, BaseResponseDTO response, boolean forceSystemErrorDisplayMessage, Integer languageId, CacheManagementService cacheManagementService) {
        response.setErrorCode(errorCode);
        response.setErrorMessage(cacheManagementService.findMessageLookupByCode(errorCode, languageId));
        response.setResponseMessage(!forceSystemErrorDisplayMessage ? response.getErrorMessage() : cacheManagementService.getSystemError(languageId));
    }

    public static <T> T getInnerObject(String rawRequestBody, String objectName, Class<T> clazz) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        Map rawMap = objectMapper.readValue(rawRequestBody, Map.class);
        Map objectMap = (Map) rawMap.remove(objectName);
        String objectBody = objectMapper.writeValueAsString(objectMap);
        return objectMapper.readValue(objectBody, clazz);
    }

    public static String getJson(Object dto) {
        String json = null;
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            StringWriter sw = new StringWriter();
            objectMapper.writeValue(sw, dto);
            json = sw.toString();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return json;
    }
}