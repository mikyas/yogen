
package com.yogen.api.util;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.exception.ExceptionUtils;
import com.yogen.datafeed.data.service.CacheManagementService;
import com.yogen.datafeed.data.service.LogManagementService;
import com.yogen.integrator.data.service.EncryptionService;
import com.yogen.integrator.data.service.IntegratorService;
import com.yogen.integrator.model.Integrator;
import com.yogen.integrator.model.dto.IntegratorInfoDTO;
import com.yogen.log.data.service.LogService;
import com.yogen.util.EnumUtil;
import com.yogen.util.StringUtil;
import com.yogen.util.WebUtil;
import com.yogen.util.enumtype.ApiLogModuleAndServiceName;
import com.yogen.util.enumtype.BaseResponseDTO;
import com.yogen.util.enumtype.EncryptionKeyType;
import com.yogen.util.enumtype.GenericStatus;
import com.yogen.util.enumtype.LogModule;
import com.yogen.util.enumtype.LogPriority;
import com.yogen.util.enumtype.MessageCode;

public abstract class AbstractValidator {

    private Object dto;
    private HttpServletRequest httpServletRequest;
    private CacheManagementService cacheManagementService;
    private EncryptionService encryptionService;
    private LogManagementService logManagementService;
    private LogService logService;
    private ApiLogModuleAndServiceName apiLogModuleAndServiceName;
    private IntegratorService integratorService;

    protected AbstractValidator(BaseResponseDTO response, ApiLogModuleAndServiceName apiLogModuleAndServiceName, Object dto, HttpServletRequest httpServletRequest, CacheManagementService cacheManagementService, LogManagementService logManagementService, EncryptionService encryptionService, LogService logService, IntegratorService integratorService) throws Exception {
        if (dto == null) {
            response.setResult(GenericStatus.PASSIVE_NEGATIVE.getValue().byteValue());
            ResourceUtil.setResponseErrorValues(MessageCode.REQUEST_EMPTY.getValue(), response, true, getLanguageFromHeaderSafely(), cacheManagementService);

            throw new Exception("error.ws.security");
        }

        this.dto = dto;
        this.httpServletRequest = httpServletRequest;
        this.cacheManagementService = cacheManagementService;
        this.logManagementService = logManagementService;
        this.encryptionService = encryptionService;
        this.logService = logService;
        this.apiLogModuleAndServiceName = apiLogModuleAndServiceName;
        this.integratorService = integratorService;
    }

    private Integer getLanguageFromHeaderSafely() {
        String language = ResourceUtil.getLanguageFromHeader(httpServletRequest);
        if (!StringUtil.isNullOrZeroLength(language)) {
            return cacheManagementService.controlGivenLanguageAndReturnLanguageId(language.trim());
        }
        return null;
    }

    public abstract void validateRequestDTO(BaseResponseDTO response, IntegratorInfoDTO integratorInfoDTO) throws Exception;

    public IntegratorInfoDTO validateRequest(BaseResponseDTO response, String[] hashParamValues, String integratorIp) throws Exception {
        response.setResult(GenericStatus.PASSIVE_NEGATIVE.getValue().byteValue());
        IntegratorInfoDTO integratorInfoDTO = new IntegratorInfoDTO();
        integratorInfoDTO.setServerIp(WebUtil.getClientIp(httpServletRequest));
        integratorInfoDTO.setClientIp(integratorIp);

        try {
            ResourceUtil.controlRequestSecurity(httpServletRequest, response, integratorInfoDTO);
        } catch (Exception e) {
            ResourceUtil.setResponseErrorValues(EnumUtil.safeValueOf(MessageCode.class, response.getErrorCode()).getValue(), response, true, getLanguageFromHeaderSafely(), cacheManagementService);
            logManagementService.logRequest(null, apiLogModuleAndServiceName, ResourceUtil.getTokenFromHeader(httpServletRequest), dto, new Date(), ResourceUtil.getVersionFromHeader(httpServletRequest), integratorIp, WebUtil.getClientIp(httpServletRequest), ResourceUtil.getLanguageFromHeader(httpServletRequest));
            logManagementService.logResponse(null, apiLogModuleAndServiceName, response, response.getResult(), response.getResponseMessage(), response.getErrorCode(), response.getErrorMessage(), new Date());
            throw new Exception("error.ws.security");
        }

        Integrator integrator;

        try {
            // Magaza acik anahtar ile db den getirilir
            integrator = integratorService.findByPublicKeyForWebService(integratorInfoDTO.getPublicKey());
            integratorInfoDTO.setIntegrator(integrator);
            logManagementService.logRequest(integrator.getId(), apiLogModuleAndServiceName, integratorInfoDTO.getToken(), dto, new Date(), integratorInfoDTO.getVersion(), integratorIp, WebUtil.getClientIp(httpServletRequest), ResourceUtil.getLanguageFromHeader(httpServletRequest));
        } catch (Exception e) {
            ResourceUtil.setResponseErrorValues(MessageCode.API_USER_NOT_FOUND_WITH_PUBLIC_KEY.getValue(), response, true, getLanguageFromHeaderSafely(), cacheManagementService);
            logManagementService.logRequest(null, apiLogModuleAndServiceName, ResourceUtil.getTokenFromHeader(httpServletRequest), dto, new Date(), ResourceUtil.getVersionFromHeader(httpServletRequest), integratorIp, WebUtil.getClientIp(httpServletRequest), ResourceUtil.getLanguageFromHeader(httpServletRequest));
            logManagementService.logResponse(null, apiLogModuleAndServiceName, response, response.getResult(), response.getResponseMessage(), response.getErrorCode(), response.getErrorMessage(), new Date());
            throw new Exception("error.ws.security");
        }

        String language = ResourceUtil.getLanguageFromHeader(httpServletRequest);
        if (!StringUtil.isNullOrZeroLength(language)) {
            integratorInfoDTO.setLanguageId(cacheManagementService.controlGivenLanguageAndReturnLanguageId(language.trim()));
            if (integratorInfoDTO.getLanguageId() == null) {
                ResourceUtil.setResponseErrorValues(MessageCode.GIVEN_LANGUAGE_NOT_SUPPORTED.getValue(), response, true, getLanguageFromHeaderSafely(), cacheManagementService);
                logManagementService.logResponse(integrator.getId(), apiLogModuleAndServiceName, response, response.getResult(), response.getResponseMessage(), response.getErrorCode(), response.getErrorMessage(), new Date());
                throw new Exception("error.ws.security");
            }
        } else {
            integratorInfoDTO.setLanguageId(cacheManagementService.getDefaultLanguageId());
        }

        try {
            validateRequestDTO(response, integratorInfoDTO);
        } catch (Exception e) {
             logManagementService.logResponse(integrator.getId(), apiLogModuleAndServiceName, response, response.getResult(), response.getResponseMessage(), response.getErrorCode(), response.getErrorMessage(), new Date());
            throw new Exception("error.ws.security");
        }

        try {
            String token = StringUtil.getSHA1Text(encryptionService.decrypt(integrator.getPrivateKey(), EncryptionKeyType.MAIN) + prepareHashValues(hashParamValues) + ResourceUtil.getTransactionDateFromHeader(httpServletRequest));
            if (!token.equals(ResourceUtil.getHashFromHeader(httpServletRequest))) {
                ResourceUtil.setResponseErrorValues(MessageCode.TOKEN_NOT_MATCH.getValue(), response, true, getLanguageFromHeaderSafely(), cacheManagementService);
                throw new Exception("error.ws.security");
            }
        } catch (Exception e) {
            if (response.getErrorCode() == null) {
                logService.log(LogPriority.ERROR, LogModule.YOGEN, "YOGEN token kontrolünde beklenmedik hata oluştu!", null, ExceptionUtils.getFullStackTrace(e));
                ResourceUtil.setResponseErrorValues(MessageCode.SYSTEM_ERROR.getValue(), response, true, getLanguageFromHeaderSafely(), cacheManagementService);
            }
            logManagementService.logResponse(integrator.getId(), apiLogModuleAndServiceName, response, response.getResult(), response.getResponseMessage(), response.getErrorCode(), response.getErrorMessage(), new Date());
            throw new Exception("error.ws.security");
        }

        return integratorInfoDTO;
    }

    private static String prepareHashValues(String[] hashValues) {
        String hashParams = "";
        if (hashValues != null) {
            for (String s : hashValues) {
                if (!StringUtil.isNullOrZeroLength(s)) {
                    hashParams += s;
                } else {
                    hashParams += StringUtil.EMPTY_STRING;
                }
            }
        } else {
            return StringUtil.EMPTY_STRING;
        }

        return hashParams;
    }

}