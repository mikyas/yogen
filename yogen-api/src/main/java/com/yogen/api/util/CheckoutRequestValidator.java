package com.yogen.api.util;

import com.yogen.api.dto.request.*;
import com.yogen.integrator.data.service.EncryptionService;
import com.yogen.integrator.model.dto.IntegratorInfoDTO;
import com.yogen.util.enumtype.BaseResponseDTO;
import com.yogen.util.enumtype.MessageCode;
import com.yogen.util.validator.ValidatorUtil;
import org.springframework.stereotype.Component;

@Component
public class CheckoutRequestValidator {

    public static void validateCreationRequest(CheckoutRequestDTO requestDTO, BaseResponseDTO responseDTO, IntegratorInfoDTO integratorInfoDTO, EncryptionService encryptionService) throws Exception {

        RequestValidator.validateIntegratorIdAndIntegratorIp(requestDTO, responseDTO, integratorInfoDTO, encryptionService);
        requestDTO.getOrder().setOrderUID(RequestValidator.validateUID(requestDTO.getOrder().getOrderUID(), true, MessageCode.ORDER_ID_FIELD_EMPTY, MessageCode.ORDER_ID_FIELD_INVALID, responseDTO));

        requestDTO.getOrder().setName(RequestValidator.validateName(requestDTO.getOrder().getName(), MessageCode.NAME_FIELD_EMPTY, MessageCode.NAME_FIELD_EMPTY, responseDTO));
        requestDTO.getOrder().setCreatedAtTransient(RequestValidator.validateDate(requestDTO.getOrder().getCreatedAt(), false, MessageCode.CREATED_DATE_FIELD_EMPTY, MessageCode.CREATED_DATE_FIELD_INVALID, responseDTO));
        requestDTO.getOrder().setUpdatedAtTransient(RequestValidator.validateDate(requestDTO.getOrder().getUpdatedAt(), false, MessageCode.UPDATED_DATE_FIELD_EMPTY, MessageCode.UPDATED_DATE_FIELD_INVALID, responseDTO));
        requestDTO.getOrder().setClosedAtTransient(RequestValidator.validateDate(requestDTO.getOrder().getClosedAt(), false, MessageCode.UPDATED_DATE_FIELD_EMPTY, MessageCode.UPDATED_DATE_FIELD_INVALID, responseDTO));

        for (ItemRequestDTO itemRequestDTO : requestDTO.getOrder().getItems())
            validateItem(itemRequestDTO, responseDTO);
        validateShipment(requestDTO.getOrder().getShipment(), responseDTO);
        validateBilling(requestDTO.getOrder().getBilling(), responseDTO);
        validatePurchaser(requestDTO.getOrder().getPurchaser(), responseDTO);
        validatePaymentDetail(requestDTO.getOrder().getPaymentDetail(), responseDTO);
        validateDevice(requestDTO.getOrder().getDevice(),responseDTO);
        validateContact(requestDTO.getOrder().getContact(),responseDTO);

    }

    private static void validateContact(ContactRequestDTO contact, BaseResponseDTO responseDTO) throws Exception {
        RequestValidator.validateObject(contact, "Contact Field is Empty", MessageCode.CONTACT_OBJECT_EMPTY, responseDTO);
        contact.setContactUID(RequestValidator.validateUID(contact.getContactUID(), false, MessageCode.CONTACT_UID_FIELD_EMPTY, MessageCode.CONTACT_UID_FIELD_INVALID, responseDTO));
        contact.setType(RequestValidator.validateName(contact.getType(), false, MessageCode.CONTACT_NAME_FIELD_EMPTY, MessageCode.CONTACT_NAME_FIELD_INVALID, responseDTO));
        contact.setEmail(RequestValidator.validateEmail(contact.getEmail(), false, responseDTO));
        contact.setPhone(RequestValidator.validatePhoneNumber(contact.getPhone(), false, MessageCode.CONTACT_PHONE_FIELD_EMPTY, MessageCode.CONTACT_PHONE_FIELD_INVALID, responseDTO));
        contact.setAccountUrl(RequestValidator.validateUrl(contact.getAccountUrl(), false, MessageCode.CONTACT_URL_FIELD_EMPTY, MessageCode.CONTACT_URL_FIELD_INVALID, responseDTO));

    }

    private static void validateDevice(DeviceRequestDTO device, BaseResponseDTO responseDTO) throws Exception {
        RequestValidator.validateObject(device, "Device Field is Empty", MessageCode.DEVICE_OBJECT_EMPTY, responseDTO);
        device.setDeviceUID(RequestValidator.validateUID(device.getDeviceUID(), true, MessageCode.DEVICE_UID_FIELD_EMPTY, MessageCode.DEVICE_UID_FIELD_INVALID, responseDTO));
        device.setType(RequestValidator.validateName(device.getType(), false, MessageCode.DEVICE_TYPE_EMPTY, MessageCode.DEVICE_TYPE_INVALID, responseDTO));
        device.setManufacturer(RequestValidator.validateName(device.getManufacturer(), false, MessageCode.DEVICE_MANUFACTURER_EMPTY, MessageCode.DEVICE_MANUFACTURER_INVALID, responseDTO));
        device.setModel(RequestValidator.validateRegex(device.getModel(), false, ValidatorUtil.STRING_REGEX, MessageCode.DEVICE_MODEL_EMPTY, MessageCode.DEVICE_MODEL_INVALID, responseDTO));
        device.setOs(RequestValidator.validateName(device.getOs(), false, MessageCode.DEVICE_OS_EMPTY, MessageCode.DEVICE_OS_INVALID, responseDTO));
        device.setIpAddress(RequestValidator.validateRegex(device.getIpAddress(), false, ValidatorUtil.STRING_REGEX, MessageCode.DEVICE_IP_EMPTY, MessageCode.DEVICE_OS_INVALID, responseDTO));

    }

    private static void validatePaymentDetail(PaymentDetailRequestDTO paymentDetailRequestDTO, BaseResponseDTO responseDTO) throws Exception {
        RequestValidator.validateObject(paymentDetailRequestDTO, "Payment Field is Empty", MessageCode.PAYMENT_OBJECT_EMPTY, responseDTO);
        paymentDetailRequestDTO.setPaymentDetailUID(RequestValidator.validateUID(paymentDetailRequestDTO.getPaymentDetailUID(), true, MessageCode.PAYMENT_UID_FIELD_EMPTY, MessageCode.PAYMENT_UID_FIELD_INVALID, responseDTO));
        paymentDetailRequestDTO.setPaymentTypeTransient(RequestValidator.validatePaymentType(paymentDetailRequestDTO.getPaymentType(), MessageCode.PAYMENT_TYPE_EMPTY, MessageCode.PAYMENT_TYPE_INVALID, responseDTO));

        paymentDetailRequestDTO.setInstallmentTransient(RequestValidator.validateInstallment(paymentDetailRequestDTO.getInstallment(), MessageCode.INSTALLMENT_EMPTY, MessageCode.INSTALLMENT_INVALID, responseDTO));
        paymentDetailRequestDTO.setTotalPriceTransient(RequestValidator.validateDouble(paymentDetailRequestDTO.getTotalPrice(), false, MessageCode.TOTAL_PRICE_FIELD_EMPTY, MessageCode.TOTAL_PRICE_FIELD_INVALID, responseDTO));
        paymentDetailRequestDTO.setTotalPaidPriceTransient(RequestValidator.validateDouble(paymentDetailRequestDTO.getTotalPaidPrice(), false, MessageCode.TOTAL_PAID_PRICE_FIELD_EMPTY, MessageCode.TOTAL_PAID_PRICE_FIELD_INVALID, responseDTO));
        paymentDetailRequestDTO.setCurrency(RequestValidator.validateRegex(paymentDetailRequestDTO.getCurrency(), false, ValidatorUtil.CURRENCY_REGEX, MessageCode.FIELD_EMPTY, MessageCode.FIELD_INVALID, responseDTO));
        paymentDetailRequestDTO.setCreditCardBin(RequestValidator.validateRegex(paymentDetailRequestDTO.getCreditCardBin(), false, ValidatorUtil.BIN_NUMBER_REGEX, MessageCode.CREDIT_CARD_BIN_EMPTY, MessageCode.CREDIT_CARD_BIN_INVALID, responseDTO));
        paymentDetailRequestDTO.setCreditCardLastFourDigit(RequestValidator.validateRegex(paymentDetailRequestDTO.getCreditCardLastFourDigit(), false, ValidatorUtil.FOUR_DIGIT_REGEX, MessageCode.CREDIT_CARD_BIN_LAST_FOUR_DIGIT_EMPTY, MessageCode.CREDIT_CARD_LAST_FOUR_DIGIT_INVALID, responseDTO));
        paymentDetailRequestDTO.setCreditCardCompany(RequestValidator.validateName(paymentDetailRequestDTO.getCreditCardCompany(), false, MessageCode.CREDIT_CARD_COMPANY_EMPTY, MessageCode.CREDIT_CARD_COMPANY_INVALID, responseDTO));
        paymentDetailRequestDTO.setThreeDTransient(RequestValidator.validateBooleanIntegerValue(paymentDetailRequestDTO.getIsThreeD(), true, MessageCode.IS_THREED_EMPTY, MessageCode.IS_THREED_INVALID, responseDTO));
        paymentDetailRequestDTO.setThreeDInquiryResultTransient(RequestValidator.validateBooleanIntegerValue(paymentDetailRequestDTO.getThreeDInquiryResult(), true, MessageCode.IS_THREED_INQUIRY_EMPTY, MessageCode.IS_THREED_INQUIRY_INVALID, responseDTO));
        paymentDetailRequestDTO.setPaymentResultTransient(RequestValidator.validateBooleanIntegerValue(paymentDetailRequestDTO.getPaymentResult(), false, MessageCode.PAYMENT_RESULT_EMPTY, MessageCode.PAYMENT_RESULT_INVALID, responseDTO));
        paymentDetailRequestDTO.setEciCode(RequestValidator.validateRegex(paymentDetailRequestDTO.getEciCode(), false, ValidatorUtil.ECI_REGEX, MessageCode.ECI_CODE_EMPTY, MessageCode.ECI_CODE_INVALID, responseDTO));
        paymentDetailRequestDTO.setCavvCode(RequestValidator.validateRegex(paymentDetailRequestDTO.getCavvCode(), false, ValidatorUtil.CAVV_CODE_REGEX, MessageCode.CAVV_CODE_EMPTY, MessageCode.CAVV_CODE_INVALID, responseDTO));
        paymentDetailRequestDTO.setMdStatus(RequestValidator.validateRegex(paymentDetailRequestDTO.getMdStatus(), false, ValidatorUtil.MD_STATUS_REGEX, MessageCode.MD_STATUS_EMPTY, MessageCode.MD_STATUS_INVALID, responseDTO));
        paymentDetailRequestDTO.setAvsResultCode(RequestValidator.validateRegex(paymentDetailRequestDTO.getAvsResultCode(), false, ValidatorUtil.AVS_REGEX, MessageCode.AVS_RESULT_CODE_EMPTY, MessageCode.AVS_RESULT_CODE_INVALID, responseDTO));
        paymentDetailRequestDTO.setCvvResultCode(RequestValidator.validateRegex(paymentDetailRequestDTO.getCvvResultCode(), false, ValidatorUtil.CVV_REGEX, MessageCode.CVV_RESULT_CODE_EMPTY, MessageCode.CVV_RESULT_CODE_INVALID, responseDTO));
    }

    private static void validatePurchaser(PurchaserRequestDTO purchaser, BaseResponseDTO responseDTO) throws Exception {
        RequestValidator.validateObject(purchaser, "Purchaser Field is Empty", MessageCode.PURCHASER_OBJECT_EMPTY, responseDTO);
        purchaser.setPurchaserUID(RequestValidator.validateUID(purchaser.getPurchaserUID(), true, MessageCode.PURCHASER_UID_FIELD_EMPTY, MessageCode.PURCHASER_UID_FIELD_INVALID, responseDTO));
        purchaser.setFirstName(RequestValidator.validateName(purchaser.getFirstName(), true, MessageCode.PURCHASER_FIRSTNAME_FIELD_EMPTY, MessageCode.PURCHASER_FIRSTNAME_FIELD_INVALID, responseDTO));
        purchaser.setLastName(RequestValidator.validateName(purchaser.getLastName(), true, MessageCode.PURCHASER_LASTNAME_FIELD_EMPTY, MessageCode.PURCHASER_LASTNAME_FIELD_INVALID, responseDTO));
        purchaser.setMail(RequestValidator.validateEmail(purchaser.getMail(), true, responseDTO));
        purchaser.setVerifiedMail(RequestValidator.validateEmail(purchaser.getVerifiedMail(), false, responseDTO));
        purchaser.setMailVerifiedDateTransient(RequestValidator.validateDate(purchaser.getMailVerifiedDate(), false, MessageCode.EMAIL_VERIFIED_DATE_FIELD_EMPTY, MessageCode.EMAIL_VERIFIED_DATE_FIELD_INVALID, responseDTO));
        purchaser.setPhone(RequestValidator.validatePhoneNumber(purchaser.getPhone(), false, MessageCode.PHONE_FIELD_EMPTY, MessageCode.PHONE_FIELD_INVALID, responseDTO));
        purchaser.setVerifiedPhone(RequestValidator.validatePhoneNumber(purchaser.getVerifiedPhone(), false, MessageCode.PHONE_VERIFIED_FIELD_EMPTY, MessageCode.PHONE_VERIFIED_FIELD_INVALID, responseDTO));
        purchaser.setPhoneVerifiedDateTransient(RequestValidator.validateDate(purchaser.getPhoneVerifiedDate(), false, MessageCode.PHONE_VERIFIED_DATE_FIELD_EMPTY, MessageCode.PHONE_VERIFIED_DATE_FIELD_INVALID, responseDTO));
        purchaser.setFirstPurchaseAtTransient(RequestValidator.validateDate(purchaser.getFirstPurchaseAt(), false, MessageCode.FIRST_PURCHASED_DATE_FIELD_EMPTY, MessageCode.FIRST_PURCHASED_DATE_FIELD_INVALID, responseDTO));
        purchaser.setOrderCountTransient(RequestValidator.validateInteger(purchaser.getOrderCount(), false, true, MessageCode.ORDER_COUNT_FIELD_EMPTY, MessageCode.ORDER_COUNT_FIELD_INVALID, responseDTO));
        purchaser.setDateOfBirthTransient(RequestValidator.validateDate(purchaser.getDateOfBirth(), false, MessageCode.FIELD_EMPTY, MessageCode.FIELD_INVALID, responseDTO));
        purchaser.setUserName(RequestValidator.validateRegex(purchaser.getUserName(), false, ValidatorUtil.USERNAME_REGEX, MessageCode.USERNAME_FIELD_EMPTY, MessageCode.USERNAME_FIELD_INVALID, responseDTO));
        purchaser.setLanguage(RequestValidator.validateRegex(purchaser.getLanguage(), false, ValidatorUtil.LANGUAGE_REGEX, MessageCode.LANGUAGE_FIELD_EMPTY, MessageCode.LANGUAGE_FIELD_INVALID, responseDTO));
        purchaser.setGender(RequestValidator.validateRegex(purchaser.getGender(), false, ValidatorUtil.GENDER_REGEX, MessageCode.GENDER_FIELD_EMPTY, MessageCode.GENDER_FIELD_INVALID, responseDTO));
        validateSocialDetails(purchaser.getSocialDetail(),responseDTO);
    }
    // TODO HATA mesajlarini duzenle
    private static void validateSocialDetails(SocialDetailRequestDTO socialDetail, BaseResponseDTO responseDTO) throws Exception {
        RequestValidator.validateObject(socialDetail, "Social Details Field is Empty", MessageCode.SOCIAL_DETAILS_OBJECT_EMPTY, responseDTO);
        socialDetail.setSocialDetailUID(RequestValidator.validateUID(socialDetail.getSocialDetailUID(), false, MessageCode.FIELD_EMPTY, MessageCode.FIELD_INVALID, responseDTO));
        socialDetail.setNetwork(RequestValidator.validateName(socialDetail.getNetwork(), false, MessageCode.FIELD_EMPTY, MessageCode.FIELD_INVALID, responseDTO));
        socialDetail.setPublicUserName(RequestValidator.validateName(socialDetail.getPublicUserName(), false, MessageCode.FIELD_EMPTY, MessageCode.FIELD_INVALID, responseDTO));
        socialDetail.setCommunityScoreTransient(RequestValidator.validateInteger(socialDetail.getCommunityScore(),false,false, MessageCode.FIELD_EMPTY, MessageCode.FIELD_INVALID, responseDTO));
        socialDetail.setAccountUrl(RequestValidator.validateUrl(socialDetail.getAccountUrl(), false, MessageCode.FIELD_EMPTY, MessageCode.FIELD_INVALID, responseDTO));
        socialDetail.setMail(RequestValidator.validateEmail(socialDetail.getMail(), false, responseDTO));
        socialDetail.setBio(RequestValidator.validateName(socialDetail.getBio(), false, MessageCode.FIELD_EMPTY, MessageCode.FIELD_INVALID, responseDTO));
        socialDetail.setFollowingTransient(RequestValidator.validateInteger(socialDetail.getFollowing(), false, false, MessageCode.FIELD_EMPTY, MessageCode.FIELD_INVALID, responseDTO));
        socialDetail.setFollowedTransient(RequestValidator.validateInteger(socialDetail.getFollowed(), false, false, MessageCode.FIELD_EMPTY, MessageCode.FIELD_INVALID, responseDTO));
        socialDetail.setPostCountTransient(RequestValidator.validateInteger(socialDetail.getPostCount(), false, false, MessageCode.FIELD_EMPTY, MessageCode.FIELD_INVALID, responseDTO));
    }

    private static void validateItem(ItemRequestDTO itemRequestDTO, BaseResponseDTO responseDTO) throws Exception {
        RequestValidator.validateObject(itemRequestDTO, "Item Field is Empty", MessageCode.ITEM_OBJECT_EMPTY, responseDTO);
        itemRequestDTO.setItemUID(RequestValidator.validateUID(itemRequestDTO.getItemUID(), false, MessageCode.ITEM_ID_FIELD_EMPTY, MessageCode.ITEM_ID_FIELD_INVALID, responseDTO));
        itemRequestDTO.setTypeTransient(RequestValidator.validateInteger(itemRequestDTO.getType(), true, true, MessageCode.ITEM_TYPE_FIELD_EMPTY, MessageCode.ITEM_TYPE_FIELD_INVALID, responseDTO));
        itemRequestDTO.setUpc(RequestValidator.validateRegex(itemRequestDTO.getUpc(), false, ValidatorUtil.UPC_REGEX, MessageCode.UPC_FIELD_EMPTY, MessageCode.UPC_FIELD_INVALID, responseDTO));
        itemRequestDTO.setSku(RequestValidator.validateRegex(itemRequestDTO.getSku(), false, ValidatorUtil.SKU_REGEX, MessageCode.SKU_FIELD_EMPTY, MessageCode.SKU_FIELD_INVALID, responseDTO));
        itemRequestDTO.setPriceTransient(RequestValidator.validateDouble(itemRequestDTO.getPrice(), true, MessageCode.PRICE_FIELD_EMPTY, MessageCode.PRICE_FIELD_INVALID, responseDTO));
        itemRequestDTO.setPaidPriceTransient(RequestValidator.validateDouble(itemRequestDTO.getPaidPrice(), true, MessageCode.PAID_PRICE_FIELD_EMPTY, MessageCode.PAID_PRICE_FIELD_INVALID, responseDTO));
        itemRequestDTO.setQuantityTransient(RequestValidator.validateInteger(itemRequestDTO.getQuantity(), true, true, MessageCode.QUANTITY_FIELD_EMPTY, MessageCode.QUANTITY_FIELD_INVALID, responseDTO));
        itemRequestDTO.setBrand(RequestValidator.validateRegex(itemRequestDTO.getBrand(), false, ValidatorUtil.STRING_REGEX, MessageCode.BRAND_FIELD_EMPTY, MessageCode.BRAND_FIELD_INVALID, responseDTO));
        itemRequestDTO.setManufacturer(RequestValidator.validateRegex(itemRequestDTO.getManufacturer(), false, ValidatorUtil.STRING_REGEX, MessageCode.MANUFACTURER_FIELD_EMPTY, MessageCode.MANUFACTURER_FIELD_INVALID, responseDTO));
        itemRequestDTO.setTitle(RequestValidator.validateRegex(itemRequestDTO.getTitle(), false, ValidatorUtil.STRING_REGEX, MessageCode.TITLE_FIELD_EMPTY, MessageCode.TITLE_FIELD_INVALID, responseDTO));
        RequestValidator.validateObject(itemRequestDTO.getSeller(), "Seller Field is Empty", MessageCode.SELLER_OBJECT_EMPTY, responseDTO);
        itemRequestDTO.getSeller().setSellerUID(RequestValidator.validateUID(itemRequestDTO.getSeller().getSellerUID(), true, MessageCode.SELLER_UID_FIELD_EMPTY, MessageCode.SELLER_UID_FIELD_INVALID, responseDTO));
        itemRequestDTO.getSeller().setName(RequestValidator.validateName(itemRequestDTO.getSeller().getName(), MessageCode.SELLER_NAME_FIELD_EMPTY, MessageCode.SELLER_NAME_FIELD_INVALID, responseDTO));
        validateLocation(itemRequestDTO.getSeller().getLocation(), responseDTO);
    }

    private static void validateShipment(ShipmentRequestDTO shipmentRequestDTO, BaseResponseDTO responseDTO) throws Exception {
        RequestValidator.validateObject(shipmentRequestDTO, "Shipment Field is Empty", MessageCode.SHIPMENT_OBJECT_EMPTY, responseDTO);
        shipmentRequestDTO.setShipmentUID(RequestValidator.validateUID(shipmentRequestDTO.getShipmentUID(), true, MessageCode.SHIPMENT_UID_FIELD_EMPTY, MessageCode.SHIPMENT_UID_FIELD_INVALID, responseDTO));
        shipmentRequestDTO.setFirstName(RequestValidator.validateName(shipmentRequestDTO.getFirstName(), true, MessageCode.SHIPMENT_FIRSTNAME_FIELD_EMPTY, MessageCode.SHIPMENT_FIRSTNAME_FIELD_INVALID, responseDTO));
        shipmentRequestDTO.setLastName(RequestValidator.validateName(shipmentRequestDTO.getLastName(), true, MessageCode.SHIPMENT_LASTNAME_FIELD_EMPTY, MessageCode.SHIPMENT_LASTNAME_FIELD_INVALID, responseDTO));
        shipmentRequestDTO.setCitizenNum(RequestValidator.validateRegex(shipmentRequestDTO.getCitizenNum(), true, ValidatorUtil.CITIZENNUM_REGEX, MessageCode.SHIPMENT_CITIZENNUM_FIELD_EMPTY, MessageCode.SHIPMENT_CITIZENNUM_FIELD_INVALID, responseDTO));
        shipmentRequestDTO.setDeliveryCompany(RequestValidator.validateName(shipmentRequestDTO.getDeliveryCompany(), true, MessageCode.SHIPMENT_COMPANY_FIELD_EMPTY, MessageCode.SHIPMENT_COMPANY_FIELD_INVALID, responseDTO));
        validateLocation(shipmentRequestDTO.getLocation(), responseDTO);

    }

    private static void validateBilling(BillingRequestDTO billingRequestDTO, BaseResponseDTO responseDTO) throws Exception {
        RequestValidator.validateObject(billingRequestDTO, "Billing Field is Empty", MessageCode.BILLING_OBJECT_EMPTY, responseDTO);
        billingRequestDTO.setBillingUID(RequestValidator.validateUID(billingRequestDTO.getBillingUID(), true, MessageCode.BILLING_UID_FIELD_EMPTY, MessageCode.BILLING_UID_FIELD_INVALID, responseDTO));
        billingRequestDTO.setFirstName(RequestValidator.validateName(billingRequestDTO.getFirstName(), true, MessageCode.BILLING_FIRSTNAME_FIELD_EMPTY, MessageCode.BILLING_FIRSTNAME_FIELD_INVALID, responseDTO));
        billingRequestDTO.setLastName(RequestValidator.validateName(billingRequestDTO.getLastName(), true, MessageCode.BILLING_LASTNAME_FIELD_EMPTY, MessageCode.BILLING_LASTNAME_FIELD_INVALID, responseDTO));
        billingRequestDTO.setCitizenNum(RequestValidator.validateRegex(billingRequestDTO.getCitizenNum(), true, ValidatorUtil.CITIZENNUM_REGEX, MessageCode.BILLING_CITIZENNUM_FIELD_EMPTY, MessageCode.BILLING_CITIZENNUM_FIELD_INVALID, responseDTO));
        billingRequestDTO.setTaxNum(RequestValidator.validateTaxNumber(billingRequestDTO.getTaxNum(), true, false, responseDTO));
        billingRequestDTO.setSerialNum(RequestValidator.validateRegex(billingRequestDTO.getSerialNum(), true, ValidatorUtil.BILLING_SERIALNUM_REGEX, MessageCode.BILLING_SERIALNUM_FIELD_EMPTY, MessageCode.BILLING_SERIALNUM_FIELD_INVALID, responseDTO));
        validateLocation(billingRequestDTO.getLocation(), responseDTO);


    }

    private static void validateLocation(LocationRequestDTO locationRequestDTO, BaseResponseDTO responseDTO) throws Exception {
        RequestValidator.validateObject(locationRequestDTO, "Location Field is Empty", MessageCode.LOCATION_OBJECT_EMPTY, responseDTO);
        locationRequestDTO.setAddress1(RequestValidator.validateRegex(locationRequestDTO.getAddress1(), true, ValidatorUtil.ADDRESS_REGEX, MessageCode.ADDRESS1_EMPTY, MessageCode.ADDRESS1_INVALID, responseDTO));
        locationRequestDTO.setAddress2(RequestValidator.validateRegex(locationRequestDTO.getAddress2(), false, ValidatorUtil.ADDRESS_REGEX, MessageCode.ADDRESS2_EMPTY, MessageCode.ADDRESS2_INVALID, responseDTO));
        locationRequestDTO.setNeighbourhood(RequestValidator.validateRegex(locationRequestDTO.getNeighbourhood(), false, ValidatorUtil.ADDRESS_REGEX, MessageCode.NEIGHBORHOOD_EMPTY, MessageCode.NEIGHBORHOOD_INVALID, responseDTO));
        locationRequestDTO.setCity(RequestValidator.validateName(locationRequestDTO.getCity(), MessageCode.CITY_EMPTY, MessageCode.CITY_INVALID, responseDTO));
        locationRequestDTO.setRegion(RequestValidator.validateName(locationRequestDTO.getRegion(), MessageCode.REGION_EMPTY, MessageCode.REGION_INVALID, responseDTO));
        locationRequestDTO.setCountry(RequestValidator.validateName(locationRequestDTO.getCountry(), MessageCode.COUNTRY_EMPTY, MessageCode.COUNTRY_INVALID, responseDTO));
        locationRequestDTO.setPoBoxNumber(RequestValidator.validateRegex(locationRequestDTO.getPoBoxNumber(), false, ValidatorUtil.POBOX_REGEX, MessageCode.POBOXNUM_EMPTY, MessageCode.POBOXNUM_INVALID, responseDTO));
        locationRequestDTO.setPostalCode(RequestValidator.validateRegex(locationRequestDTO.getPostalCode(), true, ValidatorUtil.ZIP_CODE_REGEX, MessageCode.POSTALCODE_EMPTY, MessageCode.POSTALCODE_INVALID, responseDTO));
        locationRequestDTO.setLatitude(RequestValidator.validateRegex(locationRequestDTO.getLatitude(), false, ValidatorUtil.ADDRESS_REGEX, MessageCode.LATITUDE_EMPTY, MessageCode.LATITUDE_INVALID, responseDTO));
        locationRequestDTO.setLongitude(RequestValidator.validateRegex(locationRequestDTO.getLongitude(), false, ValidatorUtil.ADDRESS_REGEX, MessageCode.LONGITUDE_EMPTY, MessageCode.LONGITUDE_INVALID, responseDTO));
        locationRequestDTO.setGeohash(RequestValidator.validateRegex(locationRequestDTO.getGeohash(), false, ValidatorUtil.ADDRESS_REGEX, MessageCode.GEOHASH_EMPTY, MessageCode.GEOHASH_INVALID, responseDTO));
    }
}


