package com.yogen.api.resource;

import com.yogen.api.dto.request.*;
import com.yogen.api.dto.response.CheckoutResponseDTO;
import com.yogen.api.util.AbstractValidator;
import com.yogen.api.util.CheckoutRequestValidator;
import com.yogen.api.util.ResourceUtil;
import com.yogen.datafeed.data.service.CacheManagementService;
import com.yogen.datafeed.data.service.LogManagementService;
import com.yogen.datafeed.data.service.TransactionBusinessService;
import com.yogen.datafeed.model.dto.snapshot.*;
import com.yogen.factory.data.service.FraudDecisionService;
import com.yogen.factory.model.dto.AddressInfoDTO;
import com.yogen.factory.model.dto.DecisionDetailDTO;
import com.yogen.factory.model.dto.ItemInfoDTO;
import com.yogen.factory.model.dto.TransactionInfoDTO;
import com.yogen.integrator.data.service.EncryptionService;
import com.yogen.integrator.data.service.IntegratorService;
import com.yogen.integrator.model.dto.IntegratorInfoDTO;
import com.yogen.log.data.service.ApiLogService;
import com.yogen.log.data.service.LogService;
import com.yogen.util.EnumUtil;
import com.yogen.util.StringUtil;
import com.yogen.util.enumtype.*;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@RestController
@RequestMapping(value = "/api", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class YogenResource {
    @Autowired
    private CacheManagementService cacheManagementService;
    @Autowired
    private LogManagementService logManagementService;
    @Autowired
    private EncryptionService encryptionService;
    @Autowired
    private LogService logService;
    @Autowired
    private ApiLogService apiLogService;
    @Autowired
    private TransactionBusinessService checkoutBusinessService;
    @Autowired
    private IntegratorService integratorService;
    @Autowired
    private FraudDecisionService fraudDecisionService;

    @RequestMapping(value = "/checkout")
    public CheckoutResponseDTO checkout(@RequestBody final CheckoutRequestDTO requestDTO, HttpServletRequest httpServletRequest) {

        CheckoutResponseDTO responseDTO = new CheckoutResponseDTO();
        responseDTO.setResult(GenericStatus.PASSIVE_NEGATIVE.getValue().byteValue());
        ApiLogModuleAndServiceName apiLogModuleAndServiceName = ApiLogModuleAndServiceName.CHECKOUT;
        IntegratorInfoDTO integratorInfoDTO;
        try {
            AbstractValidator abstractValidator = new AbstractValidator(responseDTO, apiLogModuleAndServiceName, requestDTO, httpServletRequest, cacheManagementService, logManagementService, encryptionService, logService, integratorService) {

                @Override
                public void validateRequestDTO(BaseResponseDTO response, IntegratorInfoDTO integratorInfoDTO) throws Exception {
                    CheckoutRequestValidator.validateCreationRequest(requestDTO, responseDTO, integratorInfoDTO, encryptionService);

                }
            };
            String[] hashParamValues = new String[]{
                    requestDTO.getOrder().getOrderUID(),
                    requestDTO.getIntegratorId(),
                    requestDTO.getIntegratorIp()
            };
            integratorInfoDTO = abstractValidator.validateRequest(responseDTO, hashParamValues, requestDTO.getIntegratorIp());
        } catch (Exception e) {
            return responseDTO;
        }

        Integer languageId = integratorInfoDTO.getLanguageId();

        try {
            TransactionDTO transactionDTO = initializeTransactionDTOByRequestDTO(requestDTO);
            transactionDTO.setIntegratorRiskScoreLimit(integratorInfoDTO.getIntegrator().getRiskScoreLimit());
            transactionDTO.setTransactionUID(UUID.randomUUID().toString());
            long transactionVersionId = checkoutBusinessService.checkout(transactionDTO);
            DecisionDetailDTO fraudDecisionDetailDTO = fraudDecisionService.getRuleExecutorResult(initializeTransactionInfoDTO(transactionDTO, transactionVersionId));
            responseDTO.setOrderUID(requestDTO.getOrder().getOrderUID());
            responseDTO.setTransactionUID(transactionDTO.getTransactionUID());
            responseDTO.setResult(GenericStatus.ACTIVE_POSITIVE.getValue().byteValue());
            responseDTO.setResponseMessage(cacheManagementService.findMessageLookupByCode(MessageCode.CHECKOUT_CREATED_SUCCESFULLY, languageId));
            responseDTO.setFraudDecisionDate(fraudDecisionDetailDTO.getDecisionDate());
            responseDTO.setFraudDecisionResult(fraudDecisionDetailDTO.getResult() ? 1 : 0);
            responseDTO.setFraudDecisionNotes(fraudDecisionDetailDTO.getNotes());
            return ResourceUtil.logAndReturnResponse(requestDTO.getIntegratorIdTransient(), responseDTO, apiLogModuleAndServiceName, requestDTO.getOrder().getOrderUID(), apiLogService);
        } catch (Exception e) {
            logService.log(LogPriority.FATAL, LogModule.YOGEN, "Recurring payment createRecurringPaymentOrder request error.", "Error: SYSTEM_ERROR. Integrator Id:" + requestDTO.getIntegratorId(), ExceptionUtils.getFullStackTrace(e));
            return ResourceUtil.populateErrorValuesAndReturnResponse(MessageCode.SYSTEM_ERROR.getValue(), cacheManagementService.getSystemError(languageId), requestDTO.getIntegratorIdTransient(), responseDTO, apiLogModuleAndServiceName, requestDTO.getOrder().getOrderUID(), apiLogService, languageId, cacheManagementService);
        }
    }

    private TransactionInfoDTO initializeTransactionInfoDTO(TransactionDTO transactionDTO, long transactionVersionId) {
        TransactionInfoDTO transactionInfoDTO = new TransactionInfoDTO();
        transactionInfoDTO.setRiskScoreLimit(transactionDTO.getIntegratorRiskScoreLimit());
        transactionInfoDTO.setTransactionDate(transactionDTO.getTransactionDate());
        transactionInfoDTO.setTransactionUID(transactionDTO.getTransactionUID());
        transactionInfoDTO.setTransactionVersionId(transactionVersionId);
        transactionInfoDTO.setItems(initializeItemInfoDTOs(transactionDTO.getOrderDTO().getItems()));
        transactionInfoDTO.setShipmentDeliveryCompany(transactionDTO.getOrderDTO().getShipmentDTO().getDeliveryCompany());
        transactionInfoDTO.setShipmentAddress(initializeAddressInfoDTO(transactionDTO.getOrderDTO().getShipmentDTO().getLocation()));
        transactionInfoDTO.setBillingSerialNo(transactionDTO.getOrderDTO().getBillingDTO().getSerialNum());
        transactionInfoDTO.setBillingAddress(initializeAddressInfoDTO(transactionDTO.getOrderDTO().getBillingDTO().getLocation()));
        transactionInfoDTO.setPurchaserFirstName(transactionDTO.getOrderDTO().getPurchaserDTO().getFirstName());
        transactionInfoDTO.setPurchaserLastName(transactionDTO.getOrderDTO().getPurchaserDTO().getLastName());
        transactionInfoDTO.setPurchaserPhone(transactionDTO.getOrderDTO().getPurchaserDTO().getPhone());
        transactionInfoDTO.setPurchaserMail(transactionDTO.getOrderDTO().getPurchaserDTO().getMail());
        transactionInfoDTO.setPaymentInstallment(transactionDTO.getOrderDTO().getPaymentDetailDTO().getInstallment());
        transactionInfoDTO.setTotalPrice(transactionDTO.getOrderDTO().getPaymentDetailDTO().getTotalPrice());
        transactionInfoDTO.setTotalPaidPrice(transactionDTO.getOrderDTO().getPaymentDetailDTO().getTotalPaidPrice());
        transactionInfoDTO.setPaymentCurrency(transactionDTO.getOrderDTO().getPaymentDetailDTO().getCurrency());
        transactionInfoDTO.setIntegeratorId(transactionDTO.getIntegratorId());
        if (StringUtil.isNullOrZeroLength(transactionDTO.getOrderDTO().getPaymentDetailDTO().getCreditCardNo())) {
            transactionInfoDTO.setCreditCardNo(transactionDTO.getOrderDTO().getPaymentDetailDTO().getCreditCardBin() + "-" + transactionDTO.getOrderDTO().getPaymentDetailDTO().getCreditCardLastFourDigit());
        } else {
            transactionInfoDTO.setCreditCardNo(transactionDTO.getOrderDTO().getPaymentDetailDTO().getCreditCardNo());
        }
        transactionInfoDTO.setDeviceIpAddress(transactionDTO.getOrderDTO().getDeviceDTO().getIpAddress());
        return transactionInfoDTO;
    }

    private List<ItemInfoDTO> initializeItemInfoDTOs(List<ItemDTO> items) {
        List<ItemInfoDTO> itemInfoDTOs = new ArrayList<>();
        for (ItemDTO item : items) {
            ItemInfoDTO itemInfoDTO = new ItemInfoDTO();
            itemInfoDTO.setItemPrice(item.getPrice());
            itemInfoDTO.setItemQuantity(item.getQuantity());
            itemInfoDTO.setItemType(EnumUtil.safeValueOf(ItemType.class, item.getType()));
            itemInfoDTO.setSellerName(item.getSellerDTO().getName());
            itemInfoDTO.setSellerAddress(initializeAddressInfoDTO(item.getSellerDTO().getSellerLocation()));
            itemInfoDTOs.add(itemInfoDTO);
        }
        return itemInfoDTOs;
    }

    private AddressInfoDTO initializeAddressInfoDTO(LocationDTO sellerLocation) {
        AddressInfoDTO addressInfoDTO = new AddressInfoDTO();
        addressInfoDTO.setAddress1(sellerLocation.getAddress1());
        addressInfoDTO.setAddress2(sellerLocation.getAddress2());
        addressInfoDTO.setNeighbourhood(sellerLocation.getNeighbourhood());
        addressInfoDTO.setCity(sellerLocation.getCity());
        addressInfoDTO.setCountry(sellerLocation.getCountry());
        addressInfoDTO.setPoBoxNumber(sellerLocation.getPoBoxNumber());
        addressInfoDTO.setPostalCode(sellerLocation.getPostalCode());
        addressInfoDTO.setRegion(sellerLocation.getRegion());
        return null;
    }

    private TransactionDTO initializeTransactionDTOByRequestDTO(CheckoutRequestDTO requestDTO) {
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setTransactionUID(UUID.randomUUID().toString());
        transactionDTO.setIntegratorId(requestDTO.getIntegratorIdTransient());
        OrderDTO orderDTO = initializeOrderDTO(requestDTO.getOrder());
        transactionDTO.setOrderDTO(orderDTO);
        return transactionDTO;
    }

    private OrderDTO initializeOrderDTO(OrderRequestDTO orderRequestDTO) {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setOrderUID(orderRequestDTO.getOrderUID());
        orderDTO.setName(orderRequestDTO.getName());
        orderDTO.setCreatedAt(orderRequestDTO.getCreatedAtTransient());
        orderDTO.setClosedAt(orderRequestDTO.getClosedAtTransient());
        orderDTO.setUpdatedAt(orderRequestDTO.getUpdatedAtTransient());

        //set orderDTO's features
        orderDTO.setItems(initializeItemDTOs(orderRequestDTO.getItems()));
        orderDTO.setShipmentDTO(initializeShipmentDTO(orderRequestDTO.getShipment()));
        orderDTO.setBillingDTO(initializeBillingDTO(orderRequestDTO.getBilling()));
        orderDTO.setPurchaserDTO(initializePurchaserDTO(orderRequestDTO.getPurchaser()));
        orderDTO.setPaymentDetailDTO(initializePaymentDetailDTO(orderRequestDTO.getPaymentDetail()));
        orderDTO.setDeviceDTO(initializeDeviceDTO(orderRequestDTO.getDevice()));
        orderDTO.setContactDTO(initializeContactDTO(orderRequestDTO.getContact()));

        return orderDTO;
    }

    private List<ItemDTO> initializeItemDTOs(List<ItemRequestDTO> requestItems) {
        List<ItemDTO> items = requestItems.stream()
                .map(requestItem -> new ItemDTO(
                                requestItem.getItemUID(), requestItem.getTypeTransient(), requestItem.getUpc(), requestItem.getSku(),
                                requestItem.getPriceTransient(), requestItem.getPaidPriceTransient(), requestItem.getQuantityTransient(),
                                requestItem.getBrand(), requestItem.getManufacturer(), requestItem.getTitle(), initializeSellerDTO(requestItem.getSeller())
                        )
                ).collect(Collectors.toList());
        return items;
    }

    private SellerDTO initializeSellerDTO(SellerRequestDTO sellerRequestDTO) {
        SellerDTO sellerDTO = new SellerDTO();
        sellerDTO.setSellerUID(sellerRequestDTO.getSellerUID());
        sellerDTO.setName(sellerRequestDTO.getName());
        sellerDTO.setSellerLocation(initializeLocation(sellerRequestDTO.getLocation()));
        return sellerDTO;
    }

    private ShipmentDTO initializeShipmentDTO(ShipmentRequestDTO requestShipmentDTO) {
        ShipmentDTO shipmentDTO = new ShipmentDTO();
        shipmentDTO.setShipmentUID(requestShipmentDTO.getShipmentUID());
        shipmentDTO.setFirstName(requestShipmentDTO.getFirstName());
        shipmentDTO.setLastName(requestShipmentDTO.getLastName());
        shipmentDTO.setCitizenNum(requestShipmentDTO.getCitizenNum());
        shipmentDTO.setTaxNum(requestShipmentDTO.getTaxNum());
        shipmentDTO.setDeliveryCompany(requestShipmentDTO.getDeliveryCompany());
        shipmentDTO.setLocation(initializeLocation(requestShipmentDTO.getLocation()));

        return shipmentDTO;
    }

    private BillingDTO initializeBillingDTO(BillingRequestDTO requestBillingDTO) {
        BillingDTO billingDTO = new BillingDTO();
        billingDTO.setBillingUID(requestBillingDTO.getBillingUID());
        billingDTO.setFirstName(requestBillingDTO.getFirstName());
        billingDTO.setLastName(requestBillingDTO.getLastName());
        billingDTO.setCitizenNum(requestBillingDTO.getCitizenNum());
        billingDTO.setTaxNum(requestBillingDTO.getTaxNum());
        billingDTO.setSerialNum(requestBillingDTO.getSerialNum());
        billingDTO.setLocation(initializeLocation(requestBillingDTO.getLocation()));
        return billingDTO;
    }

    private PurchaserDTO initializePurchaserDTO(PurchaserRequestDTO requestPurchaserDTO) {
        PurchaserDTO purchaserDTO = new PurchaserDTO();
        purchaserDTO.setPurchaserUID(requestPurchaserDTO.getPurchaserUID());
        purchaserDTO.setFirstName(requestPurchaserDTO.getFirstName());
        purchaserDTO.setLastName(requestPurchaserDTO.getLastName());
        purchaserDTO.setMail(requestPurchaserDTO.getMail());
        purchaserDTO.setVerifiedMail(requestPurchaserDTO.getVerifiedMail());
        purchaserDTO.setMailVerifiedDate(requestPurchaserDTO.getMailVerifiedDateTransient());
        purchaserDTO.setPhone(requestPurchaserDTO.getPhone());
        purchaserDTO.setVerifiedPhone(requestPurchaserDTO.getVerifiedPhone());
        purchaserDTO.setPhoneVerifiedDate(requestPurchaserDTO.getPhoneVerifiedDateTransient());
        purchaserDTO.setFirstPurchaseAt(requestPurchaserDTO.getFirstPurchaseAtTransient());
        purchaserDTO.setOrderCount(requestPurchaserDTO.getOrderCountTransient());
        purchaserDTO.setAccountType(requestPurchaserDTO.getAccountType());
        purchaserDTO.setDateOfBirth(requestPurchaserDTO.getDateOfBirthTransient());
        purchaserDTO.setUserName(requestPurchaserDTO.getUserName());
        purchaserDTO.setLanguage(requestPurchaserDTO.getLanguage());
        purchaserDTO.setGender(requestPurchaserDTO.getGender());
        purchaserDTO.setSocialDetailDTO(initializeSocialDetailDTO(requestPurchaserDTO.getSocialDetail()));
        return purchaserDTO;
    }

    private SocialDetailDTO initializeSocialDetailDTO(SocialDetailRequestDTO socialDetailRequestDTO) {
        SocialDetailDTO socialDetailDTO = new SocialDetailDTO();
        socialDetailDTO.setSocialDetailUID(socialDetailRequestDTO.getSocialDetailUID());
        socialDetailDTO.setNetwork(socialDetailRequestDTO.getNetwork());
        socialDetailDTO.setPublicUserName(socialDetailRequestDTO.getPublicUserName());
        socialDetailDTO.setCommunityScore(socialDetailRequestDTO.getCommunityScoreTransient());
        socialDetailDTO.setAccountUrl(socialDetailRequestDTO.getAccountUrl());
        socialDetailDTO.setMail(socialDetailRequestDTO.getMail());
        socialDetailDTO.setBio(socialDetailRequestDTO.getBio());
        socialDetailDTO.setFollowing(socialDetailRequestDTO.getFollowingTransient());
        socialDetailDTO.setFollowed(socialDetailRequestDTO.getFollowedTransient());
        socialDetailDTO.setPostCount(socialDetailRequestDTO.getPostCountTransient());
        return socialDetailDTO;
    }

    private PaymentDetailDTO initializePaymentDetailDTO(PaymentDetailRequestDTO paymentDetailRequestDTO) {
        PaymentDetailDTO paymentDetailDTO = new PaymentDetailDTO();
        paymentDetailDTO.setPaymentDetailUID(paymentDetailRequestDTO.getPaymentDetailUID());
        paymentDetailDTO.setPaymentType(paymentDetailRequestDTO.getPaymentTypeTransient());
        paymentDetailDTO.setInstallment(paymentDetailRequestDTO.getInstallmentTransient());
        paymentDetailDTO.setTotalPrice(paymentDetailRequestDTO.getTotalPriceTransient());
        paymentDetailDTO.setTotalPaidPrice(paymentDetailRequestDTO.getTotalPaidPriceTransient());
        paymentDetailDTO.setCurrency(paymentDetailRequestDTO.getCurrency());
        paymentDetailDTO.setCreditCardBin(paymentDetailRequestDTO.getCreditCardBin());
        paymentDetailDTO.setCreditCardLastFourDigit(paymentDetailRequestDTO.getCreditCardLastFourDigit());
        paymentDetailDTO.setCreditCardCompany(paymentDetailRequestDTO.getCreditCardCompany());
        paymentDetailDTO.setThreeD(paymentDetailRequestDTO.getThreeDTransient());
        paymentDetailDTO.setThreeDInquiryResult(paymentDetailRequestDTO.getThreeDInquiryResultTransient());
        paymentDetailDTO.setPaymentResult(true);
        paymentDetailDTO.setEciCode(paymentDetailRequestDTO.getEciCode());
        paymentDetailDTO.setCavvCode(paymentDetailRequestDTO.getCavvCode());
        paymentDetailDTO.setMdStatus(paymentDetailRequestDTO.getMdStatus());
        paymentDetailDTO.setAvsResultCode(paymentDetailRequestDTO.getAvsResultCode());
        paymentDetailDTO.setCvvResultCode(paymentDetailRequestDTO.getCvvResultCode());
        return paymentDetailDTO;
    }


    private DeviceDTO initializeDeviceDTO(DeviceRequestDTO requestDeviceDTO) {
        DeviceDTO deviceDTO = new DeviceDTO();
        deviceDTO.setDeviceUID(requestDeviceDTO.getDeviceUID());
        deviceDTO.setType(requestDeviceDTO.getType());
        deviceDTO.setManufacturer(requestDeviceDTO.getManufacturer());
        deviceDTO.setModel(requestDeviceDTO.getModel());
        deviceDTO.setOs(requestDeviceDTO.getOs());
        deviceDTO.setIpAddress(requestDeviceDTO.getIpAddress());
        deviceDTO.setBrowser(requestDeviceDTO.getDeviceUID());
        return deviceDTO;
    }

    private ContactDTO initializeContactDTO(ContactRequestDTO requestContactDTO) {
        ContactDTO contactDTO = new ContactDTO();
        contactDTO.setContactUID(requestContactDTO.getContactUID());
        contactDTO.setContactType(requestContactDTO.getType());
        contactDTO.setEmail(requestContactDTO.getEmail());
        contactDTO.setPhone(requestContactDTO.getPhone());
        contactDTO.setAccountUrl(requestContactDTO.getAccountUrl());
        return contactDTO;
    }


    private LocationDTO initializeLocation(LocationRequestDTO locationRequestDTO) {
        LocationDTO location = new LocationDTO();
        location.setAddress1(locationRequestDTO.getAddress1());
        location.setAddress2(locationRequestDTO.getAddress2());
        location.setNeighbourhood(locationRequestDTO.getNeighbourhood());
        location.setCity(locationRequestDTO.getCity());
        location.setRegion(locationRequestDTO.getRegion());
        location.setCountry(locationRequestDTO.getCountry());
        location.setPoBoxNumber(locationRequestDTO.getPoBoxNumber());
        location.setPostalCode(locationRequestDTO.getPostalCode());
        location.setLatitude(locationRequestDTO.getLatitude());
        location.setLongitude(locationRequestDTO.getLongitude());
        location.setGeohash(locationRequestDTO.getGeohash());
        return location;
    }

}
