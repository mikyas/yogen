package com.yogen.api.resource;


import com.yogen.api.dto.request.TestRequestDTO;
import com.yogen.api.dto.response.TestResponseDTO;
import com.yogen.datafeed.data.service.CacheManagementService;
import com.yogen.datafeed.data.service.LogManagementService;
import com.yogen.datafeed.data.service.TransactionService;
import com.yogen.integrator.data.service.EncryptionService;
import com.yogen.integrator.data.service.IntegratorService;
import com.yogen.log.data.service.LogService;
import com.yogen.util.enumtype.EncryptionKeyType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/api", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class TestResouce {
    @Autowired
    private CacheManagementService cacheManagementService;
    @Autowired
    private LogManagementService logManagementService;
    @Autowired
    private EncryptionService encryptionService;
    @Autowired
    private LogService logService;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private IntegratorService integratorService;

    @RequestMapping(value = "/publichNewKeys")
    public TestResponseDTO publichNewKeys(@RequestBody final TestRequestDTO requestDTO, HttpServletRequest httpServletRequest) {
        System.out.println("started");
        System.out.println("1 main : " + encryptionService.encrypt("1", EncryptionKeyType.MAIN));
        System.out.println("1 temp: " + encryptionService.encrypt("1", EncryptionKeyType.TEMP));
        System.out.println("publicAsHash" + encryptionService.hash("AzQasdW1rsGy"));
        System.out.println("finished");
        return null;
    }

}
