package com.yogen.api;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import com.yogen.log.util.YogenLogConfigruation;
import com.yogen.service.util.YogenServiceConfigruation;

@Configuration
@ComponentScan("com.yogen.api.resource")
@Import({YogenServiceConfigruation.class, YogenLogConfigruation.class})
public class YogenApiConfiguration {
}

