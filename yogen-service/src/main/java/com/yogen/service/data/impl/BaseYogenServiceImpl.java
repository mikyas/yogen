package com.yogen.service.data.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.yogen.datafeed.data.service.OrderBusinessService;
import com.yogen.service.data.BaseYogenService;

@Service
@Transactional
public class BaseYogenServiceImpl implements BaseYogenService {

    @Autowired
    private OrderBusinessService orderBusinessService;

    @Override
    public void doo() {
        orderBusinessService.createOrder();
    }
}
