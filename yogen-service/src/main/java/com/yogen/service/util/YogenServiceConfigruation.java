package com.yogen.service.util;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import com.yogen.bocontroller.util.YogenBoControllerConfigrutaion;
import com.yogen.datafeed.util.YogenDataFeedConfigrutaion;
import com.yogen.factory.util.YogenDecisionFactoryConfigrutaion;
import com.yogen.integrator.util.YogenIntegratorConfigruation;
import com.yogen.log.util.YogenLogConfigruation;

@Configuration
@ComponentScan("com.yogen.service")
@Import({YogenDataFeedConfigrutaion.class,
        YogenDecisionFactoryConfigrutaion.class,
        YogenIntegratorConfigruation.class,
        YogenBoControllerConfigrutaion.class,
        YogenLogConfigruation.class,
        YogenHibernateConfiguration.class})
public class YogenServiceConfigruation {
}
