package com.yogen.service.util;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.SSLContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

public class TLS12RestTemplate extends RestTemplate {

    public TLS12RestTemplate() {
        super();
        SSLContext context;
        try {
            context = SSLContext.getInstance("TLSv1.2");
            context.init(null, null, null);
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            throw new RuntimeException(e);
        }

        CloseableHttpClient httpClient = HttpClientBuilder
                .create()
                .setSSLContext(context)
                .build();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(httpClient);
        setRequestFactory(factory);
    }
}
