package com.yogen.integrator.data.service.impl;

import com.yogen.integrator.data.dao.IntegratorDao;
import com.yogen.integrator.data.service.EncryptionService;
import com.yogen.integrator.data.service.IntegratorService;
import com.yogen.integrator.model.Integrator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class IntegratorServiceImpl implements IntegratorService {
    @Autowired
    IntegratorDao integratorDao;
    @Autowired
    EncryptionService encryptionService;


    @Override
    public Integrator getById(Long id) {
        return integratorDao.getIntegratorById(id);
    }

    @Override
    public Integrator findByPublicKeyForWebService(String publicKey) {
        List<String> hashedPublicKeys = encryptionService.getHashedList(publicKey);
        return integratorDao.getIntegratorByHashedPublicKeys(hashedPublicKeys);
    }

    @Override
    public Integer getRiskScoreLimitByIntegratorId(Long integratorId) {
        return integratorDao.getRiskScoreLimitByIntegratorId(integratorId);
    }
}
