package com.yogen.integrator.data.dao.impl;

import com.yogen.integrator.data.dao.IntegratorDao;
import com.yogen.integrator.model.Integrator;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class IntegratorDaoImpl extends BaseIntegratorHibernateDaoSupport<Integrator, Long> implements IntegratorDao {


    @Override
    public Integrator getIntegratorById(Long integratorId) {
        CriteriaQuery<Integrator> criteriaQuery = getCurrentSession().getCriteriaBuilder().createQuery(Integrator.class);
        Root<Integrator> root = criteriaQuery.from(Integrator.class);
        return (Integrator) criteriaQuery.select(root).where(getCurrentSession().getCriteriaBuilder().equal(root.get("id"), integratorId));
//        return (Integrator) getCurrentSession().createCriteria(Integrator.class).uniqueResult();
    }

    @Override
    public Integrator getIntegratorByCriteria(Long integratorId, String publicKey) {
        return null;
    }

    @Override
    public Integrator getIntegratorByHashedPublicKeys(List<String> hashedPublicKeys) {
        CriteriaBuilder builder = getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<Integrator> criteriaQuery = builder.createQuery(Integrator.class);
        Root<Integrator> root = criteriaQuery.from(Integrator.class);
        criteriaQuery.select(root).where(builder.and(root.get("publicKeyAsHash").in(hashedPublicKeys)));
        return getCurrentSession().createQuery(criteriaQuery).getSingleResult();

    }

    @Override
    public Integer getRiskScoreLimitByIntegratorId(Long integratorId) {
        CriteriaBuilder builder = getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<Integer> criteriaQuery = builder.createQuery(Integer.class);
        Root<Integrator> root = criteriaQuery.from(Integrator.class);
        criteriaQuery.select(root.get("riskScoreLimit")).where(getCurrentSession().getCriteriaBuilder().equal(root.get("id"), integratorId));
        return getCurrentSession().createQuery(criteriaQuery).getSingleResult();
    }
}
