package com.yogen.integrator.data.dao;

import com.yogen.integrator.model.Integrator;

import java.util.List;

public interface IntegratorDao extends IBaseIntegratorHibernateDao<Integrator, Long> {

    Integrator getIntegratorById(Long integratorId);

    Integrator getIntegratorByCriteria(Long integratorId, String publicKey);

    Integrator getIntegratorByHashedPublicKeys(List<String> hashedPublicKeys);

    Integer getRiskScoreLimitByIntegratorId(Long integratorId);

    }
