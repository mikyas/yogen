package com.yogen.integrator.data.dao;

import java.util.List;
import com.yogen.integrator.model.EncryptionKey;

public interface EncryptionKeyDao extends IBaseIntegratorHibernateDao<EncryptionKey, Long> {


    int findLatestVersion(String name);

    String findKeyByNameAndVersion(String name, int version);

    String findLatestHashKey();

    List<EncryptionKey> findHashKeys();

}
