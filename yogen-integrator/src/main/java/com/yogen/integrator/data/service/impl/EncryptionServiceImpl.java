package com.yogen.integrator.data.service.impl;

import com.yogen.integrator.data.dao.EncryptionKeyDao;
import com.yogen.integrator.data.dao.IntegratorDao;
import com.yogen.integrator.data.service.EncryptionService;
import com.yogen.integrator.model.EncryptionKey;
import com.yogen.log.data.service.LogService;
import com.yogen.util.StringUtil;
import com.yogen.util.encryptor.EncryptionUtil;
import com.yogen.util.enumtype.EncryptionKeyType;
import com.yogen.util.enumtype.LogModule;
import com.yogen.util.enumtype.LogPriority;
import com.yogen.util.exception.EncryptionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class EncryptionServiceImpl implements EncryptionService {

    @Autowired
    private EncryptionKeyDao encryptionKeyDao;
    //    @Autowired
//    private UserRepository userRepository;
    @Autowired
    private IntegratorDao integratorDao;
    @Autowired
    private LogService logService;

    @Override
    public String encrypt(String plainText, EncryptionKeyType keyType) throws EncryptionException {
        if (StringUtil.isNullOrZeroLength(plainText)) {
            throw new EncryptionException(new Exception("plainText.is_empty"));
        }
        int latestVersion = encryptionKeyDao.findLatestVersion(keyType.getText());
        String version = String.valueOf(latestVersion);
        if (version.length() < 3) {
            int versionLength = version.length();
            for (int i = 0; i < 3 - versionLength; i++) {
                version = "0" + version;
            }
        }
        String encryptedText = EncryptionUtil.encrypt(plainText, getKeyBytes(keyType, latestVersion));
        return version + encryptedText;
    }

    @Override
    public String decrypt(String encryptedText, EncryptionKeyType keyType) throws EncryptionException {
        if (StringUtil.isNullOrZeroLength(encryptedText)) {
            throw new EncryptionException(new Exception("encryptedText.is_empty"));
        }
        String encryptedTextWithoutVersion;
        int version;
        try {
            encryptedTextWithoutVersion = encryptedText.substring(3);
            version = Integer.valueOf(encryptedText.substring(0, 3));
        } catch (Exception e) {
            throw new EncryptionException(e);
        }
        return EncryptionUtil.decrypt(encryptedTextWithoutVersion, getKeyBytes(keyType, version));
    }

    private byte[] getKeyBytes(EncryptionKeyType keyType, int latestVersion) {
        String[] array = encryptionKeyDao.findKeyByNameAndVersion(keyType.getText(), latestVersion).split(",");
        byte[] keyBytes = new byte[16];
        for (int i = 0; i < 16; i++) {
            keyBytes[i] = Byte.parseByte(array[i]);
        }

        return keyBytes;
    }

    @Override
    public String hash(String plainText) throws EncryptionException {
        if (StringUtil.isNullOrZeroLength(plainText)) {
            throw new EncryptionException(new Exception("plainText.is_empty"));
        }

        String[] hashPrePostFix = encryptionKeyDao.findLatestHashKey().split(",");

        String hashPrefix = hashPrePostFix[0];
        String hashPostFix = hashPrePostFix[1];

        return EncryptionUtil.hash(hashPrefix + plainText + hashPostFix);
    }

    @Override
    public List<String> getHashedList(String plainText) throws EncryptionException {
        if (StringUtil.isNullOrZeroLength(plainText)) {
            throw new EncryptionException(new Exception("plainText.is_empty"));
        }

        List<String> hashedList = new ArrayList<>();
        hashedList.add(EncryptionUtil.hash(plainText));

        List<EncryptionKey> encryptionKeys = encryptionKeyDao.findHashKeys();

        for (EncryptionKey key : encryptionKeys) {
            String[] hashPrePostFix = key.getValue().split(",");

            String hashPrefix = hashPrePostFix[0];
            String hashPostFix = hashPrePostFix[1];
            hashedList.add(EncryptionUtil.hash(hashPrefix + plainText + hashPostFix));
        }

        return hashedList;
    }

    @Override
    public void publishNewKeys() {
        EncryptionKey mainEncryptionKey = new EncryptionKey();
        mainEncryptionKey.setName(EncryptionKeyType.MAIN.getText());
        mainEncryptionKey.setValue(generateNewKey());
        mainEncryptionKey.setVersion(encryptionKeyDao.findLatestVersion(EncryptionKeyType.MAIN.getText()) + 1);
        encryptionKeyDao.persist(mainEncryptionKey);

        EncryptionKey tempEncryptionKey = new EncryptionKey();
        tempEncryptionKey.setName(EncryptionKeyType.TEMP.getText());
        tempEncryptionKey.setValue(generateNewKey());
        tempEncryptionKey.setVersion(encryptionKeyDao.findLatestVersion(EncryptionKeyType.TEMP.getText()) + 1);
        encryptionKeyDao.persist(tempEncryptionKey);

        EncryptionKey hashPreFixPostFix = new EncryptionKey();
        hashPreFixPostFix.setName(EncryptionKeyType.HASH.getText());
        hashPreFixPostFix.setValue(generateNewHashPreFixPostFix());
        hashPreFixPostFix.setVersion(encryptionKeyDao.findLatestVersion(EncryptionKeyType.HASH.getText()) + 1);
        encryptionKeyDao.persist(hashPreFixPostFix);

        logService.log(LogPriority.DEBUG, LogModule.SECURITY, "Şifreleme algoritması tuz bilgilerinin yeni versiyonları başarılı olarak oluşturulmuştur!", "Şifreleme algoritması tuz bilgilerinin yeni versiyonları başarılı olarak oluşturulmuştur!");
    }

    private String generateNewKey() {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[16];
        random.nextBytes(bytes);

        String bytesAsText = "";
        for (Byte b : bytes) {
            bytesAsText += b.toString() + ",";
        }
        return bytesAsText.substring(0, bytesAsText.length() - 1);
    }

    private String generateNewHashPreFixPostFix() {
        String prefix = StringUtil.generateRandomHashPrePostFix();
        String postfix = StringUtil.generateRandomHashPrePostFix();
        return prefix + "," + postfix;
    }

}