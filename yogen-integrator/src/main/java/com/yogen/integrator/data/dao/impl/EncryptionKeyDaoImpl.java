package com.yogen.integrator.data.dao.impl;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.yogen.integrator.data.dao.EncryptionKeyDao;
import com.yogen.integrator.model.EncryptionKey;
import com.yogen.util.enumtype.EncryptionKeyType;

@Repository
public class EncryptionKeyDaoImpl extends BaseIntegratorHibernateDaoSupport<EncryptionKey, Long> implements EncryptionKeyDao {

    @Override
    public int findLatestVersion(String name) {
        CriteriaBuilder builder = getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<Integer> criteria = builder.createQuery(Integer.class);
        Root<EncryptionKey> root = criteria.from(EncryptionKey.class);
        List<Predicate> restrictions = new ArrayList<>();
        criteria.where(restrictions.toArray(new Predicate[restrictions.size()]));
        criteria.select(builder.max(root.get("version")));
        criteria.where(builder.equal(root.get("name"), name));
        return getCurrentSession().createQuery(criteria).getSingleResult();

    }

    @Override
    public String findKeyByNameAndVersion(String name, int version) {

        CriteriaBuilder criteriaBuilder = getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<EncryptionKey> criteriaQuery = criteriaBuilder.createQuery(EncryptionKey.class);
        Root<EncryptionKey> root = criteriaQuery.from(EncryptionKey.class);
        criteriaQuery.select(root).where(criteriaBuilder.and(criteriaBuilder.equal(root.get("name"), name), criteriaBuilder.equal(root.get("version"), version)));
        return getCurrentSession().createQuery(criteriaQuery).getSingleResult().getValue();

    }

    @Override
    public String findLatestHashKey() {
        Criteria criteria = getCurrentSession().createCriteria(EncryptionKey.class);
        criteria.add(Restrictions.eq("name", EncryptionKeyType.HASH.getText()));
        criteria.addOrder(Order.desc("version"));
        criteria.setMaxResults(1);
        return ((EncryptionKey) criteria.uniqueResult()).getValue();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EncryptionKey> findHashKeys() {
        Criteria criteria = getCurrentSession().createCriteria(EncryptionKey.class);
        criteria.add(Restrictions.eq("name", EncryptionKeyType.HASH.getText()));
        return criteria.list();
    }
}
