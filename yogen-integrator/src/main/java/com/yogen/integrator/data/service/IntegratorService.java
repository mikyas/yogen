package com.yogen.integrator.data.service;

import com.yogen.integrator.model.Integrator;

public interface IntegratorService {
    Integrator getById(Long id);

    Integrator findByPublicKeyForWebService(String publicKey);

    Integer getRiskScoreLimitByIntegratorId(Long integratorId);
}
