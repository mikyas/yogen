package com.yogen.integrator.data.service;

import java.util.List;
import com.yogen.util.enumtype.EncryptionKeyType;
import com.yogen.util.exception.EncryptionException;

public interface EncryptionService {
    String encrypt(String plainText, EncryptionKeyType keyType) throws EncryptionException;

    String decrypt(String plainText, EncryptionKeyType keyType) throws EncryptionException;

    String hash(String plainText) throws EncryptionException;

    List<String> getHashedList(String plainText) throws EncryptionException;

    void publishNewKeys();
}