package com.yogen.integrator.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "integrator", catalog = "yogen_integrator", schema = "dbo")
public class Integrator extends AbstractBaseIntegratorEntity implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @Column(name = "RISK_SCORE_LIMIT")
    private Integer riskScoreLimit;

    @Column(name = "PUBLIC_KEY", nullable = false, unique = true)
    private String publicKey;

    @Column(name = "PRIVATE_KEY", nullable = false, unique = true)
    private String privateKey;

    @Column(name = "PUBLIC_KEY_AS_HASH", nullable = false, unique = true)
    private String publicKeyAsHash;

    @Column(name = "OLD_PUBLIC_KEY_BACKUP", nullable = true)
    private String oldPublicKeyBackup;

    @Column(name = "OLD_PRIVATE_KEY_BACKUP", nullable = true)
    private String oldPrivateKeyBackup;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getRiskScoreLimit() {
        return riskScoreLimit;
    }

    public void setRiskScoreLimit(Integer riskScoreLimit) {
        this.riskScoreLimit = riskScoreLimit;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPublicKeyAsHash() {
        return publicKeyAsHash;
    }

    public void setPublicKeyAsHash(String publicKeyAsHash) {
        this.publicKeyAsHash = publicKeyAsHash;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getOldPublicKeyBackup() {
        return oldPublicKeyBackup;
    }

    public void setOldPublicKeyBackup(String oldPublicKeyBackup) {
        this.oldPublicKeyBackup = oldPublicKeyBackup;
    }

    public String getOldPrivateKeyBackup() {
        return oldPrivateKeyBackup;
    }

    public void setOldPrivateKeyBackup(String oldPrivateKeyBackup) {
        this.oldPrivateKeyBackup = oldPrivateKeyBackup;
    }

}
