package com.yogen.integrator.util;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import com.yogen.log.util.YogenLogConfigruation;

@Configuration
@ComponentScan("com.yogen.integrator")
@Import({YogenLogConfigruation.class})
public class YogenIntegratorConfigruation {
    public YogenIntegratorConfigruation() {
    }
}
