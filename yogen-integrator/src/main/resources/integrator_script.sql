USE [master]
GO
/****** Object:  Database [yogen_integrator]    Script Date: 2.11.2018 10:04:44 ******/
CREATE DATABASE [yogen_integrator]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'yogen_integrator', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\yogen_integrator.mdf' , SIZE = 94400KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'yogen_integrator_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\yogen_integrator_log.ldf' , SIZE = 359424KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [yogen_integrator] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [yogen_integrator].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [yogen_integrator] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [yogen_integrator] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [yogen_integrator] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [yogen_integrator] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [yogen_integrator] SET ARITHABORT OFF 
GO
ALTER DATABASE [yogen_integrator] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [yogen_integrator] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [yogen_integrator] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [yogen_integrator] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [yogen_integrator] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [yogen_integrator] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [yogen_integrator] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [yogen_integrator] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [yogen_integrator] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [yogen_integrator] SET  DISABLE_BROKER 
GO
ALTER DATABASE [yogen_integrator] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [yogen_integrator] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [yogen_integrator] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [yogen_integrator] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [yogen_integrator] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [yogen_integrator] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [yogen_integrator] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [yogen_integrator] SET RECOVERY FULL 
GO
ALTER DATABASE [yogen_integrator] SET  MULTI_USER 
GO
ALTER DATABASE [yogen_integrator] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [yogen_integrator] SET DB_CHAINING OFF 
GO
ALTER DATABASE [yogen_integrator] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [yogen_integrator] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [yogen_integrator] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'yogen_integrator', N'ON'
GO
USE [yogen_integrator]
GO
/****** Object:  Table [dbo].[integrator]    Script Date: 2.11.2018 10:04:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[integrator](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[NAME] [nvarchar](255) NOT NULL,
	[DESCRIPTION] [nvarchar](255) NULL,
	[PUBLIC_KEY] [nvarchar](255) NULL,
	[PRIVATE_KEY] [nvarchar](255) NULL,
	[PUBLIC_KEY_AS_HASH] [nvarchar](100) NULL,
	[OLD_PUBLIC_KEY_BACKUP] [nvarchar](255) NULL,
	[OLD_PRIVATE_KEY_BACKUP] [nvarchar](255) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_consumer_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
USE [master]
GO
ALTER DATABASE [yogen_integrator] SET  READ_WRITE 
GO
