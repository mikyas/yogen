package com.yogen.mail.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "email_queue", catalog = "yogen_mail", schema = "dbo")
public class EmailQueue implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "IDATE", nullable = false)
    private Date creationTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UDATE", nullable = true)
    private Date updateTime;

    @Column(name = "TEMPLATE_ID", nullable = true)
    private Integer templateId;

    @Column(name = "SMTP_HOST_PROVIDER", nullable = false)
    private String smtpHostProvider;

    @Column(name = "FROM_EMAIL", nullable = false)
    private String fromEmail;

    @Column(name = "FROM_NAME", nullable = true)
    private String fromName;

    @Column(name = "TO_EMAILS", nullable = false)
    private String toEmails;

    @Column(name = "TO_CC_EMAILS", nullable = true)
    private String toCcEmails;

    @Column(name = "TO_BCC_EMAILS", nullable = true)
    private String toBccEmails;

    @Column(name = "SUBJECT", nullable = false)
    private String subject;

    @Column(name = "CONTENT", nullable = false)
    private String content;

    @Column(name = "ATTACHMENT_FILES", nullable = true)
    private String attachmentFiles;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "SCHEDULED_SEND_DATE", nullable = true)
    private Date scheduledSendDate;

    @Column(name = "TRY_COUNT", nullable = false)
    private int tryCount = 1;

    @Column(name = "SENT", nullable = false)
    private boolean sent = false;

    @Column(name = "LANGUAGE_ID", nullable = true)
    private Integer languageId;

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getSmtpHostProvider() {
        return smtpHostProvider;
    }

    public void setSmtpHostProvider(String smtpHostProvider) {
        this.smtpHostProvider = smtpHostProvider;
    }

    public Integer getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getToEmails() {
        return toEmails;
    }

    public void setToEmails(String toEmails) {
        this.toEmails = toEmails;
    }

    public String getToCcEmails() {
        return toCcEmails;
    }

    public void setToCcEmails(String toCcEmails) {
        this.toCcEmails = toCcEmails;
    }

    public String getToBccEmails() {
        return toBccEmails;
    }

    public void setToBccEmails(String toBccEmails) {
        this.toBccEmails = toBccEmails;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAttachmentFiles() {
        return attachmentFiles;
    }

    public void setAttachmentFiles(String attachmentFiles) {
        this.attachmentFiles = attachmentFiles;
    }

    public Date getScheduledSendDate() {
        return scheduledSendDate;
    }

    public void setScheduledSendDate(Date scheduledSendDate) {
        this.scheduledSendDate = scheduledSendDate;
    }

    public int getTryCount() {
        return tryCount;
    }

    public void setTryCount(int tryCount) {
        this.tryCount = tryCount;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }
}