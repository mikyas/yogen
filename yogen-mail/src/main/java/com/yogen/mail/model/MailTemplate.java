package com.yogen.mail.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "mail_template", catalog = "yogen_mail", schema = "dbo")
public class MailTemplate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Integer id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "IDATE", nullable = false)
    private Date creationTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UDATE", nullable = true)
    private Date updateTime;

    @Column(name = "MAIL_TEMPLATE_NAME", nullable = false, unique = true)
    private String mailTemplateName;

    @Column(name = "IS_HTML_TEMPLATE", nullable = false)
    private Integer isHtmlTemplate;

    @Column(name = "MAIL_FROM", nullable = true)
    private String mailFrom;

    @Column(name = "MAIL_FROM_NAME", nullable = true)
    private String mailFromName;

    public MailTemplate() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public String getMailTemplateName() {
        return mailTemplateName;
    }

    public Integer getIsHtmlTemplate() {
        return isHtmlTemplate;
    }

    public String getMailFrom() {
        return mailFrom;
    }

    public String getMailFromName() {
        return mailFromName;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setMailTemplateName(String mailTemplateName) {
        this.mailTemplateName = mailTemplateName;
    }

    public void setIsHtmlTemplate(Integer isHtmlTemplate) {
        this.isHtmlTemplate = isHtmlTemplate;
    }

    public void setMailFrom(String mailFrom) {
        this.mailFrom = mailFrom;
    }

    public void setMailFromName(String mailFromName) {
        this.mailFromName = mailFromName;
    }
}