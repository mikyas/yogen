package com.yogen.mail.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "email_log", catalog = "yogen_mail", schema = "dbo")
public class EMailLog implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FIRST_REQUEST_DATE", nullable = false)
    private Date firstRequestDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "REQUEST_DATE", nullable = false)
    private Date requestDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RESPONSE_DATE", nullable = false)
    private Date responseDate;

    @Column(name = "TRY_COUNT", nullable = false)
    private int tryCount = 1;

    @Column(name = "FROM_EMAIL", nullable = false)
    private String fromEmail;

    @Column(name = "TO_EMAILS", nullable = false)
    private String toEmails;

    @Column(name = "TO_CC_EMAILS", nullable = true)
    private String toCcEmails;

    @Column(name = "TO_BCC_EMAILS", nullable = true)
    private String toBccEmails;

    @Column(name = "TEMPLATE_ID", nullable = true)
    private Integer templateId;

    @Column(name = "SUBJECT", nullable = true)
    private String subject;

    @Column(name = "CONTENT", nullable = true)
    private String content;

    @Column(name = "SCHEDULED_MAIL", nullable = false)
    private boolean scheduledMail = false;

    @Column(name = "SUCCESS", nullable = false)
    private boolean success = false;

    @Column(name = "ERROR_MESSAGE", nullable = true)
    private String errorMessage;

    @Column(name = "LANGUAGE_ID", nullable = true)
    private Integer languageId;

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }

    public int getTryCount() {
        return tryCount;
    }

    public void setTryCount(int tryCount) {
        this.tryCount = tryCount;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getToEmails() {
        return toEmails;
    }

    public void setToEmails(String toEmails) {
        this.toEmails = toEmails;
    }

    public String getToCcEmails() {
        return toCcEmails;
    }

    public void setToCcEmails(String toCcEmails) {
        this.toCcEmails = toCcEmails;
    }

    public String getToBccEmails() {
        return toBccEmails;
    }

    public void setToBccEmails(String toBccEmails) {
        this.toBccEmails = toBccEmails;
    }

    public Integer getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isScheduledMail() {
        return scheduledMail;
    }

    public void setScheduledMail(boolean scheduledMail) {
        this.scheduledMail = scheduledMail;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Date getFirstRequestDate() {
        return firstRequestDate;
    }

    public void setFirstRequestDate(Date firstRequestDate) {
        this.firstRequestDate = firstRequestDate;
    }
}