package com.yogen.mail.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "email_forbidden_domain", catalog = "yogen_mail", schema = "dbo")
public class EmailForbiddenDomain implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Integer id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "IDATE", nullable = false)
    private Date creationTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UDATE", nullable = true)
    private Date updateTime;

    @Column(name = "FORBIDDEN_DOMAIN", nullable = false)
    private String forbiddenDomain;

    @Column(name = "STATUS", nullable = false)
    private Integer status;

    public EmailForbiddenDomain() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public String getForbiddenDomain() {
        return forbiddenDomain;
    }

    public Integer getStatus() {
        return status;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setForbiddenDomain(String forbiddenDomain) {
        this.forbiddenDomain = forbiddenDomain;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}