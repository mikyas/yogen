package com.yogen.mail.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "localized_mail_template", catalog = "yogen_mail", schema = "dbo")
public class LocalizedMailTemplate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LANGUAGE_ID", referencedColumnName = "ID", nullable = false)
    private MailLanguage mailLanguage;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TEMPLATE_ID", referencedColumnName = "ID", nullable = false)
    private MailTemplate mailTemplate;

    @Lob
    @Column(name = "CONTENT", nullable = false)
    private String content;

    @Column(name = "MAIL_SUBJECT", nullable = true)
    private String mailSubject;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MailLanguage getMailLanguage() {
        return mailLanguage;
    }

    public void setMailLanguage(MailLanguage mailLanguage) {
        this.mailLanguage = mailLanguage;
    }

    public MailTemplate getMailTemplate() {
        return mailTemplate;
    }

    public void setMailTemplate(MailTemplate mailTemplate) {
        this.mailTemplate = mailTemplate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMailSubject() {
        return mailSubject;
    }

    public void setMailSubject(String mailSubject) {
        this.mailSubject = mailSubject;
    }
}