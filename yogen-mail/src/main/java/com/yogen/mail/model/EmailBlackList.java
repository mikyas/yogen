package com.yogen.mail.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "email_black_list", catalog = "yogen_mail", schema = "dbo")
public class EmailBlackList implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "IDATE", nullable = false)
    private Date creationTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UDATE", nullable = true)
    private Date updateTime;

    @Column(name = "EMAIL", nullable = false)
    private String email;

    @Column(name = "ERROR_COUNT", nullable = false)
    private Integer errorCount;

    @Column(name = "MAX_ERROR_COUNT", nullable = false)
    private Integer maxErrorCount;

    public EmailBlackList() {
        super();
    }

    public Long getId() {
        return id;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public String getEmail() {
        return email;
    }

    public Integer getErrorCount() {
        return errorCount;
    }

    public Integer getMaxErrorCount() {
        return maxErrorCount;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setErrorCount(Integer errorCount) {
        this.errorCount = errorCount;
    }

    public void setMaxErrorCount(Integer maxErrorCount) {
        this.maxErrorCount = maxErrorCount;
    }
}