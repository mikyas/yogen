package com.yogen.bocontroller.util;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import com.yogen.log.util.YogenLogConfigruation;

@Configuration
@ComponentScan("com.yogen.bocontroller")
@Import({YogenLogConfigruation.class})
public class YogenBoControllerConfigrutaion {
    public YogenBoControllerConfigrutaion() {
    }
}
