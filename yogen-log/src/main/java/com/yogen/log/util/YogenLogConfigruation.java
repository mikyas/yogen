package com.yogen.log.util;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.yogen.log")
public class YogenLogConfigruation {
    public YogenLogConfigruation() {
    }
}
