package com.yogen.log.model.dto;

import java.io.Serializable;
import java.util.Date;
import com.yogen.util.enumtype.ApiLogModuleAndServiceName;

public class ApiResponseLogDTO implements Serializable {

    private Long id;
    private Long integratorOrApiId;
    private Integer module;
    private Integer method;
    private String orderId;
    private Date responseDate;
    private String response;
    private Integer result;
    private String resultCode;
    private String errorMessage;
    private String returnUrl;
    private String requestType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIntegratorOrApiId() {
        return integratorOrApiId;
    }

    public void setIntegratorOrApiId(Long integratorOrApiId) {
        this.integratorOrApiId = integratorOrApiId;
    }

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }

    public Integer getMethod() {
        return method;
    }

    public void setMethod(Integer method) {
        this.method = method;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getMethodName() {
        for (ApiLogModuleAndServiceName a : ApiLogModuleAndServiceName.values()) {
            if (a.getWebServiceId().equals(method)) {
                return a.name();
            }
        }
        return null;
    }

}