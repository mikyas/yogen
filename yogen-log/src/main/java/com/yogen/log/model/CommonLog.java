package com.yogen.log.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.yogen.util.enumtype.LogPriority;

@Entity
@Table(name = "common_log", catalog = "yogen_log", schema = "dbo")
public class CommonLog extends AbstractBaseLogEntity implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LOG_DATE", nullable = false)
    private Date logDate;

    @Column(name = "SERVER", nullable = false)
    private int server;

    @Column(name = "SERVER_IP", nullable = true)
    private String serverIp;

    @Column(name = "PRIORITY", nullable = false)
    private int priority;

    @Column(name = "MODULE", nullable = false)
    private String module;

    @Column(name = "CLASS", nullable = false)
    private String className;

    @Column(name = "METHOD", nullable = false)
    private String method;

    @Column(name = "LINE_NUMBER", nullable = false)
    private int lineNumber;

    @Column(name = "SUMMARY_INFO", nullable = false)
    private String summaryInfo;

    @Column(name = "DETAIL_INFO", nullable = true)
    private String detailInfo;

    @Column(name = "DATA", nullable = true)
    private String data;

    @Column(name = "ACTOR", nullable = false)
    private String actor;

    @Column(name = "ACTOR_ID", nullable = true)
    private Long actorId;


    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Date getLogDate() {
        return logDate;
    }

    public void setLogDate(Date logDate) {
        this.logDate = logDate;
    }

    public int getServer() {
        return server;
    }

    public void setServer(int server) {
        this.server = server;
    }

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getSummaryInfo() {
        return summaryInfo;
    }

    public void setSummaryInfo(String summaryInfo) {
        this.summaryInfo = summaryInfo;
    }

    public String getDetailInfo() {
        return detailInfo;
    }

    public void setDetailInfo(String detailInfo) {
        this.detailInfo = detailInfo;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public Long getActorId() {
        return actorId;
    }

    public void setActorId(Long actorId) {
        this.actorId = actorId;
    }

    public String getStyleClass() {
        if (getPriority() == LogPriority.INFO.getValue()) {
            return "payment-waiting";
        } else if (getPriority() == LogPriority.WARN.getValue()) {
            return "payment-waiting";
        } else if (getPriority() == LogPriority.DEBUG.getValue()) {
            return "payment-warning";
        } else if (getPriority() == LogPriority.ERROR.getValue()) {
            return "payment-error";
        } else if (getPriority() == LogPriority.FATAL.getValue()) {
            return "payment-error";
        }
        return "";
    }
}