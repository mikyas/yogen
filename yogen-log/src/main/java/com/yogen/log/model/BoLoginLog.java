package com.yogen.log.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "login_log", catalog = "yogen_log", schema = "dbo")
public class BoLoginLog extends AbstractBaseLogEntity implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "USER_ID", nullable = false)
    private Integer userId;

    @Column(name = "CHANNEL_ID", nullable = false)
    private Integer channelId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LOGIN_DATE", nullable = false)
    private Date loginDate;

    @Column(name = "IP", nullable = false)
    private String ip;

    @Column(name = "MOBILE_DEVICE_TOKEN", nullable = true)
    private String mobileDeviceToken;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public String getMobileDeviceToken() {
        return mobileDeviceToken;
    }

    public void setMobileDeviceToken(String mobileDeviceToken) {
        this.mobileDeviceToken = mobileDeviceToken;
    }
}