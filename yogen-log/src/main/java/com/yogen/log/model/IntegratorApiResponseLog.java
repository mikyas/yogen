package com.yogen.log.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "api_response_log", catalog = "yogen_log", schema = "dbo")
public class IntegratorApiResponseLog extends AbstractBaseLogEntity {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "INTEGRATOR_ID", nullable = true)
    private Long integratorId;

    @Column(name = "MODULE", nullable = false)
    private Integer module;

    @Column(name = "METHOD", nullable = false)
    private Integer method;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RESPONSE_DATE", nullable = false)
    private Date responseDate;

    @Column(name = "RESPONSE", nullable = true)
    private String response;

    @Column(name = "RESULT", nullable = false)
    private int result = 0;

    @Column(name = "RESULT_CODE", nullable = true)
    private String resultCode;

    @Column(name = "ERROR_MESSAGE", nullable = true)
    private String errorMessage;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Long getIntegratorId() {
        return integratorId;
    }

    public void setIntegratorId(Long integratorId) {
        this.integratorId = integratorId;
    }

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }

    public Integer getMethod() {
        return method;
    }

    public void setMethod(Integer method) {
        this.method = method;
    }

    public Date getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}