package com.yogen.log.model.dto;

import java.io.Serializable;
import java.util.Date;
import com.yogen.util.enumtype.ApiLogModuleAndServiceName;

public class ApiRequestLogDTO implements Serializable {

    private Long id;
    private Long integratorOrApiId;
    private Integer module;
    private Integer method;
    private Date requestDate;
    private String orderId;
    private String request;
    private String token;
    private String transactionDate;
    private String version;
    private String currency;
    private String language;
    private String requestType;
    private String clientIp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIntegratorOrApiId() {
        return integratorOrApiId;
    }

    public void setIntegratorOrApiId(Long integratorOrApiId) {
        this.integratorOrApiId = integratorOrApiId;
    }

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }

    public Integer getMethod() {
        return method;
    }

    public void setMethod(Integer method) {
        this.method = method;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getMethodName() {
        for (ApiLogModuleAndServiceName a : ApiLogModuleAndServiceName.values()) {
            if (a.getWebServiceId().equals(method)) {
                return a.name();
            }
        }
        return null;
    }

}