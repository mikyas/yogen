package com.yogen.log.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "api_call_log", catalog = "yogen_log", schema = "dbo")
public class ApiCallLog extends AbstractBaseLogEntity implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "SYSTEM", nullable = false)
    private Integer system;

    @Column(name = "SERVICE", nullable = false)
    private Integer service;

    @Column(name = "TRANSACTION_UID", nullable = true)
    private String transactionUID;

    @Column(name = "REFERENCE_NO", nullable = true)
    private String referenceNo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "REQUEST_DATE", nullable = false)
    private Date requestDate;

    @Column(name = "REQUEST", nullable = true)
    private String request;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RESPONSE_DATE", nullable = true)
    private Date responseDate;

    @Column(name = "RESPONSE", nullable = true)
    private String response;

    @Column(name = "RESULT", nullable = false)
    private int result = 0;

    @Column(name = "RESPONSE_CODE", nullable = true)
    private String responseCode;

    @Column(name = "RESPONSE_TEXT", nullable = true)
    private String responseText;

    @Column(name = "ERROR_MESSAGE", nullable = true)
    private String errorMessage;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSystem() {
        return system;
    }

    public void setSystem(Integer system) {
        this.system = system;
    }

    public Integer getService() {
        return service;
    }

    public void setService(Integer service) {
        this.service = service;
    }

    public String getTransactionUID() {
        return transactionUID;
    }

    public void setTransactionUID(String transactionUID) {
        this.transactionUID = transactionUID;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public Date getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}