package com.yogen.log.model;


/**
 * @author Tarik.Mikyas<br>
 * AbstractBaseEntity nin implement ettigi ve her entity nin temel degiskenlerini barindiran arayuzdur.
 */
public interface IGenericLogEntity {

    /**
     * @return <br>
     * this method getId
     * @author Tarik.Mikyas
     */
    Long getId();

    /**
     * @param id <br>
     *           this method setId
     * @author Tarik.Mikyas
     */
    void setId(Long id);

}
