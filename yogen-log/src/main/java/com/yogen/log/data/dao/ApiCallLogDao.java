package com.yogen.log.data.dao;

import java.util.Date;
import java.util.List;
import com.yogen.log.model.ApiCallLog;

public interface ApiCallLogDao extends IBaseLogHibernateDao<ApiCallLog, Long> {

    List<ApiCallLog> findByCriteria(Integer value, Integer serviceId, String transactionId, String referenceNo, Integer result, Date logStartDate, Date logEndDate);
}
