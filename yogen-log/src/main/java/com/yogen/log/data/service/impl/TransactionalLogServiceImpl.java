package com.yogen.log.data.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.yogen.log.data.dao.ApiCallLogDao;
import com.yogen.log.data.dao.CommonLogDao;
import com.yogen.log.data.dao.IntegratorApiRequestLogDao;
import com.yogen.log.data.dao.IntegratorApiResponseLogDao;
import com.yogen.log.data.service.TransactionalLogService;
import com.yogen.log.model.ApiCallLog;
import com.yogen.log.model.CommonLog;
import com.yogen.log.model.IntegratorApiRequestLog;
import com.yogen.log.model.IntegratorApiResponseLog;
import com.yogen.log.model.dto.ApiRequestLogDTO;
import com.yogen.log.model.dto.ApiResponseLogDTO;
import com.yogen.util.DateUtil;
import com.yogen.util.StringUtil;
import com.yogen.util.cardnumber.CardNumberUtil;
import com.yogen.util.enumtype.ApiCalledServiceName;
import com.yogen.util.enumtype.ApiLogChannel;
import com.yogen.util.enumtype.ApiLogModule;
import com.yogen.util.enumtype.ApiLogModuleAndServiceName;
import com.yogen.util.enumtype.LogActor;
import com.yogen.util.enumtype.LogModule;
import com.yogen.util.enumtype.LogPriority;
import com.yogen.util.enumtype.YogenServer;
import com.yogen.util.validator.BankCardValidator;

@Service
@Transactional
public class TransactionalLogServiceImpl implements TransactionalLogService {

    private static final int LOG_MAIL_TEMPLATE_ID = 2043;

    @Autowired
    private CommonLogDao commonLogDao;
    @Autowired
    private IntegratorApiRequestLogDao integratorApiRequestLogDao;
    @Autowired
    private IntegratorApiResponseLogDao integratorApiResponseLogDao;
    @Autowired
    private ApiCallLogDao apiCallLogDao;
//    @Autowired
//    private MailSenderService mailSenderService;

    @Async
    @Override
    public void logSystemOut(Date logDate, LogPriority priority, LogModule module, String className, String methodName, Integer lineNumber, String summaryInfo, String detailInfo, Object data, LogActor logActor, Long actorId, String txId) {
        try {
            CommonLog log = new CommonLog();
            log.setLogDate(logDate);
            log.setPriority(priority.getValue());
            log.setModule(module.name());
            if (!StringUtil.isNullOrZeroLength(className)) {
                String[] splittedClassName = className.split("\\.");
                className = splittedClassName[splittedClassName.length - 1];
            }
            log.setClassName(className);
            log.setMethod(methodName);
            log.setLineNumber(lineNumber);
            summaryInfo = CardNumberUtil.maskCardNumbers(summaryInfo);
            log.setSummaryInfo(summaryInfo);
            detailInfo = CardNumberUtil.maskCardNumbers(detailInfo);
            log.setDetailInfo(detailInfo);
            log.setServerIp(StringUtil.getServerIpAddress());
            log.setServer(YogenServer.getServer(log.getServerIp()));
            try {
                if (data != null) {
                    if (data instanceof String) {
                        log.setData(CardNumberUtil.maskCardNumbers((String) data));
                    } else {
                        log.setData(CardNumberUtil.maskCardNumbers(new Gson().toJson(data)));
                    }
                }
            } catch (Throwable ignored) {
            }
            log.setActor(logActor.name());
            log.setActorId(actorId);
            commonLogDao.persist(log);

            if (priority.equals(LogPriority.ERROR) || priority.equals(LogPriority.FATAL)) {
                Map<String, String> subjectValueMap = new HashMap<>();
                subjectValueMap.put("priority", priority.name());
                subjectValueMap.put("module", module.name());

                Map<String, String> contentValueMap = new HashMap<>();
                contentValueMap.put("date", DateUtil.toStringSlashed(log.getLogDate()));
                contentValueMap.put("priority", priority.name());
                contentValueMap.put("module", module.name());
                contentValueMap.put("server", log.getServer() + " - " + log.getServerIp());
                contentValueMap.put("class", log.getClassName());
                contentValueMap.put("method", log.getMethod());
                contentValueMap.put("line", String.valueOf(log.getLineNumber()));
                contentValueMap.put("subject", log.getSummaryInfo());
                contentValueMap.put("detail", !StringUtil.isNullOrZeroLength(log.getDetailInfo()) ? log.getDetailInfo() : StringUtil.EMPTY_STRING);
                contentValueMap.put("exception", !StringUtil.isNullOrZeroLength(log.getData()) ? log.getData() : StringUtil.EMPTY_STRING);

//                if (priority.equals(LogPriority.ERROR)) {
//                    mailSenderService.sendEmailByUsingTemplate(LOG_MAIL_TEMPLATE_ID, "pspsystem@multinet.com.tr", "PSP Team", contentValueMap, subjectValueMap, null, null);
//                } else if (priority.equals(LogPriority.FATAL)) {
//                    mailSenderService.sendEmailByUsingTemplate(LOG_MAIL_TEMPLATE_ID, "pspsystem@multinet.com.tr", "PSP Team", contentValueMap, subjectValueMap, null, null);
//                }
            }
        } catch (Throwable e) {
            summaryInfo = CardNumberUtil.maskCardNumbers(summaryInfo);
            detailInfo = CardNumberUtil.maskCardNumbers(detailInfo);
//            mailSenderService.sendErrorMail("Common Log kaydedilemedi!", "SummaryInfo: " + summaryInfo + " DetailInfo: " + detailInfo + "Exception: " + ExceptionUtils.getFullStackTrace(e));
        }
    }

    @Override
    public List<CommonLog> findSystemLogsByCriteria(Date logStartDate, Date logEndDate, YogenServer yogenServer, LogPriority priority, LogModule module, String txId) {
        return commonLogDao.findByCriteria(logStartDate, logEndDate, yogenServer, priority, module, txId);
    }

    @Override
    public List<ApiCallLog> findApiCallLogsByCriteria(Date logStartDate, Date logEndDate, ApiCalledServiceName serviceName, String transactionId, String referenceNo, Integer result) {
        if (serviceName != null) {
            return apiCallLogDao.findByCriteria(serviceName.getApiCalledSystem().getValue(), serviceName.getServiceId(), transactionId, referenceNo, result, logStartDate, logEndDate);
        }
        return null;
    }

    @Override
    public List<ApiRequestLogDTO> findApiRequestLogsByCriteria(Date logStartDate, Date logEndDate, ApiLogModule apiLogModule, ApiLogModuleAndServiceName apiLogModuleAndServiceName, Long apiOrIntegratorId, String orderId) {
        if (apiLogModule != null) {
            ApiLogChannel apiLogChannel = null;
            for (ApiLogModuleAndServiceName e : ApiLogModuleAndServiceName.values()) {
                if (e.getApiLogModule().equals(apiLogModule)) {
                    apiLogChannel = e.getApiLogChannel();
                }
            }
            if (apiLogChannel == null) {
                return null;
            }

            Integer webServiceId = null;
            if (apiLogModuleAndServiceName != null) {
                webServiceId = apiLogModuleAndServiceName.getWebServiceId();
            }
            return integratorApiRequestLogDao.findByCriteria(apiLogModule.getValue(), webServiceId, apiOrIntegratorId, logStartDate, logEndDate);
        }

        return null;
    }

    @Override
    public List<ApiResponseLogDTO> findApiResponseLogsByCriteria(Date logStartDate, Date logEndDate, ApiLogModule apiLogModule, ApiLogModuleAndServiceName apiLogModuleAndServiceName, Long apiOrIntegratorId, String orderId, Integer result, String resultCode) {
        if (apiLogModule != null) {
            ApiLogChannel apiLogChannel = null;
            for (ApiLogModuleAndServiceName e : ApiLogModuleAndServiceName.values()) {
                if (e.getApiLogModule().equals(apiLogModule)) {
                    apiLogChannel = e.getApiLogChannel();
                }
            }
            if (apiLogChannel == null) {
                return null;
            }

            Integer webServiceId = null;
            if (apiLogModuleAndServiceName != null) {
                webServiceId = apiLogModuleAndServiceName.getWebServiceId();
            }
            return integratorApiResponseLogDao.findByCriteria(apiLogModule.getValue(), webServiceId, apiOrIntegratorId, logStartDate, logEndDate, result, resultCode);
        }

        return null;
    }

    @Async
    @Override
    public void logApiRequest(Long integratorOrApiUserId, ApiLogModuleAndServiceName apiLogModuleAndServiceName, Object requestData, String orderId, String token, String transactionDate, String version, String currency, String language, String requestType, String clientIp, String serverIp) {
        try {
            if (apiLogModuleAndServiceName != null) {
                String data = null;
                if (requestData != null) {
                    if (requestData instanceof String) {
                        data = (String) requestData;
                    } else if (requestData instanceof Map) {
                        Map map = new HashMap();
                        map.putAll((Map) requestData);
                        if (map.get("cardNumber") != null) {
                            if (map.get("cardNumber") instanceof String[]) {
                                map.put("binNumber", BankCardValidator.getBinNumber(((String[]) map.get("cardNumber"))[0]));
                            } else if (map.get("cardNumber") instanceof String) {
                                map.put("binNumber", BankCardValidator.getBinNumber((String) map.get("cardNumber")));
                            }
                            map.put("cardNumber", StringUtil.EMPTY_STRING);
                        }
                        if (map.get("cardExpireMonth") != null) {
                            map.put("cardExpireMonth", StringUtil.EMPTY_STRING);
                        }
                        if (map.get("cardExpireYear") != null) {
                            map.put("cardExpireYear", StringUtil.EMPTY_STRING);
                        }
                        if (map.get("cardCvc") != null) {
                            map.put("cardCvc", StringUtil.EMPTY_STRING);
                        }
                        if (map.get("cvc") != null) {
                            map.put("cvc", StringUtil.EMPTY_STRING);
                        }
                        if (map.get("cardHolder") != null) {
                            map.put("cardHolder", StringUtil.EMPTY_STRING);
                        }
                        if (map.get("hash_text") != null) {
                            map.put("hash_text", StringUtil.EMPTY_STRING);
                        }
                        if (map.get("number") != null) {
                            map.put("number", StringUtil.EMPTY_STRING);
                        }
                        if (map.get("cardOwnerName") != null) {
                            if (map.get("cardOwnerName") instanceof String[]) {
                                map.put("cardOwnerName", CardNumberUtil.maskCardNumbers(((String[]) map.get("cardOwnerName"))[0]));
                            } else if (map.get("cardOwnerName") instanceof String) {
                                map.put("cardOwnerName", CardNumberUtil.maskCardNumbers((String) map.get("cardOwnerName")));
                            }
                        }
                        if (map.get("bankCardNumber") != null) {
                            map.put("bankCardNumber", StringUtil.EMPTY_STRING);
                            if (map.get("bankCardNumber") instanceof String[]) {
                                map.put("binNumber", BankCardValidator.getBinNumber(((String[]) map.get("bankCardNumber"))[0]));
                            } else if (map.get("bankCardNumber") instanceof String) {
                                map.put("binNumber", BankCardValidator.getBinNumber((String) map.get("bankCardNumber")));
                            }
                        }
                        if (map.get("bankCardExpireMonth") != null) {
                            map.put("bankCardExpireMonth", StringUtil.EMPTY_STRING);
                        }
                        if (map.get("bankCardExpireYear") != null) {
                            map.put("bankCardExpireYear", StringUtil.EMPTY_STRING);
                        }
                        if (map.get("bankCardCvc") != null) {
                            map.put("bankCardCvc", StringUtil.EMPTY_STRING);
                        }
                        data = (new Gson()).toJson(map);
                    } else {
                        Gson gson = new GsonBuilder().setExclusionStrategies(new ExclusionStrategy() {
                            @Override
                            public boolean shouldSkipField(FieldAttributes f) {
                                return f.getName().equals("password") ||
                                        f.getName().equals("oldPassword") ||
                                        f.getName().equals("newPassword") ||
//                                        f.getName().equals("cardHolderName") ||
                                        f.getName().equals("cardOwnerName") ||
                                        f.getName().equals("cardExpireMonth") ||
                                        f.getName().equals("cardExpireYear") ||
                                        f.getName().equals("cardNumber") ||
                                        f.getName().equals("cardCvc") ||
                                        f.getName().equals("cvc") ||
//                                        f.getName().equals("bankCardOwnerName") ||
                                        f.getName().equals("bankCardNumber") ||
                                        f.getName().equals("bankCardExpireMonth") ||
                                        f.getName().equals("bankCardExpireYear") ||
                                        f.getName().equals("bankCardCvc");
                            }

                            @Override
                            public boolean shouldSkipClass(Class<?> clazz) {
                                return false;
                            }
                        }).create();
                        try {
                            data = gson.toJson(requestData);
                        } catch (Throwable t) {
                            data = "GSON HATASI";
//                            mailSenderService.sendErrorMail("ApiRequestLog kaydedilirken JSON convertion hatası alındı!", ExceptionUtils.getFullStackTrace(t));
                        }
                    }
                }

                if (!StringUtil.isNullOrZeroLength(clientIp)) {
                    if (clientIp.length() >= 40) {
                        clientIp = clientIp.substring(0, 39);
                    }
                    clientIp = CardNumberUtil.maskCardNumbers(clientIp);
                }

                data = CardNumberUtil.maskCardNumbers(data);
                orderId = CardNumberUtil.maskCardNumbers(orderId);
                token = CardNumberUtil.maskCardNumbers(token);
                version = CardNumberUtil.maskCardNumbers(version);

                if (!StringUtil.isNullOrZeroLength(orderId)) {
                    if (orderId.length() >= 250) {
                        orderId = orderId.substring(0, 250);
                    }
                }

                if (!StringUtil.isNullOrZeroLength(token)) {
                    if (token.length() >= 250) {
                        token = token.substring(0, 250);
                    }
                }

                if (!StringUtil.isNullOrZeroLength(transactionDate)) {
                    if (transactionDate.length() >= 50) {
                        transactionDate = transactionDate.substring(0, 50);
                    }
                }

                if (!StringUtil.isNullOrZeroLength(version)) {
                    if (version.length() >= 10) {
                        version = version.substring(0, 10);
                    }
                }

                IntegratorApiRequestLog requestLog = new IntegratorApiRequestLog();
                requestLog.setModule(apiLogModuleAndServiceName.getApiLogModule().getValue());
                requestLog.setCurrency(currency);
                requestLog.setLanguage(language);
                requestLog.setMethod(apiLogModuleAndServiceName.getWebServiceId());
                requestLog.setIntegratorId(integratorOrApiUserId);
                requestLog.setRequest(data);
                requestLog.setRequestDate(new Date());
                requestLog.setToken(token);
                requestLog.setTransactionDate(transactionDate);
                requestLog.setVersion(version);
                requestLog.setClientIp(clientIp);
                requestLog.setServerIp(serverIp);
                integratorApiRequestLogDao.persist(requestLog);
            } else {
//                mailSenderService.sendErrorMail("ApiRequestLog kaydedilemedi!", "ApiLogModuleAndServiceName null gönderilmiş!");
            }
        } catch (Exception e) {
            orderId = CardNumberUtil.maskCardNumbers(orderId);
            token = CardNumberUtil.maskCardNumbers(token);
            version = CardNumberUtil.maskCardNumbers(version);
//            mailSenderService.sendErrorMail("ApiRequestLog kaydedilemedi!", "OrderId:" + orderId + " Token: " + token + " Version: " + version + " ClientIp: " + clientIp + " ServerIp: " + serverIp + " <br/> Exception: " + ExceptionUtils.getFullStackTrace(e));
        }
    }

    @Async
    @Override
    public void logApiResponse(Long integratorOrApiUserId, ApiLogModuleAndServiceName apiLogModuleAndServiceName, Object responseData, String orderId, int result, String resultCode, String errorMessage, String returnURL, String requestType) {
        try {
            if (apiLogModuleAndServiceName != null) {
                String data = null;
                if (responseData != null) {
                    if (responseData instanceof String) {
                        data = (String) responseData;
                    } else {
                        try {
                            data = (new Gson()).toJson(responseData);
                        } catch (Throwable t) {
                            data = "GSON HATASI";
//                            mailSenderService.sendErrorMail("ApiResponseLog kaydedilirken JSON convertion hatası alındı!", ExceptionUtils.getFullStackTrace(t));
                        }
                    }
                }

                data = CardNumberUtil.maskCardNumbers(data);
                orderId = CardNumberUtil.maskCardNumbers(orderId);
                returnURL = CardNumberUtil.maskCardNumbers(returnURL);

                if (!StringUtil.isNullOrZeroLength(returnURL)) {
                    if (returnURL.length() >= 1000) {
                        orderId = returnURL.substring(0, 1000);
                    }
                }

                if (!StringUtil.isNullOrZeroLength(orderId)) {
                    if (orderId.length() >= 250) {
                        orderId = orderId.substring(0, 250);
                    }
                }

                IntegratorApiResponseLog responseLog = new IntegratorApiResponseLog();
                responseLog.setModule(apiLogModuleAndServiceName.getApiLogModule().getValue());
                responseLog.setMethod(apiLogModuleAndServiceName.getWebServiceId());
                responseLog.setIntegratorId(integratorOrApiUserId);
                responseLog.setResponse(data);
                responseLog.setResponseDate(new Date());
                responseLog.setResult(result);
                responseLog.setResultCode(resultCode);
                responseLog.setErrorMessage(errorMessage);
                integratorApiResponseLogDao.persist(responseLog);
            } else {
//                mailSenderService.sendErrorMail("ApiResponseLog kaydedilemedi!", "ApiLogModuleAndServiceName null gönderilmiş!");
            }
        } catch (Exception e) {
            System.out.println(" " + e);
//            mailSenderService.sendErrorMail("ApiResponseLog kaydedilemedi!", "errorMessage:" + errorMessage + " resultCode: " + resultCode + " <br/>Exception: " + ExceptionUtils.getFullStackTrace(e));
        }
    }

    @Async
    @Override
    public void logApiCall(ApiCalledServiceName serviceName, String transactionId, String referenceNo, Object request, Date requestDate, Object response, Date responseDate, int result, String responseCode, String responseText, String errorMessage) {
        try {
            if (serviceName != null) {
                Gson gson = new Gson();
                ApiCallLog apiCallLog = new ApiCallLog();
                apiCallLog.setSystem(serviceName.getApiCalledSystem().getValue());
                apiCallLog.setService(serviceName.getServiceId());
                apiCallLog.setTransactionUID(transactionId);
                apiCallLog.setReferenceNo(referenceNo);
                apiCallLog.setRequestDate(requestDate);
                apiCallLog.setRequest(CardNumberUtil.maskCardNumbers(gson.toJson(request)));
                apiCallLog.setResponse(response != null ? CardNumberUtil.maskCardNumbers(gson.toJson(response)) : null);
                apiCallLog.setResponseDate(responseDate);
                apiCallLog.setResult(result);
                apiCallLog.setResponseCode(responseCode);
                apiCallLog.setResponseText(responseText);
                apiCallLog.setErrorMessage(errorMessage);
                apiCallLogDao.persist(apiCallLog);
            } else {
//                mailSenderService.sendErrorMail("ApiCall kaydedilemedi!", "apiCalledSystem veya serviceName null gönderilmiş!");
            }
        } catch (Exception e) {
//            mailSenderService.sendErrorMail("ApiCall kaydedilemedi!", ExceptionUtils.getFullStackTrace(e));
        }
    }
}