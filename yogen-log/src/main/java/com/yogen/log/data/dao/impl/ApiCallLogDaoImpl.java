package com.yogen.log.data.dao.impl;

import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Repository;
import com.yogen.log.data.dao.ApiCallLogDao;
import com.yogen.log.model.ApiCallLog;

@Repository
public class ApiCallLogDaoImpl extends BaseLogHibernateDaoSupport<ApiCallLog, Long> implements ApiCallLogDao {

    @Override
    public List<ApiCallLog> findByCriteria(Integer value, Integer serviceId, String transactionId, String referenceNo, Integer result, Date logStartDate, Date logEndDate) {
        return null;
    }
}
