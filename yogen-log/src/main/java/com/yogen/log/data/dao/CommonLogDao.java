package com.yogen.log.data.dao;

import java.util.Date;
import java.util.List;
import com.yogen.log.model.CommonLog;
import com.yogen.util.enumtype.LogModule;
import com.yogen.util.enumtype.LogPriority;
import com.yogen.util.enumtype.YogenServer;

public interface CommonLogDao extends IBaseLogHibernateDao<CommonLog, Long> {

    List<CommonLog> findByCriteria(Date logStartDate, Date logEndDate, YogenServer yogenServer, LogPriority priority, LogModule module, String txId);
}
