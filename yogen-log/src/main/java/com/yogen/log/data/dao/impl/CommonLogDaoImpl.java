package com.yogen.log.data.dao.impl;

import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Repository;
import com.yogen.log.data.dao.CommonLogDao;
import com.yogen.log.model.CommonLog;
import com.yogen.util.enumtype.LogModule;
import com.yogen.util.enumtype.LogPriority;
import com.yogen.util.enumtype.YogenServer;

@Repository
public class CommonLogDaoImpl extends BaseLogHibernateDaoSupport<CommonLog, Long> implements CommonLogDao {

    @Override
    public List<CommonLog> findByCriteria(Date logStartDate, Date logEndDate, YogenServer yogenServer, LogPriority priority, LogModule module, String txId) {
        return null;
    }
}
