package com.yogen.log.data.service.impl;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.yogen.log.data.service.ApiLogService;
import com.yogen.log.data.service.TransactionalLogService;
import com.yogen.util.enumtype.ApiCalledServiceName;
import com.yogen.util.enumtype.ApiLogModuleAndServiceName;

@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class ApiLogServiceImpl implements ApiLogService {

    @Autowired
    private TransactionalLogService transactionalLogService;

    @Override
    public void logRequest(Long merchantOrApiUserId, ApiLogModuleAndServiceName apiLogModuleAndServiceName, Object requestData, String token, String transactionDate, String version, String currency, String language, String clientIp, String serverIp) {
        transactionalLogService.logApiRequest(merchantOrApiUserId, apiLogModuleAndServiceName, requestData, null, token, transactionDate, version, currency, language, null, clientIp, serverIp);
    }

    @Override
    public void logRequest(Long merchantOrApiUserId, ApiLogModuleAndServiceName apiLogModuleAndServiceName, Object requestData, String orderId, String token, String transactionDate, String version, String currency, String language, String requestType, String clientIp, String serverIp) {
        transactionalLogService.logApiRequest(merchantOrApiUserId, apiLogModuleAndServiceName, requestData, orderId, token, transactionDate, version, currency, language, requestType, clientIp, serverIp);
    }

    @Override
    public void logResponse(Long merchantOrApiUserId, ApiLogModuleAndServiceName apiLogModuleAndServiceName, Object responseData, int result, String resultCode, String errorMessage) {
        transactionalLogService.logApiResponse(merchantOrApiUserId, apiLogModuleAndServiceName, responseData, null, result, resultCode, errorMessage, null, null);
    }

    @Override
    public void logResponse(Long merchantOrApiUserId, ApiLogModuleAndServiceName apiLogModuleAndServiceName, Object responseData, String orderId, int result, String resultCode, String errorMessage, String returnURL, String requestType) {
        transactionalLogService.logApiResponse(merchantOrApiUserId, apiLogModuleAndServiceName, responseData, orderId, result, resultCode, errorMessage, returnURL, requestType);
    }

    @Override
    public void logApiCall(ApiCalledServiceName serviceName, String transactionId, String referenceNo, Object request, Date requestDate, Object response, Date responseDate, int result, String responseCode, String responseText, String errorMessage) {
        transactionalLogService.logApiCall(serviceName, transactionId, referenceNo, request, requestDate, response, responseDate, result, responseCode, responseText, errorMessage);
    }
}