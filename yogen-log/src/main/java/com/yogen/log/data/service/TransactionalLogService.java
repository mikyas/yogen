package com.yogen.log.data.service;

import java.util.Date;
import java.util.List;
import org.springframework.scheduling.annotation.Async;
import com.yogen.log.model.ApiCallLog;
import com.yogen.log.model.CommonLog;
import com.yogen.log.model.dto.ApiRequestLogDTO;
import com.yogen.log.model.dto.ApiResponseLogDTO;
import com.yogen.util.enumtype.ApiCalledServiceName;
import com.yogen.util.enumtype.ApiLogModule;
import com.yogen.util.enumtype.ApiLogModuleAndServiceName;
import com.yogen.util.enumtype.LogActor;
import com.yogen.util.enumtype.LogModule;
import com.yogen.util.enumtype.LogPriority;
import com.yogen.util.enumtype.YogenServer;

public interface TransactionalLogService {
    @Async
    void logSystemOut(Date logDate, LogPriority priority, LogModule module, String className, String methodName, Integer lineNumber, String summaryInfo, String detailInfo, Object data, LogActor logActor, Long actorId, String txId);

    List<CommonLog> findSystemLogsByCriteria(Date logStartDate, Date logEndDate, YogenServer yogenServer, LogPriority priority, LogModule module, String txId);

    List<ApiCallLog> findApiCallLogsByCriteria(Date logStartDate, Date logEndDate, ApiCalledServiceName serviceName, String transactionId, String referenceNo, Integer result);

    List<ApiRequestLogDTO> findApiRequestLogsByCriteria(Date logStartDate, Date logEndDate, ApiLogModule apiLogModule, ApiLogModuleAndServiceName apiLogModuleAndServiceName, Long apiOrIntegratorId, String orderId);

    List<ApiResponseLogDTO> findApiResponseLogsByCriteria(Date logStartDate, Date logEndDate, ApiLogModule apiLogModule, ApiLogModuleAndServiceName apiLogModuleAndServiceName, Long apiOrIntegratorId, String orderId, Integer result, String resultCode);

    @Async
    void logApiRequest(Long integratorOrApiUserId, ApiLogModuleAndServiceName apiLogModuleAndServiceName, Object requestData, String orderId, String token, String transactionDate, String version, String currency, String language, String requestType, String clientIp, String serverIp);

    @Async
    void logApiResponse(Long integratorOrApiUserId, ApiLogModuleAndServiceName apiLogModuleAndServiceName, Object responseData, String orderId, int result, String resultCode, String errorMessage, String returnURL, String requestType);

    @Async
    void logApiCall(ApiCalledServiceName serviceName, String transactionId, String referenceNo, Object request, Date requestDate, Object response, Date responseDate, int result, String responseCode, String responseText, String errorMessage);
}