package com.yogen.log.data.dao.impl;

import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Repository;
import com.yogen.log.data.dao.IntegratorApiResponseLogDao;
import com.yogen.log.model.IntegratorApiResponseLog;
import com.yogen.log.model.dto.ApiResponseLogDTO;

@Repository
public class IntegratorApiResponseLogDaoImpl extends BaseLogHibernateDaoSupport<IntegratorApiResponseLog, Long> implements IntegratorApiResponseLogDao {

    @Override
    public List<ApiResponseLogDTO> findByCriteria(Integer value, Integer webServiceId, Long apiOrMerchantId, Date logStartDate, Date logEndDate, Integer result, String resultCode) {
        return null;
    }
}
