package com.yogen.log.data.service;

import java.util.Date;
import com.yogen.util.enumtype.ApiCalledServiceName;
import com.yogen.util.enumtype.ApiLogModuleAndServiceName;

public interface ApiLogService {

    void logRequest(Long merchantOrApiUserId, ApiLogModuleAndServiceName apiLogModuleAndServiceName, Object requestData, String token, String transactionDate, String version, String currency, String language, String clientIp, String serverIp);

    void logRequest(Long merchantOrApiUserId, ApiLogModuleAndServiceName apiLogModuleAndServiceName, Object requestData, String orderId, String token, String transactionDate, String version, String currency, String language, String requestType, String clientIp, String serverIp);

    void logResponse(Long merchantOrApiUserId, ApiLogModuleAndServiceName apiLogModuleAndServiceName, Object responseData, int result, String resultCode, String errorMessage);

    void logResponse(Long merchantOrApiUserId, ApiLogModuleAndServiceName apiLogModuleAndServiceName, Object responseData, String orderId, int result, String resultCode, String errorMessage, String returnURL, String requestType);

    void logApiCall(ApiCalledServiceName serviceName, String transactionId, String referenceNo, Object request, Date requestDate, Object response, Date responseDate, int result, String responseCode, String responseText, String errorMessage);
}