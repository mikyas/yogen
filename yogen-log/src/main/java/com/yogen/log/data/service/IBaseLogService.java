package com.yogen.log.data.service;

import java.io.Serializable;
import java.util.List;
import com.yogen.log.model.AbstractBaseLogEntity;
import com.yogen.util.exception.YogenDatabaseException;

/**
 * @param <T>
 * @param <ID>
 * @author Tarik.Mikyas
 * <p>
 * Siniflara ait olusturulan is mantigi burada saklanacak. Siniflarda o sinifa ait IS MANTIKLARI bulunmayacak.
 */
public interface IBaseLogService<T extends AbstractBaseLogEntity, ID extends Serializable> {

    /**
     * persists the newInstance object into database
     *
     * @param newInstance new instance to persist
     * @return Primary Key of the newly persisted instance
     */
    ID persist(T newInstance);


    /**
     * finds a persistent object by its primary key
     * <p/>
     * If you aren’t certain there is a persistent instance with the given
     * identifier, use get() and test the return value to see if it’s null.
     *
     * @param id Primary Key of the object to find
     * @return Found object or <code>null</code> if not found
     */
    T get(ID id);


    /**
     * finds a persistent object by its primary key
     * <p/>
     * If you’re certain the persistent
     * object exists, and nonexistence would be considered exceptional,
     * load() is a good option.
     *
     * @param id Primary Key of the object to find
     * @return Found the object in the cache or database or
     * an exception is thrown
     */
    T load(ID id);


    /**
     * finds all persistent objects
     *
     * @return List of found objects
     */
    List<T> findAll();


    /**
     * Try to find an entity by id, if it is not found
     * throw a descriptive exception.
     *
     * @return Entity with id
     * @throws YogenDatabaseException
     */
    ID getIdentifier(T transientObject);
}
