package com.yogen.log.data.service.impl;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.yogen.log.data.dao.CommonLogDao;
import com.yogen.log.data.service.LogService;
import com.yogen.log.data.service.TransactionalLogService;
import com.yogen.log.model.CommonLog;
import com.yogen.util.DateUtil;
import com.yogen.util.ProfileUtil;
import com.yogen.util.enumtype.LogActor;
import com.yogen.util.enumtype.LogModule;
import com.yogen.util.enumtype.LogPriority;

@Service
@Transactional
public class LogServiceImpl implements LogService {

    @Autowired
    private TransactionalLogService transactionalLogService;
//	@Autowired
//	private MailSenderService mailSenderService;

    @Autowired
    CommonLogDao commonLogDao;

    @Override
    public void log(LogPriority priority, LogModule module, String summaryInfo, String detailInfo, Object data, LogActor logActor, Long actorId, String txId) {
        logInternal(priority, module, summaryInfo, detailInfo, data, logActor, actorId, txId);
    }

    @Override
    public void log(LogPriority priority, LogModule module, String summaryInfo) {
        logInternal(priority, module, summaryInfo, null, null, LogActor.SYSTEM, null, null);
    }

    @Override
    public void log(LogPriority priority, LogModule module, String summaryInfo, String detailInfo) {
        logInternal(priority, module, summaryInfo, detailInfo, null, LogActor.SYSTEM, null, null);
    }

    @Override
    public void log(LogPriority priority, LogModule module, String summaryInfo, String detailInfo, Object data) {
        logInternal(priority, module, summaryInfo, detailInfo, data, LogActor.SYSTEM, null, null);
    }

    @Override
    public void log(LogPriority priority, LogModule module, String summaryInfo, String detailInfo, Object data, LogActor logActor, Long actorId) {
        logInternal(priority, module, summaryInfo, detailInfo, data, logActor, actorId, null);
    }

    @Override
    public void fooLog(CommonLog commonLog) {
        commonLogDao.persist(commonLog);
    }

    private void logInternal(LogPriority priority, LogModule module, String summaryInfo, String detailInfo, Object data, LogActor logActor, Long actorId, String txId) {
        try {
            String className = "";
            String methodName = "";
            int lineNumber = 0;
            StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
            int i = 0;
            int errorClassIndex = getErrorClassIndex(stackTraceElements);
            for (StackTraceElement element : stackTraceElements) {
                if (element.getClassName().contains("com.yogen")) {
                    if (i == errorClassIndex) {
                        className = element.getClassName();
                        methodName = element.getMethodName();
                        lineNumber = element.getLineNumber();
                        break;
                    }
                    i++;
                }
            }
            if (ProfileUtil.isLocal()) {
                StringBuilder builder = new StringBuilder();
                builder.append(DateUtil.toMYSQLDateTimeFormat(new Date()) + " " + priority.name() + " ");
                builder.append(module.name() + " " + className + "." + methodName + ":" + lineNumber + "\n");
                builder.append(summaryInfo + ((detailInfo != null) ? ("\n" + detailInfo) : "") + ((data != null) ? ("\n" + data) : ""));
                System.out.println(builder.toString());
            }
            transactionalLogService.logSystemOut(new Date(), priority, module, className, methodName, lineNumber, summaryInfo, detailInfo, data, logActor, actorId, txId);
        } catch (Throwable e) {
//			mailSenderService.sendErrorMail("LogServisinde beklenmedik HATA!", ExceptionUtils.getFullStackTrace(e));
        }
    }

    private int getErrorClassIndex(StackTraceElement[] stackTraceElements) {
        for (StackTraceElement element : stackTraceElements) {
            if (element.getClassName().contains("FastClassBySpringCGLIB")) {
                return 4;
            }
        }
        return 2;
    }
}