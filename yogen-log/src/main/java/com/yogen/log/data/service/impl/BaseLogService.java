package com.yogen.log.data.service.impl;

import java.io.Serializable;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import com.yogen.log.data.dao.IBaseLogHibernateDao;
import com.yogen.log.data.service.IBaseLogService;
import com.yogen.log.model.AbstractBaseLogEntity;


/**
 * @param <T>
 * @param <ID>
 * @author Tarik.Mikyas<br>
 */
@Transactional
public abstract class BaseLogService<T extends AbstractBaseLogEntity, ID extends Serializable> implements IBaseLogService<T, ID> {

    protected IBaseLogHibernateDao<T, ID> getDaoTemplate;

    @Override
    public ID persist(T newInstance) {
        return getDaoTemplate.persist(newInstance);
    }

    @Override
    public T get(ID id) {
        return getDaoTemplate.get(id);
    }

    @Override
    public T load(ID id) {
        return getDaoTemplate.load(id);
    }

    @Override
    public List<T> findAll() {
        return getDaoTemplate.findAll();
    }

    @Override
    public ID getIdentifier(T transientObject) {
        return getDaoTemplate.getIdentifier(transientObject);
    }
}
