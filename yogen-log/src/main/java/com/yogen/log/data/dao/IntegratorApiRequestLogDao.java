package com.yogen.log.data.dao;

import java.util.Date;
import java.util.List;
import com.yogen.log.model.IntegratorApiRequestLog;
import com.yogen.log.model.dto.ApiRequestLogDTO;

public interface IntegratorApiRequestLogDao extends IBaseLogHibernateDao<IntegratorApiRequestLog, Long> {

    List<ApiRequestLogDTO> findByCriteria(Integer value, Integer webServiceId, Long apiOrMerchantId, Date logStartDate, Date logEndDate);
}
