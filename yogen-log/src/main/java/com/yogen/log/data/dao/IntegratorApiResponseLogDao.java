package com.yogen.log.data.dao;

import java.util.Date;
import java.util.List;
import com.yogen.log.model.IntegratorApiResponseLog;
import com.yogen.log.model.dto.ApiResponseLogDTO;

public interface IntegratorApiResponseLogDao extends IBaseLogHibernateDao<IntegratorApiResponseLog, Long> {

    List<ApiResponseLogDTO> findByCriteria(Integer value, Integer webServiceId, Long apiOrMerchantId, Date logStartDate, Date logEndDate, Integer result, String resultCode);
}
