package com.yogen.log.data.dao.impl;

import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Repository;
import com.yogen.log.data.dao.IntegratorApiRequestLogDao;
import com.yogen.log.model.IntegratorApiRequestLog;
import com.yogen.log.model.dto.ApiRequestLogDTO;

@Repository
public class IntegratorApiRequestLogDaoImpl extends BaseLogHibernateDaoSupport<IntegratorApiRequestLog, Long> implements IntegratorApiRequestLogDao {

    @Override
    public List<ApiRequestLogDTO> findByCriteria(Integer value, Integer webServiceId, Long apiOrMerchantId, Date logStartDate, Date logEndDate) {
        return null;
    }
}
