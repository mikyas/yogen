package com.yogen.backoffice.util;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import com.yogen.backoffice.app.scope.ViewScope;
import com.yogen.log.util.YogenLogConfigruation;
import com.yogen.service.util.YogenServiceConfigruation;

@Configuration
@ComponentScan("com.yogen.backoffice")
@Import({YogenServiceConfigruation.class, YogenLogConfigruation.class})
public class YogenBoConfiguration {

    public YogenBoConfiguration() {
        System.out.println("YogenBoConfiguration initialized....");
    }

    @Bean
    public static CustomScopeConfigurer customScopeConfigurer(){
        CustomScopeConfigurer configurer = new CustomScopeConfigurer();
        Map<String, Object> scopes = new HashMap<>();
        scopes.put("view", new ViewScope());
        configurer.setScopes(scopes);
        return configurer;
    }

}
