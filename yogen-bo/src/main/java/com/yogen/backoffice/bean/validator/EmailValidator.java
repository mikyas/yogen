package com.yogen.backoffice.bean.validator;

import java.util.regex.Matcher;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.springframework.stereotype.Component;
import com.yogen.util.validator.ValidatorUtil;

@Component
public class EmailValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (value != null) {
            Matcher matcher = ValidatorUtil.EMAIL_REGEX.matcher((String) value);

            if (!matcher.matches()) {
                FacesMessage message = new FacesMessage();
                message.setDetail("Lütfen geçerli bir E-Posta adresi giriniz.");
                message.setSummary("Lütfen geçerli bir E-Posta adresi giriniz.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(message);
            }
        }
    }
}