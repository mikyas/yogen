package com.yogen.backoffice.bean.validator;

import java.text.ParseException;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.springframework.stereotype.Component;
import com.yogen.backoffice.app.util.FacesContextUtil;

/**
 * Date alanların dinamik validasyonunu sağlamaktadır.
 *
 * @param fieldRequired                        Alan zorunluluğu kontrolünün gerçekleştirilmesi, Opsiyoneldir, gönderilmez ise kontrol yapılmaz. - True / False.
 * @param minValue                             Alan minimum kontrolünün gerçekleştirilmesi. Opsiyoneldir, alan dolu ise kontrol gerçekleştirilir. Integer değer verilmelidir.- "100" vb.
 * @param maxValue                             Alan maksimum kontrolünün gerçekleştirilmesi. Opsiyoneldir, alan dolu ise kontrol gerçekleştirilir. Integer değer verilmelidir.- "152" vb.
 * @param labelNameForMessage                  Hata mesajları için alan adıdır. Zorunludur. - "Tutar" vb.
 * @param doMinValueGreaterThanMaxValueControl İlk tutar son tutardan büyük olamaz kontrolünün gerçekleştirilmesi, Opsiyoneldir, - True / False.
 * @param minValueFieldId                      doMinValueGreaterThanMaxValueControl alanı true olarak gönderildiği durumda bu alan zorunludur, ilk tutar alanının id bilgisi verilmelidir.
 * @param doValueRangeControl                  İlk ve son tutar aralık kontrolünün gerçekleştirilmesi, Opsiyoneldir, gönderilmez ise kontrol yapılmaz. - True / False.
 * @param rangeValue                           doValueRangeControl alanı true olarak gönderildiği durumda bu alan zorunludur, Integer olarak aralık belirtilmelidir. "0.01" vb.
 **/
@Component
public class IntegerValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object object) throws ValidatorException {
        String labelName = getLabelName(component);
        Integer value = null;
        try {
            value = (Integer) object;
        } catch (Exception e) {
            FacesContextUtil.setFacesErrorAndThrowException("Lütfen geçerli bir " + labelName + " giriniz.");
        }

        boolean required = isRequired(component);

        if (required && value == null) {
            FacesContextUtil.setFacesErrorAndThrowException("Lütfen " + labelName + " alanını giriniz.");
        }

        if (value != null) {
            Integer maxValue = null, minValue = null;
            try {
                maxValue = getMaxValue(component);
                minValue = getMinValue(component);
            } catch (ParseException e) {
                FacesContextUtil.setFacesErrorAndThrowException("Parse işleminde beklenmedik hata oluşmuştur.");
            }

            if (maxValue != null && value > maxValue) {
                FacesContextUtil.setFacesErrorAndThrowException(labelName + " alanı " + maxValue + " değerinden büyük olamaz.");
            }

            if (minValue != null && minValue > value) {
                FacesContextUtil.setFacesErrorAndThrowException(labelName + " alanı " + minValue + " değerinden küçük olamaz.");
            }

            boolean doMinValueGreaterThanMaxValueControl = doMinValueGreaterThanMaxValueControl(component);

            if (doMinValueGreaterThanMaxValueControl) {
                minValue = getMinValueFieldValue(component, context);
                if (minValue == null) {
                    FacesContextUtil.setFacesErrorAndThrowException("İlk veri " + labelName + " alanı kontrolü için boş geçilemez.");
                } else if (value < minValue) {
                    FacesContextUtil.setFacesErrorAndThrowException("İlk veri " + labelName + " alanından büyük olamaz.");
                }
            }

            boolean doValueRangeControl = doValueRangeControl(component);

            if (doValueRangeControl) {
                minValue = getMinValueFieldValue(component, context);
                Integer rangeValue = getValueRange(component);
                if (minValue == null) {
                    FacesContextUtil.setFacesErrorAndThrowException("İlk veri " + labelName + " alanı kontrolü için boş geçilemez.");
                } else if (rangeValue == null) {
                    FacesContextUtil.setFacesErrorAndThrowException("Aralık değeri " + labelName + " alanı kontrolü için boş geçilemez.");
                } else if (value - minValue > rangeValue) {
                    FacesContextUtil.setFacesErrorAndThrowException("Aralık en fazla " + labelName + " alanı kontrolü için " + rangeValue + " olabilir.");
                }
            }
        }
    }

    private String getLabelName(UIComponent component) {
        Object attr = component.getAttributes().get("labelNameForMessage");
        if (attr != null) {
            return (String) attr;
        }
        return "???";
    }

    private Integer getMaxValue(UIComponent component) throws ParseException {
        Object attr = component.getAttributes().get("maxValue");
        if (attr != null) {
            return Integer.valueOf(String.valueOf(attr));
        }
        return null;
    }

    private Integer getMinValue(UIComponent component) throws ParseException {
        Object attr = component.getAttributes().get("minValue");
        if (attr != null) {
            return Integer.valueOf(String.valueOf(attr));
        }
        return null;
    }

    private boolean isRequired(UIComponent component) {
        Object attr = component.getAttributes().get("fieldRequired");
        return attr != null && Boolean.parseBoolean((String) attr);
    }

    private boolean doMinValueGreaterThanMaxValueControl(UIComponent component) {
        Object attr = component.getAttributes().get("doMinValueGreaterThanMaxValueControl");
        return attr != null && Boolean.parseBoolean((String) attr);
    }

    private Integer getMinValueFieldValue(UIComponent component, FacesContext context) {
        Object attr = component.getAttributes().get("minValueFieldId");
        if (attr != null) {
            UIInput minValueField = (UIInput) context.getViewRoot().findComponent((String) attr);
            if (minValueField.isValid()) {
                return (Integer) minValueField.getValue();
            }
        }

        return null;
    }

    private boolean doValueRangeControl(UIComponent component) {
        Object attr = component.getAttributes().get("doValueRangeControl");
        return attr != null && Boolean.parseBoolean((String) attr);
    }

    private Integer getValueRange(UIComponent component) {
        Object attr = component.getAttributes().get("rangeValue");
        if (attr != null) {
            return Integer.valueOf(String.valueOf(attr));
        }

        return null;
    }
}