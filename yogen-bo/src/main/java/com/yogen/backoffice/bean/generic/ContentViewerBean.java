package com.yogen.backoffice.bean.generic;

import java.io.Serializable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("view")
public class ContentViewerBean implements Serializable {

    private String content;

    public void preview(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
