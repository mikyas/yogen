package com.yogen.backoffice.bean.dto.view;

public class MechantDefinedViewDTO {

    private String type; // 1 - Geçmiş işlemlere bakılmadan kontrol edilecek kurallar, 2 - Geçmiş işlemlerden referans alınarak kontrol edilecek bir boyutlu kurallar, 3 - Geçmiş işlemlerden referans alınarak kontrol edilecek iki boyutlu kurallar
    private int code;
    private Integer timeBound;
    private Integer limit;
    private String value;
    private String description;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Integer getTimeBound() {
        return timeBound;
    }

    public void setTimeBound(Integer timeBound) {
        this.timeBound = timeBound;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
