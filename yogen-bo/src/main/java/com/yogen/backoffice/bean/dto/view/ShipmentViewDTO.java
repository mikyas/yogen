package com.yogen.backoffice.bean.dto.view;

public class ShipmentViewDTO {

    private String shipmentUID;
    private String firstName;
    private String lastName;
    private String citizenNum;
    private String taxNum;
    private String deliveryCompany;
    private LocationViewDTO location;

    public String getShipmentUID() {
        return shipmentUID;
    }

    public void setShipmentUID(String shipmentUID) {
        this.shipmentUID = shipmentUID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCitizenNum() {
        return citizenNum;
    }

    public void setCitizenNum(String citizenNum) {
        this.citizenNum = citizenNum;
    }

    public String getTaxNum() {
        return taxNum;
    }

    public void setTaxNum(String taxNum) {
        this.taxNum = taxNum;
    }

    public String getDeliveryCompany() {
        return deliveryCompany;
    }

    public void setDeliveryCompany(String deliveryCompany) {
        this.deliveryCompany = deliveryCompany;
    }

    public LocationViewDTO getLocation() {
        return location;
    }

    public void setLocation(LocationViewDTO location) {
        this.location = location;
    }
}
