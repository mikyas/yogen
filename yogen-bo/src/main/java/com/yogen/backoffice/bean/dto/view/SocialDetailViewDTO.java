package com.yogen.backoffice.bean.dto.view;

public class SocialDetailViewDTO {
    private String socialDetailUID;
    private String network;
    private String publicUserName;
    private String communityScore;
    private String accountUrl;
    private String mail;
    private String bio;
    private String following;
    private String followed;
    private String postCount;

    public String getSocialDetailUID() {
        return socialDetailUID;
    }

    public void setSocialDetailUID(String socialDetailUID) {
        this.socialDetailUID = socialDetailUID;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getPublicUserName() {
        return publicUserName;
    }

    public void setPublicUserName(String publicUserName) {
        this.publicUserName = publicUserName;
    }

    public String getCommunityScore() {
        return communityScore;
    }

    public void setCommunityScore(String communityScore) {
        this.communityScore = communityScore;
    }

    public String getAccountUrl() {
        return accountUrl;
    }

    public void setAccountUrl(String accountUrl) {
        this.accountUrl = accountUrl;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getFollowing() {
        return following;
    }

    public void setFollowing(String following) {
        this.following = following;
    }

    public String getFollowed() {
        return followed;
    }

    public void setFollowed(String followed) {
        this.followed = followed;
    }

    public String getPostCount() {
        return postCount;
    }

    public void setPostCount(String postCount) {
        this.postCount = postCount;
    }
}
