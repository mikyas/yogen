package com.yogen.backoffice.bean.converter;

import com.yogen.backoffice.bean.dto.view.RuleViewDTO;
import com.yogen.util.StringUtil;
import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;
import org.springframework.stereotype.Component;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

@Component
public class RuleLabelConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String text) {
        DualListModel<RuleViewDTO> model = (DualListModel<RuleViewDTO>) ((PickList) component).getValue();
        for (RuleViewDTO ruleView : model.getSource()) {
            if ((ruleView.getSystemRuleCode().toString()).equalsIgnoreCase(text)) {
                return ruleView;
            }
        }
        for (RuleViewDTO ruleView : model.getTarget()) {
            if ((ruleView.getSystemRuleCode().toString()).equalsIgnoreCase(text)) {
                return ruleView;
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object object) {
        if (object != null) {
            return ((RuleViewDTO) object).getSystemRuleCode().toString();
        }
        return StringUtil.EMPTY_STRING;
    }
}