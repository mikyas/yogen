package com.yogen.backoffice.bean.dto.view;

public class ItemDetailViewDTO {
    private String itemNo;
    private String itemPaidPrice;
    private String sellerNo;
    private String sellerName;

    public String getItemNo() {
        return itemNo;
    }

    public void setItemNo(String itemNo) {
        this.itemNo = itemNo;
    }

    public String getItemPaidPrice() {
        return itemPaidPrice;
    }

    public void setItemPaidPrice(String itemPaidPrice) {
        this.itemPaidPrice = itemPaidPrice;
    }

    public String getSellerNo() {
        return sellerNo;
    }

    public void setSellerNo(String sellerNo) {
        this.sellerNo = sellerNo;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }
}
