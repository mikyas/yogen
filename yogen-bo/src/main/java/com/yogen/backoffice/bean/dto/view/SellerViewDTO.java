package com.yogen.backoffice.bean.dto.view;

public class SellerViewDTO {

    private String sellerUID;
    private String name;
    private LocationViewDTO location;

    public String getSellerUID() {
        return sellerUID;
    }

    public void setSellerUID(String sellerUID) {
        this.sellerUID = sellerUID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocationViewDTO getLocation() {
        return location;
    }

    public void setLocation(LocationViewDTO location) {
        this.location = location;
    }
}
