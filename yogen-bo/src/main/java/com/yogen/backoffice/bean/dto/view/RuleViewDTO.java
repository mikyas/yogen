package com.yogen.backoffice.bean.dto.view;

import com.yogen.util.enumtype.RuleType;
import com.yogen.util.enumtype.TimePeriod;

import java.io.Serializable;

public class RuleViewDTO implements Serializable {
    private Long id;
    private RuleType type; // 1 - Geçmiş işlemlere bakılmadan kontrol edilecek kurallar, 2 - Geçmiş işlemlerden referans alınarak kontrol edilecek bir boyutlu kurallar, 3 - Geçmiş işlemlerden referans alınarak kontrol edilecek iki boyutlu kurallar
    private Integer systemRuleCode;
    private String description;
    private TimePeriod timePeriod;
    private Integer timeBound;
    private Integer limit;
    private String value;
    private Integer riskScore;

    public RuleViewDTO() {
    }

    public RuleViewDTO(RuleType type, Integer systemRuleCode, String description, TimePeriod timePeriod, Integer timeBound, Integer limit, String value, Integer riskScore) {
        this.type = type;
        this.systemRuleCode = systemRuleCode;
        this.description = description;
        this.timePeriod = timePeriod;
        this.timeBound = timeBound;
        this.limit = limit;
        this.value = value;
        this.riskScore = riskScore;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RuleType getType() {
        return type;
    }

    public void setType(RuleType type) {
        this.type = type;
    }

    public Integer getSystemRuleCode() {
        return systemRuleCode;
    }

    public void setSystemRuleCode(Integer systemRuleCode) {
        this.systemRuleCode = systemRuleCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TimePeriod getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(TimePeriod timePeriod) {
        this.timePeriod = timePeriod;
    }

    public Integer getTimeBound() {
        return timeBound;
    }

    public void setTimeBound(Integer timeBound) {
        this.timeBound = timeBound;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getRiskScore() {
        return riskScore;
    }

    public void setRiskScore(Integer riskScore) {
        this.riskScore = riskScore;
    }
}
