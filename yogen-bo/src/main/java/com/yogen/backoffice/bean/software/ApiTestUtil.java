package com.yogen.backoffice.bean.software;

import com.yogen.service.util.TLS12RestTemplate;
import com.yogen.util.DateUtil;
import com.yogen.util.StringUtil;
import org.apache.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Date;


class ApiTestUtil {

    private static String publicKey= "AzQasdW1rsGy";// LOCAL
    private static String privateKey="F4rK";// LOCAL
    private static String apiUrl = "http://localhost:8083/";


    static String sendRequest(Object requestEntity, String path, String[] hashParamValues) {
        RestTemplate restTemplate = new TLS12RestTemplate();
        String url = apiUrl + path;
        String transactionDate = DateUtil.toMYSQLDateTimeFormat(new Date());
        String token = null;
        try {
            token = StringUtil.getSHA1Text(privateKey + prepareHashValues(hashParamValues) + transactionDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String header = publicKey + ":" + token;

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.add("token", header);
        headers.add("transactionDate", transactionDate);
        headers.add("version", "1.0");

        RequestEntity entity = new RequestEntity<>(requestEntity, headers, HttpMethod.POST, UriComponentsBuilder.fromHttpUrl(url).build().toUri());

        ResponseEntity<String> responseEntity = restTemplate.exchange(entity, String.class);
        return responseEntity.getBody();
//        return null;
    }

    private static String prepareHashValues(String[] hashValues) {
        String hashParams = "";
        if (hashValues != null) {
            for (String s : hashValues) {
                if (!StringUtil.isNullOrZeroLength(s)) {
                    hashParams += s;
                } else {
                    hashParams += StringUtil.EMPTY_STRING;
                }
            }
        } else {
            return StringUtil.EMPTY_STRING;
        }

        return hashParams;
    }
}