package com.yogen.backoffice.bean.dto.view;

import com.yogen.factory.model.dto.RuleExecuteResult;

import java.util.List;

public class ResultViewDTO {

    private String uid;
    private List<RuleExecuteResult> resultList;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public List<RuleExecuteResult> getResultList() {
        return resultList;
    }

    public void setResultList(List<RuleExecuteResult> resultList) {
        this.resultList = resultList;
    }
}
