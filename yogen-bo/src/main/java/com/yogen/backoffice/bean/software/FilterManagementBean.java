package com.yogen.backoffice.bean.software;

import com.yogen.backoffice.bean.dto.view.RuleViewDTO;
import com.yogen.factory.data.service.DefinedRuleService;
import com.yogen.factory.model.dto.DefinedRuleDTO;
import com.yogen.integrator.data.service.IntegratorService;
import com.yogen.util.enumtype.SystemRule;
import com.yogen.util.enumtype.TimePeriod;
import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;


@Component
@Scope("view")
public class FilterManagementBean implements Serializable, InitializingBean {

    @Autowired
    private transient DefinedRuleService definedRuleService;

    @Autowired
    private transient IntegratorService integratorService;

    private DualListModel<RuleViewDTO> rulesDualList;
    private Long testIntegratorId;
    private List<SelectItem> timePeriods;
    private List<RuleViewDTO> definedRules;
    private RuleViewDTO updateDefinedRuleViewDTO;
    private Integer integratorRiskScoreLimit;


    @Override
    public void afterPropertiesSet() {
        init();
    }

    private void init() {
        try {
            testIntegratorId = 1L;
            integratorRiskScoreLimit = integratorService.getRiskScoreLimitByIntegratorId(testIntegratorId);
            updateDefinedRuleViewDTO = new RuleViewDTO();
            this.timePeriods = new ArrayList<>();
            definedRules = initializeRuleViewDTObyDefineRuleDTO(definedRuleService.getDefinedRuleDTOListByIntegratorId(testIntegratorId));
            for (TimePeriod timePeriod : TimePeriod.values()) {
                this.timePeriods.add(new SelectItem(timePeriod, timePeriod.getText()));
            }
            //rulesTarget = ruleService.getDefinedRuleListByIntegratorId(testIntegratorId);
            List<RuleViewDTO> rulesSource = new ArrayList<>();
            List<RuleViewDTO> rulesTarget = new ArrayList<>();

            for (SystemRule systemRule : SystemRule.values()) {
                if (definedRules.stream().noneMatch(rule -> Objects.equals(systemRule.getRuleCode(), rule.getSystemRuleCode()))) { // eger entegratore daha once tanimlanmamis kuralsa source a eklenir
                    RuleViewDTO ruleViewDTO = new RuleViewDTO();
                    ruleViewDTO.setSystemRuleCode(systemRule.getRuleCode());
                    ruleViewDTO.setDescription(systemRule.getDescription());
                    ruleViewDTO.setType(systemRule.getRuleType());
                    rulesSource.add(ruleViewDTO);
                }
            }
            rulesSource.sort(Comparator.comparing(RuleViewDTO::getSystemRuleCode));
            definedRules.sort(Comparator.comparing(RuleViewDTO::getSystemRuleCode));

            rulesDualList = new DualListModel<>(rulesSource, rulesTarget);
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Sayfa Yüklenirken Hata Oluştu!", "HATA :" + e.getMessage()));
        }
    }

    private List<RuleViewDTO> initializeRuleViewDTObyDefineRuleDTO(List<DefinedRuleDTO> definedRuleList) {
        List<RuleViewDTO> definedRuleViewDOS = new ArrayList<>();
        try {
            for (DefinedRuleDTO definedRuleDTO : definedRuleList) {
                SystemRule systemRule = SystemRule.getSystemRuleFromCode(definedRuleDTO.getSystemRuleCode());
                RuleViewDTO ruleViewDTO = new RuleViewDTO();
                ruleViewDTO.setId(definedRuleDTO.getId());
                ruleViewDTO.setSystemRuleCode(systemRule.getRuleCode());
                ruleViewDTO.setType(systemRule.getRuleType());
                ruleViewDTO.setDescription(systemRule.getDescription());
                ruleViewDTO.setTimePeriod(definedRuleDTO.getTimePeriod());
                ruleViewDTO.setTimeBound(definedRuleDTO.getTimeBound());
                ruleViewDTO.setLimit(definedRuleDTO.getLimit());
                ruleViewDTO.setValue(definedRuleDTO.getValue());
                ruleViewDTO.setRiskScore(definedRuleDTO.getRiskScore());
                definedRuleViewDOS.add(ruleViewDTO);
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Tanımlı kurallar getirilirken hata oluştu!", "HATA :" + e.getMessage()));
        }

        return definedRuleViewDOS;
    }

    public void saveDefinedRules() {
        try {
            definedRuleService.createDefinedRulesByDTOs(initializeDefinedRulesDTOByViewDTO(rulesDualList.getTarget()));
            definedRules.addAll(rulesDualList.getTarget());
            rulesDualList.setTarget(new ArrayList<>());
            definedRules.sort(Comparator.comparing(RuleViewDTO::getSystemRuleCode));
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Kurallar entegrator için başarılı bir şekilde kaydedildi.", "Kurallar entegrator için başarılı bir şekilde kaydedildi."));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Kuralların tanımlanma işleminde hata alındı!", "HATA :" + e.getMessage()));
        }

    }

    private List<DefinedRuleDTO> initializeDefinedRulesDTOByViewDTO(List<RuleViewDTO> definedRules) {
        List<DefinedRuleDTO> definedRuleDTOs = new ArrayList<>();
        for (RuleViewDTO viewDTO : definedRules) {
            DefinedRuleDTO definedRuleDTO = initializeDefinedRule(viewDTO, testIntegratorId);
            definedRuleDTOs.add(definedRuleDTO);
        }
        return definedRuleDTOs;
    }


    public void deleteDefinedRule(Integer systemRuleCode) {
        try {
            RuleViewDTO deletedDefinedRule = definedRules.stream().filter(rule -> Objects.equals(systemRuleCode, rule.getSystemRuleCode())).findAny().orElse(null);
            definedRules.removeIf((RuleViewDTO rule) -> Objects.equals(systemRuleCode, rule.getSystemRuleCode()));
            if (deletedDefinedRule != null) {
                rulesDualList.getSource().add(deletedDefinedRule);
                rulesDualList.getSource().sort(Comparator.comparing(RuleViewDTO::getSystemRuleCode));
                definedRuleService.deleteDefinedRuleByDTO(deletedDefinedRule.getId());
            }
            definedRules.sort(Comparator.comparing(RuleViewDTO::getSystemRuleCode));
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Kural silme işlemi başarılı.", "Kural silme işlemi başarılı."));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Kural silme işleminde hata oluştu!", "HATA :" + e.getMessage()));
        }
    }

    public void initializeUpdateRule(Integer systemRuleCode) {
        try {
            updateDefinedRuleViewDTO = definedRules.stream().filter(rule -> Objects.equals(systemRuleCode, rule.getSystemRuleCode())).findAny().orElse(null);
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Kural güncelleme işleminde hata oluştu!", "HATA :" + e.getMessage()));
        }
    }

    public void updateDefinedRule() {
        try {
            DefinedRuleDTO definedRuleDTO = initializeDefinedRule(updateDefinedRuleViewDTO, testIntegratorId);
            definedRules.sort(Comparator.comparing(RuleViewDTO::getSystemRuleCode));
            definedRuleService.updateDefinedRuleByDTO(definedRuleDTO);
            definedRules.sort(Comparator.comparing(RuleViewDTO::getSystemRuleCode));
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Kural güncelleme işlemi başarılı.", "Kural güncelleme işlemi başarılı."));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ürün güncelleme işleminde hata oluştu!", "HATA :" + e.getMessage()));
        }
    }


    private DefinedRuleDTO initializeDefinedRule(RuleViewDTO viewDTO, Long integratorId) {
        DefinedRuleDTO definedRuleDTO = new DefinedRuleDTO();
        definedRuleDTO.setId(viewDTO.getId());
        definedRuleDTO.setIntegratorId(integratorId);
        definedRuleDTO.setRuleType(viewDTO.getType());
        definedRuleDTO.setSystemRuleCode(viewDTO.getSystemRuleCode());
        definedRuleDTO.setDescription(viewDTO.getDescription());
        definedRuleDTO.setTimeBound(viewDTO.getTimeBound());
        definedRuleDTO.setTimePeriod(viewDTO.getTimePeriod());
        definedRuleDTO.setLimit(viewDTO.getLimit());
        definedRuleDTO.setValue(viewDTO.getValue());
        definedRuleDTO.setRiskScore(viewDTO.getRiskScore());
        return definedRuleDTO;
    }

    public DualListModel<RuleViewDTO> getRulesDualList() {
        return rulesDualList;
    }

    public void setRulesDualList(DualListModel<RuleViewDTO> rulesDualList) {
        this.rulesDualList = rulesDualList;
    }

    public List<SelectItem> getTimePeriods() {
        return timePeriods;
    }

    public List<RuleViewDTO> getDefinedRules() {
        return definedRules;
    }

    public void setDefinedRules(List<RuleViewDTO> definedRules) {
        this.definedRules = definedRules;
    }

    public RuleViewDTO getUpdateDefinedRuleViewDTO() {
        return updateDefinedRuleViewDTO;
    }

    public void setUpdateDefinedRuleViewDTO(RuleViewDTO updateDefinedRuleViewDTO) {
        this.updateDefinedRuleViewDTO = updateDefinedRuleViewDTO;
    }

    public Integer getIntegratorRiskScoreLimit() {
        return integratorRiskScoreLimit;
    }

    public void setIntegratorRiskScoreLimit(Integer integratorRiskScoreLimit) {
        this.integratorRiskScoreLimit = integratorRiskScoreLimit;
    }
}
