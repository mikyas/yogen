package com.yogen.backoffice.bean.validator;

import java.lang.reflect.Field;
import java.util.regex.Pattern;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.springframework.stereotype.Component;
import com.yogen.backoffice.app.util.FacesContextUtil;
import com.yogen.util.StringUtil;
import com.yogen.util.validator.ValidatorUtil;

/**
 * String alanların dinamik validasyonunu sağlamaktadır.
 *
 * @param fieldRequired                      Alan zorunluluğu kontrolünün gerçekleştirilmesi, Opsiyoneldir, gönderilmez ise kontrol yapılmaz. - True / False.
 * @param minLength                          Alan minimum uzunluk kontrolünün gerçekleştirilmesi. Opsiyoneldir, alan dolu ise kontrol gerçekleştirilir. - "5" vb.
 * @param maxLength                          Alan maksimum uzunluk kontrolünün gerçekleştirilmesi. Opsiyoneldir, alan dolu ise kontrol gerçekleştirilir. - "20" vb.
 * @param labelNameForMessage                Hata mesajları için alan adıdır. Zorunludur. - "E-posta adresi" vb.
 * @param dontExecuteNormalizeControl        İlgili string alan değeri için normalize kontrolünün yapılmaması. Opsiyoneldir, sadece false olarak gönderilmelidir.
 * @param dontExecuteCardNumbersExistControl İlgili string alan değeri içerisinde kart numarası var mı kontrolünün yapılmaması. Opsiyoneldir, sadece true olarak gönderilmelidir.
 * @param regexPatternName                   ValidatorUtil classında bulunan static Pattern değişken adları girilmelidir, opsiyoneldir.
 **/
@Component
public class StringValidator implements Validator {

    private ValidatorUtil validatorUtil = new ValidatorUtil();

    @Override
    public void validate(FacesContext context, UIComponent component, Object object) throws ValidatorException {
        String value = (String) object;
        Integer maxLength = getMaxLength(component);
        Integer minLength = getMinLength(component);
        boolean required = isRequired(component);
        String labelName = getLabelName(component);
        boolean dontExecuteCardNumbersExistControl = getDontExecuteCardNumbersExistControlParameter(component);
        boolean dontExecuteNormalizeControl = getDontExecuteNormalizeControlParameter(component);

        if (required && StringUtil.isNullOrZeroLength(value)) {
            FacesContextUtil.setFacesErrorAndThrowException("Lütfen " + labelName + " alanını giriniz.");
        }

        if (StringUtil.isNotNullAndNotZeroLength(value)) {
            if (maxLength != null && value.length() > maxLength) {
                FacesContextUtil.setFacesErrorAndThrowException(labelName + " alanı " + maxLength + " karakterden uzun olamaz.");
            }

            if (minLength != null && value.length() < minLength) {
                FacesContextUtil.setFacesErrorAndThrowException(labelName + " alanı " + minLength + " karakterden kısa olamaz.");
            }


            try {
                Pattern regexPattern = getRegexPattern(component);
                if (regexPattern != null && !ValidatorUtil.validateRegex(regexPattern, value)) {
                    FacesContextUtil.setFacesErrorAndThrowException("Lütfen geçerli bir " + labelName + " giriniz.");
                }
            } catch (ValidatorException e) {
                throw new ValidatorException(e.getFacesMessage());
            } catch (Exception e) {
                FacesContextUtil.setFacesErrorAndThrowException("Regex hesaplamasında beklenmedik hata oluşmuştur.");
            }
        }
    }

    private boolean getDontExecuteNormalizeControlParameter(UIComponent component) {
        Object attr = component.getAttributes().get("dontExecuteNormalizeControl");
        return attr != null && Boolean.parseBoolean((String) attr);
    }

    private boolean getDontExecuteCardNumbersExistControlParameter(UIComponent component) {
        Object attr = component.getAttributes().get("dontExecuteCardNumbersExistControl");
        return attr != null && Boolean.parseBoolean((String) attr);
    }

    private String getLabelName(UIComponent component) {
        Object attr = component.getAttributes().get("labelNameForMessage");
        if (attr != null) {
            return (String) attr;
        }
        return "???";
    }

    private Integer getMaxLength(UIComponent component) {
        Object attr = component.getAttributes().get("maxLength");
        if (attr != null) {
            return Integer.parseInt((String) attr);
        }
        return null;
    }

    private Integer getMinLength(UIComponent component) {
        Object attr = component.getAttributes().get("minLength");
        if (attr != null) {
            return Integer.parseInt((String) attr);
        }
        return null;
    }

    private boolean isRequired(UIComponent component) {
        Object attr = component.getAttributes().get("fieldRequired");
        return attr != null && Boolean.parseBoolean((String) attr);
    }

    private Pattern getRegexPattern(UIComponent component) throws NoSuchFieldException, IllegalAccessException {
        Object attr = component.getAttributes().get("regexPatternName");
        if (attr != null) {
            String regexPatternName = (String) attr;
            if (StringUtil.isNotNullAndNotZeroLength(regexPatternName)) {
                Field field = ValidatorUtil.class.getDeclaredField(regexPatternName);
                field.setAccessible(true);
                return (Pattern) field.get(validatorUtil);
            }
        }
        return null;
    }
}