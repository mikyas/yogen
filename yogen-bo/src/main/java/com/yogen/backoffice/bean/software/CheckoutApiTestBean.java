package com.yogen.backoffice.bean.software;

import com.google.gson.Gson;
import com.yogen.backoffice.bean.dto.response.CheckoutResponseViewDTO;
import com.yogen.backoffice.bean.dto.view.*;
import com.yogen.util.enumtype.GenericStatus;
import com.yogen.util.enumtype.ItemType;
import org.primefaces.PrimeFaces;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

@Component
@Scope("view")
public class CheckoutApiTestBean implements Serializable, InitializingBean {

    private CheckoutViewDTO checkoutViewDTO;
    private ItemViewDTO newItemViewDTO;
    private ItemViewDTO updateItemViewDTO;
    private List<ItemViewDTO> itemList;
    private CheckoutResponseViewDTO responseViewDTO;


    @Override
    public void afterPropertiesSet() throws Exception {
        init();
    }

    private void init() {
        itemList = new ArrayList<>();
        checkoutViewDTO = new CheckoutViewDTO();
        initializeCheckoutDto();
        itemList = checkoutViewDTO.getOrder().getItems();
    }

    public void initializeNewItemViewDTO() {
        newItemViewDTO = new ItemViewDTO();
        try {
            ItemViewDTO itemViewDTO = checkoutViewDTO.getOrder().getItems().stream().max(Comparator.comparing(ItemViewDTO::getIndex)).orElse(null);
            newItemViewDTO.setIndex(itemViewDTO == null ? 0 : itemViewDTO.getIndex() + 1);
            newItemViewDTO.setItemUID("IT-" + UUID.randomUUID().toString());
            SellerViewDTO sellerViewDTO = new SellerViewDTO();
            sellerViewDTO.setSellerUID("SLR-" + UUID.randomUUID().toString());
            newItemViewDTO.setSeller(sellerViewDTO);
            newItemViewDTO.getSeller().setLocation(new LocationViewDTO());
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Yeni ürün oluşturulamadı!", "HATA :" + e.getMessage()));

        }
    }

    public void createItem() {
        try {
            itemList.add(newItemViewDTO);
            itemList.sort(Comparator.comparing(ItemViewDTO::getIndex));
            checkoutViewDTO.getOrder().setItems(itemList);
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Yeni ürün oluşturulamadı!", "HATA :" + e.getMessage()));
        }

    }

    public void deleteItem(int index) {
        try {
            itemList.removeIf((ItemViewDTO item) -> item.getIndex() == index);
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Yeni silme işleminde hata oluştu!", "HATA :" + e.getMessage()));
        }

    }

    public void initializeUpdateItem(int index) {
        try {
            updateItemViewDTO = itemList.stream().filter(item -> index == item.getIndex()).findAny().orElse(null);

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ürün güncelleme işleminde hata oluştu!", "HATA :" + e.getMessage()));
        }
    }

    public void updateItem(int index) {
        try {
            itemList.removeIf((ItemViewDTO item) -> item.getIndex() == index);
            itemList.add(updateItemViewDTO);
            itemList.sort(Comparator.comparing(ItemViewDTO::getIndex));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ürün güncelleme işleminde hata oluştu!", "HATA :" + e.getMessage()));
        }
    }

    public void doCheckoutRequest() {
        try {
            String[] hashParamValues = new String[]{
                    checkoutViewDTO.getOrder().getOrderUID(),
                    //checkoutViewDTO.getOrder().getName(),
                    //checkoutViewDTO.getOrder().getCreatedAt()
                    checkoutViewDTO.getIntegratorId(),
                    checkoutViewDTO.getIntegratorIp()
            };
            String url = "api/checkout";
            Long now = System.currentTimeMillis();
            String apiResponse = ApiTestUtil.sendRequest(checkoutViewDTO, url, hashParamValues);
            Long responseTime = System.currentTimeMillis();
            Gson gson = new Gson();
            responseViewDTO = gson.fromJson(apiResponse, CheckoutResponseViewDTO.class);
            if (responseViewDTO.getResult() != GenericStatus.ACTIVE_POSITIVE.getValue().byteValue()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "İstek başarılı sonuçlanmadı!", responseViewDTO.getResponseMessage()));
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "HATA KODU : ", responseViewDTO.getErrorCode() + ""));
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "HATA TANIMI : ", responseViewDTO.getErrorMessage()));
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Cevap Süresi: " + (responseTime - now), "Cevap Süresi: " + (responseTime - now)));
            } else {
                responseViewDTO.setFraudDecisionResultString((responseViewDTO.getFraudDecisionResult() == 1) ? "Sahtekarlık Var" : "Sahtekarlık Yok");
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Cevap Süresi: " + (responseTime - now), "Cevap Süresi: " + (responseTime - now)));
                PrimeFaces current = PrimeFaces.current();
                current.executeScript("PF('apiResponseDialog').show();");
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "İstek yapılırken hata oluştu!", "HATA :" + e.getMessage()));
        }
    }

    private void initializeCheckoutDto() {
        OrderViewDTO orderDTO = new OrderViewDTO();
        orderDTO.setOrderUID("OR-" + UUID.randomUUID().toString());
        orderDTO.setName("Sipariş sepeti 43562");
        orderDTO.setCreatedAt("2017-10-20 12:43:25");
        orderDTO.setUpdatedAt("2017-10-21 15:47:05");
        orderDTO.setClosedAt("2017-10-21 15:47:05");
        //Items
        List<ItemViewDTO> itemDTOList = new ArrayList<>();
        SellerViewDTO sellerDTO = new SellerViewDTO();
        LocationViewDTO sellerLocationDTO = new LocationViewDTO();
        sellerDTO.setSellerUID("SLR-" + UUID.randomUUID().toString());
        sellerDTO.setName("ABC Company");
        sellerLocationDTO.setAddress1("Here is the address1");
        sellerLocationDTO.setAddress2("Here is the address2");
        sellerLocationDTO.setCity("London");
        sellerLocationDTO.setRegion("Massachusetts");
        sellerLocationDTO.setCountry("USA");
        sellerLocationDTO.setPostalCode("02950");
        sellerLocationDTO.setGeohash("qW12STys");
        sellerDTO.setLocation(sellerLocationDTO);
        ItemViewDTO itemDTO1 = new ItemViewDTO();
        itemDTO1.setType("" + ItemType.PHYSICAL.getValue());
        itemDTO1.setIndex(1);
        itemDTO1.setItemUID("IT-" + UUID.randomUUID().toString());
        itemDTO1.setBrand("Nike");
        itemDTO1.setManufacturer("Nike Company");
        itemDTO1.setPrice("111915");
        itemDTO1.setPaidPrice("111915");
        itemDTO1.setTitle("Womens Arch Sweater M 55");
        itemDTO1.setQuantity("2");
        itemDTO1.setSeller(sellerDTO);
        ItemViewDTO itemDTO2 = new ItemViewDTO();
        itemDTO2.setType("" + ItemType.PHYSICAL.getValue());
        itemDTO2.setIndex(2);
        itemDTO2.setItemUID("IT-" + UUID.randomUUID().toString());
        itemDTO2.setBrand("Nike");
        itemDTO2.setManufacturer("Nike Company");
        itemDTO2.setPrice("92000");
        itemDTO2.setPaidPrice("92000");
        itemDTO2.setTitle("Womens Yellow Tee M 54");
        itemDTO2.setSeller(sellerDTO);
        itemDTO2.setQuantity("1");
        itemDTOList.add(itemDTO1);
        itemDTOList.add(itemDTO2);
        orderDTO.setItems(itemDTOList);

        //Shipment
        ShipmentViewDTO shipmentDTO = new ShipmentViewDTO();
        shipmentDTO.setShipmentUID("SHP-" + UUID.randomUUID().toString());
        shipmentDTO.setFirstName("Corc");
        shipmentDTO.setLastName("Borc");
        shipmentDTO.setCitizenNum("12312312312");
        shipmentDTO.setTaxNum("1231231231");
        shipmentDTO.setDeliveryCompany("Cipetpet Kargo");
        LocationViewDTO shipmentLocationDTO = new LocationViewDTO();
        shipmentLocationDTO.setAddress1("Here is the address1");
        shipmentLocationDTO.setAddress2("Here is the address1");
        shipmentLocationDTO.setCity("Istanbul");
        shipmentLocationDTO.setGeohash("LkQws12s1");
        shipmentLocationDTO.setRegion("Massachusetts");
        shipmentLocationDTO.setCountry("USA");
        shipmentLocationDTO.setPostalCode("02950");
        shipmentDTO.setLocation(shipmentLocationDTO);
        orderDTO.setShipment(shipmentDTO);

        //Billing
        BillingViewDTO billingDTO = new BillingViewDTO();
        billingDTO.setBillingUID("BLL-" + UUID.randomUUID().toString());
        billingDTO.setFirstName("Corc");
        billingDTO.setLastName("Borc");
        billingDTO.setCitizenNum("12312312312");
        billingDTO.setTaxNum("1231231231");
        billingDTO.setSerialNum("SN-123456");
        LocationViewDTO billingLocationDTO = new LocationViewDTO();
        billingLocationDTO.setAddress1("Here is the address1");
        billingLocationDTO.setAddress2("Here is the address1");
        billingLocationDTO.setCity("Boston");
        billingLocationDTO.setGeohash("LkQws12s1");
        billingLocationDTO.setRegion("Massachusetts");
        billingLocationDTO.setCountry("USA");
        billingLocationDTO.setPostalCode("02950");
        billingDTO.setLocation(billingLocationDTO);
        orderDTO.setBilling(billingDTO);

        //Purcharser
        PurchaserViewDTO purchaserDTO = new PurchaserViewDTO();
        purchaserDTO.setPurchaserUID("PRC-" + UUID.randomUUID().toString());
        purchaserDTO.setFirstName("Mehmet");
        purchaserDTO.setLastName("Yılmaz");
        purchaserDTO.setMail("myilmaz@mail.com");
        purchaserDTO.setPhone("05321234567");
        purchaserDTO.setCitizenNum("12345678901");
        purchaserDTO.setGender("M");
        purchaserDTO.setDateOfBirth("1985-10-21");
        //Social Details under Purchaser
        SocialDetailViewDTO socialDetailViewDTO = new SocialDetailViewDTO();
        socialDetailViewDTO.setSocialDetailUID("SOC-" + UUID.randomUUID().toString());
        socialDetailViewDTO.setNetwork("Facebook");
        socialDetailViewDTO.setPublicUserName("mehmetyilmaz");
        socialDetailViewDTO.setCommunityScore("78");
        //socialDetailViewDTO.setAccountUrl("www.facebook.com");
        socialDetailViewDTO.setMail("myilmaz@mail.com");
        socialDetailViewDTO.setBio("insan");
        socialDetailViewDTO.setFollowing("500");
        socialDetailViewDTO.setFollowed("600");
        socialDetailViewDTO.setPostCount("2000");
        purchaserDTO.setSocialDetail(socialDetailViewDTO);
        orderDTO.setPurchaser(purchaserDTO);

        //PaymentDetail
        PaymentDetailViewDTO paymentDetailDTO = new PaymentDetailViewDTO();
        paymentDetailDTO.setPaymentDetailUID("PAY-" + UUID.randomUUID().toString());
        paymentDetailDTO.setType("CARD");
        paymentDetailDTO.setInstallment("3");
        paymentDetailDTO.setTotalPrice("120015");
        paymentDetailDTO.setTotalPaidPrice("118000");
        paymentDetailDTO.setCitizenNum("56321478512");
        paymentDetailDTO.setCurrency("GBP");
        paymentDetailDTO.setCreditCardBin("123123");
        paymentDetailDTO.setCreditCardLastFourDigit("1234");
        paymentDetailDTO.setCreditCardCompany("TROY");
        paymentDetailDTO.setIsThreeD("1");
        paymentDetailDTO.setThreeDInquiryResult("1");
        paymentDetailDTO.setEciCode("02");
        paymentDetailDTO.setCavvCode("2");
        paymentDetailDTO.setMdStatus("6");
        paymentDetailDTO.setAvsResultCode("A");
        paymentDetailDTO.setCvvResultCode("M");
        orderDTO.setPaymentDetail(paymentDetailDTO);

        ContactViewDTO contactViewDTO = new ContactViewDTO();
        contactViewDTO.setContactUID("CONT-" + UUID.randomUUID().toString());
        contactViewDTO.setType("type");
        contactViewDTO.setEmail("myilmaz@mail.com");
        contactViewDTO.setPhone("05321234567");
        //contactViewDTO.setAccountUrl("www.mehmetyilmaz.com");
        orderDTO.setContact(contactViewDTO);

        DeviceViewDTO deviceViewDTO = new DeviceViewDTO();
        deviceViewDTO.setDeviceUID("DEV-" + UUID.randomUUID().toString());
        deviceViewDTO.setType("Desktop");
        deviceViewDTO.setOs("Linux");
        deviceViewDTO.setManufacturer("Lenova");
        deviceViewDTO.setModel("Thinkpad");
        deviceViewDTO.setIpAddress("192.168.1.1");
        orderDTO.setDevice(deviceViewDTO);

        checkoutViewDTO.setIntegratorId("001qswAP2Z+xPL/3Bu4icZI1g==");
        checkoutViewDTO.setIntegratorIp("192.168.1.1");
        checkoutViewDTO.setOrder(orderDTO);
    }

    public CheckoutViewDTO getCheckoutViewDTO() {
        return checkoutViewDTO;
    }

    public void setCheckoutViewDTO(CheckoutViewDTO checkoutViewDTO) {
        this.checkoutViewDTO = checkoutViewDTO;
    }

    public ItemViewDTO getNewItemViewDTO() {
        return newItemViewDTO;
    }

    public void setNewItemViewDTO(ItemViewDTO newItemViewDTO) {
        this.newItemViewDTO = newItemViewDTO;
    }

    public ItemViewDTO getUpdateItemViewDTO() {
        return updateItemViewDTO;
    }

    public void setUpdateItemViewDTO(ItemViewDTO updateItemViewDTO) {
        this.updateItemViewDTO = updateItemViewDTO;
    }

    public CheckoutResponseViewDTO getResponseViewDTO() {
        return responseViewDTO;
    }

    public void setResponseViewDTO(CheckoutResponseViewDTO responseViewDTO) {
        this.responseViewDTO = responseViewDTO;
    }

}
