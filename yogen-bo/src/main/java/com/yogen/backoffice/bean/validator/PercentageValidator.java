package com.yogen.backoffice.bean.validator;

import java.text.ParseException;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.springframework.stereotype.Component;
import com.yogen.backoffice.app.util.FacesContextUtil;
import com.yogen.util.NumberUtil;

/**
 * Date alanların dinamik validasyonunu sağlamaktadır.
 *
 * @param fieldRequired       Alan zorunluluğu kontrolünün gerçekleştirilmesi, Opsiyoneldir, gönderilmez ise kontrol yapılmaz. - True / False.
 * @param minValue            Alan minimum kontrolünün gerçekleştirilmesi. Opsiyoneldir, alan dolu ise kontrol gerçekleştirilir. Double değer verilmelidir.- "100.03" vb.
 * @param maxValue            Alan maksimum kontrolünün gerçekleştirilmesi. Opsiyoneldir, alan dolu ise kontrol gerçekleştirilir. Double değer verilmelidir.- "152.02" vb.
 * @param labelNameForMessage Hata mesajları için alan adıdır. Zorunludur. - "Tutar" vb.
 **/
@Component
public class PercentageValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object object) throws ValidatorException {
        String labelName = getLabelName(component);
        Double value = null;
        try {
            value = (Double) object;
        } catch (Exception e) {
            FacesContextUtil.setFacesErrorAndThrowException("Lütfen geçerli bir " + labelName + " giriniz.");
        }

        boolean required = isRequired(component);

        if (required && value == null) {
            FacesContextUtil.setFacesErrorAndThrowException("Lütfen " + labelName + " alanını giriniz.");
        }

        if (value != null) {
            Double maxValue = null, minValue = null;
            try {
                maxValue = getMaxValue(component);
                minValue = getMinValue(component);
            } catch (ParseException e) {
                FacesContextUtil.setFacesErrorAndThrowException("Parse işleminde beklenmedik hata oluşmuştur.");
            }

            if (maxValue != null && value > maxValue) {
                FacesContextUtil.setFacesErrorAndThrowException(labelName + " alanı " + maxValue + " değerinden büyük olamaz.");
            }

            if (minValue != null && minValue > value) {
                FacesContextUtil.setFacesErrorAndThrowException(labelName + " alanı " + minValue + " değerinden küçük olamaz.");
            }

            if (value < 0 || value > 100) {
                FacesContextUtil.setFacesErrorAndThrowException(labelName + " alanı en az 0 en fazla 100 olabilir.");
            }

            if (NumberUtil.roundAsDecimalFormat(value) != value) {
                FacesContextUtil.setFacesErrorAndThrowException(labelName + " alanındaki değerde küsürat 2 haneli olmalıdır.");
            }
        }
    }

    private String getLabelName(UIComponent component) {
        Object attr = component.getAttributes().get("labelNameForMessage");
        if (attr != null) {
            return (String) attr;
        }
        return "???";
    }

    private Double getMaxValue(UIComponent component) throws ParseException {
        Object attr = component.getAttributes().get("maxValue");
        if (attr != null) {
            return Double.parseDouble(attr.toString());
        }
        return null;
    }

    private Double getMinValue(UIComponent component) throws ParseException {
        Object attr = component.getAttributes().get("minValue");
        if (attr != null) {
            return Double.parseDouble(attr.toString());
        }
        return null;
    }

    private boolean isRequired(UIComponent component) {
        Object attr = component.getAttributes().get("fieldRequired");
        return attr != null && Boolean.parseBoolean((String) attr);
    }
}