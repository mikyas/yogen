package com.yogen.backoffice.bean.validator;

/**
 * Sadece Faces desteği olmayan login sayfası tarafından kullanılmaktadır.
 * Faces desteği olan diğer sayfalarda <p:capctha/> componenti kullanılmalıdır. Bu componentin ayarları web.xml dosyasındadır.
 **/
public class RecaptchaValidator {

//    private static final String GOOGLE_SITE_VERIFICATION_URL = "https://www.google.com/recaptcha/api/siteverify";
//    private static final String RECAPTCHA_SECRET_KEY = "6LdC8R8UAAAAANhJ32tTLtMuXy5q-Ogqr16pCCkz";
//    private final static String USER_AGENT = "Mozilla/5.0";
//
//    public static boolean validate(HttpServletRequest request) throws IOException {
//        String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
//        if (gRecaptchaResponse == null || "".equals(gRecaptchaResponse)) {
//            return false;
//        }
//
//        try {
//            URL obj = new URL(null, GOOGLE_SITE_VERIFICATION_URL, new Handler());
//            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
//            con.setRequestMethod("POST");
//            con.setRequestProperty("User-Agent", USER_AGENT);
//            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
//            con.setDoOutput(true);
//
//            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
//            wr.writeBytes("secret=" + RECAPTCHA_SECRET_KEY + "&response=" + gRecaptchaResponse);
//            wr.flush();
//            wr.close();
//
//            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
//            String inputLine;
//            StringBuilder response = new StringBuilder();
//
//            while ((inputLine = in.readLine()) != null) {
//                response.append(inputLine);
//            }
//            in.close();
//
//            JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(response.toString());
//
//            return jsonObject.getBoolean("success");
//        } catch (Exception e) {
//            return false;
//        }
//    }
}