package com.yogen.backoffice.bean.dto.view;

public class PaymentDetailViewDTO {

    private String paymentDetailUID;
    private String type;
    private String installment;
    private String totalPrice;
    private String totalPaidPrice;
    private String citizenNum;
    private String currency;
    private String creditCardBin;
    private String creditCardLastFourDigit;
    private String creditCardCompany;
    private String isThreeD;
    private String threeDInquiryResult;
    private String eciCode;
    private String cavvCode;
    private String mdStatus;
    private String avsResultCode;
    private String cvvResultCode;

    public String getPaymentDetailUID() {
        return paymentDetailUID;
    }

    public void setPaymentDetailUID(String paymentDetailUID) {
        this.paymentDetailUID = paymentDetailUID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getTotalPaidPrice() {
        return totalPaidPrice;
    }

    public void setTotalPaidPrice(String totalPaidPrice) {
        this.totalPaidPrice = totalPaidPrice;
    }

    public String getCitizenNum() {
        return citizenNum;
    }

    public void setCitizenNum(String citizenNum) {
        this.citizenNum = citizenNum;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCreditCardBin() {
        return creditCardBin;
    }

    public void setCreditCardBin(String creditCardBin) {
        this.creditCardBin = creditCardBin;
    }

    public String getCreditCardLastFourDigit() {
        return creditCardLastFourDigit;
    }

    public void setCreditCardLastFourDigit(String creditCardLastFourDigit) {
        this.creditCardLastFourDigit = creditCardLastFourDigit;
    }

    public String getCreditCardCompany() {
        return creditCardCompany;
    }

    public void setCreditCardCompany(String creditCardCompany) {
        this.creditCardCompany = creditCardCompany;
    }

    public String getIsThreeD() {
        return isThreeD;
    }

    public void setIsThreeD(String isThreeD) {
        this.isThreeD = isThreeD;
    }

    public String getThreeDInquiryResult() {
        return threeDInquiryResult;
    }

    public void setThreeDInquiryResult(String threeDInquiryResult) {
        this.threeDInquiryResult = threeDInquiryResult;
    }

    public String getEciCode() {
        return eciCode;
    }

    public void setEciCode(String eciCode) {
        this.eciCode = eciCode;
    }

    public String getCavvCode() {
        return cavvCode;
    }

    public void setCavvCode(String cavvCode) {
        this.cavvCode = cavvCode;
    }

    public String getMdStatus() {
        return mdStatus;
    }

    public void setMdStatus(String mdStatus) {
        this.mdStatus = mdStatus;
    }

    public String getAvsResultCode() {
        return avsResultCode;
    }

    public void setAvsResultCode(String avsResultCode) {
        this.avsResultCode = avsResultCode;
    }

    public String getCvvResultCode() {
        return cvvResultCode;
    }

    public void setCvvResultCode(String cvvResultCode) {
        this.cvvResultCode = cvvResultCode;
    }
}
