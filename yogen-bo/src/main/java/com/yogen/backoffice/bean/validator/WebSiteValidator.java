package com.yogen.backoffice.bean.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;


@Component
public class WebSiteValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String text = safeToString(value);

        if (StringUtils.hasText(text)) {
            if (!new UrlValidator().isValid(text)) {
                FacesMessage message = new FacesMessage();
                message.setDetail("Lütfen geçerli bir Web Site giriniz.");
                message.setSummary("Lütfen geçerli bir Web Site giriniz.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(message);
            }
        }
    }

    private String safeToString(Object object) {
        return object != null ? object.toString() : null;
    }
}
