package com.yogen.backoffice.bean.dto.view;

import java.util.Date;
import java.util.List;

public class TransactionDetailViewDTO {
    private String integratorId;
    private Date transactionDate;
    private String transactionNo;
    private String orderNo;
    private String orderName;
    private List<ItemDetailViewDTO> items;
    private String billingNo;
    private String billingSeriNo;
    private String shipmentNo;
    private String shipmentTaxNo;
    private String purchaserNo;
    private String purchaserName;
    private String socialDetailNo;
    private String socialDetailUserName;
    private String deviceNo;
    private String deviceIp;
    private String contactNo;
    private String contactUrl;
    private String paymentNo;
    private String totalPaidPrice;
    private String binNo;
    private String cardNo;

    public String getIntegratorId() {
        return integratorId;
    }

    public void setIntegratorId(String integratorId) {
        this.integratorId = integratorId;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public List<ItemDetailViewDTO> getItems() {
        return items;
    }

    public void setItems(List<ItemDetailViewDTO> items) {
        this.items = items;
    }

    public String getBillingNo() {
        return billingNo;
    }

    public void setBillingNo(String billingNo) {
        this.billingNo = billingNo;
    }

    public String getBillingSeriNo() {
        return billingSeriNo;
    }

    public void setBillingSeriNo(String billingSeriNo) {
        this.billingSeriNo = billingSeriNo;
    }

    public String getShipmentNo() {
        return shipmentNo;
    }

    public void setShipmentNo(String shipmentNo) {
        this.shipmentNo = shipmentNo;
    }

    public String getShipmentTaxNo() {
        return shipmentTaxNo;
    }

    public void setShipmentTaxNo(String shipmentTaxNo) {
        this.shipmentTaxNo = shipmentTaxNo;
    }

    public String getPurchaserNo() {
        return purchaserNo;
    }

    public void setPurchaserNo(String purchaserNo) {
        this.purchaserNo = purchaserNo;
    }

    public String getPurchaserName() {
        return purchaserName;
    }

    public void setPurchaserName(String purchaserName) {
        this.purchaserName = purchaserName;
    }

    public String getSocialDetailNo() {
        return socialDetailNo;
    }

    public void setSocialDetailNo(String socialDetailNo) {
        this.socialDetailNo = socialDetailNo;
    }

    public String getSocialDetailUserName() {
        return socialDetailUserName;
    }

    public void setSocialDetailUserName(String socialDetailUserName) {
        this.socialDetailUserName = socialDetailUserName;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public String getDeviceIp() {
        return deviceIp;
    }

    public void setDeviceIp(String deviceIp) {
        this.deviceIp = deviceIp;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getContactUrl() {
        return contactUrl;
    }

    public void setContactUrl(String contactUrl) {
        this.contactUrl = contactUrl;
    }

    public String getPaymentNo() {
        return paymentNo;
    }

    public void setPaymentNo(String paymentNo) {
        this.paymentNo = paymentNo;
    }

    public String getTotalPaidPrice() {
        return totalPaidPrice;
    }

    public void setTotalPaidPrice(String totalPaidPrice) {
        this.totalPaidPrice = totalPaidPrice;
    }

    public String getBinNo() {
        return binNo;
    }

    public void setBinNo(String binNo) {
        this.binNo = binNo;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }
}
