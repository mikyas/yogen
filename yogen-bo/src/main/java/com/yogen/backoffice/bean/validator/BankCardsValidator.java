package com.yogen.backoffice.bean.validator;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.springframework.stereotype.Component;
import com.yogen.backoffice.app.util.FacesContextUtil;
import com.yogen.util.validator.BankCardValidator;

@Component
public class BankCardsValidator implements Validator {

    private static final String USER_MESSAGE = "Lütfen geçerli bir Banka Kartı numarası giriniz.";

    @Override
    public void validate(FacesContext context, UIComponent component, Object object) throws ValidatorException {
        String value = (String) object;

        try {
            if (!BankCardValidator.isCardNumberValid(value, true)) {
                FacesContextUtil.setFacesErrorAndThrowException(USER_MESSAGE);
            }
        } catch (Exception e) {
            FacesContextUtil.setFacesErrorAndThrowException(USER_MESSAGE);
        }
    }
}