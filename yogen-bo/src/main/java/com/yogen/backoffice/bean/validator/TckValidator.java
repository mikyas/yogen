package com.yogen.backoffice.bean.validator;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.springframework.stereotype.Component;
import com.yogen.backoffice.app.util.FacesContextUtil;
import com.yogen.util.StringUtil;
import com.yogen.util.validator.TcCertificateValidator;

@Component
public class TckValidator implements Validator {

    /**
     * String alanların dinamik validasyonunu sağlamaktadır.
     *
     * @param fieldRequired       Alan zorunluluğu kontrolünün gerçekleştirilmesi, Opsiyoneldir, gönderilmez ise kontrol yapılmaz. - True / False.
     * @param labelNameForMessage Hata mesajları için alan adıdır. Zorunludur. - "E-posta adresi" vb.
     **/

    private static final String USER_MESSAGE = "Lütfen geçerli bir T.C. kimlik numarası giriniz.";

    @Override
    public void validate(FacesContext context, UIComponent component, Object object) throws ValidatorException {

        boolean required = isRequired(component);
        String labelName = getLabelName(component);
        String value = (String) object;

        if (required && StringUtil.isNullOrZeroLength(value)) {
            FacesContextUtil.setFacesErrorAndThrowException("Lütfen " + labelName + " alanını giriniz.");
        }
        if (StringUtil.isNotNullAndNotZeroLength(value)) {
            if (!TcCertificateValidator.isValid(value)) {
                FacesContextUtil.setFacesErrorAndThrowException(USER_MESSAGE);
            }
        }
    }


    private boolean isRequired(UIComponent component) {
        Object attr = component.getAttributes().get("fieldRequired");
        return attr != null && Boolean.parseBoolean((String) attr);
    }

    private String getLabelName(UIComponent component) {
        Object attr = component.getAttributes().get("labelNameForMessage");
        if (attr != null) {
            return (String) attr;
        }
        return "???";
    }
}