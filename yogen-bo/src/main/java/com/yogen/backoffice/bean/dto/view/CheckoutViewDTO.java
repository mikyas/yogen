package com.yogen.backoffice.bean.dto.view;

public class CheckoutViewDTO {

    private OrderViewDTO order;
    private String integratorId;
    private String integratorIp;

    public OrderViewDTO getOrder() {
        return order;
    }

    public void setOrder(OrderViewDTO order) {
        this.order = order;
    }

    public String getIntegratorId() {
        return integratorId;
    }

    public void setIntegratorId(String integratorId) {
        this.integratorId = integratorId;
    }

    public String getIntegratorIp() {
        return integratorIp;
    }

    public void setIntegratorIp(String integratorIp) {
        this.integratorIp = integratorIp;
    }
}
