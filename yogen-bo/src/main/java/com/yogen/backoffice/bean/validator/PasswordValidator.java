package com.yogen.backoffice.bean.validator;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.springframework.stereotype.Component;
import com.yogen.backoffice.app.util.FacesContextUtil;

@Component
public class PasswordValidator implements Validator {

    private static final String USER_MESSAGE = "Lütfen 8 ile 20 karakter arasında en az bir harf ve en az bir numerik karakter içeren bir şifre giriniz.";

    @Override
    public void validate(FacesContext context, UIComponent component, Object object) throws ValidatorException {
        String password = (String) object;
        if (!com.yogen.util.validator.PasswordValidator.isPasswordValid(password)) {
            FacesContextUtil.setFacesErrorAndThrowException(USER_MESSAGE);
        }
    }
}