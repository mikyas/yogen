package com.yogen.backoffice.bean.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.springframework.stereotype.Component;
import com.yogen.util.StringUtil;
import com.yogen.util.validator.ValidatorUtil;

@Component
public class ReceiptInformationValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (StringUtil.isNotNullAndNotZeroLength(value.toString()) && !ValidatorUtil.validateRegex(ValidatorUtil.MERCHANT_CUSTOM_RECEIPT_REGEX, value.toString())) {
            FacesMessage message = new FacesMessage();
            message.setDetail("Geçersiz Ekstre Alanı");
            message.setSummary("Geçersiz Ekstre Alanı");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
    }
}