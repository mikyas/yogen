package com.yogen.backoffice.bean.software;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.yogen.backoffice.bean.dto.view.ItemDetailViewDTO;
import com.yogen.backoffice.bean.dto.view.TransactionDetailViewDTO;
import com.yogen.datafeed.data.service.TransactionService;
import com.yogen.datafeed.model.snapshot.AbstractItem;
import com.yogen.datafeed.model.snapshot.Transaction;

@Component
@Scope("view")
public class TransactionDetailsBean implements Serializable, InitializingBean {


    @Autowired
    private transient TransactionService transactionService;

    private transient List<TransactionDetailViewDTO> transactionDetailViewDTOs;
    private Long testIntegratorId;

    @Override
    public void afterPropertiesSet() throws Exception {
        init();
    }

    private void init() {
        this.testIntegratorId = 1L;
    }

    private List<TransactionDetailViewDTO> translateTransactionViewDTO(List<Transaction> transactions) {
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
        List<TransactionDetailViewDTO> viewDTOs = new ArrayList<>();

        for (Transaction transaction : transactions) {
            TransactionDetailViewDTO viewDTO = new TransactionDetailViewDTO();
            viewDTO.setIntegratorId(transaction.getIntegratorId().toString());
            viewDTO.setTransactionDate(transaction.getCreatedDate());
            viewDTO.setTransactionNo(transaction.getTransactionUID());
            viewDTO.setOrderNo(transaction.getOrder().getOrderUID());
            viewDTO.setOrderName(transaction.getOrder().getName());
            viewDTO.setItems(translateItemViewDTO(transaction.getOrder().getItems()));
            viewDTO.setBillingNo(transaction.getOrder().getBilling().getBillingUID());
            viewDTO.setBillingSeriNo(transaction.getOrder().getBilling().getSerialNum());
            viewDTO.setShipmentNo(transaction.getOrder().getShipment().getShipmentUID());
            viewDTO.setShipmentTaxNo(transaction.getOrder().getShipment().getTaxNum());
            viewDTO.setPurchaserNo(transaction.getOrder().getPurchaser().getPurchaserUID());
            viewDTO.setPurchaserName(transaction.getOrder().getPurchaser().getFirstName() + " " + transaction.getOrder().getPurchaser().getLastName());
            if (transaction.getOrder().getPurchaser().getSocialDetail() != null) {
                viewDTO.setSocialDetailNo(transaction.getOrder().getPurchaser().getSocialDetail().getSocialDetailUID());
                viewDTO.setSocialDetailUserName(transaction.getOrder().getPurchaser().getSocialDetail().getPublicUsername());
            }
            if (transaction.getOrder().getPurchaser().getOrder().getDevice() != null) {
                viewDTO.setDeviceNo(transaction.getOrder().getDevice().getDeviceUID());
                viewDTO.setDeviceIp(transaction.getOrder().getDevice().getDeviceUID());
            }

            if (transaction.getOrder().getContact() != null) {
                viewDTO.setContactNo(transaction.getOrder().getContact().getContactUID());
                viewDTO.setContactUrl(transaction.getOrder().getContact().getAccountUrl());
            }
            if (transaction.getOrder().getPaymentDetail() != null) {
                viewDTO.setPaymentNo(transaction.getOrder().getPaymentDetail().getPaymentDetailUID());
                viewDTO.setTotalPaidPrice(transaction.getOrder().getPaymentDetail().getTotalPaidPrice() == null ? "" : transaction.getOrder().getPaymentDetail().getTotalPaidPrice().toString());
                viewDTO.setBinNo(transaction.getOrder().getPaymentDetail().getCreditCardBin());
                viewDTO.setCardNo(transaction.getOrder().getPaymentDetail().getCreditCardNo());
            }
            viewDTOs.add(viewDTO);
        }
        return viewDTOs;

    }

    private List<ItemDetailViewDTO> translateItemViewDTO(List<AbstractItem> items) {
        List<ItemDetailViewDTO> itemViewDTOS = new ArrayList<>();
        for (AbstractItem item : items) {
            ItemDetailViewDTO viewDTO = new ItemDetailViewDTO();
            viewDTO.setItemNo(item.getItemUID());
            viewDTO.setItemPaidPrice(item.getPaidPrice().toString());
            viewDTO.setSellerNo(item.getSeller().getSellerUID());
            viewDTO.setSellerName(item.getSeller().getName());
            itemViewDTOS.add(viewDTO);
        }
        return itemViewDTOS;
    }


    public void listTransactions() {
        try {
            transactionDetailViewDTOs = translateTransactionViewDTO(transactionService.getTransactionListByIntegratorId(testIntegratorId));
        } catch (Exception e) {
            System.out.println(e + "");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Listeleme işleminde hata oluştu!", "HATA :" + e.getMessage()));
        }

    }

    public List<TransactionDetailViewDTO> getTransactionDetailViewDTOs() {
        return transactionDetailViewDTOs;
    }

    public void setTransactionDetailViewDTOs(List<TransactionDetailViewDTO> transactionDetailViewDTOs) {
        this.transactionDetailViewDTOs = transactionDetailViewDTOs;
    }
}
