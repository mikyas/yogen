package com.yogen.backoffice.bean.converter;

import java.util.Date;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.springframework.stereotype.Component;
import com.yogen.util.DateUtil;

@Component
public class SimpleDateLabelConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String text) {
        // This should not be called
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object object) {
        if (object != null) {
            Date date = (Date) object;
            return DateUtil.toString(date);
        }
        return null;
    }
}