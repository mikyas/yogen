package com.yogen.backoffice.bean.validator;

import java.text.ParseException;
import java.util.Date;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.springframework.stereotype.Component;
import com.yogen.backoffice.app.util.FacesContextUtil;
import com.yogen.util.DateUtil;

/**
 * Date alanların dinamik validasyonunu sağlamaktadır.
 *
 * @param fieldRequired                      Alan zorunluluğu kontrolünün gerçekleştirilmesi, Opsiyoneldir, gönderilmez ise kontrol yapılmaz. - True / False.
 * @param minDate                            Alan minimum tarih kontrolünün gerçekleştirilmesi. Opsiyoneldir, alan dolu ise kontrol gerçekleştirilir. Mysql date formatı verilmelidir.- "2017-10-11 10:00:12" vb.
 * @param maxDate                            Alan maksimum tarih kontrolünün gerçekleştirilmesi. Opsiyoneldir, alan dolu ise kontrol gerçekleştirilir. Mysql date formatı verilmelidir.- "2017-10-11" vb.
 * @param labelNameForMessage                Hata mesajları için alan adıdır. Zorunludur. - "Başlangıç Tarihi" vb.
 * @param doEndDateOlderThanStartDateControl Başlangıç tarihi bitiş tarihinden büyük olamaz kontrolünün gerçekleştirilmesi, Opsiyoneldir, - True / False.
 * @param startDateFieldId                   doEndDateOlderThanStartDateControl alanı true olarak gönderildiği durumda bu alan zorunludur, başlangıç tarihi alanının id bilgisi verilmelidir.
 * @param doDateRangeControl                 Başlangıç ve bitiş tarihi aralık kontrolünün gerçekleştirilmesi, Opsiyoneldir, gönderilmez ise kontrol yapılmaz. - True / False.
 * @param dateRangeValue                     doDateRangeControl alanı true olarak gönderildiği durumda bu alan zorunludur, gün olarak aralık belirtilmelidir. "365" vb.
 **/
@Component
public class DateValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object object) throws ValidatorException {
        String labelName = getLabelName(component);
        Date value = null;
        try {
            value = (Date) object;
        } catch (Exception e) {
            FacesContextUtil.setFacesErrorAndThrowException("Lütfen geçerli bir " + labelName + " giriniz.");
        }

        boolean required = isRequired(component);

        if (required && value == null) {
            FacesContextUtil.setFacesErrorAndThrowException("Lütfen " + labelName + " alanını giriniz.");
        }

        if (value != null) {
            Date maxDate = null, minDate = null;
            try {
                maxDate = getMaxDate(component);
                minDate = getMinDate(component);
            } catch (ValidatorException e) {
                throw new ValidatorException(e.getFacesMessage());
            } catch (Exception e) {
                FacesContextUtil.setFacesErrorAndThrowException("Regex hesaplamasında beklenmedik hata oluşmuştur.");
            }

            if (maxDate != null && value.after(maxDate)) {
                FacesContextUtil.setFacesErrorAndThrowException(labelName + " alanı " + DateUtil.toStringDetailed(maxDate) + " tarihinden büyük olamaz.");
            }

            if (minDate != null && minDate.after(value)) {
                FacesContextUtil.setFacesErrorAndThrowException(labelName + " alanı " + DateUtil.toStringDetailed(minDate) + " tarihinden küçük olamaz.");
            }

            boolean doEndDateOlderThanStartDateControl = doEndDateOlderThanStartDateControl(component);

            if (doEndDateOlderThanStartDateControl) {
                Date startDate = getStartDateFieldValue(component, context);
                if (startDate == null) {
                    FacesContextUtil.setFacesErrorAndThrowException("Başlangıç tarihi boş olamaz.");
                } else if (startDate.after(value)) {
                    FacesContextUtil.setFacesErrorAndThrowException("Bitiş tarihi başlangıç tarihinden eski olamaz.");
                }
            }

            boolean doDateRangeControl = doDateRangeControl(component);

            if (doDateRangeControl) {
                Date startDate = getStartDateFieldValue(component, context);
                Integer dateRangeAsDay = getDateRangeValue(component);
                if (startDate == null) {
                    FacesContextUtil.setFacesErrorAndThrowException("Başlangıç tarihi boş olamaz.");
                } else if (dateRangeAsDay == null) {
                    FacesContextUtil.setFacesErrorAndThrowException("Tarih aralığı değeri boş geçilemez.");
                } else if (Math.abs(DateUtil.dayDiff(startDate, value)) > dateRangeAsDay) {
                    FacesContextUtil.setFacesErrorAndThrowException("Tarih aralığı en fazla " + dateRangeAsDay + " gün olabilir.");
                }
            }
        }
    }

    private String getLabelName(UIComponent component) {
        Object attr = component.getAttributes().get("labelNameForMessage");
        if (attr != null) {
            return (String) attr;
        }
        return "???";
    }

    private Date getMaxDate(UIComponent component) throws ParseException {
        Object attr = component.getAttributes().get("maxDate");
        if (attr != null) {
            return DateUtil.getDateTimeFromMySQLFormat((String) attr);
        }
        return null;
    }

    private Date getMinDate(UIComponent component) throws ParseException {
        Object attr = component.getAttributes().get("minDate");
        if (attr != null) {
            return DateUtil.getDateTimeFromMySQLFormat((String) attr);
        }
        return null;
    }

    private boolean isRequired(UIComponent component) {
        Object attr = component.getAttributes().get("fieldRequired");
        return attr != null && Boolean.parseBoolean((String) attr);
    }

    private boolean doEndDateOlderThanStartDateControl(UIComponent component) {
        Object attr = component.getAttributes().get("doEndDateOlderThanStartDateControl");
        return attr != null && Boolean.parseBoolean((String) attr);
    }

    private Date getStartDateFieldValue(UIComponent component, FacesContext context) {
        Object attr = component.getAttributes().get("startDateFieldId");
        if (attr != null) {
            UIInput startDateField = (UIInput) context.getViewRoot().findComponent((String) attr);
            if (startDateField.isValid()) {
                return (Date) startDateField.getValue();
            }
        }

        return null;
    }

    private boolean doDateRangeControl(UIComponent component) {
        Object attr = component.getAttributes().get("doDateRangeControl");
        return attr != null && Boolean.parseBoolean((String) attr);
    }

    private Integer getDateRangeValue(UIComponent component) {
        Object attr = component.getAttributes().get("dateRangeValue");
        if (attr != null) {
            return Integer.valueOf(String.valueOf(attr));
        }

        return null;
    }
}