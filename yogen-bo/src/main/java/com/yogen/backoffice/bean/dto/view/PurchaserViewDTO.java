package com.yogen.backoffice.bean.dto.view;

public class PurchaserViewDTO {

    private String purchaserUID;
    private String firstName;
    private String lastName;
    private String mail;
    private String phone;
    private String citizenNum;
    private String gender;
    private String dateOfBirth;
    private SocialDetailViewDTO socialDetail;

    public String getPurchaserUID() {
        return purchaserUID;
    }

    public void setPurchaserUID(String purchaserUID) {
        this.purchaserUID = purchaserUID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCitizenNum() {
        return citizenNum;
    }

    public void setCitizenNum(String citizenNum) {
        this.citizenNum = citizenNum;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public SocialDetailViewDTO getSocialDetail() {
        return socialDetail;
    }

    public void setSocialDetail(SocialDetailViewDTO socialDetail) {
        this.socialDetail = socialDetail;
    }
}
