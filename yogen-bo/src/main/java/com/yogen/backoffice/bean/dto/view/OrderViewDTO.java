package com.yogen.backoffice.bean.dto.view;

import java.util.List;

public class OrderViewDTO {

    private String orderUID;
    private String name;
    private String createdAt;
    private String updatedAt;
    private String closedAt;
    private List<ItemViewDTO> items;
    private ShipmentViewDTO shipment;
    private BillingViewDTO billing;
    private PurchaserViewDTO purchaser;
    private PaymentDetailViewDTO paymentDetail;
    private ContactViewDTO contact;
    private DeviceViewDTO device;

    public String getOrderUID() {
        return orderUID;
    }

    public void setOrderUID(String orderUID) {
        this.orderUID = orderUID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getClosedAt() {
        return closedAt;
    }

    public void setClosedAt(String closedAt) {
        this.closedAt = closedAt;
    }

    public List<ItemViewDTO> getItems() {
        return items;
    }

    public void setItems(List<ItemViewDTO> items) {
        this.items = items;
    }

    public ShipmentViewDTO getShipment() {
        return shipment;
    }

    public void setShipment(ShipmentViewDTO shipment) {
        this.shipment = shipment;
    }

    public BillingViewDTO getBilling() {
        return billing;
    }

    public void setBilling(BillingViewDTO billing) {
        this.billing = billing;
    }

    public PurchaserViewDTO getPurchaser() {
        return purchaser;
    }

    public void setPurchaser(PurchaserViewDTO purchaser) {
        this.purchaser = purchaser;
    }

    public PaymentDetailViewDTO getPaymentDetail() {
        return paymentDetail;
    }

    public void setPaymentDetail(PaymentDetailViewDTO paymentDetail) {
        this.paymentDetail = paymentDetail;
    }

    public ContactViewDTO getContact() {
        return contact;
    }

    public void setContact(ContactViewDTO contact) {
        this.contact = contact;
    }

    public DeviceViewDTO getDevice() {
        return device;
    }

    public void setDevice(DeviceViewDTO device) {
        this.device = device;
    }
}
