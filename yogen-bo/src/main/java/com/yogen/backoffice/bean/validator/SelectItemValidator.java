package com.yogen.backoffice.bean.validator;

import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.springframework.stereotype.Component;
import com.yogen.backoffice.app.util.FacesContextUtil;

/**
 * Combobox-selectitem component alanlarının dinamik validasyonunu sağlamaktadır.
 *
 * @param fieldRequired       Alan zorunluluğu kontrolünün gerçekleştirilmesi, Opsiyoneldir, gönderilmez ise kontrol yapılmaz. - True / False.
 * @param labelNameForMessage Hata mesajları için alan adıdır. Zorunludur. - "Sunucu" vb.
 * @param selectItems         Değerin aranacağı selectItem listesi, selectItem component'ine verilen liste ile aynı olmalıdır. Zorunludur.
 **/
@Component
public class SelectItemValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        boolean required = isRequired(component);
        String labelName = getLabelName(component);
        List<SelectItem> selectItems = getSelectItems(component);

        if (required && value == null) {
            FacesContextUtil.setFacesErrorAndThrowException("Lütfen " + labelName + " alanını giriniz.");
        }

        if (value != null) {
            boolean isValid = false;
            for (SelectItem selectItem : selectItems) {
                if (selectItem.getValue().equals(value)) {
                    isValid = true;
                }
            }

            if (!isValid) {
                FacesContextUtil.setFacesErrorAndThrowException("Lütfen " + labelName + " alanını giriniz.");
            }
        }
    }

    private String getLabelName(UIComponent component) {
        Object attr = component.getAttributes().get("labelNameForMessage");
        if (attr != null) {
            return (String) attr;
        }
        return "???";
    }

    private boolean isRequired(UIComponent component) {
        Object attr = component.getAttributes().get("fieldRequired");
        return attr != null && Boolean.parseBoolean((String) attr);
    }

    private List<SelectItem> getSelectItems(UIComponent component) {
        Object attr = component.getAttributes().get("selectItems");
        if (attr != null) {
            return (List<SelectItem>) attr;
        }
        return null;
    }
}
