package com.yogen.backoffice.app.util;

import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import com.yogen.util.StringUtil;

public class FacesContextUtil {

    public static final String NUMBER_OF_LOGIN_ATTEMPT_SESSION_KEY = "numberOfLoginAttemptSessionKey";
    public static final String DO_CAPTCHA_CONTROL_SESSION_KEY = "doCapcthaControl";
    public static final int MAXIMUM_LOGIN_ATTEMPT_FOR_RECAPTCHA_CONTROL = 3;

    @Deprecated
    public static Long safeGetRequestParameterAsLong(String name) {
        return StringUtil.safeParseLong(getRequestParameter(name));
    }

    @Deprecated
    public static String safeGetRequestParameterAsString(String name) {
        return StringUtil.safeToString(getRequestParameter(name));
    }

    private static String getRequestParameter(String name) {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(name);
    }

    public static String getUsername() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }
        return username;
    }


    public static Map<String, Object> getRequestAttributes() {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
    }

    public static void addCallbackParam(String name, Object value) {
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam(name, value);
    }

    public static void setIntoSession(String name, Object value) {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        session.setAttribute(name, value);
    }

    public static Object getFromSession(String name) {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        return session.getAttribute(name);
    }

    public static String getServerUrl() {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        return context.getRequestScheme() + "://" + context.getRequestServerName() + ":" + context.getRequestServerPort();
    }

    public static void logout() {
        SecurityContextHolder.clearContext();
    }

    public static void setFacesErrorAndThrowException(String errorMessage) throws ValidatorException {
        FacesMessage message = new FacesMessage();
        message.setSummary(errorMessage);
        message.setSeverity(FacesMessage.SEVERITY_ERROR);
        throw new ValidatorException(message);
    }
}