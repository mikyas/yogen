package com.yogen.backoffice.app.util;

import java.util.List;
import javax.faces.application.FacesMessage;
import com.yogen.util.enumtype.FacesMessageSeverityType;

public class FacesMessagesSeverityConverter {
    public static FacesMessage.Severity convert(FacesMessageSeverityType severityType) {
        List<FacesMessage.Severity> severities = FacesMessage.VALUES;
        for (FacesMessage.Severity severity : severities) {
            if (severity.getOrdinal() == severityType.getValue()) {
                return severity;
            }
        }
        return null;
    }
}
