function assign_user_oncomplete(xhr, status, args, widgetVar) {
    if (!args.exceptionOccured && !args.success) {
        widgetVar.show();
    }
}

function handleSuccessResponseAndRediretToReturnURL(xhr, status, args, widgetVar, returnUrlFieldId) {
    if (!args.validationFailed) {
        if (!args.exceptionOccured && args.success != undefined && args.success) {
            window.location = document.getElementById(returnUrlFieldId).value;
        } else {
            widgetVar.hide();
        }
    }
}