package com.yogen.sms.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "sms_log", catalog = "yogen_sms", schema = "dbo")
public class SmsLog implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "SENDER_TYPE", nullable = false)
    private Integer senderType;

    @Column(name = "GSM", nullable = false)
    private String gsm;

    @Column(name = "MESSAGE", nullable = false)
    private String message;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "REQUEST_DATE", nullable = false)
    private Date requestDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RESPONSE_DATE", nullable = false)
    private Date responseDate;

    @Column(name = "TRANSACTION_ID", nullable = false)
    private String transactionId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FIRST_REQUEST_DATE", nullable = false)
    private Date firstRequestDate;

    @Column(name = "TRY_COUNT", nullable = false)
    private int tryCount = 1;

    @Column(name = "SCHEDULED", nullable = false)
    private boolean scheduled = false;

    @Column(name = "SUCCESS", nullable = false)
    private boolean success = false;

    @Column(name = "IP", nullable = true)
    private String ip;

    @Column(name = "ERROR_CODE", nullable = true)
    private String errorCode;

    @Column(name = "ERROR_MESSAGE", nullable = true)
    private String errorMessage;

    @Column(name = "USER_ID", nullable = true)
    private Integer userId;

    @Column(name = "CUST_FIELD", nullable = true)
    private String customField;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGsm() {
        return gsm;
    }

    public void setGsm(String gsm) {
        this.gsm = gsm;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Integer getSenderType() {
        return senderType;
    }

    public void setSenderType(Integer senderType) {
        this.senderType = senderType;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getCustomField() {
        return customField;
    }

    public void setCustomField(String customField) {
        this.customField = customField;
    }

    public Date getFirstRequestDate() {
        return firstRequestDate;
    }

    public void setFirstRequestDate(Date firstRequestDate) {
        this.firstRequestDate = firstRequestDate;
    }

    public int getTryCount() {
        return tryCount;
    }

    public void setTryCount(int tryCount) {
        this.tryCount = tryCount;
    }

    public boolean isScheduled() {
        return scheduled;
    }

    public void setScheduled(boolean scheduled) {
        this.scheduled = scheduled;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}