package com.yogen.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.Vector;

public class DataGenerator {

	private static String[] nameList = { "JALE", "ALİ", "MAHMUT", "MANSURKÜRŞAD", "GAMZE", "MİRAÇ", "YÜCEL", "KUBİLAY",
			"HAYATİ", "BEDRİYEMÜGE", "BİRSEN", "SERDAL", "BÜNYAMİN", "ÖZGÜR", "FERDİ", "REYHAN", "İLHAN", "GÜLŞAH",
			"NALAN", "SEMİH", "ERGÜN", "FATİH", "ŞENAY", "SERKAN", "EMRE", "BAHATTİN", "IRAZCA", "HATİCE", "BARIŞ",
			"REZAN", "FATİH", "FUAT", "GÖKHAN", "ORHAN", "MEHMET", "EVREN", "OKTAY", "HARUN", "YAVUZ", "PINAR",
			"MEHMET", "UMUT", "MESUDE", "HÜSEYİNCAHİT", "HAŞİMONUR", "EYYUPSABRİ", "MUSTAFA", "MUSTAFA", "UFUK",
			"AHMETALİ", "MEDİHA", "HASAN", "KAMİL", "NEBİ", "ÖZCAN", "NAGİHAN", "CEREN", "SERKAN", "HASAN",
			"YUSUFKENAN", "ÇETİN", "TARKAN", "MERALLEMAN", "ERGÜN", "KENANAHMET", "URAL", "YAHYA", "BENGÜ",
			"FATİHNAZMİ", "DİLEK", "MEHMET", "TUFANAKIN", "MEHMET", "TURGAYYILMAZ", "GÜLDEHEN", "GÖKMEN", "BÜLENT",
			"EROL", "BAHRİ", "ÖZENÖZLEM", "SELMA", "TUĞSEM", "TESLİMENAZLI", "GÜLÇİN", "İSMAİL", "MURAT", "EBRU",
			"TÜMAY", "AHMET", "EBRU", "HÜSEYİNYAVUZ", "BAŞAK", "AYŞEGÜL", "EVRİM", "YASER", "ÜLKÜ", "ÖZHAN", "UFUK",
			"AKSEL", "FULYA", "BURCU", "TAYLAN", "YILMAZ", "ZEYNEP", "BAYRAM", "GÜLAY", "RABİA", "SEVDA", "SERHAT",
			"ENGİN", "ASLI", "TUBA", "BARIŞ", "SEVGİ", "KALENDER", "HALİL", "BİLGE", "FERDA", "EZGİ", "AYSUN", "SEDA",
			"ÖZLEM", "ÖZDEN", "KORAY", "SENEM", "ZEYNEP", "EMEL", "BATURAYKANSU", "NURAY", "AYDOĞAN", "ÖZLEM", "DENİZ",
			"İLKNUR", "TEVFİKÖZGÜN", "HASANSERKAN", "KÜRŞAT", "SEYFİ", "ŞEYMA", "ÖZLEM", "ERSAGUN", "DİLBER", "MESUT",
			"ELİF", "MUHAMMETFATİH", "ÖZGÜRSİNAN", "MEHMETÖZGÜR", "MAHPERİ", "ONUR", "İBRAHİM", "FATİH", "SEVİL",
			"SÜHEYLA", "VOLKAN", "İLKAY", "İLKNUR", "ZÜMRÜTELA", "HALE", "YENER", "SEDEF", "FADIL", "SERPİL", "ZÜLFİYE",
			"SULTAN", "MUAMMERHAYRİ", "DERVİŞ", "YAŞARGÖKHAN", "TUBAHANIM", "MEHRİ", "MUSTAFAFERHAT", "SERDAR",
			"MUSTAFAERSAGUN", "ONAT", "ŞÜKRÜ", "OLCAYBAŞAK", "SERDAR", "YILDIZ", "AYDIN", "ALİHALUK", "NİHATBERKAY",
			"İSMAİL", "AYKAN", "SELÇUK", "MEHMET", "NEZİH", "MUSTAFA", "TİMUR", "ERHAN", "MUSTAFA", "MUTLU",
			"MEHMETHÜSEYİN", "İSMAİLEVREN", "OSMANERSEGUN", "MEHMET", "ELİF", "SERKAN", "MESUT", "MEHMETHİLMİ",
			"ASUDANTUĞÇE", "AHMETGÖKHAN", "BAŞAK", "CEYHAN", "MUHAMMETTAYYİP", "ESİN", "ZEYNEPGÖKÇE", "EVRİM", "YASİN",
			"SALİHA", "DENİZ", "BELGİN", "ÖZLEM", "GONCA", "ESRA", "SEÇKİN", "ESRA", "FATİH", "MUSTAFA", "FEVZİYE",
			"MUSTAFAARİF", "BİRGÜL", "ÖZLEM", "ÖZLEM", "FUNDA", "BERFİN", "DEMET", "SONAY", "SERÇİN", "ALMALAPINAR",
			"ÜMİT", "SENEM", "DENİZ", "MÜNEVER", "HATİCE", "ÖZLEM", "ÖZLEM", "ALİSEÇKİN", "COŞKUN", "ÖZGE", "ZELİHA",
			"PINAR", "AYBÜKE", "HASİBE", "GÜRKAN", "ZÜHAL", "NAZIM", "ZEYNEP", "OSMAN", "AYLA", "BEYZA", "ELİF", "ERAY",
			"DİANA", "TUBA", "SEMRA", "VELAT", "BELGİNEMİNE", "SİBEL", "GÖKMENALPASLAN", "BENHURŞİRVAN", "DİLEK",
			"HANDE", "ŞAHABETTİN", "MİRAY", "ZERRİN", "İLKNUR", "ELİF", "MÜMTAZ", "TUĞBA", "DİLEK", "MEHMETBURHAN",
			"FUAT", "NİHAL", "AYŞEGÜL", "SEMA", "ZAFER", "NURSEL", "GÜLPERİ", "BİLGE", "FATİH", "CENGİZ", "SİMGE",
			"SEMANİLAY", "EMİNE", "RİFATCAN", "SİNAN", "LATİFE", "MEHMET", "NURDAN", "MELTEM", "ÜLKÜHAN", "HASAN",
			"GÜLDEN", "SAMET", "BERNA", "ÖZLEM", "NAFİYE", "KENAN", "SERKANFAZLI", "NURSEL", "ABDULLAH", "ERGÜL",
			"HASAN", "MUSTAFA", "SEBAHAT", "EMİNE", "ERDAL", "LEZİZ", "BİRSEN", "TUBA", "AYŞEN", "EBRU", "TAYFUR",
			"MELTEM", "SERHAT", "AYCANÖZDEN", "ELİF", "SEVGÜL", "SELDA", "IŞIL", "SİBEL", "JÜLİDEZEHRA", "BERİLGÜLÜŞ",
			"İNCİ", "ENGİN", "GÜLBAHAR", "MÜBECCEL", "NURDAN", "HANDE", "ÖZNUR", "HANDAN", "OSMANTURGUT",
			"EMİNTONYUKUK", "NEJDET", "MUSTAFA", "GÜLİZ", "İPEK", "NİHAL", "MELDA", "DERYA", "DEMET", "MAHMUT", "EMEL",
			"ÖZNUR", "SONGÜL", "RESA", "GAMZE", "ÜMİT", "DENİZ", "MUAMMERMÜSLİM", "ÖMERFARUK", "TUĞÇE", "VELİENES",
			"ZAHİDE", "NURETTİNİREM", "SEDAT", "REMZİYE", "SİBEL", "İLKNUR", "YASEMİN", "AYLİN", "EMEL", "EMELCENNET",
			"ŞAFAK", "METİN", "SÜLEYMAN", "MUKADDES", "BARIŞ", "MEHMETALİ", "TEVFİK", "SERDAR", "EMİNE", "MÜRŞİT",
			"MUTLU", "FEZA", "İBRAHİMTAYFUN", "SERKAN", "AHMETSERKAN", "FATMA", "BERKER", "SERDAR", "KUBİLAY", "ERKAN",
			"KERİM", "İLKNUR", "SERKAN", "MUSTAFA", "RUKİYE", "GÖKTEN", "SEZGİ", "TUĞBA", "MURAT", "HATİCE",
			"HATİCEEYLÜL", "AYŞEGÜL", "NEVİN", "HABİBE", "KEZBAN", "AYSEL", "TALHA", "DUYGU", "GÖZDE", "FIRAT", "EBRU",
			"GÜLENECE", "SİBEL", "FULYA", "VEDAT", "HARUN", "FİLİZ", "NURAY", "ŞİRİN", "ÖZLEM", "BURCU", "PINAR",
			"HATUN", "CEYDA", "BURCU", "AYŞE", "ALPER", "FEYZA", "HACIMURAT", "MÜCELLA", "FEYZAHAN", "ŞENAY", "MERİH",
			"YUSUF", "ARDA", "EVRE", "KONURALP", "KIVANÇ", "EMİNE", "VOLKAN", "NİHAT", "RENGİNASLIHAN", "EMRE",
			"ARİFEESRA", "SEDAT", "MURAT", "CEM", "ERHAN", "ÖMÜR", "UMUTCAN", "MUSTAFANAFİZ", "DAMLA", "MÜSLİM",
			"ABDULKADİR", "SAADET", "REZZAN", "SEDAT", "İBRAHİM", "LEYLA", "TÜLAY", "ENDER", "YELİZ", "ÖZGÜL", "HALE",
			"BERÇEM", "MUSTAFA", "TUBA", "SABAHATTİN", "ŞAFAK", "EVRİM", "REŞAT", "MUMUN", "FUNDAÖZLEM", "NUR", "METE",
			"TÜLAY", "ÖZLEM", "HATİCENİLDEN", "MELTEM", "EDA", "MELTEM", "MUSTAFA", "YURDUN", "SEMA", "TUBA", "SERPİL",
			"CENK", "TANER", "ZEKERİYA", "MUHAMMEDALİ", "TUĞRUL", "YÜCEL", "ESMAÖZLEM", "AHMET", "SEVDENUR", "SAMİ",
			"GAMZE", "GÜLSÜM", "SERHAT", "BARIŞ", "GÜLSEREN", "SULTAN", "İLKER", "DERAM", "AHMET", "BALABAŞAK",
			"FEVZİFIRAT", "GÖZDE", "FERHAN", "İLKER", "SALİM", "EMİNE", "MURAT", "ATAKAN", "REFİK", "MUSTAFA", "ÖZGÜR",
			"İKLİL", "ZÜHALGÜLSÜM", "MEHTAP", "DENİZ", "ÜMİT", "MEHMET", "VOLKAN", "İLKNUR", "SELÇUK", "ÖZLEM",
			"GÖKHAN", "METE", "HÜMEYRA", "PAPATYA", "LEVENT", "FADİMESEVGİ", "ERSEN", "ŞULEMİNE", "MELİA", "ŞERMİN",
			"AYLİA", "AHMETEMRE", "VEDAT", "HALUK", "SEZGİN", "ZEHRABETÜL", "VOLKAN", "ÜNSAL", "KORAY", "GÜLŞAH",
			"HİCRAN", "YUSUFKENAN", "YUSUF", "ORHAN", "FÜSUN", "ÖZLEM", "MEHMET", "SERKAN", "İKRAM", "ÜLKÜHAN", "NUH",
			"İSMAİL", "GÜLŞAH", "AYKUT", "NEŞE", "NEZAKET", "MUHAMMETDEVRAN", "HANDAN", "ATİLLA", "AYŞEGÜL", "PINAR",
			"ARZU", "NEŞE", "CEYHAN", "HASANSAMİ", "MEHMET", "SALİH", "FERDA", "DİLEK", "AYHAN", "HASANULAŞ", "SUNA",
			"SELAMİ", "SÜREYYA", "BURCU", "CEMYAŞAR", "ÇAĞDAŞ", "DOĞAN", "AHMET", "HATİCE", "HAYRİ", "GÜHER", "SUNA",
			"ARZU", "AHMET", "AHMET", "SEMİH", "YUSUF", "ALİ", "ELİFÇİLER", "ELİF", "YETKİN", "BURAK", "VEYSEL",
			"BURCU", "REFAETTİN", "AYŞEGÜL", "HÜSEYİNKUNTER", "EMİNE", "EBRU", "ESRANUR", "SAMİ", "MUSTAFAGÜRHAN",
			"ORHAN", "HAÇÇE", "MEHMETMURAT", "NAZAN", "TUĞBA", "OĞUZHAN", "BERFİNCAN", "ÖZGÜR", "CİHAN", "DERMAN",
			"NİHAL", "IŞIN", "HATİCE", "DİDEM", "SUAT", "SİMENDER", "ADNAN", "SEZİN", "ŞERAFETTİN", "BERRİN", "ÖZGÜR",
			"TAYFUR", "SERHAT", "FUNDA", "NESLİHAN", "SERVET", "EMRE", "ORÇUN", "FATMAESİN", "DERYA", "DUYGU",
			"HÜSEYİN", "AYKUT", "AYŞE", "SEHER ÖZLEM", "SEDA", "ESRACAN", "EVREN", "NİLÜFER", "MURAT", "YUSUF",
			"MUSTAFABARAN", "GÜNEŞ", "FATİH", "MEHMET", "ORHAN", "PINAR", "ERHAN", "GÜLDEN", "LATİFE", "SİBEL",
			"FATMASELCEN", "HALİS", "YELDA", "ZEHRA", "MEHMETREŞİT", "EMCED", "OKAN", "ABDULLAHARİF", "ÖZLEM",
			"AYDEMİR", "FATİH", "AYDIN", "BENGÜHAN", "AHMET", "TOLUNAY", "TUĞRA", "ÖZGÜR", "YUSUF", "ÖNDERTURGUT",
			"BARAN", "SEYHAN", "ZEKİ", "KADİR", "MURAT", "İHSAN", "DEMİR", "MUHAMMETMURAT", "MUHAMMED", "BAHADIR",
			"İLHAN", "YILDIRIM", "OĞUZKAAN", "EFTALMURAT", "GÖKAY", "FATİHRIFAT", "NURAN", "HABİL", "ÖMERÖZKAN",
			"SELMA", "NURETTİN", "AHMET", "UTKU", "SÜLEYMAN", "CAN", "ONURKADİR", "FİLİZ", "CANSUSELCAN", "ABDULLAH",
			"NESLİHAN", "SEDAELÇİM", "KÜBRA", "HÜSEYİN", "BELMA", "MÜCAHİT", "CEYHUN", "GÜLTEKİNGÜNHAN", "HANDE",
			"GÜLHANIM", "ÖMER", "ZİYA", "ÇAĞRI", "SERKAN", "LEVENT", "İSA", "CEM", "HÜSEYİN", "CEMİLEÇİĞDEM", "MAHMUT",
			"ÖNDER", "RAŞAN", "İSMAİLYAVUZ", "ÖMER", "KENANSELÇUK", "EMRE", "SERDAR", "SONER", "ENVER", "MUHLİS",
			"TİMUR", "LEMAN", "ELA", "CEMİL", "BURCU", "SALİHASANEM", "ZELİHA", "ALEVTİNA", "ARZU", "MEHMET",
			"BİREYLÜL", "AYSEL", "ÖZGÜL", "SIDIKA", "TUNA", "ÖVGÜANIL", "EMİŞ", "NİLGÜN", "ELİF", "SEBİHA",
			"YUSUFALPER", "IŞIL", "AYŞEGÜL", "TÜMAY", "ZERİN", "MEHMET", "FATMAECE", "AHMET", "NEVROZ", "SEMA",
			"GAMZEPINAR", "FATMA", "MELTEMHALE", "SELMA", "İBRAHİM", "ASLIHAN", "FİLİZ", "BİLGİ", "TUĞBA", "HÜSEYİN",
			"EVREN", "FERDİ", "BURÇİN", "BARIŞ", "BAVER", "MAHMUT", "EMRAHKEMAL", "MURAT", "MEHMETÖZER", "GÖKAY",
			"NECİP", "ERKAN", "ÜMİT", "GANİM", "BARAN", "FATİH", "SANCAR", "MUSTAFAKEMAL", "DURAN", "HASAN", "GÖRKEM",
			"ALİOZAN", "VELİÇAĞLAR", "İRFAN", "SEYFİCEM", "CÜNEYT", "ALİ", "FIRAT", "MUHAMMEDTAHA", "BURAK", "MUSTAFA",
			"NİZAMETTİN", "RECEPGANİ", "ARZU", "MAHMUTNURİ", "GÜNAY", "BESTE", "CEM", "EMRAH", "HALİLİBRAHİM",
			"ESENİBRAHİM", "MELİKE", "MÜRSEL", "KORAY", "UMUTSİNAN", "İBRAHİMBARIŞ", "BURHAN", "MUSTAFAKÜRŞAT",
			"SERDARBORA", "FUAT", "ELİF", "SİBEL", "ADEM", "CEMİNAN", "GÖKTEKİN", "DİLARA", "SEDA", "CUMHUR", "HALUK",
			"PINAR", "AYKUT", "ÖYKÜ", "BAŞAK", "ÖMER", "SERHAN", "SULTAN", "MİNECANSU", "SUAT", "ÇİĞDEM", "HANDE",
			"UMUTSEDA", "EVRİM", "DİCLE", "İREM", "FATMA", "İLKNUR", "CEMİLEAYŞE", "FEYZA", "MUAMMER", "EBRU",
			"DİNÇERAYDIN", "AYŞEAHSEN", "PINAR", "SÜREYYABURCU", "MARİA", "SEÇİL", "BARIŞ", "ŞAHİNDE", "MEHMET",
			"NEVRİYE", "NİLAY", "RÜŞTÜ", "ASLI", "TANSU", "ASLIHAN", "NURCAN", "DEMET", "ERDEM", "BETÜLEMİNE", "ÇETİN",
			"PINAR", "RASİM", "AHMET", "ZEHRA", "SELİM", "DENİZ", "MESUT", "AYSEL", "MÜMÜNE", "DUÇEM", "YAKUP", "ERCAN",
			"MEHMETGÖKÇE", "ATİLLASÜLEYMAN", "MERAL", "NURDAN", "SEÇİL", "ELİF", "HASANBİLEN", "NİLAY", "HİLAL",
			"MUSTAFA", "ŞEYMAMELİHA", "MÜJDAT", "BURCU", "ARİF", "AYŞE", "HACİHALİL", "IŞIK", "SERAY", "SERHATBURKAY",
			"EMİN", "ÇAVLAN", "ŞEREFCAN", "KADİR", "MÜBERRA", "AYŞE", "HASAN", "SAVAŞ", "AYŞENUR", "SÜLEYMAN", "DUYGU",
			"HACIMEHMET", "MEHTAP", "ERKAN", "SEMİNE", "ELİF", "RAMAZAN", "AHMETEMRE", "SERKAN", "DENİZ", "MİNE",
			"FATİH", "İREM", "ENDER", "YASEMİN", "SEMA", "MUSTAFAULAŞ", "ALİ", "EMİNE", "TÜLAY", "BİRSEN", "GÜLSEN",
			"TUBA", "HAYRİYE", "BAHADIR", "SEZEN", "EDİPGÜVENÇ", "MEHTAP", "FİLİZ", "EYLEM", "RAMAZAN", "BİLGİN",
			"ESRA", "ATİYEMELTEM", "METİN", "ÖZLEM", "OSMAN", "AYŞE", "HİLAL", "YAVUZ", "ÖZLEM", "AYŞE", "MUSTAFA",
			"ASUMAN", "ÖMER", "ŞENAY", "GÜLNAME", "ÖZLEM", "BETÜL", "EMİNEDİLEK", "ZELİHA", "ESİNSEREN", "FİLİZ",
			"ULAŞ", "HALE", "ADEM", "İLKER", "ERHAN", "FARUK", "İBRAHİM", "SERKAN", "MAHMUTESAT", "ERAY", "OKTAY",
			"DENİZ", "OSMAN", "İZZET", "İHSAN", "URAL", "ARİF", "AZİZ", "FUATERNİS", "HAKAN", "MUZAFFEROĞUZ", "MAHİR",
			"TAYYARALP", "EREM", "CİHAN", "MEHMET", "ÜMİT", "MEHMET", "YASEMİN", "CEYDA", "FATIMAİLAY", "NİLAY",
			"HURİYE", "MERVE", "ŞEYMA", "",

	};
	private static String[] surnameList = { "ŞEN", "KANDEMİR", "ÇEVİK", "ERKURAN", "TÜTEN", "ÖZTÜRK", "YÜZBAŞIOĞLU",
			"VURAL", "YÜCEL", "SÖNMEZ", "ERTEKİN", "DEDE", "UYANIK", "ASLAN", "AKBULUT", "ORHON", "UZ", "YAVUZ",
			"ERDEM", "KULAÇ", "KAYA", "SELVİ", "AKPINAR", "ABACIOĞLU", "ÇAY", "IŞIK", "ÖZER", "ÖZDEMİR", "ÖZTÜRK",
			"TAHTACI", "BÜYÜKCAM", "KULAKSIZ", "AKSEL", "EROĞLU", "KARAKUM", "DAL", "ÖCAL", "AYHAN", "YİĞİT", "YARBİL",
			"CANACANKATAN", "GÜMÜŞAY", "MURT", "HALHALLI", "ULUÖZ", "ŞEYHANLI", "ÇALIŞKANTÜRK", "YILMAZ", "SARAÇOĞLU",
			"SEZER", "DOĞAN", "DEMİR", "KAYAYURT", "SÜRÜM", "YAVAŞİ", "TURGUT", "ŞEN TANRIKULU", "BARBAROS", "ALDİNÇ",
			"TEKİN", "GÜLŞAN", "KÜFECİLER", "ALMACIOĞLU", "ÇİLDİR", "TÜRKDOĞAN", "KAYA", "ÖNER", "ŞELİMAN", "YAMAN",
			"ATİK", "YİĞİT", "GİRAY", "YALÇINKAYA", "KILIÇ", "ŞENTÜRK", "KARABAĞ", "DEĞİRMENCİ", "BODUROĞLU", "YILDIZ",
			"GÜLER", "ERASLAN", "ÜZER", "PİŞİRGEN", "ADANIR", "KOÇ", "KORKMAZ", "YENİDOĞAN", "AYDOĞAN", "ACARBULUT",
			"ERGE", "ERDOĞAN", "ÖĞÜT AYDIN", "KUŞKU", "KUCUR TÜLÜBAŞ", "PEKTAŞ", "KAYACAN", "GÜLEN", "DOĞAN",
			"AYDIN BADILLIOĞLU", "GÜLEN AKKÜÇÜK", "CANDAN", "TEMEL", "YENİGÜN", "YILDIRIM", "BEDER", "AKINCI",
			"ÖZDEMİR", "ONUK", "AYDOĞAN", "YILMAZ", "AKCAN ATASOY", "SARAÇOĞLU ÇEKİÇ", "CÖMERT", "TOPAL", "KARAHAN",
			"ŞAHİN", "ÇETİN", "YILMAZ İNAL", "AYTAÇ", "YILDIZ ALTUN", "KİŞİ", "GÜNDÜZ", "ÖZKURT PÜRCÜ", "AK", "URFALI",
			"KARAMAN", "MEMETOĞLU", "KAZBEK", "KİREÇÇİ", "AKIN", "YADİGAROĞLU", "YÜKSEL", "ÖZÇELİK ORAL", "BABUŞ",
			"KAPLAN", "AKÖZ", "KARTAL", "BİLGİÇ", "ERDEN", "TUĞCUGİL", "KUMRAL", "ERBAŞ", "ORAL", "KILAÇ", "CENGİZ",
			"YILDIRIM", "KUTLUCAN BAĞCI", "BALABAN", "KAYA", "BALCI", "TÜFEKÇİ", "ATAY", "YARAR", "SEVER", "YILDIRIM",
			"ARSLAN KAŞDOĞAN", "ARKAN", "TUTAŞ", "ÖZTÜRK", "HAVAS", "SEÇİR", "YILDIZ", "SOYKAMER", "BEKTAŞ", "BERK",
			"GÜL", "GEDİK YILMAZ", "CENGİZ", "ÇOLAK", "BULUT", "SARI", "AKYOL", "BAĞCIK", "KUTLUYURDU", "DEMİRGAN",
			"YİĞİT KUPLAY", "GERİLMEZ", "DÜZKALIR", "KÖKSOY", "GÜLŞEN", "AKAR", "ÖZDOĞAN", "TÖNGE", "YASA", "ÖNVERMEZ",
			"YILDIRIM", "BİÇER", "KARADEMİR", "ALIMLI", "AKGÜL", "HANCIOĞLU", "BATÇIK", "OLPAK", "BOLAT", "ARSLAN",
			"SİĞA", "MERCAN", "BOZKURTER", "GÜLER", "ERGİNEL", "ŞAHİN", "KADAK", "GÜNEY KOCABAŞ", "GAYRETLİ AYDIN",
			"HEPKAYA", "BAYRAM", "KANIK YÜKSEK", "KULAK GEDİK", "AKCAN PAKSOY", "ESER", "KILIÇ YILDIRIM", "GİDER",
			"KURT", "ELLİALTI", "DEMİRTAŞ", "ARGA", "BAŞKAN VURALKAN", "ALUÇLU", "MUTLU", "ŞATIR ERTEM", "ENGİZ",
			"ÇİPE", "UYSAL", "BAŞER", "ARSLAN", "GÖZKAYA", "ULUTAŞ", "PİRİM", "ÜSTÜN", "KIZMAZOĞLU", "ULUBA", "ARSLAN",
			"KARAOĞLU", "ÖZSOY", "YALÇIN", "SAF", "VURAL", "DEMİRTAŞ", "GENÇPINAR", "AKASLAN", "UYĞUN", "ATAY",
			"ÖNDER SİVİŞ", "BAYMAK", "ATAY", "GÜVENÇ", "AKCA ÇAĞLAR", "ÖZCAN", "ERDEM ÖZCAN", "BAŞMAN", "YANNİ", "ÜNAL",
			"GÜNDOĞDU", "ÇELİK", "USTA GÜÇ", "TANRIVERDİ YILMAZ", "TAŞKIN", "ÇETİN", "YILMAZ ÇİFTDOĞAN",
			"GAZETECİ TEKİN", "SARİ", "KARAKOYUN", "KARAKUŞ EPÇAÇAN", "EKİCİ", "AYDINER", "AKTAŞ", "BELGEMEN", "ÇETİN",
			"OFLAZ", "BUĞRUL", "BAYSOY", "BÜKÜLMEZ", "YILMAZ", "BIÇAKÇI", "KARA", "TİMURTAŞ DAYAR", "ATEŞ", "BİNBOĞA",
			"KIZILTEPE", "KAYA", "ABSEYİ", "AMİROVA UÇAN", "ÖZTÜRK", "TAŞ", "CEYLAN", "KILIÇ", "EROL", "TAYFUN", "KAYA",
			"KARAKURT", "BUDUNOĞLU", "ÖZER", "SAYGIN", "ERYAVUZ", "POLAT ÇİÇEK", "YILMAZ", "ÇELİK", "ÜNSAL", "ALPINAR",
			"CİNDEMİR", "AKDUMAN", "UYAR", "TÜLPAR", "AZAK", "EREN", "GÖZCÜ", "BAYSAL", "TUNCEL", "ÇETEMEN", "YILMAZ",
			"GİNİŞ", "UZUN", "NASIROĞLU", "SEZGİN", "ÖZTÜRK", "YILDIRIM", "UZUN", "BULUR", "DUYSAK", "YENİN", "DEMİREL",
			"SAK", "KOCABAŞ", "SARAÇ", "ALKURT KAYIKÇI", "YURT", "İLKAY", "TAVŞAN", "ALAY", "ERTEM", "ÖZEL", "GENÇ",
			"UĞUZ", "EVİK", "GENÇ TALAS", "EKER", "ÇİMEN", "ÇIRAKOĞLU", "DEMİR GÖÇMEN", "ALPAYCI", "AK", "ÇELİK",
			"ERCAN", "ALTUN", "KILIÇ", "SARP", "SÖKER", "KÖSE", "BARÇAK", "ÖZEKLİ MISIRLIOĞLU", "BOLAÇ", "ASLANALP",
			"ÖRNEK", "AKDOĞAN", "ÖZÇELİK", "ERTÜRKLER", "SARAL", "ÖZKAN", "DEMİRHAN", "ASLANKARA", "EMLAKÇIOĞLU",
			"ÖZTÜRK", "ESER", "ÖDEN", "DEMİRAY", "AYHAN", "YAĞCI", "AVCI", "BAYGELDİ", "BÜKÜM", "DİNCER", "DOĞAN",
			"EKİZ", "ŞAHİNER", "ŞENGÜL", "İLGÜN", "AŞIK", "ÖZKAN", "ŞİRZAİ", "ÖCALAN", "KABA", "TÜLÜCE", "AYTEKİN",
			"KAYA", "DÜGER", "METİNEREN", "BULUT", "ŞAHİN DUYAR", "ÇETİN", "BÖLÜK", "GÖZAÇAN", "BOZKURT", "ÖNEY KURNAZ",
			"AY GÜNEY", "KÖYLÜ", "ÖZMEN SÜNER", "TALAN", "DUMLU", "ZORLU KARAYİĞİT", "KÖYCÜ", "UYGUR", "KABACAOĞLU",
			"TOPALOĞLU", "AYIK", "DEMİR", "ERDEM", "KARAMANLI", "SADİ AYKAN", "OKTAY", "YURTLU", "SALMAN SEVER",
			"CİRİT KOÇER", "SORGUN EVCİLİ", "NURÇİN", "BAŞKAN", "KAZANCI", "KIYAK YILMAZ", "METE", "UZUN", "SAĞDIK",
			"ARIKAN YORGUN", "EKİCİ", "KESİM", "GÜL", "YILDIRIM", "KARAGÖZ", "PEKEL", "YAKAR", "TARLAN", "ÇATAK",
			"ÇETİNKOR", "SAYIN", "KURT", "GÜMÜŞ", "KOCAKAYA ALTUNDAL", "ÖZMEN", "GÜNAY", "DÜZ", "DİLEK", "DEMİRTAŞ",
			"KURTULUŞ", "KARPUZOĞLU", "ERGİNTÜRK ACAR", "BEYOĞLU", "SULHAN", "ARSLAN", "NUHVEREN", "AVCIOĞLU",
			"AHISKALI", "ASENA", "KARACAN ERŞEKERCİ", "GÖLEMEZ", "YILDIRIM", "TOHUMOĞLU", "ÇELİK", "BOZARSLAN",
			"KÖŞKER", "ÇELİK", "SÜL", "KORKMAZ", "TARKAN", "DUMAN", "HODJAOGLU", "BALLI", "ŞATIROĞLU", "ÖNDE",
			"LAĞARLI", "ÖZÇAY", "ARSLAN", "AKDEMİR", "İÇBAY", "AKIN", "DEMİRÖZ", "KUYUCU", "SELÇUK", "İNCE",
			"ERGÜLÜ EŞMEN", "GÖKALP", "BABACAN", "ÜLGER", "AYVAZ", "ELVERDİ", "AYDIN", "DEMİREL", "CİMBEK", "FIRAT",
			"BAHÇEBAŞI", "DAM", "KÖROĞLU", "ÖZÇELİK", "KARACA", "SEVEN", "ÖZKURT", "ALTUN", "BÜYÜKTAŞ", "SERTKAYA",
			"ÖVEN USTAALİOĞLU", "YALNIZ", "SAVAŞ", "YILMAZ", "YALÇIN", "BEREKET", "KAYA", "PEKGÖZ", "DEMİR", "OLMAZ",
			"SEVİNÇ", "MERHAMETSİZ", "ÇOBANOĞLU", "ŞİMŞEK", "BİNNETOĞLU", "ÖĞÜTMEN KOÇ", "ÇINKIR", "CAMCI", "YAZAK",
			"NİZAM", "TÜRKOĞLU", "DEMİRKOL", "AKSAKAL", "AKIN", "BOZOĞLAN", "DEĞİRMENCİ", "AYMAN", "SAÇLI", "KARAKILIÇ",
			"BAKANAY ÖZTÜRK", "KARAKÖSE", "GÜVEN MEŞE", "YEŞİLOVA", "EŞKAZAN", "GERDAN", "MUMCUOĞLU", "VATANSEVER",
			"PAKÖZ", "ATMIŞ", "AKÇALI", "FAKIOĞLU", "YENİDÜNYA", "ANIK", "KÖSEOĞLU", "SONAY", "ÇELİKER", "ÖZDEMİRKIRAN",
			"ÇELİK", "KÖSE", "AKIN", "DURÇ ÖZTÜRK", "İNER KÖKSAL", "BEREKATOĞLU", "DİLLİ", "ELBÜKEN", "BAHÇECİ",
			"BÜLBÜL", "KADI", "IŞIK", "YÜCETÜRK", "BULUR", "ÖZİŞ", "ULUBAŞOĞLU", "AKŞAHİN", "KARPUZ", "YABUL", "GÖKSOY",
			"ÜNAL", "IŞIK", "KÖKSAL", "TEKİŞ", "AKSOY", "BAŞYURT", "YURDSEVEN", "ERDEM", "MERDEN", "KISA KARAKAYA",
			"SANHAL", "ŞAHİN", "VATANSEVER", "BİLGİ", "KAYABAŞ", "GÜRBOSTAN", "BOLAT", "KABİL KUCUR", "DEĞİRMENCİ AKAR",
			"TAYYAR", "ŞAHBAZ", "YANCAR", "OLGAÇ", "EKİZ", "EREN", "MALÇOK", "KARASU", "KARADAĞ", "TOPRAK", "SAĞLAM",
			"ŞAHİN", "KEBAPCILAR", "TATAR", "ARSLAN", "YÜCE", "TOLA", "GÜNGÖR", "KARAGÖZ", "ALTINBOĞA", "YENİÇERİ",
			"IŞIKALAN", "ÖZDEMİR", "GÜRBÜZ", "KURU", "YURDAM", "KARA", "ÇETİN", "BAŞARAN", "ŞAHİN", "ÜREYEN", "IŞIK",
			"ÖZTÜRK", "DOĞAN", "MESCİ HAFTACI", "ORHAN", "VURAL", "EROL", "BALSAK", "ÖZDEMİR", "ÇİFT", "ŞEN",
			"YAZICI EROL", "BAYRAMOĞLU", "GENÇDAL", "DESTEGÜL", "ÖZDEMİR", "KARÇİN", "ASLAN", "BAZ", "ALTUNTAŞ",
			"ÖZCAN", "KIRBAŞ", "YILMAZ", "KAYMAN KÖSE", "ÇETİN", "YEŞİLDAĞER", "YÜKSEL", "KAYAOĞLU", "KILIÇ",
			"CELTEMEN", "GÜNDÜZ", "ŞANLIKAN", "ÇELİK", "ORHAN", "TANTEKİN", "KARAALP", "TUNCER", "ATASOY", "DOĞAN",
			"GÖK", "ÖZDEMİR", "ÖZKAN", "ONAR ŞEKERCİ", "ASOĞLU", "KHALİL", "YURDAKÖK", "YILMAZ", "KESKİN", "KOÇARSLAN",
			"GÖKALP", "TUNÇAY", "SÜRMEN AKYOL", "AYDIN", "SEVİNGİL", "GENÇPINAR", "AKKAYA", "KUSERLİ", "BOZKURT",
			"ŞİMŞEK", "YILMAZ", "TALAS", "ÇEVİKER", "KAYNAK", "BAYRAKTAR", "ÇETİNTAŞ", "CANTÜRK", "KARADENİZ", "ALAN",
			"KOYUNCU", "KARTAL", "KAYA", "BAKIRCI", "NAR", "ULUSOY", "CELİLOĞLU", "YÜCEL", "DUMAN", "AKDENİZ", "YERAL",
			"GÜRDAL", "KÜTÜK", "KANYILMAZ", "ÖZBEK", "UYSAL", "AKIN", "AKDENİZ", "KAPLAN", "ALBAYRAK", "YILDIRIM",
			"MÜEZZİNOĞLU", "AYHAN", "UYGUR", "TÜFENK", "YÜCEL", "DEMİR", "ÖZDEMİR", "KIRIŞ", "KIRASLAN", "SALTÜRK",
			"AÇIKGÖZ", "YAĞCI", "SEVÜK", "KAYA", "DOĞAN", "YILDIZ", "BAYTAN", "DEMİRTAŞ", "MUTLU", "GENÇ", "AKTUĞ",
			"SERİN", "TUNCAY", "GÜNBEY", "KAYA", "TAŞAR", "AVSEREN", "BAL", "BATMAZ", "VEZİROĞLU BİRDANE",
			"ARAZ SERVER", "GÜLER", "DALYAN CİLO", "KARADAĞ GEÇGEL", "BAŞTÜRK AYHAN", "ERSOY", "TAY", "ERYILMAZ",
			"DEMİR", "AYDIN", "OCAK", "BÖLÜK", "ÖZMEN", "ÖZTÜRKERİ", "EKEN", "AKGÜL", "SARICA DAROL", "CANSEVER",
			"AKIN", "GÜZEL", "ÖZER ÇELİK", "ÇAKIR", "AKSUN", "BALAL", "BAYAM", "ŞAİR", "ÜNLÜ", "YUMURTAŞ", "AKGÜL",
			"AYKAN", "ALPSAN GÖKMEN", "CANATAN", "MUMCUOĞLU", "TAŞKIRAN", "HATİPOĞLU", "AKYOL", "SUCAK", "YILDIZ",
			"AKPINAR", "GÖKSEL", "KARSLI", "ÖZGÜROL", "ACAR", "KALEM", "ŞAHİN", "AYDIN", "DÖKMECİ", "GÖRMELİ", "ÖZATEŞ",
			"SERVET", "TOPRAK", "SÜNER", "SARIKAYA", "SULUOVA", "SERBEST", "EFE", "TOPAK", "ATBİNİCİ", "KIYAK", "ÇELİK",
			"ÖZ", "TEPE", "ÖZÜAK", "ÖNCEL", "CANBAZ", "AL", "DEMİR", "GÜRER", "GÜNGÖR", "GÜZEL", "GÖNCÜ", "ÖZDAMAR",
			"KARATOPRAK", "ÇAVDAR", "KARA", "ÖZ", "SÖZEN", "GÖKÇEK", "KARAKAYA", "GÜNGÖR", "ÇEPNİ", "KIR", "ERSOY",
			"ÇAĞLAR", "ÖZALP", "EVRENOS", "BAYRAKTAROĞLU", "USLUSOY", "SARI", "ATALAY", "TOPKARA", "BEKTAŞ", "TENEKECİ",
			"ÇAĞIL", "MERTOL", "TAŞ", "HIDIROĞLU", "ŞEN GÖKÇEİMAM", "KARAHAN", "ÖNAL MUSALAR", "DEMİREL", "YACI",
			"IŞIKLI", "KILIÇ", "ÜLGEN", "KÜÇÜKGÖNCÜ", "SU KURT", "KOÇAR", "BALOĞLU", "DUMAN", "ASLAN", "SARICANBAZ",
			"SERT", "ALTUN", "GÖRMELİ", "YILMAZ GELEBEK", "AKYOL", "ÖZAN SANHAL", "AKYILMAZ", "BAKAN", "KARAKAN",
			"GÖRKEM", "CILIZ BASHEER", "KARACAN", "TEN", "ATLANOĞLU", "ÖZTÜRK", "TOPALOĞLU", "SOYDAN", "TÜRKAY",
			"MENTEŞ", "PINARBAŞILI", "ONAY", "CERİT", "ÜNAL", "ALTUN", "YILDIZ", "İMAMOĞLU", "ÖZDEMİR AKDUR", "YANMAZ",
			"BÜBER", "AKKAYA", "BAKAN", "TAŞMALI", "BULAKÇI", "BAYRAM", "AYDIN", "GERGER", "YEŞİLKAYA", "DÖNMEZ",
			"YILMABAŞAR", "DİKİCİ", "ARİFOĞLU", "FİDAN", "SAKARYA", "ÖZEN", "ONAN", "AKHUN", "KIR", "ŞAHİN", "SU DUR",
			"YAZICI", "GÜRDEMİR", "ALTINSOY", "KALYONCU UÇAR", "ŞAŞMAZ", "GÜLCAN", "KURT GÜNEY", "ÖZTÜRK", "ULUTAŞ",
			"ALTUNA", "GÜREL", "KARAKUŞ", "KILIÇ", "ÖZKIRIŞ", "KAYA", "YILMAZ", "İNCİ KENAR", "DEMİR", "AKBAŞ ÖNCEL",
			"EREN", "BİCAN", "AYDIN", "ÖZDOĞAN KAVZOĞLU", "ATEŞ BUDAK", "KÖKSAL", "SARGIN", "AKKOYUNLU", "ŞİMŞEK",
			"ÖZTÜRK", "KAYHAN", "TEZER", "KARACAN ", "ÇAKIR", "UYSAL", "GÜRAKAN", "DOKUMACIOĞLU", "KIRHAN", "ÖZDEMİR",
			"KAYA", "GÜL ÖZMEN", "ESEN", "AK YILDIRIM", "EKER", "ÖZAVCI AYGÜN", "ÇEKİÇ", "SAVRAN", "GÖKALP",
			"GÖKMEYDAN", "EMRE", "KÜTÜKCÜ", "DİKOĞLU", "AKSOY", "GÖRENEKLİ", "KOCA", "KILINÇ", "BATGİ AZARKAN",
			"TÜRKMEN ALBAYRAK", "OKULU", "KİRİŞCİ", "DEDE", "KIZMAZ", "ARGON", "ALICI", "ARIKAN", "FINDIK GÜVENDİ",
			"ÜÇER", "ÜNAL", "YILMAZ", "ÇETİN", "ERGÖZ", "FİLİZ", "ALABALIK", "KIZANOĞLU", "YAŞAR", "ÇELEN", "DEMİRELLİ",
			"DOĞAN", "DÖNMEZ", "AKDEMİR", "DANIŞOĞLU", "GÜRSOY", "ŞENER", "ABAT", "ERGÜN", "ÇİÇEKBİLEK", "ÜNÜŞ", "OĞUZ",
			"KOL", "TOKER", "SU", "POLAT", "KELEŞ", "SEYREK", "ÖZKAN", "ASİL", "TOKTAŞ", "ARDIÇ", "ÖZDEMİR", "SÖZEN",
			"ÇOBANYILDIZI", "MÜFTÜOĞLU", "YEGEN YILMAZ", "AKSOY", "AKYOL", "OFLAZ", "KARAOĞLANOĞLU", "AYAS" };

	private static String[] phoneCodeList = {"532", "536", "538", "539", "542", "545", "548", "505", "555",  "506" };
	
			
	public static String generateName() {
		int nameIndex = (int) (Math.random() * 1000);
		return nameList[nameIndex];
	}

	public static String generateSurname() {
		int surnameIndex = (int) (Math.random() * 1000);
		return surnameList[surnameIndex];
	}

	public static String generateNameSurname() {
		int nameIndex = (int) (Math.random() * 1000);
		int surnameIndex = (int) (Math.random() * 1000);
		return nameList[nameIndex] + " " + surnameList[surnameIndex];
	}

	public static String generateEmail(String nameSurname) {
		nameSurname = nameSurname.replaceAll("\\s+", "");
		return StringUtil.replaceTurkishCharacterWithEnglishOnes(nameSurname).toLowerCase() + "@deneme12345.com";
	}

	public static String generatePhoneNumber() {
		return phoneCodeList[generateIntCode(10)] + generateStrCode(7);
	}

	public static int generateIntCode(int maxRange) {
		return (int) (Math.random() * 100) % maxRange;
	}

	public static String generateStrCode(int range) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < range; i++) {
			builder.append((int) (Math.random() * 10));
		}
		return builder.toString();
	}

	public static String generateCreditCard(String bin, int length) {
		int randomNumberLength = length - (bin.length() + 1);
		StringBuilder builder = new StringBuilder(bin);
		for (int i = 0; i < randomNumberLength; i++) {
			int digit = (int) (Math.random() * 10);
			builder.append(digit);
		}
		int checkDigit = getCheckDigit(builder.toString());
		builder.append(checkDigit);
		return builder.toString();
	}

	public static String generateTcknNumber() {
		Vector<Integer> array = new Vector<Integer>();
		Random randomGenerator = new Random();
		array.add(Integer.valueOf(1 + randomGenerator.nextInt(9)));

		for (int i = 1; i < 9; i++)
			array.add(randomGenerator.nextInt(10));

		int t1 = 0;
		for (int i = 0; i < 9; i += 2)
			t1 += array.elementAt(i);

		int t2 = 0;
		for (int i = 1; i < 8; i += 2)
			t2 += array.elementAt(i);

		int x = ((t1 * 7) - t2) % 10;

		array.add(Integer.valueOf(x));

		x = 0;
		for (int i = 0; i < 10; i++)
			x += array.elementAt(i);

		x = x % 10;
		array.add(Integer.valueOf(x));

		String res = "";
		for (int i = 0; i < 11; i++)
			res = res + Integer.toString(array.elementAt(i));
		return res;

	}
	
	public static String generateTaxNumber() {
		return DataGenerator.generateStrCode(10);
	}
	
	public static String generateIbanNumber() {
		return "TR" + DataGenerator.generateStrCode(24);
	}
	
	private static int getCheckDigit(String number) {
		int sum = 0;
		for (int i = 0; i < number.length(); i++) {
			int digit = Integer.parseInt(number.substring(i, (i + 1)));
			if ((i % 2) == 0) {
				digit = digit * 2;
				if (digit > 9) {
					digit = (digit / 10) + (digit % 10);
				}
			}
			sum += digit;
		}
		int mod = sum % 10;
		return ((mod == 0) ? 0 : 10 - mod);
	}

	public static String createOrderId(long transactionId, boolean isTrx) {
		String orderId = "";
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		orderId += transactionId;// 1206996
		orderId += calendar.get(Calendar.MILLISECOND);// 1
		orderId += calendar.get(Calendar.SECOND);// 1
		orderId += calendar.get(Calendar.MINUTE);// 8
		orderId += calendar.get(Calendar.HOUR_OF_DAY);// 1
		orderId += calendar.get(Calendar.DATE);// day of month 2
		orderId += (calendar.get(Calendar.MONTH) + 1);// 9
		orderId += String.valueOf(calendar.get(Calendar.YEAR)).substring(2, 4);// 15
		if (isTrx) {
			orderId += "1";
		}
		return orderId;
	}
	
	public static String createMerchantOrderId() {
		return UUID.randomUUID().toString();
	}

	public static void main(String[] args) {
		for (int i = 0; i < 100; i++) {
			System.out.println(generateTcknNumber());
		}
	}
}
