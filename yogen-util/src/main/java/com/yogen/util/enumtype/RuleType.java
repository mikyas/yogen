package com.yogen.util.enumtype;

public enum  RuleType implements IValueEnum{

    NON_CONTROLLED(1), // geçmişe bakmayan
    CONTROLLED_ONE_DIMENSION(2), // geçmişe bakan bir boyutlu kural
    CONTROLLED_TWO_DIMENSION(3), // geçmişe bakan iki boyutlu kural
    DEFINED_CONTROLLED_ONE_DIMENSION(4), // kayıtlı değeleri için geçmişe bakan bir boyutlu kural
    DEFINED_CONTROLLED_TWO_DIMENSION(5); // kayıtlı değeleri için geçmişe bakan iki boyutlu kural

    private final Integer value;
    private RuleType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

}
