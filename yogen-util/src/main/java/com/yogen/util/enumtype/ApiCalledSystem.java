package com.yogen.util.enumtype;

public enum ApiCalledSystem implements IValueEnum {

    MULTINET(1),
    PARANTEZ(2);

    private Integer value;

    ApiCalledSystem(Integer value) {
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }
}
