package com.yogen.util.enumtype;

public enum ApiCalledServiceName {

    PARANTEZ_REGISTER(ApiCalledSystem.PARANTEZ, "multimobilregister", 1),
    MULTINET_RESTONET_CREATE_INDIVIDUAL_ORDER(ApiCalledSystem.MULTINET, "CreateIndividualOrder", 2),
    MULTINET_RESTONET_HAS_CARD_ONLINE_SALE_PERMISSION(ApiCalledSystem.MULTINET, "HasCardOnlineSalesPermission", 3),
    MULTINET_RESTONET_WEB_CREATE_SALE(ApiCalledSystem.MULTINET, "WebCreateSale", 4),

    MULTINET_MA_MEMBER_QUERY(ApiCalledSystem.MULTINET, "MemberQuery", 5),
    MULTINET_MA_AUTH(ApiCalledSystem.MULTINET, "Authorization", 6),
    MULTINET_MA_PREAUTH(ApiCalledSystem.MULTINET, "PreAuthorization", 7),
    MULTINET_MA_POSTAUTH(ApiCalledSystem.MULTINET, "PreAuthorizationFinance", 8),
    MULTINET_MA_REFUND(ApiCalledSystem.MULTINET, "CancelAuthorization", 9),
    MULTINET_MA_CARD_BALANCE_QUERY(ApiCalledSystem.MULTINET, "CardBalanceQuery", 10),
    MULTINET_MA_INSTALLMENT_COMMISSION_LIST(ApiCalledSystem.MULTINET, "InstallmentAndCommissionRateTransfer", 11),

    MULTINET_EWALLET_CARD_VALIDATE(ApiCalledSystem.MULTINET, "CardValidate", 12),
    MULTINET_EWALLET_GET_CARD_BALANCE(ApiCalledSystem.MULTINET, "GetCardBalance", 13),
    MULTINET_EWALLET_GET_REPLACE_CARDS(ApiCalledSystem.MULTINET, "GetReplacedCards", 14),

    MULTINET_RESTONET_GET_ARP_CODE(ApiCalledSystem.MULTINET, "GetARPCode", 15),
    MULTINET_RESTONET_BLACK_CARD_NOTIFICATION(ApiCalledSystem.MULTINET, "BlackCard", 16),
    MULTINET_MULTIMOBIL_3D_ORDER_RESULT_UPDATE(ApiCalledSystem.MULTINET, "OrderUpdateThreeDResult", 17),

    MULTINET_SHELLCARD_GET_DETAIL(ApiCalledSystem.MULTINET, "CardExternalDetailGet", 18),
    MULTINET_SHELLCARD_TOPUP(ApiCalledSystem.MULTINET, "CardExternalCreditAdd", 19),
    MULTINET_SHELLCARD_GET_HISTORY(ApiCalledSystem.MULTINET, "CardExternalBalanceHistoryGet", 20),
    MULTINET_SHELLCARD_REGISTER(ApiCalledSystem.MULTINET, "CardExternalInsert", 21),

    MULTINET_GIFT_CARD_USER_INFO(ApiCalledSystem.MULTINET, "CardUserInfo", 22),
    MULTINET_GIFT_CARD_AUTH(ApiCalledSystem.MULTINET, "Sale", 23),
    MULTINET_GIFT_CARD_EOD(ApiCalledSystem.MULTINET, "EndOfDay", 24),
    MULTINET_GIFT_CARD_VALIDATE(ApiCalledSystem.MULTINET, "IndividualCardInfo", 25),

    MULTINET_EWALLET_CREATE_CREDIT_CARD_PAYMENT(ApiCalledSystem.MULTINET, "CreateCreditCardPayment", 26),
    MULTINET_SMS(ApiCalledSystem.MULTINET, "SendSms", 27),
    MULTINET_CREATE_INDIVIDUAL_USER(ApiCalledSystem.MULTINET, "CreateIndividualUser", 28),
    MULTINET_RESTONET_CREATE_INDIVIDUAL_LOADING_ORDER(ApiCalledSystem.MULTINET, "CreateIndividualLoadingOrder", 29),
    MULTINET_DELETE_INDIVIDUAL_USER(ApiCalledSystem.MULTINET, "DeleteIndividualUser", 30),
    MULTINET_DELETE_INDIVIDUAL_USER_CARD(ApiCalledSystem.MULTINET, "DeleteIndividualUserCard", 31),
    MULTINET_UPDATE_INDIVIDUAL_USER(ApiCalledSystem.MULTINET, "UpdateIndividualUser", 32);

    private String serviceName;
    private Integer serviceId;
    private ApiCalledSystem apiCalledSystem;

    ApiCalledServiceName(ApiCalledSystem apiCalledSystem, String serviceName, Integer serviceId) {
        this.apiCalledSystem = apiCalledSystem;
        this.serviceName = serviceName;
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public ApiCalledSystem getApiCalledSystem() {
        return apiCalledSystem;
    }
}