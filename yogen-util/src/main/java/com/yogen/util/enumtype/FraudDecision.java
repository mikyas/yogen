package com.yogen.util.enumtype;

public enum FraudDecision implements IValueEnum {
    ALLOW(0),
    REVIEW(1),
    PREVENT(2);

    private Integer value;

    FraudDecision(Integer value) {
        this.value = value;
    }

    @Override
    public Integer getValue() {

        return value;
    }
}
