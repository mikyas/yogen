package com.yogen.util.enumtype;

public enum TimePeriod implements IValueEnum, ITextEnum {
    MINUTE(1, "Dakika"),
    HOUR(2, "Saat"),
    DAY(3, "Gün"),
    WEEK(4, "Hafta"),
    MONTH(5, "Ay"),
    YEAR(6, "Yıl");

    private Integer value;
    private String text;

    TimePeriod(Integer value, String text) {
        this.value = value;
        this.text = text;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getText() {
        return text;
    }
}