package com.yogen.util.enumtype;

public enum DatePeriod implements IValueEnum, ITextEnum {
    ONCE(0, "Tek Sefer"),
    DAILY(1, "Günlük"),
    WEEKLY(2, "Haftalık"),
    MONTHLY(3, "Aylık"),
    YEARLY(4, "Yıllık");

    private Integer value;
    private String text;

    DatePeriod(Integer value, String text) {
        this.value = value;
        this.text = text;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getText() {
        return text;
    }
}