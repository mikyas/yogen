package com.yogen.util.enumtype;

public enum EncryptionKeyType implements ITextEnum {

    MAIN("MAIN"),
    TEMP("TEMP"),
    HASH("HASH");

    private String value;

    EncryptionKeyType(String value) {
        this.value = value;
    }

    public String getText() {
        return value;
    }
}