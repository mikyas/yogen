package com.yogen.util.enumtype;

public enum ChargebackState implements IValueEnum {

    ACCEPTED(1), // You have opted out of providing evidence for the chargeback or pre-arb.
    DISPUTED	(2), // You have submitted evidence against the claim and are waiting for the decision from the bank.
    WON	(3), // The bank has ruled in your favor, and the funds should return to your bank account within 2-3 business days.
    LOST (4), // The bank has  ruled in your favor, and the funds will remain with the cardholder.
    EXPIRED(5); // The reply-by date to submit evidence has passed and you have forfeited your right to dispute the case.

    private final Integer value;

    private ChargebackState(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}