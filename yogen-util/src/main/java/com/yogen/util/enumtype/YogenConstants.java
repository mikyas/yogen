package com.yogen.util.enumtype;

import java.nio.charset.StandardCharsets;

public class YogenConstants {
    public static final String INTEGRATOR_TEST_PUBLIC_KEY = "AzQasdW1rsGy"; //001ZFVojYeXv+UYtu4UvvEB7g==
    public static final String INTEGRATOR_TEST_PRIVATE_KEY = "F4rK"; //001SJre/cS5Fgdjg6NPogzzhw==
    public static final String UTF8 = "UTF-8";
    public static final double MAXIMUM_DIFFERENCE_FOR_FRACTION_DIGITS = 0.01;
}
