package com.yogen.util.enumtype;

public enum PaymentType implements IValueEnum {

    CARD(1),
    PAYPALL(2);

    private final Integer value;

    private PaymentType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}