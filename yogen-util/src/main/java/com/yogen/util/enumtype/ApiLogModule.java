package com.yogen.util.enumtype;

public enum ApiLogModule implements IValueEnum {

    YOGEN(1);

    private Integer value;

    ApiLogModule(Integer value) {
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }
}
