package com.yogen.util.enumtype;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BaseResponseDTO {

    private byte result;
    private String responseMessage;
    private Integer errorCode;
    private String errorMessage;
    private boolean forceSystemErrorMessage;

    public byte getResult() {
        return result;
    }

    public void setResult(byte result) {
        this.result = result;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @JsonIgnore
    public boolean isForceSystemErrorMessage() {
        return forceSystemErrorMessage;
    }

    public void setForceSystemErrorMessage(boolean forceSystemErrorMessage) {
        this.forceSystemErrorMessage = forceSystemErrorMessage;
    }
}
