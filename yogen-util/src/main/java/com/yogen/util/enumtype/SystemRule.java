package com.yogen.util.enumtype;

import java.util.Arrays;
import java.util.Objects;

public enum SystemRule {

    /**
     * Geçmişe bakmayan kurallar
     */
    RULE_1001(RuleType.NON_CONTROLLED, 1001, "IP alanı için yasaklı ip filtresi uygulanması"),
    RULE_1002(RuleType.NON_CONTROLLED, 1002, "Mail uzantısı için yasak mail uzantısı filtresi uygulanması"),
    RULE_1003(RuleType.NON_CONTROLLED, 1003, "Alıcı Adı alanı için yasaklı karakter filtresi uygulanması"),
    RULE_1004(RuleType.NON_CONTROLLED, 1004, "Alıcı Soyadı alanı için yasaklı karakter filtresi uygulanması"),
    RULE_1005(RuleType.NON_CONTROLLED, 1005, "Alıcı Adresi alanı için yasaklı karakter filtresi uygulanması"),
    RULE_1006(RuleType.NON_CONTROLLED, 1006, "Ödeme Tutarı alanı için üst limit filtresi uygulanması"),
    RULE_1007(RuleType.NON_CONTROLLED, 1007, "Ödeme Tutarı alanı için alt limit filtresi uygulanması"),
//    RULE_1008(RuleType.NON_CONTROLLED, "1008", "Ödeme Tutarı alanı için 3D ödemelerde üst limit filtresi uygulanması"),
//    RULE_1009(RuleType.NON_CONTROLLED, "1009", "Ödeme Tutarı alanı için 3D ödemelerde alt limit filtresi uygulanması"),
//    RULE_1010(RuleType.NON_CONTROLLED, "1010", "Ödeme Tutarı alanı için 23:00-00:00 arası üst limit filtresi uygulanması"),
//    RULE_1011(RuleType.NON_CONTROLLED, "1011", "Ödeme Tutarı alanı için 23:00-00:00 arası alt limit filtresi uygulanması"),
    /**
     * Geçmişe bakan bir boyutlu  kurallar
     */
    RULE_2001(RuleType.CONTROLLED_ONE_DIMENSION, 2001, "Belirli bir zaman içerisinde bir kart numarasından gelen ödeme isteği sınırı"),
    RULE_2002(RuleType.CONTROLLED_ONE_DIMENSION, 2002, "Belirli bir zaman içerisinde bir müşteri mailinden gelen ödeme isteği sınırı"),
    RULE_2003(RuleType.CONTROLLED_ONE_DIMENSION, 2003, "Belirli bir zaman içerisinde bir IP adresinden gelen ödeme isteği sınırı"),
    RULE_2004(RuleType.CONTROLLED_ONE_DIMENSION, 2004, "Belirli bir zaman içerisinde bir müşteri numarasından gelen ödeme isteği sınırı"),
    /**
     * Geçmişe bakan iki boyutlu  kurallar
     */
    RULE_3001(RuleType.CONTROLLED_TWO_DIMENSION, 3001, "Belirli bir zaman içerisinde bir kart numarasından gelen ödemeler için farklı IP adresinden gelme sınırı"),
//    RULE_3002(RuleType.CONTROLLED_TWO_DIMENSION, "3002", "Belirli bir zaman içerisinde bir kart numarasından gelen ödemeler için farklı mail adresi sayısı gelme sınırı"),
//    RULE_3003(RuleType.CONTROLLED_TWO_DIMENSION, "3003", "Belirli bir zaman içerisinde bir kart numarasından gelen ödemeler için farklı müşteri numarası sayısı gelme sınırı"),
//    RULE_3004(RuleType.CONTROLLED_TWO_DIMENSION, "3004", "Belirli bir zaman içerisinde bir kart numarasından gelen ödemeler için farklı müşteri soyadından gelme sayısı sınırı"),

    RULE_3101(RuleType.CONTROLLED_TWO_DIMENSION, 3101, "Belirli bir zaman içerisinde bir ip adresinden gelen ödemeler için farklı kart numarasından gelme sayısı sınırı"),
//    RULE_3102(RuleType.CONTROLLED_TWO_DIMENSION, "3102", "Belirli bir zaman içerisinde bir ip adresinden gelen ödemeler için farklı mail adresinden gelme sayısı sınırı"),
//    RULE_3103(RuleType.CONTROLLED_TWO_DIMENSION, "3103", "Belirli bir zaman içerisinde bir ip adresinden gelen ödemeler için farklı müşteri numarası gelme sayısı sınırı"),
//    RULE_3104(RuleType.CONTROLLED_TWO_DIMENSION, "3104", "Belirli bir zaman içerisinde bir ip adresinden gelen ödemeler için farklı müşteri soyadından gelme sayısı sınırı"),

    RULE_3201(RuleType.CONTROLLED_TWO_DIMENSION, 3201, "Belirli bir zaman içerisinde bir mail adresinden gelen ödemeler için farklı IP adresinden gelme sınırı"),
//    RULE_3202(RuleType.CONTROLLED_TWO_DIMENSION, "3202", "Belirli bir zaman içerisinde bir mail adresinden gelen ödemeler için farklı kart numarasından gelme sayısı sınırı"),
//    RULE_3203(RuleType.CONTROLLED_TWO_DIMENSION, "3203", "Belirli bir zaman içerisinde bir mail adresinden gelen ödemeler için farklı müşteri numarasından gelme sayısı sınırı"),
//    RULE_3204(RuleType.CONTROLLED_TWO_DIMENSION, "3204", "Belirli bir zaman içerisinde bir mail adresinden gelen ödemeler için farklı müşteri soyadından gelme sayısı sınırı"),

    //    RULE_3301(RuleType.CONTROLLED_TWO_DIMENSION, "3301", "Belirli bir zaman içerisinde bir müşteri numarasından gelen ödemeler için farklı IP adresinden gelme sınırı"),
    RULE_3302(RuleType.CONTROLLED_TWO_DIMENSION, 3302, "Belirli bir zaman içerisinde bir müşteri numarasından gelen ödemeler için farklı kart numarasından gelme sayısı sınırı"),
//    RULE_3303(RuleType.CONTROLLED_TWO_DIMENSION, "3303", "Belirli bir zaman içerisinde bir müşteri numarasından gelen ödemeler için farklı mail adresinden gelme sayısı sınırı"),
//    RULE_3304(RuleType.CONTROLLED_TWO_DIMENSION, "3304", "Belirli bir zaman içerisinde bir müşteri numarasından gelen ödemeler için farklı müşteri soyadından gelme sayısı sınırı"),
    /**
     * Geçmişe bakan iki boyutlu  kurallar
     */
//    RULE_4303(RuleType.DEFINED_CONTROLLED_ONE_DIMENSION, "4001", "tanim"),
    /**
     * Geçmişe bakan iki boyutlu  kurallar
     */
//    RULE_5303(RuleType.DEFINED_CONTROLLED_TWO_DIMENSION, "5001", "tanim"),

    RULE_9999(RuleType.NON_CONTROLLED, 9999, "kural 9999");

    private RuleType ruleType;
    private Integer ruleCode;
    private String description;

    SystemRule(RuleType ruleType, Integer ruleCode, String description) {
        this.ruleType = ruleType;
        this.ruleCode = ruleCode;
        this.description = description;
    }

    public static SystemRule getSystemRuleFromCode(Integer code) {
        return Arrays.stream(SystemRule.values())
                .filter(e -> Objects.equals(e.getRuleCode(), code))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(String.format("Unsupported type %s.", code)));
    }

    public RuleType getRuleType() {
        return ruleType;
    }

    public Integer getRuleCode() {
        return ruleCode;
    }

    public String getDescription() {
        return description;
    }
}
