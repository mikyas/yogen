package com.yogen.util.enumtype;

public enum ApiLogModuleAndServiceName {

    TEST(ApiLogChannel.YOGEN_API, ApiLogModule.YOGEN, "test", 0),
    CREATE(ApiLogChannel.YOGEN_API, ApiLogModule.YOGEN, "create", 1),
    UPDATE(ApiLogChannel.YOGEN_API, ApiLogModule.YOGEN, "update", 2),
    CHECKOUT(ApiLogChannel.YOGEN_API, ApiLogModule.YOGEN, "test", 4);
    private String webServiceName;
    private Integer webServiceId;
    private ApiLogModule apiLogModule;
    private ApiLogChannel apiLogChannel;

    ApiLogModuleAndServiceName(ApiLogChannel apiLogChannel, ApiLogModule apiLogModule, String webServiceName, Integer webServiceId) {
        this.apiLogChannel = apiLogChannel;
        this.apiLogModule = apiLogModule;
        this.webServiceName = webServiceName;
        this.webServiceId = webServiceId;
    }

    public Integer getWebServiceId() {
        return webServiceId;
    }

    public ApiLogModule getApiLogModule() {
        return apiLogModule;
    }

    public void setApiLogModule(ApiLogModule apiLogModule) {
        this.apiLogModule = apiLogModule;
    }

    public String getWebServiceName() {
        return webServiceName;
    }

    public void setWebServiceName(String webServiceName) {
        this.webServiceName = webServiceName;
    }

    public ApiLogChannel getApiLogChannel() {
        return apiLogChannel;
    }
}
