package com.yogen.util.enumtype;

public enum FacesMessageSeverityType implements IValueEnum {
    SEVERITY_INFO(0),
    SEVERITY_WARN(1),
    SEVERITY_ERROR(2),
    SEVERITY_FATAL(3);

    int value;

    private FacesMessageSeverityType(int value) {
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }
}
