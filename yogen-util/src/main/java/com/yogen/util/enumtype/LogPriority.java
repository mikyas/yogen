package com.yogen.util.enumtype;

public enum LogPriority implements IValueEnum {

    INFO(10),
    WARN(20),
    DEBUG(30),
    ERROR(40),
    FATAL(50);

    private Integer value;

    LogPriority(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}