package com.yogen.util.enumtype;

public enum ProfileMode {
    LOCAL, DEV, TEST, PREPROD, PROD
}