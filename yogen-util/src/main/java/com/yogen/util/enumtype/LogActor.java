package com.yogen.util.enumtype;

public enum LogActor implements IValueEnum {

    SYSTEM(1),
    INTEGRATOR(2),
    OPERATOR(3);

    private Integer value;

    LogActor(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}