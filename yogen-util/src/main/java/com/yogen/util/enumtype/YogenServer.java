package com.yogen.util.enumtype;

public enum YogenServer implements IValueEnum {

    WEB1(01, "172.20.9.220", "172.20.12.70"),
    WEB2(02, "172.20.9.202", "172.20.12.69"),
    API1(11, "172.20.9.203", "172.20.12.63"),
    API2(22, "172.20.9.204", "172.20.12.64"),
    SCHEDULAR(31, "172.20.8.211", "172.20.12.59"),
    BACKOFFICE1(41, "172.20.8.207", "172.20.12.54"),
    BACKOFFICE2(42, "172.20.8.208", "172.20.12.55"),
    TEST(99, "");

    private Integer value;
    private String[] ips;

    YogenServer(Integer value, String... text) {
        this.value = value;
        this.ips = text;
    }

    public static int getServer(String ip) {
        for (YogenServer server : YogenServer.values()) {
            for (String serverIp : server.getIps()) {
                if (serverIp.equals(ip)) {
                    return server.getValue();
                }
            }
        }
        return 0;
    }

    public Integer getValue() {
        return value;
    }

    public String[] getIps() {
        return ips;
    }
}