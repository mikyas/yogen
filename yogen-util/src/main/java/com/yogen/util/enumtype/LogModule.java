package com.yogen.util.enumtype;

public enum LogModule {

    SYSTEM,
    YOGEN,
    LOG,
    SMS,
    SECURITY,
    BACKOFFICE
}