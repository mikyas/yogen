package com.yogen.util.enumtype;

public enum ItemType implements IValueEnum {

    PHYSICAL(1),
    TRAVEL(2),
    EVENT(3),
    DIGITAL(4);

    private final Integer value;

    private ItemType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}