package com.yogen.util.enumtype;

public enum GenericStatus implements IValueEnum {

    PASSIVE_NEGATIVE(0),
    ACTIVE_POSITIVE(1);

    private final Integer value;

    private GenericStatus(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}