package com.yogen.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import javax.servlet.http.HttpServletRequest;

public class WebUtil {

    private static final String UNKNOWN_IP_TEXT_FROM_WAF = "unknown";

    public static String getClientIp(HttpServletRequest request) {
        String ip;
        try {
            ip = request.getHeader("CLIENT_IP_ADDR"); //set up on  by system administrator

            if (StringUtil.isNullOrZeroLength(ip) || UNKNOWN_IP_TEXT_FROM_WAF.equalsIgnoreCase(ip)) {
                ip = request.getHeader("X-Forwarded-For");
            }
            if (StringUtil.isNullOrZeroLength(ip) || UNKNOWN_IP_TEXT_FROM_WAF.equalsIgnoreCase(ip)) {
                ip = request.getHeader("Proxy-Client-IP");
            }
            if (StringUtil.isNullOrZeroLength(ip) || UNKNOWN_IP_TEXT_FROM_WAF.equalsIgnoreCase(ip)) {
                ip = request.getHeader("WL-Proxy-Client-IP");
            }
            if (StringUtil.isNullOrZeroLength(ip) || UNKNOWN_IP_TEXT_FROM_WAF.equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_CLIENT_IP");
            }
            if (StringUtil.isNullOrZeroLength(ip) || UNKNOWN_IP_TEXT_FROM_WAF.equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_X_FORWARDED_FOR");
            }
            if (StringUtil.isNullOrZeroLength(ip) || UNKNOWN_IP_TEXT_FROM_WAF.equalsIgnoreCase(ip)) {
                ip = request.getRemoteAddr();
            }
        } catch (Exception ex) {
            ip = request.getRemoteAddr();
        }

        return ip;
    }

    public static String post(String urlAddress, String data) {
        try {
            URL url = new URL(urlAddress);
            URLConnection urlConnection = url.openConnection();
            HttpURLConnection connection = (HttpURLConnection) urlConnection;
            connection.setRequestProperty("Content-Type", "application/xml; charset=utf-8");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            OutputStream outputStream = connection.getOutputStream();
            OutputStreamWriter wout = new OutputStreamWriter(outputStream, "UTF-8");
            wout.write(data);
            wout.flush();
            outputStream.close();
            InputStream inputStream = connection.getInputStream();
            StringBuilder builder = new StringBuilder();
            int c;
            while ((c = inputStream.read()) != -1) {
                builder.append((char) c);
            }
            inputStream.close();
            outputStream.close();
            connection.disconnect();
            return builder.toString();
        } catch (IOException e) {
            return null;
        }
    }
}