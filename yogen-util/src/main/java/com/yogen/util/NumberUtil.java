package com.yogen.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;


public class NumberUtil {

    private static DecimalFormat decimalFormat2 = new DecimalFormat("#,##0.00");
    private static DecimalFormat decimalFormat = new DecimalFormat("#.##");
    private static DecimalFormat fourDigitDecimalFormat = new DecimalFormat("#.####");

    public static double roundAsDecimalFormat(double number) {
        return Double.parseDouble(decimalFormat.format(number));
    }

    public static double round(double number) {
        return roundAsDecimalFormat(number);
    }

    public static String formatForAccounting(double number) {
        return decimalFormat2.format(number);
    }

    public static String format(double number) {
        return decimalFormat.format(number);
    }

    public static double roundAsFourDigitDecimalFormat(double number) {
        return Double.parseDouble(fourDigitDecimalFormat.format(number));
    }

    public static double roundDown(double number) {
        BigDecimal bigDecimal = new BigDecimal(number);
        return bigDecimal.setScale(2, RoundingMode.DOWN).doubleValue();
    }

    public static double roundHalfUp(double number) {
        BigDecimal bigDecimal = new BigDecimal(String.valueOf(number));
        return bigDecimal.setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public static double parseDouble(Integer price) {
        String priceText = price.toString();
        return parseDouble(priceText);
    }

    public static double parseDouble(String price) {
        if (price.length() == 1) {
            return Double.valueOf("0.0" + price);
        } else if (price.length() == 2) {
            return Double.valueOf("0." + price);
        } else if (price.length() > 2) {
            String decimalText = price.substring(price.length() - 2, price.length());
            String numberText = price.substring(0, price.length() - 2);
            return Double.valueOf(numberText + "." + decimalText);
        }
        return 0d;
    }

    public static int parseInt(Double amount) {
        String price = (decimalFormat.format(new BigDecimal(amount)));
        String[] splittedAmount = price.split("\\.");
        String[] nums = new String[2];
        nums[0] = splittedAmount[0];
        if (splittedAmount.length == 1) {
            nums[1] = "00";
        } else if (splittedAmount.length == 2 && splittedAmount[1].length() == 1) {
            nums[1] = splittedAmount[1] + "0";
        } else {
            nums[1] = splittedAmount[1];
        }
        return Integer.valueOf(nums[0] + nums[1]);
    }

    public static String parseString(Double amount) {
        String price = (decimalFormat.format(new BigDecimal(amount)));
        String[] splittedAmount = price.split("\\.");
        String[] nums = new String[2];
        nums[0] = splittedAmount[0];
        if (splittedAmount.length == 1) {
            nums[1] = "00";
        } else if (splittedAmount.length == 2 && splittedAmount[1].length() == 1) {
            nums[1] = splittedAmount[1] + "0";
        } else {
            nums[1] = splittedAmount[1];
        }
        return nums[0] + nums[1];
    }

    /**
     * @param price ex: 20.0
     * @return 2 digit decimal ex: 20.00
     */
    public static String getFormattedPrice(Double price) {
        String priceText = String.valueOf(decimalFormat.format(new BigDecimal(price)));

        String[] splittedPrice = priceText.split("\\.");
        if (splittedPrice.length > 1) {
            if (splittedPrice[1].length() == 1) {
                priceText = splittedPrice[0] + "." + splittedPrice[1] + "0";
            }
        } else {
            priceText = priceText + ".00";
        }
        return priceText;
    }

    public static String getNumberPlaces(Double number) {
        return String.valueOf(decimalFormat.format(new BigDecimal(number))).split("\\.")[0];
    }

    public static String getDecimalPlaces(Double number) {
        String numberText = String.valueOf(decimalFormat.format(new BigDecimal(number)));
        String[] splittedPrice = numberText.split("\\.");
        if (splittedPrice.length > 1) {
            if (splittedPrice[1].length() == 1) {
                return splittedPrice[1] + "0";
            } else {
                return splittedPrice[1];
            }
        } else {
            return "00";
        }
    }
    public static String geneterateStringStatementFromNumber(int amount) {
        int birler, onlar, yuzler, binler, onbinler, yuzbinler;
        birler = amount % 10;
        onlar = (amount / 10) % 10;
        yuzler = (amount / 100) % 10;
        binler = (amount / 1000) % 10;
        onbinler = (amount / 10000) % 10;
        yuzbinler = (amount / 100000) % 10;
        String[] birlik = {"", "bir", "iki", "üç", "dört", "beş", "altı", "yedi", "sekiz", "dokuz"};
        String[] onluk = {"", "on", "yirmi", "otuz", "kırk", "elli", "altmış", "yetmiş", "seksen", "doksan"};
        String[] yuzluk = {"", "yüz", "ikiyüz", "üçyüz", "dörtyüz", "beşyüz", "altıyüz", "yediyüz", "sekizyüz", "dokuzyüz"};
        String binlikAyraci = (binler > 0 || onbinler > 0 || yuzbinler > 0) ? "bin" : "";
        return yuzluk[yuzbinler] + onluk[onbinler] + birlik[binler] + binlikAyraci + yuzluk[yuzler] + onluk[onlar] + birlik[birler];
    }

    public static String amountToText(double amount) {
        String[] strings = String.valueOf(amount).split("\\.");
        String preDot = strings[0];
        String posDot = strings[1];
        if (posDot.length() == 1) {
            posDot += "0";
        }
        preDot = convertDoubleAmountToTurkishSpelling(Integer.valueOf(preDot));
        posDot = convertDoubleAmountToTurkishSpelling(Integer.valueOf(posDot));
        return "Yalnız " + (preDot.equals("") ? "" : preDot + " TL ") + (posDot.equals("") ? "" : posDot + " kuruş");
    }

    public static String convertDoubleAmountToTurkishSpelling(int amount) {
        int birler, onlar, yuzler, binler, onbinler, yuzbinler;
        birler = amount % 10;
        onlar = (amount / 10) % 10;
        yuzler = (amount / 100) % 10;
        binler = (amount / 1000) % 10;
        onbinler = (amount / 10000) % 10;
        yuzbinler = (amount / 100000) % 10;
        String[] birlik = {"", "bir", "iki", "üç", "dört", "beş", "altı", "yedi", "sekiz", "dokuz"};
        String[] onluk = {"", "on", "yirmi", "otuz", "kırk", "elli", "altmış", "yetmiş", "seksen", "doksan"};
        String[] yuzluk = {"", "yüz", "ikiyüz", "üçyüz", "dörtyüz", "beşyüz", "altıyüz", "yediyüz", "sekizyüz", "dokuzyüz"};
        String[] binlik = {"", "bin", "ikibin", "üçbin", "dörtbin", "beşbin", "altıbin", "yedibin", "sekizbin", "dokuzbin"};
        return yuzluk[yuzbinler] + onluk[onbinler] + binlik[binler] + yuzluk[yuzler] + onluk[onlar] + birlik[birler];
    }
}