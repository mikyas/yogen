package com.yogen.util;

import java.util.ResourceBundle;
import com.yogen.util.enumtype.ProfileMode;

public class ProfileUtil {

    public static final ProfileMode PROFILE;
    private static final ResourceBundle PROFILE_RESOURCE_BUNDLE = ResourceBundle.getBundle("profile");

    static {
        PROFILE = ProfileMode.valueOf(PROFILE_RESOURCE_BUNDLE.getString("profile"));
    }

    public static boolean isProd() {
        return PROFILE.equals(ProfileMode.PROD);
    }
    
    public static boolean isLocal() {
        return PROFILE.equals(ProfileMode.LOCAL);
    }
}
