package com.yogen.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import com.yogen.util.enumtype.DatePeriod;

public class DateUtil {

    public static final String MYSQL_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String MYSQL_DATE_FORMAT = "yyyy-MM-dd";

    public static String genericFormatDate(String pattern, Date date) {
        return new SimpleDateFormat(pattern).format(date);
    }

    public static String toMYSQLDateFormat(Date date) {
        return new SimpleDateFormat(MYSQL_DATE_FORMAT).format(date);
    }

    public static String toMYSQLDateTimeFormat(Date date) {
        return new SimpleDateFormat(MYSQL_DATE_TIME_FORMAT).format(date);
    }

    public static Date getDate(int year, int month, int day, int hour, int minute, int second) {
        Calendar cal = new GregorianCalendar(year, intToCalendarMonth(month), day, hour, minute, second);
        return cal.getTime();
    }

    public static Date getDate(int year, int month, int day, int hour, int minute) {
        Calendar cal = new GregorianCalendar(year, intToCalendarMonth(month), day, hour, minute);
        return cal.getTime();
    }

    public static final Date getDate(int year, int month, int day) {
        Calendar cal = new GregorianCalendar(year, intToCalendarMonth(month), day);
        return cal.getTime();
    }

    public static Date getDate(String formattedDate, String format) {
        Date date = null;
        if (!StringUtil.isNullOrZeroLength(formattedDate)) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            try {
                date = simpleDateFormat.parse(formattedDate);
            } catch (ParseException ignored) {
            }
        }
        return date;
    }

    public static Date setHourMinuteSecondAs235959(Date date) {
        return setHourMinuteSecond(date, 23, 59, 59);
    }

    public static Date setHourMinuteSecondMilisecondAs235959999(Date date) {
        return setHourMinuteSecondMiliSecond(date, 23, 59, 59, 999);
    }

    public static Date setHourMinuteSecondAs000000(Date date) {
        return setHourMinuteSecond(date, 0, 0, 0);
    }

    public static Date setHourMinuteSecondMilisecondAs000000000(Date date) {
        return setHourMinuteSecondMiliSecond(date, 0, 0, 0, 0);
    }

    public static Date setHourMinuteSecond(Date date, int hour, int minute, int second) {
        if (date != null) {
            Calendar cal = new GregorianCalendar();
            cal.setTime(date);
            cal.set(Calendar.HOUR_OF_DAY, hour);
            cal.set(Calendar.MINUTE, minute);
            cal.set(Calendar.SECOND, second);
            cal.set(Calendar.MILLISECOND, 0);
            return cal.getTime();
        }
        return date;
    }

    public static Date setHourMinuteSecondMiliSecond(Date date, int hour, int minute, int second, int milisecond) {
        if (date != null) {
            Calendar cal = new GregorianCalendar();
            cal.setTime(date);
            cal.set(Calendar.HOUR_OF_DAY, hour);
            cal.set(Calendar.MINUTE, minute);
            cal.set(Calendar.SECOND, second);
            cal.set(Calendar.MILLISECOND, milisecond);
            return cal.getTime();
        }
        return date;
    }

    public static Date setSecondAs59(Date date) {
        if (date != null) {
            Calendar cal = new GregorianCalendar();
            cal.setTime(date);
            cal.set(Calendar.SECOND, 59);
            return cal.getTime();
        }
        return date;
    }

    public static String getDayAsString(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        if (day < 10) {
            return "0" + day;
        }
        return "" + day;
    }

    private static int calendarMonthToInt(int calendarMonth) {
        if (calendarMonth == Calendar.JANUARY)
            return 1;
        else if (calendarMonth == Calendar.FEBRUARY)
            return 2;
        else if (calendarMonth == Calendar.MARCH)
            return 3;
        else if (calendarMonth == Calendar.APRIL)
            return 4;
        else if (calendarMonth == Calendar.MAY)
            return 5;
        else if (calendarMonth == Calendar.JUNE)
            return 6;
        else if (calendarMonth == Calendar.JULY)
            return 7;
        else if (calendarMonth == Calendar.AUGUST)
            return 8;
        else if (calendarMonth == Calendar.SEPTEMBER)
            return 9;
        else if (calendarMonth == Calendar.OCTOBER)
            return 10;
        else if (calendarMonth == Calendar.NOVEMBER)
            return 11;
        else if (calendarMonth == Calendar.DECEMBER)
            return 12;
        else
            return 1;
    }

    private static int intToCalendarMonth(int month) {
        if (month == 1)
            return Calendar.JANUARY;
        else if (month == 2)
            return Calendar.FEBRUARY;
        else if (month == 3)
            return Calendar.MARCH;
        else if (month == 4)
            return Calendar.APRIL;
        else if (month == 5)
            return Calendar.MAY;
        else if (month == 6)
            return Calendar.JUNE;
        else if (month == 7)
            return Calendar.JULY;
        else if (month == 8)
            return Calendar.AUGUST;
        else if (month == 9)
            return Calendar.SEPTEMBER;
        else if (month == 10)
            return Calendar.OCTOBER;
        else if (month == 11)
            return Calendar.NOVEMBER;
        else if (month == 12)
            return Calendar.DECEMBER;
        else
            return Calendar.JANUARY;
    }

    public static String getMonthNameInTurkish(int month) {
        return getMonthNameInTurkishWithCalendarMonth(intToCalendarMonth(month));
    }

    public static String getMonthNameInTurkishWithCalendarMonth(int calendarMonth) {
        if (calendarMonth == Calendar.JANUARY)
            return "OCAK";
        else if (calendarMonth == Calendar.FEBRUARY)
            return "ŞUBAT";
        else if (calendarMonth == Calendar.MARCH)
            return "MART";
        else if (calendarMonth == Calendar.APRIL)
            return "NİSAN";
        else if (calendarMonth == Calendar.MAY)
            return "MAYIS";
        else if (calendarMonth == Calendar.JUNE)
            return "HAZİRAN";
        else if (calendarMonth == Calendar.JULY)
            return "TEMMUZ";
        else if (calendarMonth == Calendar.AUGUST)
            return "AĞUSTOS";
        else if (calendarMonth == Calendar.SEPTEMBER)
            return "EYLÜL";
        else if (calendarMonth == Calendar.OCTOBER)
            return "EKİM";
        else if (calendarMonth == Calendar.NOVEMBER)
            return "KASIM";
        else if (calendarMonth == Calendar.DECEMBER)
            return "ARALIK";
        else
            return "OCAK";
    }

    public static String getMonthNumberAsString(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        int month = calendarMonthToInt(cal.get(Calendar.MONTH));
        if (month < 10)
            return "0" + month;
        return "" + month;
    }

    public static int getDay(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    public static int getMonth(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        return calendarMonthToInt(cal.get(Calendar.MONTH));
    }

    public static int getYear(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.get(Calendar.YEAR);
    }

    public static String getHourAsString(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        if (hour < 10) {
            return "0" + hour;
        }
        return "" + hour;
    }

    public static String getMinuteAsString(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        int minute = cal.get(Calendar.MINUTE);
        if (minute < 10) {
            return "0" + minute;
        }
        return "" + minute;
    }

    public static String getSecondAsString(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        int second = cal.get(Calendar.SECOND);
        if (second < 10) {
            return "0" + second;
        }
        return "" + second;
    }

    public static String getHourMinuteSecondAsString(Date date) {
        if (date == null) {
            return "";
        }
        return getHourAsString(date) + ":" + getMinuteAsString(date) + ":" + getSecondAsString(date);
    }

    public static int getHour(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.get(Calendar.HOUR_OF_DAY);
    }

    public static int getMinute(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.get(Calendar.MINUTE);
    }

    public static int getSecond(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.get(Calendar.SECOND);
    }

    public static int getMillisecond(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.get(Calendar.MILLISECOND);
    }

    public static int getDayOfWeek(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_WEEK);
    }

    public static Date getToday() {
        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getStartOfToday() {
        return getToday();
    }

    public static Date getEndOfToday() {
        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return cal.getTime();
    }

    public static Date getEndOfDay(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return cal.getTime();
    }

    public static Date getEndOfDayWithoutMilis(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal.getTime();
    }

    public static Date getStartOfDay(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getStartOfNextMonth() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + 1);
        cal.set(Calendar.DATE, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getEndOfPreviousMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return setHourMinuteSecondAs235959(calendar.getTime());
    }

    public static Date getEndOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return setHourMinuteSecondAs235959(calendar.getTime());
    }

    public static Date getEndOfMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return setHourMinuteSecondAs235959(calendar.getTime());
    }

    public static String toString(Date date) {
        if (date == null) {
            return "";
        }
        return getDayAsString(date) + "." + getMonthNumberAsString(date) + "." + getYear(date);
    }

    public static String toString(Date date, String seperator) {
        if (date == null) {
            return "";
        }
        return getDayAsString(date) + seperator + getMonthNumberAsString(date) + seperator + getYear(date);
    }

    public static String toStringDetailed(Date date) {
        if (date == null) {
            return "";
        }
        return getDayAsString(date) + "." + getMonthNumberAsString(date) + "." + getYear(date) + " " +
                getHourAsString(date) + ":" + getMinuteAsString(date) + ":" + getSecondAsString(date);
    }

    public static String toStringDetailedWithMiliSecond(Date date) {
        if (date == null) {
            return "";
        }
        return getDayAsString(date) + "." + getMonthNumberAsString(date) + "." + getYear(date) + " " +
                getHourAsString(date) + ":" + getMinuteAsString(date) + ":" + getSecondAsString(date) + ":" + getMillisecond(date);
    }

    public static String toStringDetailedNotHour(Date date) {
        if (date == null) {
            return "";
        }
        return getDayAsString(date) + "." + getMonthNumberAsString(date) + "." + getYear(date);
    }

    public static String toStringDetailedNotSecond(Date date) {
        if (date == null) {
            return "";
        }
        return getDayAsString(date) + "." + getMonthNumberAsString(date) + "." + getYear(date) + " " +
                getHourAsString(date) + ":" + getMinuteAsString(date);
    }

    public static String toStringDetailedAsTimestamp(Date date) {
        if (date == null) {
            return "";
        }
        return getYear(date) + getMonthNumberAsString(date) + getDayAsString(date) + getHourAsString(date) + getMinuteAsString(date) + getSecondAsString(date);
    }

    public static String toStringSlashed(Date date) {
        if (date == null) {
            return "";
        }
        return getDayAsString(date) + "/" + getMonthNumberAsString(date) + "/" + getYear(date) + " " +
                getHour(date) + ":" + getMinute(date) + ":" + getSecond(date);
    }

    public static String toStringSlashedNotHour(Date date) {
        if (date == null) {
            return "";
        }
        return getDayAsString(date) + "/" + getMonthNumberAsString(date) + "/" + getYear(date);
    }

    public static Date getCurrentDateWithTimes(int hourOfDay, int minute, int second, int millisecond) {
        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);
        cal.set(Calendar.MILLISECOND, millisecond);

        return cal.getTime();
    }

    public static final Date addSubstractDays(Date target, int days, boolean isAdd) {
        if (!isAdd) {
            days = -1 * days;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(target);
        cal.add(Calendar.DAY_OF_MONTH, days);

        return cal.getTime();
    }

    public static final Date addYears(Date target, int years) {
        return addRemoveCustomAmounts(target, Calendar.YEAR, years);
    }

    public static final Date addMonth(Date target, int months) {
        return addRemoveCustomAmounts(target, Calendar.MONTH, months);
    }

    public static final Date addDays(Date target, int days) {
        return addSubstractDays(target, days, true);
    }

    /**
     * @param firstDate
     * @param datePeriod
     * @param frequency
     * @param occurrence
     * @return
     * @author tmikyas
     * Bu yordam gelen parametreler dogrultusunda ilk tarihten itibaren gelecek tarihler listesini olusturarak doner. <br>
     * <p>
     * firstDate parametresine verilen datePeriod ve frequency parametrelerine istinaden <br>
     * occurence(tekrar) kadar eklemeler yapilarak <br>
     * elde edilen tarihler liste şeklinde donderilir.
     */
    public static List<Date> getFutureDateList(Date firstDate, DatePeriod datePeriod, int frequency, Integer occurrence) {
        List<Date> futureDateList = new ArrayList<>();
        futureDateList.add(firstDate);
        for (int i = 1; i < occurrence; i++) {
            Date addedDate = getAddedDateByDatePeriod(datePeriod, firstDate, i, frequency);
            futureDateList.add(addedDate);
        }
        return futureDateList;
    }

    /**
     * @param firstDate
     * @param datePeriod
     * @param frequency
     * @param lastDate
     * @return
     * @author tmikyas
     * Bu yordam gelen parametreler dogrultusunda ilk tarihten itibaren gelecek tarihler listesini olusturarak doner. <br>
     * <p>
     * firstDate parametresine verilen datePeriod ve frequency parametrelerine istinaden <br>
     * lastDate(son tarih)'e kadar lastDate dahil son uygun tarihe kadar eklemeler yapilarak <br>
     * elde edilen tarihler liste şeklinde donderilir.
     */
    public static List<Date> getFutureDateList(Date firstDate, DatePeriod datePeriod, int frequency, Date lastDate) {
        List<Date> futureDateList = new ArrayList<>();
        futureDateList.add(firstDate);
        boolean isAfterLastDate = false;
        for (int i = 1; !isAfterLastDate; i++) {
            Date addedDate = getAddedDateByDatePeriod(datePeriod, firstDate, i, frequency);
            isAfterLastDate = addedDate.after(lastDate);
            if (!isAfterLastDate) {
                futureDateList.add(addedDate);
            }
        }
        return futureDateList;
    }

    /**
     * verilen period'a uygun eklemeleri yaparak verilen tarihe Eklenmis tarihi doner
     *
     * @param datePeriod
     * @param date
     * @param order = kacinci siradaki deger. ornek 3 icin 3. hafta*sıklık eklenmis tarihi getir
     * @param frequency
     * @return
     */
    public static Date getAddedDateByDatePeriod(DatePeriod datePeriod, Date date, int order, int frequency) {
        switch (datePeriod) {
            case DAILY:
                return addDays(date, (order * frequency));
            case WEEKLY:
                return addDays(date, (order * (7 * frequency)));
            case MONTHLY:
                return addMonth(date, (order * frequency));
            case YEARLY:
                return addYears(date, (order * frequency));
            default:
                return new Date();
        }
    }

    public static final Date substractDays(Date target, int days) {
        return addSubstractDays(target, days, false);
    }

    private static Date addRemoveCustomAmounts(Date date, int field, int amount) {
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(date);
        calendar.add(field, amount);

        return calendar.getTime();
    }

    public static Date addRemoveDays(Date date, int days) {
        return addRemoveCustomAmounts(date, Calendar.DAY_OF_MONTH, days);
    }

    public static Date addRemoveHours(Date date, int hours) {
        return addRemoveCustomAmounts(date, Calendar.HOUR_OF_DAY, hours);
    }

    public static Date addRemoveMinutes(Date date, int minutes) {
        return addRemoveCustomAmounts(date, Calendar.MINUTE, minutes);
    }

    public static Date addRemoveSeconds(Date date, int seconds) {
        return addRemoveCustomAmounts(date, Calendar.SECOND, seconds);
    }

    public static Date getStartOfThisWeek() {
        Date date = new Date();
        int dayOfWeek = DateUtil.getDayOfWeek(date);
        if (dayOfWeek == 1) {
            date = addRemoveDays(date, -6);
        } else if (dayOfWeek > 2) {
            date = addRemoveDays(date, -(dayOfWeek - Calendar.MONDAY));
        }
        return setHourMinuteSecondMilisecondAs000000000(date);
    }

    public static Date getStartOfThisMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        return setHourMinuteSecondMilisecondAs000000000(calendar.getTime());
    }

    public static Date getDateFromMySQLFormat(String dateStr) {
        Date date = null;
        if (!StringUtil.isNullOrZeroLength(dateStr)) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(MYSQL_DATE_FORMAT);
            try {
                date = simpleDateFormat.parse(dateStr);
            } catch (ParseException ignored) {
            }
        }

        return date;
    }

    public static Date getDateTimeFromMySQLFormat(String dateStr) throws ParseException {
        Date date = null;
        if (!StringUtil.isNullOrZeroLength(dateStr)) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(MYSQL_DATE_TIME_FORMAT);
            date = simpleDateFormat.parse(dateStr);
        }

        return date;
    }

    public static String genericDiffAsString(long diff, boolean printDays, boolean printHours, boolean printMinutes, boolean printSeconds, boolean printMilliseconds) {
        String text = "";

        if (diff > 0) {
            long msPerMillisecond = 1;
            long msPerSecond = msPerMillisecond * 1000;
            long msPerMinute = msPerSecond * 60;
            long msPerHour = msPerMinute * 60;
            long msPerDay = msPerHour * 24;

            int days = Long.valueOf(diff / msPerDay).intValue();

            if (days > 0 && printDays) {
                text += days + " g\u00FCn";
                diff = diff - (days * msPerDay);
            }

            int hours = Long.valueOf(diff / msPerHour).intValue();
            if (hours > 0 && printHours) {
                text += " " + hours + " saat";
                diff = diff - (hours * msPerHour);
            }

            int minutes = Long.valueOf(diff / msPerMinute).intValue();
            if (minutes > 0 && printMinutes) {
                text += " " + minutes + " dakika";
                diff = diff - (minutes * msPerMinute);
            }

            int seconds = Long.valueOf(diff / msPerSecond).intValue();
            if (seconds > 0 && printSeconds) {
                text += " " + seconds + " saniye";
                diff = diff - (seconds * msPerSecond);
            }

            int milliseconds = Long.valueOf(diff / msPerMillisecond).intValue();
            if (milliseconds > 0 && printMilliseconds) {
                text += " " + milliseconds + " salise";
                diff = diff - (milliseconds * msPerMillisecond);
            }
        }

        return text.trim();
    }

    public static String dayHourMinuteSecondDiffAsString(long diff) {
        String text = genericDiffAsString(diff, true, true, true, true, false);

        return text;
    }

    public static String dayHourMinuteSecondDiffAsString(Date endDate, Date startDate) {
        if (startDate != null && endDate != null && endDate.after(startDate)) {
            return dayHourMinuteSecondDiffAsString(endDate.getTime() - startDate.getTime());
        }

        return "";
    }

    public static String dayHourMinuteDiffAsString(long diff) {
        String text = genericDiffAsString(diff, true, true, true, false, false);

        return text;
    }

    public static String dayHourMinuteDiffAsString(Date endDate, Date startDate) {
        if (startDate != null && endDate != null && endDate.after(startDate)) {
            return dayHourMinuteDiffAsString(endDate.getTime() - startDate.getTime());
        }

        return "";
    }

    public static String hourMinuteSecondDiffAsString(long diff) {
        return genericDiffAsString(diff, false, true, true, true, true);
    }

    public static String hourMinuteSecondDiffAsString(Date endDate, Date startDate) {
        if (startDate != null && endDate != null && endDate.after(startDate)) {
            return hourMinuteSecondDiffAsString(endDate.getTime() - startDate.getTime());
        }

        return "";
    }

    public static String minuteSecondDiffAsString(long diff) {
        String text = genericDiffAsString(diff, false, false, true, true, false);

        return text;
    }

    public static String minuteSecondDiffAsString(Date endDate, Date startDate) {
        if (startDate != null && endDate != null && endDate.after(startDate)) {
            return minuteSecondDiffAsString(endDate.getTime() - startDate.getTime());
        }

        return "";
    }

    public static int dayDiff(Date first, Date second) {
        long msPerDay = 1000 * 60 * 60 * 24;

        long diff = (first.getTime() / msPerDay) - (second.getTime() / msPerDay);

        return Long.valueOf(diff).intValue();
    }

    public static long toTimeInSeconds(Date date) {
        long timeInSecond = 0;

        if (date != null && date.getTime() > 0) {
            timeInSecond = date.getTime() / 1000;
        }

        return timeInSecond;
    }

    public static boolean isInSameDay(Date date1, Date date2) {
        Calendar cal1 = new GregorianCalendar();
        cal1.setTime(date1);
        Calendar cal2 = new GregorianCalendar();
        cal2.setTime(date2);

        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH)
                && cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH);
    }

    public static boolean isInSameMonth(Date date1, Date date2) {
        Calendar cal1 = new GregorianCalendar();
        cal1.setTime(date1);
        Calendar cal2 = new GregorianCalendar();
        cal2.setTime(date2);

        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH);
    }

    public static long timeDiffAsSecond(Date endDate, Date startDate) {
        Calendar calendarEndDate = Calendar.getInstance();
        Calendar calendarStartDate = Calendar.getInstance();
        calendarEndDate.setTime(endDate);
        calendarStartDate.setTime(startDate);
        return (calendarEndDate.getTimeInMillis() - calendarStartDate.getTimeInMillis()) / 1000;
    }

    public static boolean isDayDiffMoreThanGivenHour(Date first, Date second, int hour) {
        long msPerDay = 1000 * 60 * 60 * hour;
        long diff = first.getTime() - second.getTime();
        return diff > msPerDay;
    }

    public static Date getStartOfNextMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, 1);
        cal.set(Calendar.DATE, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static List<Integer> getPreviousYearList(Integer yearsBefore, Integer startYear) {
        List<Integer> yearList = new ArrayList<Integer>();
        Calendar calNow = new GregorianCalendar();
        Calendar calstartYear = new GregorianCalendar();
        calNow.getTime();

        if (yearsBefore != null) {
            calstartYear.set(Calendar.YEAR, calNow.get(Calendar.YEAR) - yearsBefore);
        } else {
            calstartYear.set(Calendar.YEAR, startYear);
        }

        for (int y = calNow.get(Calendar.YEAR); y >= calstartYear.get(Calendar.YEAR); y--) {
            yearList.add(y);
        }
        return yearList;
    }

    public static boolean isDateInWeekend(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
    }

    public static List<Date> getDayListInInterval(Date startDate, Date endDate) {
        List<Date> dayList = new ArrayList<>();

        if (startDate.getTime() > endDate.getTime()) {
            return dayList;
        }

        dayList.add(startDate);
        while (!DateUtil.isInSameDay(startDate, endDate)) {
            startDate = addDays(startDate, 1);
            dayList.add(startDate);
        }

        return dayList;
    }

    public static Date addToDateByMinutes(Date baseDate, Integer minutes) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(baseDate);
        calendar.add(Calendar.MINUTE, minutes);

        return calendar.getTime();

    }//End of Method

    public static Integer getNumberOfDaysBetweenTwoDate(Date dateOne, Date dateTwo) {
        return (int) ((dateTwo.getTime() - dateOne.getTime()) / (1000 * 60 * 60 * 24));
    }
}