package com.yogen.util;

import java.net.URL;
import java.util.Locale;

public class UrlUtil {

    public static String getDomain(URL url) {
        String domain = url.getHost();

        boolean hasTr = domain.substring(domain.length() - 2, domain.length()).equalsIgnoreCase("tr");
        if (hasTr) {
            domain = domain.substring(0, domain.length() - 3);
        }

        // Exceptional Case
        boolean hasTc = domain.substring(domain.length() - 6, domain.length()).equalsIgnoreCase("com.tc");
        if (hasTc) {
            domain = domain.substring(0, domain.length() - 3);
        }

        // Exceptional Case
        boolean hasTrGG = domain.substring(domain.length() - 5, domain.length()).equalsIgnoreCase("tr.gg");
        if (hasTrGG) {
            domain = domain.substring(0, domain.length() - 5);
        }

        if (!domain.contains("www")) {
            domain = "www." + domain;
        }

        int lastIndex = 0;
        int count = 0;
        String dot = ".";

        while (lastIndex != -1) {
            lastIndex = domain.indexOf(dot, lastIndex);
            if (lastIndex != -1) {
                lastIndex++;
                count++;
            }
        }

        if (count > 1) {
            domain = new StringBuffer(domain).reverse().toString();
            domain = domain.substring(0, domain.indexOf(dot, domain.indexOf(dot) + 1));
            domain = new StringBuffer(domain).reverse().toString();
        }

        if (hasTr) {
            domain = domain + ".tr";
        }

        if (hasTc) {
            domain = domain + ".tc";
        }

        if (hasTrGG) {
            domain = domain + ".tr.gg";
        }

        domain = StringUtil.replaceTurkishCharacterWithEnglishOnes(domain);
        return domain;
    }

    public static String getDomainWithoutExtension(String website) {
        try {
            URL url = new URL(website);
            String domain = url.getHost();

            boolean hasTr = domain.substring(domain.length() - 2, domain.length()).equalsIgnoreCase("tr");
            if (hasTr) {
                domain = domain.substring(0, domain.length() - 3);
            }

            // Exceptional Case
            boolean hasTc = domain.substring(domain.length() - 6, domain.length()).equalsIgnoreCase("com.tc");
            if (hasTc) {
                domain = domain.substring(0, domain.length() - 3);
            }

            // Exceptional Case
            boolean hasTrGG = domain.substring(domain.length() - 5, domain.length()).equalsIgnoreCase("tr.gg");
            if (hasTrGG) {
                domain = domain.substring(0, domain.length() - 5);
            }

            if (!domain.contains("www")) {
                domain = "www." + domain;
            }

            int lastIndex = 0;
            int count = 0;
            String dot = ".";

            while (lastIndex != -1) {
                lastIndex = domain.indexOf(dot, lastIndex);
                if (lastIndex != -1) {
                    lastIndex++;
                    count++;
                }
            }

            if (count > 1) {
                domain = new StringBuffer(domain).reverse().toString();
                domain = domain.substring(0, domain.indexOf(dot, domain.indexOf(dot) + 1));
                domain = new StringBuffer(domain).reverse().toString();
                domain = domain.substring(0, domain.indexOf(dot));
            }

            domain = StringUtil.replaceTurkishCharacterWithEnglishOnes(domain);
            return domain.toUpperCase(Locale.ENGLISH);
        } catch (Exception e) {
            return "";
        }
    }
}