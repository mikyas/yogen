package com.yogen.util.exception;

public interface YogenThrowable {

    String getMessage();
}
