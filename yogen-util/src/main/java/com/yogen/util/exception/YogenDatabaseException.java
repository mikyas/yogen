/**
 * 
 */
package com.yogen.util.exception;

import java.io.Serializable;
import java.sql.SQLException;

/**
 * @author mikyas
 *
 */
public class YogenDatabaseException extends SQLException {
	/**
	 * 
	 */
	public YogenDatabaseException() {
		super();
	}

	/**
	 * @param message
	 */
	public YogenDatabaseException(String message) {
		super(message);
	}

	/**
	 * @param throwable
	 */
	public YogenDatabaseException(Throwable throwable) {
		super(throwable);
	}

	/**
	 * @param message
	 * @param throwable
	 */
	public YogenDatabaseException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public <ID extends Serializable> YogenDatabaseException(String msgKey, ID id, String simpleName) {

	}
}
