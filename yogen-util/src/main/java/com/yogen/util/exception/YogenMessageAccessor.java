package com.yogen.util.exception;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class YogenMessageAccessor {
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("error_messages");

    public static final String CODE = "code.";
    public static final String EN = "en.";
    public static final String TR = "tr.";

    private YogenMessageAccessor() {
        super();
    }

    public static String getString(String key) {
        try {
            return RESOURCE_BUNDLE.getString(key);
        } catch (MissingResourceException e) {
            return '!' + key + '!';
        }
    }
}