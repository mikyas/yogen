package com.yogen.util.exception;

public class YogenRuntimeException extends RuntimeException implements YogenThrowable {

    private String key;
    private Object[] arguments;

    public YogenRuntimeException(String key) {
        this.key = key;
    }

    public YogenRuntimeException(Object... arguments) {
        this.arguments = arguments;
    }

    public YogenRuntimeException(String key, Object... arguments) {
        this.key = key;
        this.arguments = arguments;
    }

    public YogenRuntimeException(String key, Throwable cause, Object... arguments) {
        super(cause);
        this.key = key;
        this.arguments = arguments;
    }

    private String getMessage(String prefix) {
        String message = YogenMessageAccessor.getString(prefix + key);
        if (arguments != null) {
            for (int i = 0; i < arguments.length; i++) {
                message = message.replace("{" + i + "}", arguments[i] != null ? arguments[i].toString() : "null");
            }
        }
        return message;
    }

    private String getArgumentMessage() {
        if (arguments != null) {
            StringBuffer buffer = new StringBuffer();
            for (int i = 0; i < arguments.length; i++) {
                buffer.append(arguments[i] != null ? arguments[i].toString() : "null");
            }
            return buffer.toString();
        }
        return "";
    }

    public String getMessage() {
        String message = key != null ? getMessage(YogenMessageAccessor.EN) : getArgumentMessage();
        return message != null ? message : "";
    }

    public String getLocalizedMessage() {
        String message = key != null ? getMessage(YogenMessageAccessor.TR) : getArgumentMessage();
        return message != null ? message : "";
    }

    public String getErrorCode() {
        if (key != null) {
            return YogenMessageAccessor.getString(YogenMessageAccessor.CODE + key);
        }
        return "";
    }
}