package com.yogen.util.exception;

public class YogenException extends Exception implements YogenThrowable {

    private String key;
    private Object[] arguments;

    public YogenException(String key) {
        this.key = key;
    }

    public YogenException(String key, Object... arguments) {
        this.key = key;
        this.arguments = arguments;
    }

    private String getMessage(String prefix) {
        String message = YogenMessageAccessor.getString(prefix + key);
        if (arguments != null) {
            for (int i = 0; i < arguments.length; i++) {
                if (arguments[i] != null) {
                    message = message.replace("{" + i + "}", arguments[i].toString());
                }
            }
        }
        return message;
    }

    @Override
    public String getMessage() {
        if (key != null) {
            return getMessage(YogenMessageAccessor.EN);
        }
        return "";
    }

    @Override
    public String getLocalizedMessage() {
        if (key != null) {
            return getMessage(YogenMessageAccessor.TR);
        }
        return "";
    }

    public String getErrorCode() {
        if (key != null) {
            return YogenMessageAccessor.getString(YogenMessageAccessor.CODE + key);
        }
        return "";
    }

    public Object[] getArguments() {
        return arguments;
    }

    public void setArguments(Object[] arguments) {
        this.arguments = arguments;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}