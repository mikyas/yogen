package com.yogen.util.exception;

public class EncryptionException extends RuntimeException {

    public EncryptionException(Exception exception) {
        super(exception);
    }
}