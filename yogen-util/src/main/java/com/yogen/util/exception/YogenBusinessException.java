package com.yogen.util.exception;

import com.yogen.util.enumtype.MessageCode;

public class YogenBusinessException extends YogenException {

    private Integer yogenErrorCode;

    public YogenBusinessException(String key) {
        super(key);
    }

    public YogenBusinessException(String key, Integer yogenErrorCode) {
        super(key);
        this.yogenErrorCode = yogenErrorCode;
    }

    public YogenBusinessException(String key, MessageCode yogenErrorCode) {
        super(key);
        if (this.yogenErrorCode != null) {
            this.yogenErrorCode = yogenErrorCode.getValue();
        }
    }

    public YogenBusinessException(String key, Object... arguments) {
        super(key, arguments);
    }

    public YogenRuntimeException toYogenRuntimeException() {
        return new YogenRuntimeException(this.getKey(), this.getArguments());
    }

    public Integer getYogenErrorCode() {
        return yogenErrorCode;
    }

    public void setYogenErrorCode(Integer yogenErrorCode) {
        this.yogenErrorCode = yogenErrorCode;
    }
}