package com.yogen.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import com.yogen.util.enumtype.ITextEnum;
import com.yogen.util.enumtype.IValueEnum;

public final class EnumUtil {

    private EnumUtil() {
    }


    public static <T extends Enum<T> & ITextEnum> Map<String, T> asMap(Class<T> enumType) {
        return asMap(enumType, new LinkedHashMap<String, T>());
    }

    public static <T extends Enum<T> & ITextEnum> Map<String, T> asMap(Class<T> enumType, Map<String, T> map) {

        T[] enums = enumType.getEnumConstants();
        for (T t : enums) {
            map.put(t.getText(), t);
        }

        return map;
    }

    public static <T extends Enum<T> & IValueEnum, D extends Enum<D> & IDecoratorEnum<T>> D safeDecoratorEnumOf(Class<D> decoratorEnumType, Integer value) {
        D foundDecorator = null;
        if (value != null) {
            D[] enums = decoratorEnumType.getEnumConstants();
            for (D decorator : enums) {
                if (decorator.getActualEnum().getValue().equals(value)) {
                    foundDecorator = decorator;
                    break;
                }
            }
        }
        return foundDecorator;
    }

    public static <T extends Enum<T>, D extends Enum<D> & IDecoratorEnum<T>> D safeDecoratorEnumOf(Class<D> decoratorEnumType, T actualEnum) {
        D foundDecorator = null;
        if (actualEnum != null) {
            D[] enums = decoratorEnumType.getEnumConstants();
            for (D decorator : enums) {
                if (decorator.getActualEnum().equals(actualEnum)) {
                    foundDecorator = decorator;
                    break;
                }
            }
        }
        return foundDecorator;
    }

    public static <T extends Enum<T>, D extends Enum<D> & IDecoratorEnum<T>> D safeDecoratorEnumOf(Class<D> decoratorEnumType, String name) {
        D foundDecorator = null;
        if (name != null) {
            D[] enums = decoratorEnumType.getEnumConstants();
            for (D decorator : enums) {
                if (decorator.getActualEnum().name().equals(name)) {
                    foundDecorator = decorator;
                    break;
                }
            }
        }
        return foundDecorator;
    }

    public static <T extends Enum<T> & IValueEnum> T valueOf(Class<T> enumType, Integer value) {
        if (value == null)
            throw new NullPointerException("Value is null");

        T foundEnum = safeValueOf(enumType, value);
        if (foundEnum == null)
            throw new IllegalArgumentException("No enum const enumType: " + enumType + " value: " + value);

        return foundEnum;
    }

    public static <T extends Enum<T> & IValueEnum> T safeValueOf(Class<T> enumType, Integer value) {
        T foundEnum = null;

        T[] enums = enumType.getEnumConstants();
        for (T t : enums) {
            if (t.getValue().equals(value)) {
                foundEnum = t;
                break;
            }
        }
        return foundEnum;
    }

    public static <T extends Enum<T> & ITextEnum> T safeValueOf(Class<T> enumType, String text) {
        T foundEnum = null;

        T[] enums = enumType.getEnumConstants();
        for (T t : enums) {
            if (t.getText().equals(text)) {
                foundEnum = t;
                break;
            }
        }
        return foundEnum;
    }

    public static List<Integer> toValueList(List<? extends IValueEnum> enumList) {
        List<Integer> list = new ArrayList<Integer>();
        for (IValueEnum valueEnum : enumList) {
            list.add(valueEnum.getValue());
        }
        return list;
    }

}
