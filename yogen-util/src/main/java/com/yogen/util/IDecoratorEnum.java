package com.yogen.util;

public interface IDecoratorEnum<T extends Enum<T>> {

    T getActualEnum();
}
