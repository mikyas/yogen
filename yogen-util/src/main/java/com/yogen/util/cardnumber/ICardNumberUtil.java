package com.yogen.util.cardnumber;

import java.util.Set;

/**
 * Base scheme for core Card Number utility functions. Basic functionality includes:
 * <p>
 * <nl>
 * <li>{@linkplain CardNumberUtil#maskCardNumbers(String) Masking Card Numbers in a given text}</li>
 * <li>{@linkplain CardNumberUtil#findCardNumbersDeep(String) Finding all possible Card Number Schemes in a given text}</li>
 * <li>{@linkplain CardNumberUtil#findCardNumbersShallow(String) Finding first encompasing Card Number Schemes in a given text}</li>
 * <li>{@linkplain CardNumberUtil#doCardNumbersExist(String) Signaling whether if a given text includes a Card Number}</li>
 * </nl>
 * </p>
 *
 * @author oaksoy
 * @see CardNumberUtilCodeAtPointEagerly Eager Code at Point Behavior
 * @see CardNumberUtilCodeAtPointPatterned Patterned Code at Point Behavior
 * @see CardNumberUtilRegexBased Regex Based Behavior
 */
interface ICardNumberUtil {

    /**
     * Factory Application Interface Method for {@linkplain CardNumberUtil#maskCardNumbers(String) maskCardNumbers} function
     *
     * @param value input string value to search for possible card numbers
     * @return string value with masked pausible card numbers
     * @see {@linkplain CardNumberUtil#maskCardNumbers(String)}
     */
    public String maskCardNumbers(String value);

    /**
     * Factory Application Interface Method for {@linkplain CardNumberUtil#findCardNumbersDeep(String) findCardNumbersDeep} function
     *
     * @param value input string value to search for possible card numbers
     * @return all possible known card number sets
     * @see {@linkplain CardNumberUtil#findCardNumbersDeep(String)}
     */
    public Set<String> findCardNumbersDeep(String value);

    /**
     * Factory Application Interface Method for {@linkplain CardNumberUtil#findCardNumbersShallow(String) findCardNumbersShallow} function
     *
     * @param value input string value to search for possible card numbers
     * @return first matched possible known card number sets
     * @see {@linkplain CardNumberUtil#findCardNumbersShallow(String)}
     */
    public Set<String> findCardNumbersShallow(String value);

    /**
     * Factory Application Interface Method for {@linkplain CardNumberUtil#doCardNumbersExist(String) doCardNumbersExist} function
     *
     * @param value input string value to search for possible card numbers
     * @return <b>true</b> if card number exits, else returns <b>false</b>
     * @see {@linkplain CardNumberUtil#doCardNumbersExist(String)}
     */
    public boolean doCardNumbersExist(String value);
}
