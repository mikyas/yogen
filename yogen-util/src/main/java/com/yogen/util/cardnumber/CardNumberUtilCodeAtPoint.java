package com.yogen.util.cardnumber;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <b>Base abstract scheme definition for Card Number Utilities that works with
 * <q>Code at Point</q></b> Strategy based utility modules. </br>
 * <p>
 * <em>Code at Point</em> is a linked list based strategy. Although it is a linked list idea, it works more like a linear graph. For this strategy to work, an input text is parsed
 * to its digits and branches created with possible card numbers in them. Later these branches are processed to check for card numbers. Behavior of finding possible branches and
 * separating digits from the text are methods that differantiates between search strategies. Consequently, result will be different depending on the choosen search strategies.
 * </p>
 * <p>
 * Currently supports Sub operator types of
 * <nl>
 * <li>{@linkplain CardNumberUtilCodeAtPointPatterned Patterned}</li>
 * <li>{@linkplain CardNumberUtilCodeAtPointEagerly Eager}</li>
 * </nl>
 * </p>
 *
 * @author oaksoy
 * @see CardNumberUtil Card Number Utility
 * @see CardNumberUtil.CardNumberUtilityOperator Card Number Utility Operator
 */
abstract class CardNumberUtilCodeAtPoint {

    /**
     * <p>
     * In <em>Code at Point</em> strategy, a card number check can be done in two different depths.</br>
     * These are:
     * <ol>
     * <li>Shallow</li>
     * <li>Deep</li>
     * </ol>
     * search levels.
     * </p>
     * <p>
     * In a branch under check, a set of Characters of possible card number lenghts are prepared for searching. This is called the <strong>Search Window</strong>.
     * <p>
     * <b>A Shallow Search</b> finds the first plausible card number that matches to known Card Number Schemes in the Search Window, and switches to the upcoming search window
     * within the branch. If Branch does not contain a possible lenght of characters, then the Search Window is opened on the upcoming branch on the queue.
     * </p>
     * <p>
     * <b>A Deep Search</b> will try to find as many as possible card numbers in the search window. Therefore it will constantly shrink the search windows width to check for
     * shorter Card Number Schemes within a already defined card number digit sequence.
     * </p>
     *
     * @author oaksoy
     */
    private enum SearchDepth {
        SHALLOW, DEEP
    }

    /**
     * Code Point based card number masking function tries to find digits in an input value. After identifying digits, branches to process for a card number check is operated. Once
     * the branches are deduced, a <em>Frame Set</em> is generated. These frame sets are constructed from sets of digits that matches in a Card Number Scheme. Once the frame sets
     * are listed under a main frame set, they are caught by index in the input value and all encompassing digits are replaced with {@linkplain CardNumberUtil#MASKING_CHAR Masking
     * Character}. Final result is converted to String returned as output.
     *
     * @param value input text
     * @return string value with masked pausible card numbers
     * @see CardNumberUtil#maskCardNumbers(String) Mask Card Numbers Method
     */
    public String maskCardNumbers(String value) {
        if (!(value != null && !value.trim().isEmpty())) {
            return value;
        }
        List<CharAtPoint> digitAtPointList = findDigits(value);
        List<List<CharAtPoint>> branchList = findBranches(digitAtPointList);
        Set<CharFrame> mainFrame = populateMainFrameSet(digitAtPointList, branchList, SearchDepth.SHALLOW);
        String masked = generateMaskedData(value, mainFrame, digitAtPointList);
        return masked;
    }

    /**
     * Operates Code Point based card number discovery with <em>Deep Level Search Strategy</em>. Input text is parsed to find digits in the text and branches are generated to limit
     * digit search pool Afterwards, a deep search is conducted on branches, concluding in char frames with all possible card number schemes.
     *
     * @param value input text
     * @return all possible known card number sets
     */
    public Set<String> findCardNumbersDeep(String value) {
        if (!(value != null && !value.trim().isEmpty())) {
            return null;
        }
        List<CharAtPoint> digitAtPointList = findDigits(value);
        List<List<CharAtPoint>> branchList = findBranches(digitAtPointList);
        if (!(branchList != null && !branchList.isEmpty())) {
            return new HashSet<>();
        }
        Set<CharFrame> mainFrame = populateMainFrameSet(digitAtPointList, branchList, SearchDepth.DEEP);
        Set<String> set = fetchCardNumbersFromData(value, mainFrame, digitAtPointList);
        return CardNumberUtil.convertToOrderedSet(set.toArray(new String[set.size()]));
    }

    /**
     * Operates Code Point based card number discovery with <em>Shallow Level Search Strategy</em>. Input text is parsed to find digits in the text and branches are generated to
     * limit digit search pool Afterwards, a shallow search is conducted on branches, concluding in char frames with first matched possible card number schemes.
     *
     * @param value input text
     * @return first matched possible known card number sets
     */
    public Set<String> findCardNumbersShallow(String value) {
        if (!(value != null && !value.trim().isEmpty())) {
            return null;
        }
        List<CharAtPoint> digitAtPointList = findDigits(value);
        List<List<CharAtPoint>> branchList = findBranches(digitAtPointList);
        if (!(branchList != null && !branchList.isEmpty())) {
            return new HashSet<>();
        }
        Set<CharFrame> mainFrame = populateMainFrameSet(digitAtPointList, branchList, SearchDepth.SHALLOW);
        Set<String> set = fetchCardNumbersFromData(value, mainFrame, digitAtPointList);
        return CardNumberUtil.convertToOrderedSet(set.toArray(new String[set.size()]));
    }

    /**
     * Signals whether if a card number scheme exits in the input text. To achieve this, text is parsed and all digits and relatable characher sets are clustered in branches. If
     * any card number is found during search, operation stops and returns true, else returns false.
     *
     * @param value input text
     * @return <b>true</b> if card number exits, else returns <b>false</b>
     */
    public boolean doCardNumbersExist(String value) {
        if (!(value != null && !value.trim().isEmpty())) {
            return false;
        }
        List<CharAtPoint> digitAtPointList = findDigits(value);
        List<List<CharAtPoint>> branchList = findBranches(digitAtPointList);
        if (!(branchList != null && !branchList.isEmpty())) {
            return false;
        }
        for (List<CharAtPoint> branch : branchList) {
            if (areThereAnyCardNumbers(branch)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param digitAtPointList list of digit characters with index, location and traling clause info
     * @param branchList       deduced set of characters with metadata in a meaningful distance to operate card number discovery operation
     * @param searchDepth      type of calibration operation to manage search level
     * @return set of metadata and characters that matches a valid card number scheme. Each set matches a card number scheme.
     */
    private Set<CharFrame> populateMainFrameSet(List<CharAtPoint> digitAtPointList, List<List<CharAtPoint>> branchList, SearchDepth searchDepth) {
        Set<CharFrame> mainFrame = new HashSet<>();
        for (List<CharAtPoint> branch : branchList) {
            Set<CharFrame> charFrameSet = traverseForCardNumbers(branch, searchDepth);
            for (CharFrame charFrame : charFrameSet) {
                int indexFrom = digitAtPointList.indexOf(branch.get(charFrame.getStart()));
                int toIndex = digitAtPointList.indexOf(branch.get(charFrame.getEnd() - 1));
                mainFrame.add(new CharFrame(indexFrom, toIndex));
            }
        }
        return mainFrame;
    }

    /**
     * Processes digits that correlates with matched card numbers in the main frame of character sets and replaces the overlaping indexes on input text value
     *
     * @param value            input text
     * @param mainFrame        set of all metadata and characters that is discovered as a valid card number schemes
     * @param digitAtPointList list of digit characters with index, location and traling clause info
     * @return input text with masked card numbers
     */
    private String generateMaskedData(String value, Set<CharFrame> mainFrame, List<CharAtPoint> digitAtPointList) {
        char[] data = value.toCharArray();
        for (CharFrame charFrame : mainFrame) {
            for (int i = charFrame.getStart(); i <= charFrame.getEnd(); i++) {
                data[digitAtPointList.get(i).getLocation()] = CardNumberUtil.MASKING_CHAR;
            }
        }
        return String.valueOf(data);
    }

    /**
     * Works on list of digit character metadata and correlates this list with found card numbers in the character frame sets. Finally generates the real card number value from the
     * indexed input text location.
     *
     * @param value            input text
     * @param mainFrame        set of all metadata and characters that is discovered as a valid card number schemes
     * @param digitAtPointList list of digit characters with index, location and traling clause info
     * @return unique and real values of card numbers on main frame character sets
     */
    private Set<String> fetchCardNumbersFromData(String value, Set<CharFrame> mainFrame, List<CharAtPoint> digitAtPointList) {
        char[] data = value.toCharArray();
        Set<String> set = new HashSet<>();
        for (CharFrame charFrame : mainFrame) {
            String cardNumber = "";
            for (int i = charFrame.getStart(); i <= charFrame.getEnd(); i++) {
                cardNumber = cardNumber + data[digitAtPointList.get(i).getLocation()];
            }
            set.add(cardNumber);
        }
        return set;
    }

    /**
     * Validates digit metadata on branches and later sends it to main card number search operation with the same signature
     *
     * @param branch      list of digit character metadata with meaningful distance to each other
     * @param searchDepth type of calibration operation to manage search level
     * @return character frame sets which each matches to a plausible card number scheme
     */
    private Set<CharFrame> traverseForCardNumbers(List<CharAtPoint> branch, SearchDepth searchDepth) {
        if (!(branch != null && !branch.isEmpty())) {
            return new HashSet<CharFrame>();
        }
        if (branch.size() < CardNumberUtil.MIN) {
            return new HashSet<CharFrame>();
        }
        String value = "";
        for (CharAtPoint digitCharAtPoint : branch) {
            value = value.concat(String.valueOf(digitCharAtPoint.getCharacter()));
        }
        return traverseForCardNumbers(CardNumberUtil.MIN.intValue(), CardNumberUtil.MAX.intValue(), value, searchDepth);
    }

    /**
     * Main card number scheme searching operation. Works as traversing windows. Width of the window enables the maximum lenght of a card number scheme that can be inferred between
     * digits. In order to unrelate the impossible digit characters, distance to the last digit is kept in a meaningful distanse to each other. To achieve this last index of the
     * possible card number lenght is kept in a distance under the maximum know card number. Once a search window is processed until the minimum card number lenght, then window
     * iterates to the next index. If a shallow strategy is selected, then the window will calibrate itself to start at the beginning of the last found card number digit index,
     * skipping to shrink the search window on the current character pool.
     *
     * @param minNumLenght minimum card number lenght within the known card number schemes
     * @param maxNumLenght maximum card number lenght within the known card number schemes
     * @param value        input text
     * @param searchDepth  type of calibration operation to manage search level
     * @return character frame sets which each matches to a plausible card number scheme
     */
    private Set<CharFrame> traverseForCardNumbers(int minNumLenght, int maxNumLenght, String value, SearchDepth searchDepth) {
        int windowWidth = maxNumLenght - minNumLenght;
        int buff = value.length() - minNumLenght;
        Set<CharFrame> charFrames = new HashSet<>();
        for (int start = 0; start <= buff; start++) {
            for (int index = 0; index <= windowWidth; index++) {
                int end = minNumLenght + start + index;
                if (end >= value.length()) {
                    end = value.length();
                }
                String subString = value.substring(start, end);
                boolean isTryToCalibrate = false;
                if (CardNumberUtil.isValidCardType(subString) && CardNumberUtil.isValidCardNumber(subString)) {
                    charFrames.add(new CharFrame(start, end));
                    isTryToCalibrate = true;
                }
                if (isTryToCalibrate && searchDepth.equals(SearchDepth.SHALLOW)) {
                    start = end--;
                    index = 0;
                }
            }
        }
        return charFrames;
    }

    /**
     * Validates digit metadata on branches and later sends it to main card number discovery operation with the same signature
     *
     * @param digitAtPointList list of digit characters with index, location and traling clause info
     * @return true if a card number scheme is discovered, else false
     */
    private boolean areThereAnyCardNumbers(List<CharAtPoint> digitAtPointList) {
        if (!(digitAtPointList != null && !digitAtPointList.isEmpty())) {
            return false;
        }
        if (digitAtPointList.size() < CardNumberUtil.MIN) {
            return false;
        }
        String value = "";
        for (CharAtPoint digitCharAtPoint : digitAtPointList) {
            value = value.concat(String.valueOf(digitCharAtPoint.getCharacter()));
        }
        return areThereAnyCardNumbers(CardNumberUtil.MIN.intValue(), CardNumberUtil.MAX.intValue(), value);
    }

    /**
     * Main card number discovery operation. Directly operated on input text and list of digits found on the input text. Once any matching card number scheme is found, returns
     * true, else it will return false
     *
     * @param minNumLenght minimum card number lenght within the known card number schemes
     * @param maxNumLenght maximum card number lenght within the known card number schemes
     * @param value        input text as digits in meaningful distance to each other
     * @return true if there is a valid card number scheme, else returns false
     */
    private boolean areThereAnyCardNumbers(int minNumLenght, int maxNumLenght, String value) {
        int windowWidth = maxNumLenght - minNumLenght;
        int buff = value.length() - minNumLenght;
        for (int start = 0; start <= buff; start++) {
            for (int index = 0; index <= windowWidth; index++) {
                int end = minNumLenght + start + index;
                if (end >= value.length()) {
                    end = value.length();
                }
                String subString = value.substring(start, end);
                if (CardNumberUtil.isValidCardType(subString) && CardNumberUtil.isValidCardNumber(subString)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * This method must fill the location and charater information of a digit character. Trail information is required for pattern recognition and can be used in
     * {@linkplain CardNumberUtilCodeAtPoint#findBranches(List) find branches method} if necessary. And yes, this is against Liskov's Substitution Princible.
     *
     * @param value raw input text
     * @return List of Digits in the given text with {@linkplain CardNumberUtilCodeAtPoint.CharAtPoint Character Metadata}
     * @see CardNumberUtilCodeAtPoint.CharAtPoint Char at Point
     */
    protected abstract List<CharAtPoint> findDigits(String value);

    /**
     * This method must create a set of digit metadata list by breaking discovered digits in a consequent list to exclude overfitting during card number scheme discovery
     * operations. This method is interdependend with {@linkplain CardNumberUtilCodeAtPoint#findDigits(String) find digits method}
     *
     * @param digitsAtPointList list of digit characters with index, location and traling clause info
     * @return a list composed from the list of digits that are in a meaningful distance to each other
     * @see CardNumberUtilCodeAtPoint#findDigits(String) find digits method
     */
    protected abstract List<List<CharAtPoint>> findBranches(List<CharAtPoint> digitsAtPointList);

    /**
     * Metadata about the indexes for a matching card number scheme in a list of digit characters
     *
     * @author oaksoy
     */
    protected final class CharFrame {

        private int start;
        private int end;

        public CharFrame(int start, int end) {
            this.start = start;
            this.end = end;
        }

        public int getStart() {
            return start;
        }

        public int getEnd() {
            return end;
        }

        @Override
        public String toString() {
            return "START: " + this.start + " END: " + this.end;
        }

    }

    /**
     * Metadata about the digit characters
     *
     * @author oaksoy
     */
    protected final class CharAtPoint {

        private int location;
        private char character;
        private String trail;

        public CharAtPoint(int location, char character) {
            this.location = location;
            this.character = character;
        }

        public CharAtPoint(int location, char character, String trail) {
            this.location = location;
            this.character = character;
            this.trail = trail;
        }

        public char getCharacter() {
            return character;
        }

        public int getLocation() {
            return location;
        }

        public String getTrail() {
            return trail;
        }

        @Override
        public String toString() {
            return "CHAR: " + this.character + " LOC: " + this.location + " TRAIL: " + this.trail;
        }

    }

}
