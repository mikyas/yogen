package com.yogen.util.cardnumber;

import java.util.ArrayList;
import java.util.List;

/**
 * Immutable class for the implementation of the card number util code at point strategy to Eager Behaviour. Mainly changes the behaviour of the key digit discovery and branch
 * segmentation functions to manipulate the card number discovery and effected characters.
 *
 * @author oaksoy
 * @see CardNumberUtilCodeAtPoint Core Code at Point Strategy Implementation
 */
final class CardNumberUtilCodeAtPointEagerly extends CardNumberUtilCodeAtPoint implements ICardNumberUtil {

    private CardNumberUtilCodeAtPointEagerly() {
    }

    private static ICardNumberUtil instance;

    /**
     * Returns the singleton instance for ICardNumberUtil with Eager Code At Point Behavior
     *
     * @return ICardNumberUtil instance
     */
    public static ICardNumberUtil getInstance() {
        if (instance == null) {
            instance = new CardNumberUtilCodeAtPointEagerly();
        }
        return instance;
    }

    /**
     * {@inheritDoc CardNumberUtilCodeAtPoint#findDigits(String)}
     *
     * <b>Eager implementation would simply aim to find any Digit Character and then record the <i>location(index)</i> and the <i>character</i> itself and appends all to a
     * list.</b>
     */
    @Override
    protected List<CharAtPoint> findDigits(String value) {
        if (!(value != null && !value.trim().isEmpty())) {
            return null;
        }
        List<CharAtPoint> digitAtPointList = new ArrayList<>();
        for (int i = 0; i < value.length(); i++) {
            char ch = value.charAt(i);
            if (Character.isDigit(ch)) {
                digitAtPointList.add(new CharAtPoint(i, ch));
            }
        }
        return digitAtPointList;
    }

    /**
     * {@inheritDoc CardNumberUtilCodeAtPoint#findBranches(List)}
     *
     * <b>Eager implementation would simply break the list on the</b>{@linkplain CardNumberUtil.MAX Maximum Card Number Lenght} <b>and would create <i>a list of lists</i></b>
     */
    @Override
    protected List<List<CharAtPoint>> findBranches(List<CharAtPoint> digitsAtPointList) {
        List<List<CharAtPoint>> branchList = new ArrayList<>();
        if (!(digitsAtPointList != null && !digitsAtPointList.isEmpty())) {
            return branchList;
        }
        int previousLocation = digitsAtPointList.get(0).getLocation();
        int breakpoint = 0;

        for (int i = 0; i < digitsAtPointList.size(); i++) {
            if ((digitsAtPointList.get(i).getLocation() - previousLocation) > CardNumberUtil.MAX) {
                List<CharAtPoint> branch = new ArrayList<>();
                branch.addAll(digitsAtPointList.subList(breakpoint, i));
                branchList.add(branch);
                breakpoint = i;
            }
            previousLocation = digitsAtPointList.get(i).getLocation();
        }

        List<CharAtPoint> branch = new ArrayList<>();
        branch.addAll(digitsAtPointList.subList(breakpoint, digitsAtPointList.size()));
        branchList.add(branch);

        return branchList;
    }

}
