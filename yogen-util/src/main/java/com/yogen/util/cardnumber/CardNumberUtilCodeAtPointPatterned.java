package com.yogen.util.cardnumber;

import java.util.ArrayList;
import java.util.List;

/**
 * Immutable class for the implementation of the card number util code at point strategy to Patterned Behaviour. Mainly changes the behaviour of the key digit discovery and branch
 * segmentation functions to manipulate the card number discovery and effected characters.
 *
 * @author oaksoy
 * @see CardNumberUtilCodeAtPoint Core Code at Point Strategy Implementation
 */
final class CardNumberUtilCodeAtPointPatterned extends CardNumberUtilCodeAtPoint implements ICardNumberUtil {

    private CardNumberUtilCodeAtPointPatterned() {
    }

    private static ICardNumberUtil instance;

    /**
     * Returns the singleton instance for ICardNumberUtil with Patterned Code At Point Behavior
     *
     * @return ICardNumberUtil instance
     */
    public static ICardNumberUtil getInstance() {
        if (instance == null) {
            instance = new CardNumberUtilCodeAtPointPatterned();
        }
        return instance;
    }

    /**
     * {@inheritDoc CardNumberUtilCodeAtPoint#findBranches(List)}
     *
     * <b>Patterned implementation would aim to find any Digit Character and then record the <i>location(index)</i>, the <i>character</i> itself and the <i>trail of characters (a
     * clause) that is until another Digit</i> and appends all to a list.</b>
     */
    protected List<CharAtPoint> findDigits(String value) {
        List<CharAtPoint> digitAtPointList = new ArrayList<>();
        if (!(value != null && !value.trim().isEmpty())) {
            return digitAtPointList;
        }
        for (int i = 0; i < value.length(); i++) {
            char ch = value.charAt(i);
            if (Character.isDigit(ch)) {
                boolean iterate = true;
                int j = 1;
                String trail = "";
                while (iterate && (i + j) < value.length() && j <= CardNumberUtil.MAX) {
                    char chTr = value.charAt(i + j);
                    if (Character.isDigit(chTr)) {
                        iterate = false;
                    } else {
                        trail = trail + chTr;
                        j = j + 1;
                    }
                }
                digitAtPointList.add(new CharAtPoint(i, ch, trail));
                i = i + j - 1;
            }
        }
        return digitAtPointList;
    }

    /**
     * {@inheritDoc CardNumberUtilCodeAtPoint#findBranches(List)}
     *
     * <p>
     * <b>Patterned implementation would break the list on the conditions of</b>
     * <nl>
     * <li>{@linkplain CardNumberUtil.MAX Maximum Card Number Lenght}</li>
     * <li>Whether if the trailing clause consists of wild characters and/or white spaces</li>
     * <li>Or Whether if the trailing clause consists of same characters and white spaces</li></br>
     * </nl>
     * <b>and would create <i>a list of lists</i></b>
     * </p>
     */
    protected List<List<CharAtPoint>> findBranches(List<CharAtPoint> digitsAtPointList) {
        List<List<CharAtPoint>> branchList = new ArrayList<>();
        if (!(digitsAtPointList != null && !digitsAtPointList.isEmpty())) {
            return branchList;
        }
        int previousLocation = digitsAtPointList.get(0).getLocation();
        int breakpoint = 0;
        String previousTrail = "";
        for (int i = 0; i < digitsAtPointList.size(); i++) {
            boolean isBreakpoint = false;
            if ((digitsAtPointList.get(i).getLocation() - previousLocation) > CardNumberUtil.MAX) {
                isBreakpoint = true;
            } else if (previousTrail != null && !previousTrail.isEmpty()) {
                boolean isPlausibleToRelate = isAllSameCharsIncludingWhiteSpace(previousTrail) || isAllWildCharsIncludingWhiteSpace(previousTrail);
                if (!isPlausibleToRelate) {
                    isBreakpoint = true;
                }
            }
            if (isBreakpoint) {
                List<CharAtPoint> branch = new ArrayList<>();
                branch.addAll(digitsAtPointList.subList(breakpoint, i));
                branchList.add(branch);
                breakpoint = i;
            }
            previousLocation = digitsAtPointList.get(i).getLocation();
            previousTrail = digitsAtPointList.get(i).getTrail();
        }
        List<CharAtPoint> branch = new ArrayList<>();
        branch.addAll(digitsAtPointList.subList(breakpoint, digitsAtPointList.size()));
        branchList.add(branch);
        return branchList;
    }

    /**
     * Processes a text, which is expected to be a trail clause of a digit character, and tries to discover a pattern of wild characters and whitespaces
     *
     * @param trail trailing clause of a digit character
     * @return <b>true</b> if matches a wild characters set & whitespaces set; else <b>false</b>
     */
    private boolean isAllWildCharsIncludingWhiteSpace(String trail) {
        if (!(trail != null && !trail.trim().isEmpty())) {
            return false;
        }
        char[] chars = trail.toCharArray();
        for (char ch : chars) {
            if (Character.isLetter(ch) || Character.isDigit(ch)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Processes a text, which is expected to be a trail clause of a digit character, and tries to discover a pattern of all same characters and whitespaces
     *
     * @param trail trailing clause of a digit character
     * @return <b>true</b> if matches the "all same characters" set & whitespaces set; else <b>false</b>
     */
    private boolean isAllSameCharsIncludingWhiteSpace(String trail) {
        if (!(trail != null && !trail.isEmpty())) {
            return false;
        }
        char[] chars = trail.toCharArray();
        Character character = chars[0];
        for (char ch : chars) {
            if (!character.equals(Character.valueOf(ch))) {
                return false;
            }
        }
        return true;
    }

}
