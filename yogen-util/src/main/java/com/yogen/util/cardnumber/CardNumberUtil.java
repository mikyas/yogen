package com.yogen.util.cardnumber;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.ResourceBundle;
import java.util.Set;
import org.springframework.util.StringUtils;

/**
 * <p>
 * <b>Card Number Utility</b> is a procedural code product that works with operator factories under the hood. It aims to recognize Credit Card Numbers and Debit Card Numbers that
 * belongs to various Card Issuer Organizations worldwide and operate on them by <blockquote>
 * <nl>
 * <li>Card Number Existance Check</li>
 * <li>Finding Card Numbers within Text</li>
 * <li>Masking Found Card Numbers</li>
 * </nl>
 * </p>
 * </blockquote>
 * <p>
 * Card Number Utility can use 3 different strategies to find, mask and check text for card numbers. These <em>Operators</em> are:
 * <nl>
 * <li>Regex Based</li>
 * <li>Code at Point - Patterned</li>
 * <li>Code at Point - Eager</li>
 * </nl>
 * </p>
 * <p>
 * <em>Operators</em> are listed in a <em>nested enum type</em> called {@linkplain CardNumberUtil.CardNumberUtilityOperator CardNumberUtilityOperator}. In order to ensure that Card
 * Number Utility is simple and functional, it is coded in static form as a procedural flow. <strong>Therefore operator switching is not object oriented!</strong>. This might cause
 * problems if you want to switch between operators without code deployments or with hot switches. There are number of ways you can alter the code in the future to either build it
 * fully in an object oriented architecture, or continue with procedural architecture:
 * <ol>
 * <li><b>Object Oriented Way</b> : Convert CardNumberUtil to non-static purposed class and also feed the CardNumberUtilityOperator in the Constructor</li>
 * <li><b>Procedural way</b>: Get CardNumberUtilityOperator via a bundle or a system property by creating an entry on system properties.</li>
 * </ol>
 * Currently Card Number Utility follows the second way and retrieves the operator variable though a bundle called <b>{@code cardnumberutil.properties}</b> under
 * <b>{@code src/main/resources}</b>
 * </p>
 * <p>
 * <strong>Performance:</strong> <em>Card Number Utility</em> acts with different outcomes for every strategy, however as a general rule <em>Eager Code at Point</em> strategy is
 * faster than <em>Patterned Code at Point</em> strategy, and both are nearly ~%40 faster than <em>Regex Matching</em> strategy.
 * </p>
 *
 * <blockquote><b>Referance Benchmarks:</b></br>
 * Current version can execure around 3000~ test cases(1000 lightweight, 500 midweight, and 1500 heavyweight operations) as
 * <ol>
 * <li>Eager Code at Point : 620 ms</li>
 * <li>Patterned Code at Point : 685 ms</li>
 * <li>Regex Matching : 1050 ms</li> Results can differ depending on the machine used for benchmarking, however the ratios is expected to be similar.</blockquote>
 *
 * <b>Supported Cards:</b> <blockquote>
 * <nl>
 * <li>MasterCard - Maestro</li>
 * <li>Visa - Visa Debit</li>
 * <li>American Express</li>
 * <li>Diners Club</li>
 * <li>Discover</li>
 * <li>Enroute</li>
 * <li>JCB (Japan)</li>
 * <li>Voyager</li>
 * <li>BankCard (US)</li>
 * <li>UnionPay (China)</li>
 * <li>InterPayment</li>
 * <li>Verve</li>
 * </nl>
 * </blockquote>
 * </nl>
 *
 * <p>
 * <b>Tests:</b> All related Junit tests can be found under {@linkplain test.cardnumberutil Test Package for Card Number Utility}. A quick overlook to performance can be achieved
 * via {@linkplain test.cardnumberutil.suites.TestAllCardNumberUtilMethods Card Number Utility Complete Test Suite}
 * </p>
 *
 * @author Ozan Aksoy (oaksoy)
 * @see CardNumberUtil.CardNumberUtilityOperator Card Number Operators
 * @see test.cardnumberutil.suites.TestAllCardNumberUtilMethods Testing All Functions of Card Number Utility
 */
public final class CardNumberUtil {

    /**
     * <p>
     * Constant value that holds the minimum recognized Credit Card or Debit Card number lenght.
     * </p>
     */
    public final static Integer MIN = 12;
    /**
     * <p>
     * Constant value that holds the maximum recognized Credit Card or Debit Card number lenght.
     * </p>
     */
    public final static Integer MAX = 19;
    /**
     * <p>
     * Character which replaces the digits of the recognized card number, thus masking the card number. <br>
     * <blockquote>For Example :
     *
     * <pre>
     * <code>For a card number that starts with values of 553325...; <br> while the masking character is X;<br> masked card number would be XXXXXX...</code>
     * </pre>
     *
     * </blockquote>
     * </p>
     */
    public final static Character MASKING_CHAR = 'X';

    /**
     * <p>
     * Properities bundle name for CardNumberUtil. It is used in fetching the resource bundle and retrieving properties by property keys.
     * </p>
     */
    private final static String PROPERTIES_BUNDLE_NAME = "cardnumberutil";
    /**
     * <p>
     * Property keyword for operator variable that is used in CardNumberUtil.operator field.
     * </p>
     */
    private final static String OPERATOR_PROPERTY_KEY = "util.cardnumber.operator";

    /**
     * <p>
     * This enum is specialized to encapsulate the operation strategies for finding, checking and masking card numbers. Available operators and their operation strategies are as
     * follows
     * </p>
     * <blockquote><b>REGEX MATCH</b> :
     * <p>
     * On this operator, card number operations will use a regex matcher logic flow to parse any matching card number block. These blocks could have been separated by any number of
     * non-alphanumeric character However any existance of letter will stop the card number block relation. Note that Regex Matching Strategy uses: <em>
     * <q>CLASSPATH/util/src/main/java/com//java/util/cardnumber/RegexUtil.java</q></em> during regex operations. This helper class aids to disperse any character that a regex
     * matcher might mistake with a regex clause, by both escaping regex signifiers and normalizing unicode regex signifiers where it is necessary.
     * </p>
     * <p>
     * Regex matches are bound to fail where percieved card number writing possibilies outperform the matching regex. Even though this issue is very unlikely, it is possible.
     * Another distinct attribute of this strategy is the <b>broad masking</b>. Regex matches will result in masking all the characters of a separated block, causing non-digit or
     * not-as-card-number block text to be masked. Depending on the situation, this can be a desired, or a disliked feature.
     * </p>
     * </blockquote> <blockquote><b>CODE AT POINT</b> :
     * <p>
     * <em>Code at Point</em> is a linked list based strategy that parses digits that is found on text and operates card number checking methods on the branches of the graph. This
     * can be done in either <b><em>Eagerly</em></b> or <b><em>Patterned</em></b> manner.
     * <p>
     * <b> In Eager Mode,</b> branching behavior and digit discovery does not distinguish any pattern, thus causing any matching sequence of possible-card-number blocks to be
     * caught. This strategy is a fast and secure way to catch any possible card number in a given text. However, this will also increase the outcomes where non-related digits to
     * be matched in a branch and overfit the possiblities to actually sent card numbers.
     * </p>
     * <p>
     * <b> In Patterned Mode,</b> branching behavior and digit discovery will separate blocks with unmatching text trails. This will increase the number of possible branches to
     * operate on, and also separate the sampling character pool to narrow the possible characters. As a result, overfitting of the results is drastically decreased on patterned
     * strategy, yet possible matching pool is narrowed with the same logic.
     * </p>
     * </blockquote>
     * <p>
     * It is advised to use <b>
     * <q>Code at Point - Patterned</q></b> strategy on default. However, when a broad masking and text based search logic is needed, <b>
     * <q>Regex Match</q></b> will function better. Finally, if it is possible to narrow the input values previously, <b>
     * <q>Code at Point - Eager</q></b> strategy is less likely to cause overfitting and thus will be a good choice.
     * </p>
     *
     * @author oaksoy
     * @see CardNumberUtil#getCardNumberUtilFactory() Card Number Utility Factory Selector
     */
    public enum CardNumberUtilityOperator {
        REGEX_MATCH,
        CODE_AT_POINT_EAGER,
        CODE_AT_POINT_PATTERNED
    }

    /**
     * <p>
     * CardNumberUtilityOperator Strategy Switch
     * </p>
     *
     * @see CardNumberUtil.CardNumberUtilityOperator Card Number Utility Operator
     */
    private volatile static CardNumberUtilityOperator operator = renewOperator();

    /**
     * <p>
     * Retrieves the current {@linkplain CardNumberUtilityOperator Card Number Utility Operator} of {@linkplain CardNumberUtil Card Number Utility}
     * </p>
     *
     * @return current operator switch
     */
    public synchronized static CardNumberUtilityOperator getOperator() {
        return operator;
    }

    /**
     * Renews the static {@linkplain CardNumberUtilityOperator Card Number Utility Operator} parameter for {@linkplain CardNumberUtil Card Number Utility} via
     * <b>cardnumberutil.properties</b> resouce under
     * <q><b>{@docRoot}/util/src/main/resources/cardnumberutil.properties</b></q>
     */
    public synchronized static void updateOperator() {
        operator = renewOperator();
    }

    /**
     * <p>
     * Retrieves any matching value to the input keyword from registered properties of {@link CardNumberUtil}.
     * </p>
     *
     * @param key
     * @return matching property value
     */
    private synchronized static String getFromProperties(String key) {
        ResourceBundle CARD_NUMBER_UTIL_PROPERTIES = ResourceBundle.getBundle(PROPERTIES_BUNDLE_NAME);
        return CARD_NUMBER_UTIL_PROPERTIES.getString(key);
    }

    /**
     * <p>
     * Fetches the operator property value from the cardnumberutil properties and reruns the operator selection to renew the operator switch
     * </p>
     *
     * @return currenty set operator value from the resource
     * @see CardNumberUtil.CardNumberUtilityOperator CardNumberUtilityOperator
     */
    private synchronized static CardNumberUtilityOperator renewOperator() {
        try {
            String opValue = getFromProperties(OPERATOR_PROPERTY_KEY);
            if (opValue == null) {
                throw new Exception();
            }
            if (opValue.equals(CardNumberUtilityOperator.CODE_AT_POINT_EAGER.name())) {
                return CardNumberUtilityOperator.CODE_AT_POINT_EAGER;
            } else if (opValue.equals(CardNumberUtilityOperator.CODE_AT_POINT_PATTERNED.name())) {
                return CardNumberUtilityOperator.CODE_AT_POINT_PATTERNED;
            } else if (opValue.equals(CardNumberUtilityOperator.REGEX_MATCH.name())) {
                return CardNumberUtilityOperator.REGEX_MATCH;
            } else {
                return CardNumberUtilityOperator.CODE_AT_POINT_PATTERNED;
            }
        } catch (Throwable e) {
            return CardNumberUtilityOperator.CODE_AT_POINT_PATTERNED;
        }
    }

    /**
     * <p>
     * Chooses the correct card number utility instance according to static operator
     * </p>
     *
     * @return Card Number Utility factory instance based on the static operator parameter
     * @see CardNumberUtil.CardNumberUtilityOperator CardNumberUtilityOperator
     * @see CardNumberUtil#getCardNumberUtilFactory(CardNumberUtilityOperator) Card Number Utility Factory Selection
     */
    private synchronized static ICardNumberUtil getCardNumberUtilFactory() {
        return getCardNumberUtilFactory(operator);
    }

    /**
     * <p>
     * <b>Card Number Utility Factory Selection Method</b> Chooses the correct card number utility instance according to input operator
     * </p>
     *
     * @param operator
     * @return Card Number Utility factory instance based on input operator parameter
     * @see CardNumberUtil.CardNumberUtilityOperator CardNumberUtilityOperator
     * @see CardNumberUtilCodeAtPointPatterned Patterned Code at Point Card Number Utility
     * @see CardNumberUtilCodeAtPointEagerly Eager Code at Point Card Number Utility
     * @see CardNumberUtilRegexBased Regex Based Card Number Utility
     */
    private synchronized static ICardNumberUtil getCardNumberUtilFactory(CardNumberUtilityOperator operator) {
        switch (operator) {
            case CODE_AT_POINT_PATTERNED:
                return CardNumberUtilCodeAtPointPatterned.getInstance();
            case CODE_AT_POINT_EAGER:
                return CardNumberUtilCodeAtPointEagerly.getInstance();
            case REGEX_MATCH:
                return CardNumberUtilRegexBased.getInstance();
            default:
                return CardNumberUtilCodeAtPointPatterned.getInstance();
        }
    }

    /* PUBLIC CARD NUMBER UTILIZATION METHODS */

    /**
     * <p>
     * In a given input, method will try to find any possible card number block and will mask it with the {@linkplain CardNumberUtil#MASKING_CHAR Masking Character}. Masking
     * strategy uses the {@linkplain CardNumberUtil#findCardNumbersShallow(String) Shallow Strategy} to find which card number blocks to mask. Note that if there are not any
     * matching card numbers, value be not be altered.
     * </p>
     *
     * @param value which might or might not include card numbers
     * @return value with masked card numbers if there was any
     */
    public static String maskCardNumbers(String value) {
        return getCardNumberUtilFactory().maskCardNumbers(value);
    }

    /**
     * <p>
     * Overrides the current static operator with input operator switch and in a given input tries to find any possible card number block and will mask it with the character of
     * {@link CardNumberUtil#MASKING_CHAR Masking Character}. Masking strategy uses {@link CardNumberUtil#findCardNumbersShallow(String) Shallow Strategy} to find which card number blocks to mask. Note that if there
     * are not any matching card numbers, value be not be altered.
     * </p>
     *
     * @param value    which might or might not include card numbers
     * @param operator which assigns the operator strategy to find card numbers
     * @return value with masked card numbers if there was any
     */
    public static String maskCardNumbers(String value, CardNumberUtilityOperator operator) {
        return getCardNumberUtilFactory(operator).maskCardNumbers(value);
    }

    /**
     * <p>
     * In a given input, method will try to find any possible card numbers of any matching sequence without skipping already found card number blocks. This method will try to find
     * all possible card number blocks in a possible test partition, resulting in layers of possible card numbers which might be part of, or already inside of, another already
     * found card number block. <blockquote>Let's say that there are two blocks of numbers in the input text as
     * </p>
     *
     * <pre>
     * input = [First Block of Digits]</br> + [Block of Possible Separator Characters]</br> + [Second Block of Digits];
     * </pre>
     *
     * <b>During running deep search on the input text, logic flows as the following:</b>
     * <ol>
     * <li><strong>if</strong> <code>[Block of Possible Separator Characters]</code> <strong>are recognized as number separators and are not in distance more than maximum possible
     * card number:</strong> {@linkplain CardNumberUtil#MAX}</li> <blockquote>
     * <ol>
     * <li>All possible card numbers from <em>First and Second Block of Digits</em> starting from <em>first digit</em> to the last digit</li>
     * <li>All possible card numbers from <em>First and Second Block of Digits</em> starting from <em>second digit</em> to the last digit</li>
     * <li>All possible card numbers from <em>First and Second Block of Digits</em> starting from <em>third digit</em> to the last digit</li>
     * <li>...</li><strong>Adds any block which matches to card number validations.</strong> <strong>Iterates until distance to the last digit is closer than minimum expected card
     * number lenght:</strong> {@linkplain CardNumberUtil#MIN}
     * </ol>
     * </blockquote>
     * <li><strong>if</strong> <code>[Block of Possible Separator Characters]</code> <strong>are not recognized as number separators or are in distance more than maximum expected
     * card number lenght:</strong> {@linkplain CardNumberUtil#MAX}</li> <blockquote>
     * <ol>
     * <li>All possible card numbers from <em>First Block of Digits</em> starting from <em>first digit</em> to the last digit</li>
     * <li>All possible card numbers from <em>First Block of Digits</em> starting from <em>second digit</em> to the last digit</li>
     * <li>All possible card numbers from <em>First Block of Digits</em> starting from <em>third digit</em> to the last digit</li>
     * <li>...</li><strong>Adds any block which matches to card number validations.</strong> <strong>iterate until distance to the last digit is closer than minimum expected card
     * number lenght of </strong>{@linkplain CardNumberUtil#MIN} <strong>on <em>First Block of Digits</em>.</strong>
     * </ol>
     * <ol>
     * <li>All possible card numbers from <em>Second Block of Digits</em> starting from <em>first digit</em> to the last digit</li>
     * <li>All possible card numbers from <em>Second Block of Digits</em> starting from <em>second digit</em> to the last digit</li>
     * <li>All possible card numbers from <em>Second Block of Digits</em> starting from <em>third digit</em> to the last digit</li>
     * <li>...</li><strong>Adds any block which matches to card number validations.</strong> <strong>iterate until distance to the last digit is closer than minimum expected card
     * number lenght of</strong> {@linkplain CardNumberUtil#MIN} <strong>on <em>Second Block of Digits</em>.</strong>
     * </ol>
     * </blockquote>
     * </ol>
     *
     * @param value which might or might not include card numbers
     * @return all possible card numbers under current set static operator
     */
    public static Set<String> findCardNumbersDeep(String value) {
        return getCardNumberUtilFactory().findCardNumbersDeep(value);
    }

    /**
     * <p>
     * Overrides the current static operator with input operator switch and in a given input, method will try to find any possible card numbers of any matching sequence while
     * skipping already found card number blocks.
     * </p>
     *
     * @param value    which is a text that might or might not include card numbers
     * @param operator which assigns the operator strategy to find card numbers
     * @return all possible card numbers with selected operator
     * @see CardNumberUtil#findCardNumbersDeep(String)
     */
    public static Set<String> findCardNumbersDeep(String value, CardNumberUtilityOperator operator) {
        return getCardNumberUtilFactory(operator).findCardNumbersDeep(value);
    }

    /**
     * <p>
     * In a given input, tries to find any possible card numbers of any matching sequence while skipping already found card number blocks. Any recognized card number block will be
     * added to the result set and search will start from the last index of the last positive matched card number result.
     * </p>
     *
     * <pre>
     * input = [First Block of Digits]</br> + [Block of Possible Separator Characters]</br> + [Second Block of Digits];
     * </pre>
     *
     * <b>During running shallow search on the input text, logic flows as the following:</b>
     * <ol>
     * <li><strong>if</strong> <code>[Block of Possible Separator Characters]</code> <strong>are recognized as number separators and are not in distance more than maximum possible
     * card number:</strong> {@linkplain CardNumberUtil#MAX}</li> <blockquote>
     * <ol>
     * <li>All possible card numbers from <em>First and Second Block of Digits</em> starting from <em>first digit</em> to the last digit</li>
     * <li>All possible card numbers from <em>First and Second Block of Digits</em> starting from <em>second digit</em> to the last digit</li>
     * <li>All possible card numbers from <em>First and Second Block of Digits</em> starting from <em>third digit</em> to the last digit</li>
     * <li>...</li><strong>Adds any block which matches to card number validations.</strong> <strong><em>Set new starting index to begin search as the last index to the last found
     * match.</em></strong> <strong>Iterate until distance to the last digit is closer than minimum expected card number lenght:</strong> {@linkplain CardNumberUtil#MIN}
     * </ol>
     * </blockquote>
     * <li><strong>if</strong> <code>[Block of Possible Separator Characters]</code> <strong>are not recognized as number separators or are in distance more than maximum expected
     * card number lenght:</strong> {@linkplain CardNumberUtil#MAX}</li> <blockquote>
     * <ol>
     * <li>All possible card numbers from <em>First Block of Digits</em> starting from <em>first digit</em> to the last digit</li>
     * <li>All possible card numbers from <em>First Block of Digits</em> starting from <em>second digit</em> to the last digit</li>
     * <li>All possible card numbers from <em>First Block of Digits</em> starting from <em>third digit</em> to the last digit</li>
     * <li>...</li><strong>Adds any block which matches to card number validations.</strong> <strong><em>Set new starting index to begin search as the last index to the last found
     * match.</em></strong> <strong>iterate until distance to the last digit is closer than minimum expected card number lenght of </strong>{@linkplain CardNumberUtil#MIN}
     * <strong>on <em>First Block of Digits</em>.</strong>
     * </ol>
     * <ol>
     * <li>All possible card numbers from <em>Second Block of Digits</em> starting from <em>first digit</em> to the last digit</li>
     * <li>All possible card numbers from <em>Second Block of Digits</em> starting from <em>second digit</em> to the last digit</li>
     * <li>All possible card numbers from <em>Second Block of Digits</em> starting from <em>third digit</em> to the last digit</li>
     * <li>...</li><strong>iterate until distance to the last digit is closer than minimum expected card number lenght of</strong> {@linkplain CardNumberUtil#MIN} <strong>on
     * <em>Second Block of Digits</em>.</strong>
     * </ol>
     * </blockquote>
     * </ol>
     *
     * @param value which is a text that might or might not include card numbers
     * @return all possible card numbers in descending order and sequential in comparision to character index order
     */
    public static Set<String> findCardNumbersShallow(String value) {
        return getCardNumberUtilFactory().findCardNumbersShallow(value);
    }

    /**
     * <p>
     * Overrides the current static operator with input operator switch and in a given input, tries to find any possible card numbers of any matching sequence while skipping
     * already found card number blocks. Any recognized card number block will be added to the result set and search will start from the last index of the last positive matched
     * card number result.
     * </p>
     *
     * @param value    which is a text that might or might not include card numbers
     * @param operator which assigns the operator strategy to find card numbers
     * @return all possible card numbers with selected operator
     * @see CardNumberUtil#findCardNumbersShallow(String)
     */
    public static Set<String> findCardNumbersShallow(String value, CardNumberUtilityOperator operator) {
        return getCardNumberUtilFactory(operator).findCardNumbersShallow(value);
    }

    /**
     * <p>
     * In a given input, tries to find any possible card numbers of any matching sequence and will stop and return true if any match is positive. Method uses the
     * {@link CardNumberUtil#findCardNumbersDeep(String)} method with a slight change to stop and return the results at the first match.
     * </p>
     *
     * @param value which is a text that might or might not include card numbers
     * @return true if input has any possible card number blocks or false if no match is found
     */
    public static boolean doCardNumbersExist(String value) {
        return getCardNumberUtilFactory().doCardNumbersExist(value);
    }

    /**
     * <p>
     * Overrides the current static operator with input operator switch and in a given input,tries to find any possible card numbers of any matching sequence and will stop and
     * return true if any match is positive. Method uses the {@link CardNumberUtil#findCardNumbersDeep(String)} method on overridden card number instance with a slight change to
     * stop and return the result at the first match.
     * </p>
     *
     * @param value    which is a text that might or might not include card numbers
     * @param operator which assigns the operator strategy to find card numbers
     * @return true if input has any possible card number blocks or false if no match is found
     * @see CardNumberUtil#doCardNumbersExist()
     */
    public static boolean doCardNumbersExist(String value, CardNumberUtilityOperator operator) {
        return getCardNumberUtilFactory(operator).doCardNumbersExist(value);
    }

    /* COMMON OPERATIONS */
    /**
     * <p>
     * Comparator of Strings for descending order based on lenght.
     * </p>
     */
    final static Comparator<String> STRING_COMPARATOR_LONGER_FIRST = new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
            return Integer.compare(o2.length(), o1.length());
        }
    };

    /**
     * <p>
     * Sorts and array of strings depending on the lenght in descending order. Repeated strings will be unified by set data structure, thus eliminating repetitions.
     * </p>
     *
     * @param args
     * @return set of strings in descending order
     */
    static Set<String> convertToOrderedSet(String[] args) {
        Arrays.sort(args, STRING_COMPARATOR_LONGER_FIRST);
        Set<String> linkedSet = new LinkedHashSet<>();
        for (String arg : args) {
            if (!StringUtils.isEmpty(arg)) {
                linkedSet.add(arg);
            }
        }
        return linkedSet;
    }

    /* CARD NUMBER AND TYPE VALIDATORS */

    /**
     * <p>
     * Card Number validator runs a luhn algorithm on a string representing a card number. Card number should be a block text consisting only digit characters.
     * </p>
     * <p>
     * Card number validation runs Luhn algorithm on Mod 10. The Luhn algorithm or Luhn formula, also known as the “modulus 10″ or “mod 10″ algorithm, is a simple checksum formula
     * used to validate a variety of identification numbers. <b>A nice explanation of luhn algorithm can be found</b> <a href=https://www.geeksforgeeks.org/luhn-algorithm/>here</a>
     * </p>
     *
     * @param cardNumber
     * @return is input a possible card number?
     */
    static boolean isValidCardNumber(String cardNumber) {
        Boolean decision = false;
        cardNumber = new StringBuffer(cardNumber).reverse().toString();
        int total = 0;
        for (int i = 0; i < cardNumber.length(); i++) {
            int num = Integer.parseInt(String.valueOf(cardNumber.charAt(i)));
            if ((i + 1) % 2 == 0) {
                num = num * 2;
                num = (num / 10) + (num % 10);
            }
            total += num;
        }
        if (total % 10 == 0) {
            decision = true;
        }
        return decision;
    }

    /**
     * <p>
     * Card type validator method. This method works as a combined and simplified regex unit that tries to match possible BIN numbers and expected card number lenghts.
     * <blockquote>NOTE: BIN number is
     * <q>First 6 digits of a card number</q>
     * </p>
     * <style>table, tr, td {border: 1px solid black;}</style>
     * <table>
     * <thead>
     * <tr>
     * <th>Supported Card Types</th>
     * <th>Expected Card Lenght</th>
     * <th>Match Condition</th>
     * </tr>
     * </thead>
     * <tr>
     * <td>Maestro</td>
     * <td>12 to 19 digits</td>
     * <td>First two digits are 50 or between 56 to 58 or 63 or 67</td>
     * </tr>
     * <tr>
     * <td>JCB13</td>
     * <td>13 digits</td>
     * <td>Starts with 18 or 21</td>
     * </tr>
     * <tr>
     * <td>JCB</td>
     * <td>16 digits</td>
     * <td>Starts with 3</td>
     * </tr>
     * <tr>
     * <td>Diners Club</td>
     * <td>14 digits</td>
     * <td>Starts with first three digits between 301 to 305 or 309 or first two digits as 36, 38 or 39</td>
     * </tr>
     * <tr>
     * <td>American Express</td>
     * <td>15 digits</td>
     * <td>Starts with 34 or 37</td>
     * </tr>
     * <tr>
     * <td>UATP</td>
     * <td>15 digits</td>
     * <td>First digit is 1</td>
     * </tr>
     * <tr>
     * <td>EnRoute</td>
     * <td>15 digits</td>
     * <td>Starts with 2014 or 2149</td>
     * </tr>
     * <tr>
     * <td>Voyager</td>
     * <td>15 digits</td>
     * <td>First four digits are 8699</td>
     * </tr>
     * <tr>
     * <td>MasterCard</td>
     * <td>16 digits</td>
     * <td>First two digits are between 51 to 55</td>
     * </tr>
     * <tr>
     * <td>Visa</td>
     * <td>13, 16 or 19 digits</td>
     * <td>Starts with 4</td>
     * </tr>
     * <tr>
     * <td>BankCard(US)</td>
     * <td>16 digits</td>
     * <td>First six digits as 560221 to 560225 or first four digits as 5610</td>
     * </tr>
     * <tr>
     * <td>UnionPay(China)</td>
     * <td>16 or 19 digits</td>
     * <td>First two digits are 62</td>
     * </tr>
     * <tr>
     * <td>Discover</td>
     * <td>16 or 19 digits</td>
     * <td>Starts with 6011 or 644 to 649 or 622126 to 622925</td>
     * </tr>
     * <tr>
     * <td>InterPayment</td>
     * <td>19 digits</td>
     * <td>First three digits are 636</td>
     * </tr>
     * <tr>
     * <td>InterPayment16</td>
     * <td>16 digits</td>
     * <td>Starts with first digits between 636 to 639</td>
     * </tr>
     * <tr>
     * <td>Verve</td>
     * <td>16 or 19 digits</td>
     * <td>Starts with bin numbers between 506099 to 50619 or between 650002 to 650027</td>
     * </tr>
     * </table>
     *
     * @param cardNumber
     * @return whether if the cardnumber matches an issuer
     */
    static boolean isValidCardType(String cardNumber) {
        int length = cardNumber.length();
        switch (length) {
            case 19:
                return cardNumber.substring(0, 4).matches("^(?:^5[0678][\\d]*|4[\\d]*|6[2357][\\d]*||64[4-9][\\d]*|6011)");
            case 18:
                return cardNumber.substring(0, 2).matches("^(?:5[0678]|63|67)");
            case 17:
                return cardNumber.substring(0, 2).matches("^(?:5[0678]|63|67)");
            case 16:
                return cardNumber.substring(0, 4).matches("^(?:3[\\d]*|5[\\d]*|4[\\d]*|6[2357][\\d]*||64[4-9][\\d]*|6011)");
            case 15:
                return cardNumber.substring(0, 4).matches("^(?:1[\\d]*|3[47][\\d]*|2014|2149|2100|2131|8699)");
            case 14:
                return cardNumber.substring(0, 3).matches("^(?:30[0-59]|3[689]\\d|5[0678]\\d|63\\d|67\\d)");
            case 13:
                return cardNumber.substring(0, 4).matches("^(?:4[\\d]*|1800|2100|2131|5[0678][\\d]*|6[37][\\d]*)");
            case 12:
                return cardNumber.substring(0, 2).matches("^(?:5[0678]|63|67)");
            default:
                return false;
        }
    }

}
