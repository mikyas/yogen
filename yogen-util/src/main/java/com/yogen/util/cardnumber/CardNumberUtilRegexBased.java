package com.yogen.util.cardnumber;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import org.springframework.util.StringUtils;
import com.yogen.util.RegexUtil;

/**
 * <b>Immutable scheme definition for Card Number Utilities that works with
 * <q>Regex</q> Strategy</br>
 * </b>
 * <p>
 * <em>Regex</em> is hardcoded pattern inferring standarts that is used in many forms. However it also depends on parsing both the Regex Definition and the input text. Therefore
 * this creates a performance bottleneck.
 * </p>
 * <p>
 * Regex definitions uses a Card Number Recognition Regex Definition and the main strategy is to use a replacement place holder clause the is random generated hash. In a given
 * text, parts that fits to a part where it is a suspect for a card number scheme with be broken to separated parts and these parts will be replaced with the replacement
 * placeholder clause. Later on, the given text will the split via the replacement place holder to generate a clean and workable list of suspected text partitions. If a partition
 * is identified as a Card Number, the operations will commence according to its specialization.
 * <nl>
 * <li>{@linkplain CardNumberUtilCodeAtPointPatterned Patterned}</li>
 * <li>{@linkplain CardNumberUtilCodeAtPointEagerly Eager}</li>
 * </nl>
 * </p>
 *
 * @author oaksoy
 * @see CardNumberUtil Card Number Utility
 * @see CardNumberUtil.CardNumberUtilityOperator Card Number Utility Operator
 */
final class CardNumberUtilRegexBased implements ICardNumberUtil {

    private CardNumberUtilRegexBased() {
    }

    private static ICardNumberUtil instance;

    /**
     * Returns the singleton instance for ICardNumberUtil with Regex Based Behavior
     *
     * @return ICardNumberUtil instance
     */
    public static ICardNumberUtil getInstance() {
        if (instance == null) {
            instance = new CardNumberUtilRegexBased();
        }
        return instance;
    }

    /**
     * Value of <b>""</b> as convenience and readability.
     */
    private final String EMPTY = "";

    /**
     * Card Numbers are partitioned to a more managable sub parts by catching parts of text that matches to this regex.
     */
    private final String CARD_NUMBER_PARTITIONED_REGEX = "((\\d+)(\\W*?)){" + CardNumberUtil.MIN.toString() + ",}";
    /**
     * Regex to match a block of any digit.
     */
    private final String NUMBER_BLOCK_REGEX = "\\d+";

    /**
     */
    private final String REPLACEMENT_PLACE_HOLDER = UUID.randomUUID().toString();

    /**
     * <b>Regex Based Card Number Masking operation aims to find the digit sequences with possible card number schemes via </b>
     * {@linkplain CardNumberUtilRegexBased#CARD_NUMBER_PARTITIONED_REGEX Card Number Partitioner Regex}.
     * <p>
     * Once partitions are ordered in a set from longest to shortest possible digit set matches, they are also replaced in the same order by
     * {@linkplain CardNumberUtilRegexBased#REPLACEMENT_PLACE_HOLDER Replacement Placeholder}. This would enable to split the input text into only partitions that are meaningful to
     * search for card number schemes. Finally, splitted partitions will be processes in by traversing the text and finding plausible card numbers. These parts will be replaced by
     * the {@linkplain CardNumberUtil#MASKING_CHAR Masking Character}.
     * </p>
     * <p>
     * <em>Note that the identified card number carrying partitions would have to escape from the possible similar regex character combinations during mask generation
     * operation.</em>
     * </p>
     */
    @Override
    public String maskCardNumbers(String value) {
        if (!(value != null && !value.trim().isEmpty())) {
            return value;
        }
        String masked = value;
        for (String str : CardNumberUtil.convertToOrderedSet(value.split(CARD_NUMBER_PARTITIONED_REGEX))) {
            value = value.replace(str, REPLACEMENT_PLACE_HOLDER);
        }
        for (String str : CardNumberUtil.convertToOrderedSet(value.split(REPLACEMENT_PLACE_HOLDER))) {
            if (traverseNumberStringForCardNumbersAndReturnDecision(removeNonWordChars(str))) {
                masked = masked.replaceFirst(RegexUtil.escapeRegexPatternChars(str), generateMask(str.length()));
            }
        }
        return masked;
    }

    /**
     * <b>Regex Based Card Number Finding Operation with Deep Search Behavior aims to find the digit sequences with possible card number schemes via </b>
     * {@linkplain CardNumberUtilRegexBased#CARD_NUMBER_PARTITIONED_REGEX Card Number Partitioner Regex}<b>. and traverse to all possible Card Number Schemes from the</b>
     * {@linkplain CardNumberUtil#MAX}<b> to the </b>{@linkplain CardNumberUtil#MIN}.
     * <p>
     * Once partitions are ordered in a set from longest to shortest possible digit set matches, they are also replaced in the same order by
     * {@linkplain CardNumberUtilRegexBased#REPLACEMENT_PLACE_HOLDER Replacement Placeholder}. This would enable to split the input text into only partitions that are meaningful to
     * search for card number schemes. Finally, splitted partitions will be processes by traversing the text and finding plausible card numbers. These parts will processed and
     * aggregated into a list.
     * </p>
     */
    @Override
    public Set<String> findCardNumbersDeep(String value) {
        if (value != null && !value.trim().isEmpty()) {
            Set<String> set = new HashSet<>();
            for (String str : fetchPossibleCardNumbersFromPartitionedText(value)) {
                set.addAll(traverseNumberStringForCardNumbersCatchAllMatching(str));
            }
            return CardNumberUtil.convertToOrderedSet(set.toArray(new String[set.size()]));
        } else {
            return null;
        }
    }

    /**
     * <b>Regex Based Card Number Finding Operation with Shallow Search Behavior aims to find the digit sequences with possible card number schemes via </b>
     * {@linkplain CardNumberUtilRegexBased#CARD_NUMBER_PARTITIONED_REGEX Card Number Partitioner Regex}<b>. and traverse <u>to find the first possible Card Number Scheme</u> from
     * the</b> {@linkplain CardNumberUtil#MAX}<b> to the </b>{@linkplain CardNumberUtil#MIN}.
     * <p>
     * Once partitions are ordered in a set from longest to shortest possible digit set matches, they are also replaced in the same order by
     * {@linkplain CardNumberUtilRegexBased#REPLACEMENT_PLACE_HOLDER Replacement Placeholder}. This would enable to split the input text into only partitions that are meaningful to
     * search for card number schemes. Finally, splitted partitions will be processes by traversing the text and finding plausible card numbers. These parts will processed and
     * aggregated into a list.
     * </p>
     */
    @Override
    public Set<String> findCardNumbersShallow(String value) {
        if (value != null && !value.trim().isEmpty()) {
            Set<String> set = new HashSet<>();
            for (String str : fetchPossibleCardNumbersFromPartitionedText(value)) {
                set.addAll(traverseNumberStringForCardNumbersCatchFirstMatching(str));
            }
            return CardNumberUtil.convertToOrderedSet(set.toArray(new String[set.size()]));
        } else {
            return null;
        }
    }

    /**
     * <b>Regex Based Card Number Card Number Discovery Behavior aims to find a trace of a digit sequence with a possible card number scheme via </b>
     * {@linkplain CardNumberUtilRegexBased#CARD_NUMBER_PARTITIONED_REGEX Card Number Partitioner Regex}.
     * <p>
     * As it is concerned with only the signaling the existance of a Card Number Scheme, method will process the input text to find any suspitious textual partition and directy
     * traverse the complied partitions to discover any trace of Card Number Scheme matchings. First sign of such a match will fire a positive response.
     * </p>
     */
    @Override
    public boolean doCardNumbersExist(String value) {
        if (value != null && !value.trim().isEmpty()) {
            Set<String> matchingNumbersSet = fetchPossibleCardNumbersFromPartitionedText(value);
            if (matchingNumbersSet != null) {
                for (String str : matchingNumbersSet) {
                    if (traverseNumberStringForCardNumbersAndReturnDecision(str)) {
                        return true;
                    }
                }
                return false;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Convenience method to quickly convert a String Array to a set of Strings.
     * <p>
     * <em>Note: Naturally, the repeating string values will be recorded only once in the set.</em>
     * </p>
     *
     * @param args as string array
     * @return a set of string with the same values
     */
    private Set<String> convertToSet(String[] args) {
        Set<String> set = new HashSet<>();
        for (String arg : args) {
            if (!StringUtils.isEmpty(arg)) {
                set.add(arg);
            }
        }
        return set;
    }

    /**
     * Generates a set of partitions from parts of the input text. Partitions are created by the similarity to {@linkplain CardNumberUtilRegexBased#CARD_NUMBER_PARTITIONED_REGEX
     * Card Number Like Text Partitions} which can be suspected of carrying Card Number.
     *
     * @param value input value
     * @return a set of number-only text partitions
     */
    private Set<String> fetchPossibleCardNumbersFromPartitionedText(String value) {
        for (String str : CardNumberUtil.convertToOrderedSet(value.split(CARD_NUMBER_PARTITIONED_REGEX))) {
            value = value.replace(str, REPLACEMENT_PLACE_HOLDER);
        }
        Set<String> onlyNumbers = new HashSet<>();
        for (String str : convertToSet(value.split(REPLACEMENT_PLACE_HOLDER))) {
            String cleaned = removeNonWordChars(str);
            if (!cleaned.isEmpty())
                onlyNumbers.add(cleaned);
        }
        return onlyNumbers;
    }

    /**
     * Used to prepare the value for preparing a text partition to protect from Regex Matching Malfunctions. It will iterate any text partition of non-digit nature within the input
     * text and remove wild characters and whitespaces.
     *
     * @param value input text partition
     * @return input value as cleaned from wild characters and whitespaces
     */
    private String removeNonWordChars(String value) {
        String[] strArray = value.split(NUMBER_BLOCK_REGEX);
        Set<Character> charSet = new HashSet<>();
        for (String str : strArray) {
            for (char ch : str.toCharArray()) {
                charSet.add(ch);
            }
        }
        String processed = value;
        for (Character ch : charSet) {
            processed = processed.replace(ch.toString(), EMPTY);
        }
        return processed;
    }

    /**
     * <b>Card number scheme searching operation to catch all possible combinations. </b>
     * <p>
     * Works as traversing windows. Width of the window enables the maximum lenght of a card number scheme that can be inferred between digits. In order to unrelate the impossible
     * digit characters, distance to the last digit is kept in a meaningful distanse to each other. To achieve this last index of the possible card number lenght is kept in a
     * distance under the maximum know card number. Once a search window is processed until the minimum card number lenght, then window iterates to the next index.
     * </p>
     * <p>
     * <em>Note: Works with digit-only text</em>
     * </p>
     *
     * @param value input text
     * @return digit-only texts which each matches to a plausible card number scheme
     */
    private Set<String> traverseNumberStringForCardNumbersCatchAllMatching(String value) {
        Set<String> found = new HashSet<String>();
        int windowWidth = CardNumberUtil.MAX - CardNumberUtil.MIN;
        int buff = value.length() - CardNumberUtil.MIN;

        for (int start = 0; start <= buff; start++) {
            for (int index = 0; index <= windowWidth; index++) {
                int end = CardNumberUtil.MIN + start + index;
                if (end >= value.length()) {
                    end = value.length();
                }
                String subString = value.substring(start, end);
                if (CardNumberUtil.isValidCardType(subString) && CardNumberUtil.isValidCardNumber(subString)) {
                    found.add(subString);
                }
            }
        }
        return found;
    }

    /**
     * <b>Card number scheme searching operation to catch first possible combination in a text.</b>
     * <p>
     * Works as traversing windows. Width of the window enables the maximum lenght of a card number scheme that can be inferred between digits. In order to unrelate the impossible
     * digit characters, distance to the last digit is kept in a meaningful distanse to each other. To achieve this last index of the possible card number lenght is kept in a
     * distance under the maximum know card number. Once a search window is processed until the minimum card number lenght, then window iterates to the next index. <b>Once a Card
     * Number Scheme matchup is found, the window will shift to exclude and other matchups on the same text partition on shorter lenghts.</b>
     * </p>
     * <p>
     * <em>Note: Works with digit-only text</em>
     * </p>
     *
     * @param value input text
     * @return digit-only texts which each matches to a plausible card number scheme
     */
    private Set<String> traverseNumberStringForCardNumbersCatchFirstMatching(String value) {
        Set<String> found = new HashSet<String>();
        int windowWidth = CardNumberUtil.MAX - CardNumberUtil.MIN;
        int buff = value.length() - CardNumberUtil.MIN;

        for (int start = 0; start <= buff; start++) {
            for (int index = 0; index <= windowWidth; index++) {
                int end = CardNumberUtil.MIN + start + index;
                if (end >= value.length()) {
                    end = value.length();
                }
                String subString = value.substring(start, end);
                if (CardNumberUtil.isValidCardType(subString) && CardNumberUtil.isValidCardNumber(subString)) {
                    found.add(subString);
                    start = end--;
                    index = 0;
                }
            }
        }
        return found;
    }

    /**
     * <b>Card number scheme searching operation to catch first possible combination in a text and fires a response.</b>
     * <p>
     * Works as traversing windows. Width of the window enables the maximum lenght of a card number scheme that can be inferred between digits. In order to unrelate the impossible
     * digit characters, distance to the last digit is kept in a meaningful distanse to each other. To achieve this last index of the possible card number lenght is kept in a
     * distance under the maximum know card number. Once a search window is processed until the minimum card number lenght, then window iterates to the next index. Fires a positive
     * response at the first Card Number Scheme matchup.
     * </p>
     * <p>
     * <em>Note: Works with digit-only text</em>
     * </p>
     *
     * @param value input text
     * @return digit-only texts which each matches to a plausible card number scheme
     */
    private Boolean traverseNumberStringForCardNumbersAndReturnDecision(String value) {
        int windowWidth = CardNumberUtil.MAX - CardNumberUtil.MIN;
        int buff = value.length() - CardNumberUtil.MIN;
        for (int start = 0; start <= buff; start++) {
            for (int index = 0; index <= windowWidth; index++) {
                int end = CardNumberUtil.MIN + start + index;
                if (end >= value.length()) {
                    end = value.length();
                }
                String subString = value.substring(start, end);
                if (CardNumberUtil.isValidCardType(subString) && CardNumberUtil.isValidCardNumber(subString)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Generate a mask from the masking character of the input lenght
     *
     * @param length
     * @return a text constructed by the {@linkplain CardNumberUtil#MASKING_CHAR Masking Char} with the input lenght
     */
    private String generateMask(int length) {
        String mask = EMPTY;
        for (int i = 0; i < length; i++) {
            mask = mask + CardNumberUtil.MASKING_CHAR;
        }
        return mask;
    }

}
