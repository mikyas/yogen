package com.yogen.util.encryptor;

import java.security.MessageDigest;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import com.yogen.util.exception.EncryptionException;

public class EncryptionUtil {

    private static final String TRANSFORMATION = "AES/ECB/PKCS5Padding";
    private static final String HASH_ALGORITHM = "SHA-256";
    private static final String ENCRYPTION_ALGORITHM = "AES";

    public static String hash(String plainText) throws EncryptionException {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(HASH_ALGORITHM);
            byte[] hashBytes = messageDigest.digest(plainText.getBytes("UTF-8"));
            return Base64.byteArrayToBase64(hashBytes);
        } catch (Exception e) {
            throw new EncryptionException(e);
        }
    }

    public static String encrypt(String plainText, byte[] keyBytes) throws EncryptionException {
        try {
            // Create the cipher
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);

            // Initialize the cipher for encryption
            cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(keyBytes, ENCRYPTION_ALGORITHM));

            // Encrypt the cleartext
            byte[] plainTextBytes = cipher.doFinal(plainText.getBytes());
            return Base64.byteArrayToBase64(plainTextBytes);
        } catch (Exception e) {
            throw new EncryptionException(e);
        }
    }

    public static String decrypt(String cipherText, byte[] keyBytes) throws EncryptionException {
        try {
            // Create the cipher
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);

            // Initialize the same cipher for decryption
            cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(keyBytes, ENCRYPTION_ALGORITHM));

            // Decrypt the ciphertext
            byte[] plainTextBytes = cipher.doFinal(Base64.base64ToByteArray(cipherText));
            return new String(plainTextBytes);
        } catch (Exception e) {
            throw new EncryptionException(e);
        }
    }
}