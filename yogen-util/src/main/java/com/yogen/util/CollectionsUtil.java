package com.yogen.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import com.yogen.util.enumtype.IValueEnum;

public final class CollectionsUtil {

    private CollectionsUtil() {
    }

    public static List<Integer> generateValueList(List<? extends IValueEnum> list) {
        List<Integer> valueList = new ArrayList<Integer>();
        if (list != null) {
            for (IValueEnum valueEnum : list) {
                valueList.add(valueEnum.getValue());
            }
        }
        return valueList;
    }

    /**
     * Parametre olarak verilen listeyi verilen eleman buyuklugune gore alt listelere ayirir.
     */
    public static <T> List<List<T>> chop(List<T> list, final int L) {
        List<List<T>> parts = new ArrayList<List<T>>();
        final int N = list.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<T>(list.subList(i, Math.min(N, i + L))));
        }
        return parts;
    }

    public static <T> List<T> convertGenericListToTypedList(Class<T> cls, List<?> list) {
        List<T> convertedList = new ArrayList<>();
        if (list != null && !list.isEmpty()) {
            for (Object turple : list) {
                if (cls.isAssignableFrom(turple.getClass())) {
                    convertedList.add(cls.cast(turple));
                }
            }
        }
        return convertedList;
    }// End of Method

    public static <T> T getFirstElement(List<T> list) {
        if (list != null && list.size() > 0) {
            return list.get(0);
        }
        return null;
    }


    /**
     * verilen liste null veya bos ise true doner aksi halde false doner
     *
     * @param collection
     * @return
     */
    public static boolean isNullOrEmpty(Collection<?> collection) {
        if (collection == null || collection.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }
    
    public static boolean isNotNullAndNotEmpty(Collection<?> collection) {
    	return !isNullOrEmpty(collection);
    }
}