package com.yogen.util.validator;

public class TcCertificateValidator {

    public static boolean isValid(String value) {
        boolean flag = false;
        // Total Length is 11
        try {
            if (value.length() == 11) {

                //First digit can not be '0'
                if (value.charAt(0) != '0') {

                    //All characters are digits between '0'-'9'
                    int[] numberAr = new int[11];
                    for (int i = 0; i < 11; i++) {
                        numberAr[i] = charToInt(value.charAt(i));
                    }

                    // [((Sum of 1st, 3rd, 5th, 7th, 9thh digits) * 7) - (Sum of 2nd, 4th, 6th, 8th digits)] % 10 must be equal to 10th digit
                    int sumOfOdds = sumArray(numberAr, 0, 8, 2);
                    int sumOfEvens = sumArray(numberAr, 1, 7, 2);
                    int parity1 = ((sumOfOdds * 7) - sumOfEvens) % 10;
                    if (parity1 < 0) {
                        parity1 += 10;
                    }
                    if (parity1 == numberAr[9]) {
                        // (Sum of 1st to 10th digits) % 10 must be equal to 11th digit
                        int sum = sumArray(numberAr, 0, 9, 1);
                        int parity2 = sum % 10;
                        if (parity2 == numberAr[10]) {
                            //Valid
                            flag = true;
                        }
                    }
                }
            }

        } catch (Throwable ignored) {
        }

        return flag;
    }

    private static int sumArray(int[] numberAr, int start, int end, int step) {
        int sum = 0;
        for (int i = start; i <= end; i += step)
            sum += numberAr[i];
        return sum;
    }

    private static int charToInt(char ch) {
        int digit = ch - '0';
        if (digit < 0 || digit > 9) {
            throw new NumberFormatException("Digit : " + ch + " is invalid.");
        }
        return digit;
    }
}