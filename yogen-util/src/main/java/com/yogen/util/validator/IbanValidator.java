package com.yogen.util.validator;

import java.math.BigInteger;
import com.yogen.util.StringUtil;

public class IbanValidator {
    private static final BigInteger BIG_INT_97 = BigInteger.valueOf(97);

    public IbanValidator() {
        super();
    }

    private static boolean isTrIban(String value) {
        boolean flag = false;
        // Total Length is 26
        if (value.length() == 26) {
            // A->10 ... R -> 27 ... T->29 ... Z->35
            String integerStr = value.substring(4) + "29" + "27" + value.substring(2, 4);
            try {
                BigInteger integer = new BigInteger(integerStr);
                BigInteger mod = integer.mod(BIG_INT_97);
                flag = BigInteger.ONE.equals(mod);
            } catch (NumberFormatException ex) {
                return false;
            }
        }

        return flag;
    }


    public static boolean validate(String iban) {
        if (StringUtil.isNullOrZeroLength(iban)) {
            return false;
        }

        iban = iban.trim();

        return isTrIban(iban);
    }

}
