package com.yogen.util.validator;

import java.util.regex.Pattern;
import com.yogen.util.StringUtil;

public class PasswordValidator {

    public static boolean isPasswordValid(String password) {
        boolean isValid = false;
        password = StringUtil.trimAndSetNullIfBlank(password);
        if (password != null && password.length() >= 8 && password.length() <= 20) {
            isValid = (password.matches(".*[a-z]+.*") || password.matches(".*[A-Z]+.*")) && password.matches(".*\\d+.*");
        }
        return isValid;
    }

    public static boolean isPasswordValid(String password, String regex) {
        boolean isValid = false;
        password = StringUtil.trimAndSetNullIfBlank(password);
        if (password != null) {
            Pattern pattern = Pattern.compile(regex);
            isValid = pattern.matcher(password).matches();
        }
        return isValid;
    }
}
//(?=^.{8,20}$)(?=.*\d+.*)((?=.*[a-z]+.*)|(?=.*[A-Z]+.*)).*$ server side control in MultiMobil // güvenlik gereği aşağıdaki gibi olmalıdır.
//^(?=.*?\d)(?=.*?[a-zA-Z])([a-zA-Z0-9_+//-]{8,25})$ web deki
// tüm sistemdeki şifreler tek tek kontrol yukarıdakine uyan var mı eklenmelidir.