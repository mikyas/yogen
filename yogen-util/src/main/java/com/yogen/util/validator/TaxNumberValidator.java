package com.yogen.util.validator;

import com.yogen.util.StringUtil;

public class TaxNumberValidator {

    private static final String TAX_NUMBER_PATTERN = "[0-9]{10,11}";

    public static boolean isValid(String taxNumber) {
        if (!StringUtil.isNullOrZeroLength(taxNumber) && taxNumber.matches(TAX_NUMBER_PATTERN)) {
            int num1 = (Integer.valueOf(String.valueOf(taxNumber.charAt(0))) + 9) % 10;
            int num2 = (Integer.valueOf(String.valueOf(taxNumber.charAt(1))) + 8) % 10;
            int num3 = (Integer.valueOf(String.valueOf(taxNumber.charAt(2))) + 7) % 10;
            int num4 = (Integer.valueOf(String.valueOf(taxNumber.charAt(3))) + 6) % 10;
            int num5 = (Integer.valueOf(String.valueOf(taxNumber.charAt(4))) + 5) % 10;
            int num6 = (Integer.valueOf(String.valueOf(taxNumber.charAt(5))) + 4) % 10;
            int num7 = (Integer.valueOf(String.valueOf(taxNumber.charAt(6))) + 3) % 10;
            int num8 = (Integer.valueOf(String.valueOf(taxNumber.charAt(7))) + 2) % 10;
            int num9 = (Integer.valueOf(String.valueOf(taxNumber.charAt(8))) + 1) % 10;
            int last_digit = Integer.valueOf(String.valueOf(taxNumber.charAt(9)));

            int num11 = (num1 * 512) % 9;
            int num22 = (num2 * 256) % 9;
            int num33 = (num3 * 128) % 9;
            int num44 = (num4 * 64) % 9;
            int num55 = (num5 * 32) % 9;
            int num66 = (num6 * 16) % 9;
            int num77 = (num7 * 8) % 9;
            int num88 = (num8 * 4) % 9;
            int num99 = (num9 * 2) % 9;

            if (num1 != 0 && num11 == 0) num11 = 9;
            if (num2 != 0 && num22 == 0) num22 = 9;
            if (num3 != 0 && num33 == 0) num33 = 9;
            if (num4 != 0 && num44 == 0) num44 = 9;
            if (num5 != 0 && num55 == 0) num55 = 9;
            if (num6 != 0 && num66 == 0) num66 = 9;
            if (num7 != 0 && num77 == 0) num77 = 9;
            if (num8 != 0 && num88 == 0) num88 = 9;
            if (num9 != 0 && num99 == 0) num99 = 9;
            int toplam = num11 + num22 + num33 + num44 + num55 + num66 + num77 + num88 + num99;

            if (toplam % 10 == 0) {
                toplam = 0;
            } else {
                toplam = (10 - (toplam % 10));
            }
            if (toplam == last_digit) {
                return true;
            }
        }
        return false;
    }
}