package com.yogen.util.validator;

import com.yogen.util.enumtype.IValueEnum;

public enum ResponseMessage implements IValueEnum {

    FIELD_NOT_NULL_REFERENCES(0),
    FIELD_FORMAT_ERROR(1),
    FIELD_TYPE_ERROR(2);

    private final Integer value;

    private ResponseMessage(Integer value) {
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }
}