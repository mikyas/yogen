package com.yogen.util.validator;

import java.util.regex.Pattern;

public class ValidatorUtil {

    public final static Pattern DEFAULT_REGEX = Pattern.compile("^[a-zA-ZöçşığüÖÇŞİĞÜ0-9 \\s\\.]{2,50}$");
    public final static Pattern BANK_CARD_REGEX = Pattern.compile("[0-9]{15,16}");
    public final static Pattern SUPPLIER_ID_REGEX = Pattern.compile("[0-9]{8}");
    public final static Pattern GSM_REGEX = Pattern.compile("[5][0-9]{2}[0-9]{7}");
    public final static Pattern TELEPHONE_REGEX = Pattern.compile("[0-9]{11}");
    public final static Pattern NAME_SURNAME_REGEX = Pattern.compile("^[a-zA-ZöçşığüÖÇŞİĞÜ0-9 \\s \\.\\,]{2,50}$");
    public final static Pattern EMAIL_REGEX = Pattern.compile("\\b(^['_A-Za-z0-9-]+(\\.['_A-Za-z0-9-]+)*@([A-Za-z0-9-])+(\\.[A-Za-z0-9-]+)*((\\.[A-Za-z0-9]{2,})|(\\.[A-Za-z0-9]{2,}\\.[A-Za-z0-9]{2,}))$)\\b");
    public final static Pattern TC_CERTIFICATE_REGEX = Pattern.compile("[1-9][0-9]{10}");
    public final static Pattern PHONE_REGEX = Pattern.compile("[1-9][0-9]{2}[1-9][0-9]{6}");
    public final static Pattern CITY_REGEX = Pattern.compile("[0-8][0-9]");
    public final static Pattern IBAN_OWNER_NAME_REGEX = Pattern.compile("^[0-9a-zA-ZöçşığüÖÇŞİĞÜ \\.\\-]{1,100}$");
    public final static Pattern MERCHANT_NAME_REGEX = Pattern.compile("^[0-9a-zA-ZöçşığüÖÇŞİĞÜ \\.\\-]{1,100}$");
    public final static Pattern CARD_ALIAS_REGEX = Pattern.compile("^[a-zA-ZöçşığüÖÇŞİĞÜ_ \\.\\-]{1,100}$");
    public final static Pattern CARD_HOLDER_NAME_REGEX = Pattern.compile("^[0-9a-zA-ZöçşığüÖÇŞİĞÜ \\.\\-]{4,100}$");
    public final static Pattern CARD_CVC_REGEX = Pattern.compile("[0-9]{3,4}");
    public final static Pattern BIRTH_DATE_REGEX = Pattern.compile("(0[1-9]{1}|[1-2]{1}[0-9]{1}|3[0-1]{1})\\.(0[1-9]{1}|1[0-2]{1})\\.(19|20)[0-9]{2}");
    public final static Pattern ADDRESS_REGEX = Pattern.compile("^[0-9a-zA-ZöçşığüÖÇŞİĞÜ \\.\\-:/,()]{5,255}$");
    public final static Pattern BIN_NUMBER_REGEX = Pattern.compile("[0-9]{6}");
    public final static Pattern PIN_CODE_REGEX = Pattern.compile("[0-9]{4}");
    public final static Pattern FOUR_DIGIT_REGEX = Pattern.compile("^[0-9]{4}$");
    public final static Pattern ZIP_CODE_REGEX = Pattern.compile("^([0-9]{5})");
    public final static Pattern POBOX_REGEX = Pattern.compile("[0-9]{1,5}$");
    public final static Pattern MERCHANT_NUMBER_REGEX = Pattern.compile("^[0-9]{6}$");
    public final static Pattern USERNAME_REGEX = Pattern.compile("^[0-9a-zA-Z]{2,25}$");
    public final static Pattern TAX_OFFICE_REGEX = Pattern.compile("^[0-9a-zA-ZöçşığüÖÇŞİĞÜ \\.\\-]{2,50}$");
    public final static Pattern UID_REGEX = Pattern.compile("^[0-9a-zA-ZöçşığüÖÇŞİĞÜ \\-\\_\\.@!]{1,100}$");
    public final static Pattern MERCHANT_CUSTOM_RECEIPT_REGEX = Pattern.compile("^[0-9A-Z]{1,25}$");
    public final static Pattern INTEGER_WITHOUT_SIGNS_REGEX = Pattern.compile("^([1-9]+\\d*)$|^0$");
    public final static Pattern BO_USERNAME_REGEX = Pattern.compile("^[a-z\\.]{2,25}$");
    public final static Pattern API_USER_NAME_REGEX = Pattern.compile("^[a-zA-Z\\-]{2,30}$");
    public final static Pattern PASSWORD_REGEX = Pattern.compile("(?=^.{8,20}$)(?=.*\\d+.*)((?=.*[a-z]+.*)|(?=.*[A-Z]+.*)).*$");
    public final static Pattern REVENUE_REGEX = Pattern.compile("^[0-9]{1,9}$");
    public final static Pattern WORKING_BANKS_REGEX = Pattern.compile("^[0-9a-zA-ZöçşığüÖÇŞİĞÜ \\.\\-:/,]{1,100}$");
    public final static Pattern SIMPLE_DESCRIPTION_REGEX = Pattern.compile("^[0-9a-zA-ZöçşığüÖÇŞİĞÜ \\.\\-:/,]{10,250}$");
    public final static Pattern UPC_REGEX = Pattern.compile("^[0-9]{12,13}$");
    public final static Pattern SKU_REGEX = Pattern.compile("^[0-9a-zA-ZöçşığüÖÇŞİĞÜ \\.\\-\\_]{1,100}$");
    public final static Pattern STRING_REGEX = Pattern.compile("^[0-9a-zA-ZöçşığüÖÇŞİĞÜ \\.\\-\\_]{1,100}$");
    public final static Pattern CITIZENNUM_REGEX = Pattern.compile("^[0-9]{5,25}$");
    public final static Pattern TAXNUM_REGEX = Pattern.compile("^[0-9]{10,11}$");
    public final static Pattern BILLING_SERIALNUM_REGEX = Pattern.compile("^[0-9A-ZöçşığüÖÇŞİĞÜ \\.\\-\\_\\:]{5,50}$");
    public final static Pattern LANGUAGE_REGEX = Pattern.compile("^[A-Z]{2,3}$");
    public final static Pattern GENDER_REGEX = Pattern.compile("^[EKMF]$");
    public final static Pattern CURRENCY_REGEX = Pattern.compile("^[A-Z]{2,3}$");
    public final static Pattern ECI_REGEX = Pattern.compile("^[0][0-9]");
    public final static Pattern CAVV_CODE_REGEX = Pattern.compile("^[0-9ABCD]$"); // it can be blank as well
    public final static Pattern MD_STATUS_REGEX = Pattern.compile("^[0-9]$");
    public final static Pattern AVS_REGEX = Pattern.compile("^[A-Z]$");
    public final static Pattern CVV_REGEX = Pattern.compile("^[MNPSU]$");


    /**
     * Iban number scheme regex without any modular checksum calculations. Compatible only when issuing a client side parameter or content schema regulation.
     * This regex does not replace validation calculations.
     */
    public final static Pattern IBAN_SCHEME_REGEX = Pattern.compile("^([A-Z]{2})([0-9]{24})$");
    /**
     * Whitelist filter regex for European languages.
     */
    public final static Pattern DESCRIPTION_REGEX = getDescriptionRegex();

    private final static Pattern getDescriptionRegex() {
        String regex = "^[" + "\\d" // Any Number
                + "\\w" // All ASCII basic english alphabet C-chars
                + "öçşığüÖÇŞİĞÜ" // All Special Turkish chars
                + "åæêèäéøóòôÄÅÉÂÈÊÆÓÒÔ" // All Nordic Special chars
                + "ąćęłńśźżĄĆĘŁŃŚŹŻ"// Rest of All Polish Special chars
                + "ßẞ" // Rest of German Special chars
                + "Ââ" // Rest of Turkish old chars
                + "\\t \\n \\r \\s" // tabs, new line, space
                + "\\. \\… \\, \\: \\; \\! \\?" // dot, three dots, comma, colon, semicolon, exclamation mark
                + "\\# \\&" // Number sign, Ampersand
                + "\\[ \\] \\{ \\}" // Brackets, Curly brackets
                + "\\( \\)" // Double quotes, Parentheses,
                + "\\_" // Underscore
                + "\\– \\‒ \\—  \\―" // Dash (short, mid, wide1, wide2)
                + "\\\\ \\/" // Backslash, Slash
                + "\\= \\+ \\- \\* \\÷ \\%" // Equals, Plus, Minus, Asterisk, Divide, Percent
                + "\\\" \\' \\` \\‘ \\’ \\“ \\”"// Any comma, cut, double quote, single quote
                + "\\‹ \\› \\« \\»" // single and double angle (guillemet) quotation marks
                + "\\< \\>" // little than, bigger than
                + "\\~ \\@" // tilde, at
                + "\\$ \\€ \\£ \\₺ \\₤ \\元 \\¥" // common currencies (dollar, euro, pound, turkish lira, old lira, yuan(renminbi), yen
                + "]{1,}$";// at least one or more
        return Pattern.compile(regex);
    }

    public static boolean validateRegex(Pattern pattern, String value) {
        value = value.trim();
        return pattern.matcher(value).matches();
    }

    public static boolean validateRequiredString(String value, int minLength, int maxLength) {
        if (value == null || value.trim().length() <= 0) {
            return false;
        }
        value = value.trim();
        if (value.length() <= 0) {
            return false;
        }

        if ((minLength > -1) && (value.length() < minLength)) {
            return false;
        } else if ((maxLength > -1) && (value.length() > maxLength)) {
            return false;
        }
        return true;
    }

    public static String fieldErrorMsg(ResponseMessage responseMessage, String field) {
        switch (responseMessage) {
            case FIELD_FORMAT_ERROR:
                return field + "' alanının formatı hatalı.";
            case FIELD_NOT_NULL_REFERENCES:
                return "Not Null Reference Error in '" + field + "' field";
            default:
                return "Unknown Error in '" + field + "' field";
        }
    }
}