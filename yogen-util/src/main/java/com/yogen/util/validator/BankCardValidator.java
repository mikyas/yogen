package com.yogen.util.validator;

import java.util.Calendar;
import com.yogen.util.StringUtil;

public class BankCardValidator {

    private final static String AMERICAN_EXPRESS_CVC_PATTERN = "^[0-9]{3,4}$";
    private final static String CVC_PATTERN = "^[0-9]{3}$";

    public static boolean isCardNumberValid(String cardNumber, boolean allowForeignCard) throws Exception {
        boolean isValid = false;

        cardNumber = StringUtil.trimAndSetNullIfBlank(cardNumber);

        if (cardNumber != null) {
            try {
                if (!allowForeignCard) {
                    if (cardNumber.length() < 15 || cardNumber.length() > 16) {
                        throw new Exception("Credit card number length must be 15 or 16. ccNumber length: " + cardNumber.length());
                    }
                } else {
                    if (cardNumber.length() < 12 || cardNumber.length() > 19) {
                        throw new Exception("Credit card number length must be 12 or 19. ccNumber length: " + cardNumber.length());
                    }
                }


                if (Long.parseLong(cardNumber) < 0) {
                    throw new Exception("Credit card number must be positive number. ccNumber: " + cardNumber);
                }

                cardNumber = new StringBuffer(cardNumber).reverse().toString();

                int total = 0;
                for (int i = 0; i < cardNumber.length(); i++) {
                    int num = Integer.parseInt(String.valueOf(cardNumber.charAt(i)));
                    if ((i + 1) % 2 == 0) {
                        num = num * 2;
                        num = (num / 10) + (num % 10);
                    }
                    total += num;
                }
                if (total % 10 == 0) {
                    isValid = true;
                }
            } catch (NumberFormatException e) {
                throw new Exception("Credit card number must be numerical. ccNumber: " + cardNumber);
            }
        } else {
            throw new Exception("Credit card number should not be null.");
        }

        return isValid;
    }

    public static boolean isCardNumberValid(String ccNumber) throws Exception {
        return isCardNumberValid(ccNumber, false);
    }

    public static String getBinNumber(String ccNumber) {
        String binNumber = null;

        if (ccNumber != null && ccNumber.length() >= 6) {
            binNumber = ccNumber.substring(0, 6);
        }

        return binNumber;
    }

    public static boolean isCardExpireDateValid(String expireMonth, String expireYear) {
        Integer expireYearInt;
        int year;
        try {
            expireYearInt = Integer.valueOf(expireYear);
            year = Integer.valueOf(String.valueOf(Calendar.getInstance().get(Calendar.YEAR)).substring(2));
            if (expireYearInt < year) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }

        try {
            Integer expireMonthInt = Integer.valueOf(expireMonth);
            if (expireYearInt == year && expireMonthInt < (Calendar.getInstance().get(Calendar.MONTH) + 1)) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static boolean isValidCvc(String cvc) {
        return cvc.matches(CVC_PATTERN) || cvc.matches(AMERICAN_EXPRESS_CVC_PATTERN);
    }
}