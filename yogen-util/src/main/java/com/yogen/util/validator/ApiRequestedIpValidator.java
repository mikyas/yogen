package com.yogen.util.validator;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import org.apache.commons.lang.StringUtils;

public class ApiRequestedIpValidator {

    private static final String BUNDLE_NAME = "security";
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME, new Locale("tr", "TR"));

    public static boolean validate(String path, String ip) {
        try {
            String resourcePath = "api.service.resource." + StringUtils.join(path.split("/"), "_") + ".ip";
            String resourceValue = RESOURCE_BUNDLE.getString(resourcePath);

            if (resourceValue.contains(",")) {
                String[] ips = resourceValue.split(",");
                for (String s : ips) {
                    boolean result = validateIpRange(s, ip);
                    if (result) {
                        return true;
                    }
                }
                return false;
            } else {
                return validateIpRange(resourceValue, ip);
            }
        } catch (MissingResourceException ignored) {
            return false;
        }
    }

    public static boolean isResourceIpSecured(String path) {
        String resourcePath = "api.service.resource." + StringUtils.join(path.split("/"), "_") + ".ip";
        try {
            RESOURCE_BUNDLE.getString(resourcePath);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private static boolean validateIpRange(String resourceValue, String ip) {
        if (resourceValue.contains("-")) {
            String[] ipAndRangeValue = resourceValue.split("-");
            String restrictedIp = ipAndRangeValue[0];
            String[] splittedRestrictedIp = restrictedIp.split("\\.");
            int restrictedMaxRangeIpValue = Integer.valueOf(ipAndRangeValue[1]);
            int restrictedMinRangeIpValue = Integer.valueOf(splittedRestrictedIp[splittedRestrictedIp.length - 1]);
            restrictedIp = restrictedIp.substring(0, restrictedIp.length() - splittedRestrictedIp[3].length());
            for (int i = restrictedMinRangeIpValue; i <= restrictedMaxRangeIpValue; i++) {
                if ((restrictedIp + i).equals(ip)) {
                    return true;
                }
            }
            return false;
        } else {
            return resourceValue.equals(ip);
        }
    }
}