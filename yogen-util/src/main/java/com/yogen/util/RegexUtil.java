package com.yogen.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class RegexUtil {

    public final static Character SIGN_CIRCUMFLEX = '^';
    public final static Character SIGN_PLUS = '+';
    public final static Character SIGN_BLOCK_BRACETS_LEFT = '[';
    public final static Character SIGN_BLOCK_BRACETS_RIGHT = ']';
    public final static Character SIGN_CURLY_BRACETS_RIGHT = '{';
    public final static Character SIGN_CURLY_BRACETS_LEFT = '}';
    public final static Character SIGN_VERTICAl_BAR = '|';
    public final static Character SIGN_QUESTION_MARK = '?';
    public final static Character SIGN_COLON = ':';
    public final static Character SIGN_ASTERIKS = '*';
    public final static Character SIGN_HYPHEN_MINUS = '-';

    public final static String UNICODE_CIRCUMFLEX = "\\u005E";
    public final static String UNICODE_PLUS = "\\u002B";
    public final static String UNICODE_BLOCK_BRACETS_LEFT = "\\u005B";
    public final static String UNICODE_BLOCK_BRACETS_RIGHT = "\\u005D";
    public final static String UNICODE_CURLY_BRACETS_RIGHT = "\\u007B";
    public final static String UNICODE_CURLY_BRACETS_LEFT = "\\u007D";
    public final static String UNICODE_VERTICAl_BAR = "\\u007C";
    public final static String UNICODE_QUESTION_MARK = "\\u003F";
    public final static String UNICODE_COLON = "\\u003A";
    public final static String UNICODE_ASTERIKS = "\\u002A";
    public final static String UNICODE_HYPHEN_MINUS = "\\u002D";

    private static Map<Character, String> patternArgumentMap = new HashMap<>();

    static {
        patternArgumentMap.put(SIGN_CIRCUMFLEX, UNICODE_CIRCUMFLEX);
        patternArgumentMap.put(SIGN_PLUS, UNICODE_PLUS);
        patternArgumentMap.put(SIGN_BLOCK_BRACETS_RIGHT, UNICODE_BLOCK_BRACETS_RIGHT);
        patternArgumentMap.put(SIGN_BLOCK_BRACETS_LEFT, UNICODE_BLOCK_BRACETS_LEFT);
        patternArgumentMap.put(SIGN_CURLY_BRACETS_RIGHT, UNICODE_CURLY_BRACETS_RIGHT);
        patternArgumentMap.put(SIGN_CURLY_BRACETS_LEFT, UNICODE_CURLY_BRACETS_LEFT);
        patternArgumentMap.put(SIGN_VERTICAl_BAR, UNICODE_VERTICAl_BAR);
        patternArgumentMap.put(SIGN_QUESTION_MARK, UNICODE_QUESTION_MARK);
        patternArgumentMap.put(SIGN_COLON, UNICODE_COLON);
        patternArgumentMap.put(SIGN_ASTERIKS, UNICODE_ASTERIKS);
        patternArgumentMap.put(SIGN_HYPHEN_MINUS, UNICODE_HYPHEN_MINUS);
    }

    private static Set<Character> patternArguments = patternArgumentMap.keySet();

    public static String normalizeRegexPatternChars(String value) {
        if (!(value != null && !value.trim().isEmpty())) {
            return value;
        }
        for (Entry<Character, String> patternEntry : patternArgumentMap.entrySet()) {
            value = value.replace(new String(patternEntry.getValue().toCharArray()), patternEntry.getKey().toString());
        }
        return value;
    }

    public static String escapeRegexPatternChars(String value) {
        if (!(value != null && !value.trim().isEmpty())) {
            return value;
        }
        ArrayList<Character> charsToProcess = new ArrayList<Character>();
        for (Character arg : patternArguments) {
            if (value.contains(arg.toString())) {
                charsToProcess.add(arg);
            }
        }
        if (charsToProcess.size() > 0) {
            List<String> valueParts = new ArrayList<String>();
            valueParts.addAll(partitionValues(value, charsToProcess));
            String result = "";
            for (String part : valueParts) {
                result += part;
            }
            return result;
        } else {
            return value;
        }
    }

    private static List<String> partitionValues(String valuesToProcess, ArrayList<Character> charsToProcess) {
        List<String> masterList = new ArrayList<String>();
        partitionValues(valuesToProcess, charsToProcess, masterList);
        return masterList;
    }

    private static void partitionValues(String valueToProcess, ArrayList<Character> charsToProcess, List<String> masterList) {
        if (charsToProcess.size() == 1) {
            masterList.addAll(partitionValue(valueToProcess, charsToProcess.get(0)));
        } else if (charsToProcess.size() >= 1) {
            Character selected = charsToProcess.get(0);
            List<String> valueParts = partitionValue(valueToProcess, selected);

            ArrayList<Character> reduced = new ArrayList<>();
            reduced.addAll(charsToProcess);
            reduced.remove(selected);

            for (String part : valueParts) {
                partitionValues(part, reduced, masterList);
            }
        }
    }

    private static List<String> partitionValue(String value, Character ch) {
        String replacement = patternArgumentMap.get(ch);
        List<String> parts = new ArrayList<String>();
        int beginIndex = 0;
        int endIndex = value.indexOf(ch.toString());
        while (endIndex != -1) {
            String part = value.substring(beginIndex, endIndex);
            parts.add(part);
            parts.add(replacement);
            beginIndex = endIndex + 1;
            if (beginIndex < value.length()) {
                endIndex = value.substring(beginIndex).indexOf(ch.toString());
                if (endIndex == -1) {
                    break;
                }
                endIndex = beginIndex + endIndex;
            } else {
                break;
            }
        }
        if (beginIndex < value.length()) {
            parts.add(value.substring(beginIndex, value.length()));
        }
        return parts;
    }

}
