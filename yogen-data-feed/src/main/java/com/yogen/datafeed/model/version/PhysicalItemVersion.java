package com.yogen.datafeed.model.version;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "1")
public class PhysicalItemVersion extends AbstractItemVersion implements Serializable {


    @Column(name = "COLOR", nullable = true)
    private Integer color;

    @Column(name = "VOLUME", nullable = true)
    private Integer volume;

    @Column(name = "WEIGHT", nullable = true)
    private Integer weight;

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }
}
