package com.yogen.datafeed.model.dto.snapshot;

import java.util.Date;
import java.util.List;

public class OrderDTO {

    private String name;
    private String orderUID;
    private Date createdAt;
    private Date closedAt;
    private Date updatedAt;
    private List<ItemDTO> items;
    private ShipmentDTO shipmentDTO;
    private BillingDTO billingDTO;
    private PurchaserDTO purchaserDTO;
    private PaymentDetailDTO paymentDetailDTO;
    private DeviceDTO deviceDTO;
    private ContactDTO contactDTO;
    private ChargebackDetailDTO chargebackDetailDTO;
    private RefundDetailDTO refundDetailDTO;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrderUID() {
        return orderUID;
    }

    public void setOrderUID(String orderUID) {
        this.orderUID = orderUID;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getClosedAt() {
        return closedAt;
    }

    public void setClosedAt(Date closedAt) {
        this.closedAt = closedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<ItemDTO> getItems() {
        return items;
    }

    public void setItems(List<ItemDTO> items) {
        this.items = items;
    }

    public ShipmentDTO getShipmentDTO() {
        return shipmentDTO;
    }

    public void setShipmentDTO(ShipmentDTO shipmentDTO) {
        this.shipmentDTO = shipmentDTO;
    }

    public BillingDTO getBillingDTO() {
        return billingDTO;
    }

    public void setBillingDTO(BillingDTO billingDTO) {
        this.billingDTO = billingDTO;
    }

    public PurchaserDTO getPurchaserDTO() {
        return purchaserDTO;
    }

    public void setPurchaserDTO(PurchaserDTO purchaserDTO) {
        this.purchaserDTO = purchaserDTO;
    }

    public PaymentDetailDTO getPaymentDetailDTO() {
        return paymentDetailDTO;
    }

    public void setPaymentDetailDTO(PaymentDetailDTO paymentDetailDTO) {
        this.paymentDetailDTO = paymentDetailDTO;
    }

    public DeviceDTO getDeviceDTO() {
        return deviceDTO;
    }

    public void setDeviceDTO(DeviceDTO deviceDTO) {
        this.deviceDTO = deviceDTO;
    }

    public ContactDTO getContactDTO() {
        return contactDTO;
    }

    public void setContactDTO(ContactDTO contactDTO) {
        this.contactDTO = contactDTO;
    }

    public ChargebackDetailDTO getChargebackDetailDTO() {
        return chargebackDetailDTO;
    }

    public void setChargebackDetailDTO(ChargebackDetailDTO chargebackDetailDTO) {
        this.chargebackDetailDTO = chargebackDetailDTO;
    }

    public RefundDetailDTO getRefundDetailDTO() {
        return refundDetailDTO;
    }

    public void setRefundDetailDTO(RefundDetailDTO refundDetailDTO) {
        this.refundDetailDTO = refundDetailDTO;
    }

}
