package com.yogen.datafeed.model.snapshot;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.yogen.datafeed.model.AbstractBaseDataFeedEntity;

@Entity
@Table(name = "purchaser", catalog = "yogen_datafeed", schema = "dbo")
public class Purchaser extends AbstractBaseDataFeedEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_ID", referencedColumnName = "ID", nullable = true)
    private Order order;

    @Column(name = "PURCHASER_UID", nullable = false)
    private String purchaserUID;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "MAIL")
    private String mail;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "VERIFIED_MAIL")
    private String verifiedMail;

    @Column(name = "MAIL_VERIFIED_TIME")
    private Date mailVerifiedTime;

    @Column(name = "VERIFIED_PHONE")
    private String verifiedPhone;

    @Column(name = "PHONE_VERIFIED_TIME")
    private Date phoneVerifiedTime;

    @Column(name = "FIRST_PURCHASE_AT")
    private Date firstPurchaseAt;

    @Column(name = "ORDERS_COUNT")
    private Integer ordersCount;

    @Column(name = "ACCOUNT_TYPE")
    private String accountType;

    @Column(name = "DATE_OF_BIRTH")
    private Date dateOfBirth;

    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "LANGUAGE")
    private String language;

    @Column(name = "GENDER")
    private String gender;

    @OneToOne(mappedBy = "purchaser", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private SocialDetail socialDetail;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getPurchaserUID() {
        return purchaserUID;
    }

    public void setPurchaserUID(String purchaserUID) {
        this.purchaserUID = purchaserUID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getVerifiedMail() {
        return verifiedMail;
    }

    public void setVerifiedMail(String verifiedMail) {
        this.verifiedMail = verifiedMail;
    }

    public Date getMailVerifiedTime() {
        return mailVerifiedTime;
    }

    public void setMailVerifiedTime(Date mailVerifiedTime) {
        this.mailVerifiedTime = mailVerifiedTime;
    }

    public String getVerifiedPhone() {
        return verifiedPhone;
    }

    public void setVerifiedPhone(String verifiedPhone) {
        this.verifiedPhone = verifiedPhone;
    }

    public Date getPhoneVerifiedTime() {
        return phoneVerifiedTime;
    }

    public void setPhoneVerifiedTime(Date phoneVerifiedTime) {
        this.phoneVerifiedTime = phoneVerifiedTime;
    }

    public Date getFirstPurchaseAt() {
        return firstPurchaseAt;
    }

    public void setFirstPurchaseAt(Date firstPurchaseAt) {
        this.firstPurchaseAt = firstPurchaseAt;
    }

    public Integer getOrdersCount() {
        return ordersCount;
    }

    public void setOrdersCount(Integer ordersCount) {
        this.ordersCount = ordersCount;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public SocialDetail getSocialDetail() {
        return socialDetail;
    }

    public void setSocialDetail(SocialDetail socialDetail) {
        this.socialDetail = socialDetail;
    }
}
