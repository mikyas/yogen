package com.yogen.datafeed.model.dto.version;

public class TransactionVersionDTO {

    private OrderVersionDTO orderDTO;

    public OrderVersionDTO getOrderDTO() {
        return orderDTO;
    }

    public void setOrderDTO(OrderVersionDTO orderDTO) {
        this.orderDTO = orderDTO;
    }

}
