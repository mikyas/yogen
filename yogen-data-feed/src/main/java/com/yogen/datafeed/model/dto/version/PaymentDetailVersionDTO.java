package com.yogen.datafeed.model.dto.version;

public class PaymentDetailVersionDTO {
    private String paymentDetailUID;
    private Integer paymentType;
    private Integer installment;
    private Double totalPrice;
    private Double totalPaidPrice;
    private String currency;
    private String creditCardBin;
    private String creditCardLastFourDigit;
    private String creditCardCompany;
    private Boolean isThreeD;
    private Boolean threeDInquiryResult;
    private Boolean paymentResult;
    private String eciCode;
    private String cavvCode;
    private String mdStatus;
    private String avsResultCode;
    private String cvvResultCode;

    public String getPaymentDetailUID() {
        return paymentDetailUID;
    }

    public void setPaymentDetailUID(String paymentDetailUID) {
        this.paymentDetailUID = paymentDetailUID;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public Integer getInstallment() {
        return installment;
    }

    public void setInstallment(Integer installment) {
        this.installment = installment;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Double getTotalPaidPrice() {
        return totalPaidPrice;
    }

    public void setTotalPaidPrice(Double totalPaidPrice) {
        this.totalPaidPrice = totalPaidPrice;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCreditCardBin() {
        return creditCardBin;
    }

    public void setCreditCardBin(String creditCardBin) {
        this.creditCardBin = creditCardBin;
    }

    public String getCreditCardLastFourDigit() {
        return creditCardLastFourDigit;
    }

    public void setCreditCardLastFourDigit(String creditCardLastFourDigit) {
        this.creditCardLastFourDigit = creditCardLastFourDigit;
    }

    public String getCreditCardCompany() {
        return creditCardCompany;
    }

    public void setCreditCardCompany(String creditCardCompany) {
        this.creditCardCompany = creditCardCompany;
    }

    public Boolean getThreeD() {
        return isThreeD;
    }

    public void setThreeD(Boolean threeD) {
        isThreeD = threeD;
    }

    public Boolean getThreeDInquiryResult() {
        return threeDInquiryResult;
    }

    public void setThreeDInquiryResult(Boolean threeDInquiryResult) {
        this.threeDInquiryResult = threeDInquiryResult;
    }

    public Boolean getPaymentResult() {
        return paymentResult;
    }

    public void setPaymentResult(Boolean paymentResult) {
        this.paymentResult = paymentResult;
    }

    public String getEciCode() {
        return eciCode;
    }

    public void setEciCode(String eciCode) {
        this.eciCode = eciCode;
    }

    public String getCavvCode() {
        return cavvCode;
    }

    public void setCavvCode(String cavvCode) {
        this.cavvCode = cavvCode;
    }

    public String getMdStatus() {
        return mdStatus;
    }

    public void setMdStatus(String mdStatus) {
        this.mdStatus = mdStatus;
    }

    public String getAvsResultCode() {
        return avsResultCode;
    }

    public void setAvsResultCode(String avsResultCode) {
        this.avsResultCode = avsResultCode;
    }

    public String getCvvResultCode() {
        return cvvResultCode;
    }

    public void setCvvResultCode(String cvvResultCode) {
        this.cvvResultCode = cvvResultCode;
    }
}
