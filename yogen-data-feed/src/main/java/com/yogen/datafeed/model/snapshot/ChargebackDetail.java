package com.yogen.datafeed.model.snapshot;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.yogen.datafeed.model.AbstractBaseDataFeedEntity;

@Entity
@Table(name = "chargeback_detail",catalog = "yogen_datafeed", schema = "dbo")
public class ChargebackDetail extends AbstractBaseDataFeedEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_ID", referencedColumnName = "ID", nullable = true)
    private Order order;

    @Column(name = "CHARGEBACK_UID", nullable = false)
    private String chargebackUID;

    @Column(name = "IS_PARTIAL", nullable = false)
    private Boolean isPartial;

    @Column(name = "CHARGEBACK_AT", nullable = false)
    private Date chargebackAt;

    @Column(name = "AMOUNT",nullable = false)
    private Double amount;

    @Column(name = "REASON_CODE", nullable = false)
    private Integer reasonCode;

    @Column(name = "REASON_DESCRIPTION")
    private String reasonDescription;

    @Column(name = "STATE")
    private Integer state;

    @Column(name = "GATEWAY")
    private String gateway;

    @Column(name = "ARN")
    private String arn;

    @Column(name = "CREDIT_CARD_COMPANY")
    private String creditCardCompany;

    @Column(name = "RESPOND_AT")
    private Date respondAt;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getChargebackUID() {
        return chargebackUID;
    }

    public void setChargebackUID(String chargebackUID) {
        this.chargebackUID = chargebackUID;
    }

    public Boolean getPartial() {
        return isPartial;
    }

    public void setPartial(Boolean partial) {
        isPartial = partial;
    }

    public Date getChargebackAt() {
        return chargebackAt;
    }

    public void setChargebackAt(Date chargebackAt) {
        this.chargebackAt = chargebackAt;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(Integer reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public String getArn() {
        return arn;
    }

    public void setArn(String arn) {
        this.arn = arn;
    }

    public String getCreditCardCompany() {
        return creditCardCompany;
    }

    public void setCreditCardCompany(String creditCardCompany) {
        this.creditCardCompany = creditCardCompany;
    }

    public Date getRespondAt() {
        return respondAt;
    }

    public void setRespondAt(Date respondAt) {
        this.respondAt = respondAt;
    }
}
