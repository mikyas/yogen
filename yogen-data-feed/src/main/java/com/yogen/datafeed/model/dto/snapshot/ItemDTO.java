package com.yogen.datafeed.model.dto.snapshot;

public class ItemDTO {

    private String itemUID;
    private Integer type;
    private String upc;
    private String sku;
    private Double price;
    private Double paidPrice;
    private Integer quantity;
    private String brand;
    private String manufacturer;
    private String title;
    private SellerDTO sellerDTO;

    public ItemDTO(String itemUID, Integer type, String upc, String sku, Double price, Double paidPrice, Integer quantity, String brand, String manufacturer, String title, SellerDTO sellerDTO) {
        this.itemUID = itemUID;
        this.type = type;
        this.upc = upc;
        this.sku = sku;
        this.price = price;
        this.paidPrice = paidPrice;
        this.quantity = quantity;
        this.brand = brand;
        this.manufacturer = manufacturer;
        this.title = title;
        this.sellerDTO = sellerDTO;
    }

    public String getItemUID() {
        return itemUID;
    }

    public void setItemUID(String itemUID) {
        this.itemUID = itemUID;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPaidPrice() {
        return paidPrice;
    }

    public void setPaidPrice(Double paidPrice) {
        this.paidPrice = paidPrice;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SellerDTO getSellerDTO() {
        return sellerDTO;
    }

    public void setSellerDTO(SellerDTO sellerDTO) {
        this.sellerDTO = sellerDTO;
    }
}
