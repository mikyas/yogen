package com.yogen.datafeed.model.version;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.yogen.datafeed.model.AbstractBaseDataFeedEntity;

@Entity
@Table(name = "payment_detail_version",catalog = "yogen_datafeed", schema = "dbo")
public class PaymentDetailVersion extends AbstractBaseDataFeedEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_VERSION_ID", referencedColumnName = "ID", nullable = true)
    private OrderVersion orderVersion;

    @Column(name = "PAYMENT_DETAIL_UID", nullable = false)
    private String paymentDetailUId;

    @Column(name = "PAYMENT_TYPE", nullable = false)
    private Integer paymentType;

    @Column(name = "INSTALLMENT", nullable = false)
    private Integer installment;

    @Column(name = "TOTAL_PRICE", nullable = false)
    private Double totalPrice;

    @Column(name = "TOTAL_PAID_PRICE", nullable = false)
    private Double totalPaidPrice;

    @Column(name = "CURRENCY", nullable = false)
    private String currency;

    @Column(name = "CREDIT_CARD_BIN", nullable = false)
    private String creditCardBin;

    @Column(name = "CREDIT_CARD_LAST_FOUR_DIGIT", nullable = false)
    private String creditCardLastFourDigit;

    @Column(name = "CREDIT_CARD_COMPANY", nullable = false)
    private String creditCardCompany;

    @Column(name = "IS_THREE_D", nullable = false)
    private Boolean isThreeD;

    @Column(name = "THREE_D_INQUIRY_RESULT",nullable = false)
    private Boolean threeDInquiryResult;

    @Column(name = "PAYMENT_RESULT",nullable = false)
    private Boolean paymentResult;

    @Column(name = "ECI_CODE")
    private String eciCode;

    @Column(name = "CAVV_CODE")
    private String cavvCode;

    @Column(name = "MD_STATUS")
    private String mdStatus;

    @Column(name = "AVS_RESULT_CODE")
    private String avsResultCode;

    @Column(name = "CVV_RESULT_CODE")
    private String cvvResultCode;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public OrderVersion getOrderVersion() {
        return orderVersion;
    }

    public void setOrderVersion(OrderVersion orderVersion) {
        this.orderVersion = orderVersion;
    }

    public String getPaymentDetailUId() {
        return paymentDetailUId;
    }

    public void setPaymentDetailUId(String paymentDetailUId) {
        this.paymentDetailUId = paymentDetailUId;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public Integer getInstallment() {
        return installment;
    }

    public void setInstallment(Integer installment) {
        this.installment = installment;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Double getTotalPaidPrice() {
        return totalPaidPrice;
    }

    public void setTotalPaidPrice(Double totalPaidPrice) {
        this.totalPaidPrice = totalPaidPrice;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCreditCardBin() {
        return creditCardBin;
    }

    public void setCreditCardBin(String creditCardBin) {
        this.creditCardBin = creditCardBin;
    }

    public String getCreditCardLastFourDigit() {
        return creditCardLastFourDigit;
    }

    public void setCreditCardLastFourDigit(String creditCardLastFourDigit) {
        this.creditCardLastFourDigit = creditCardLastFourDigit;
    }

    public String getCreditCardCompany() {
        return creditCardCompany;
    }

    public void setCreditCardCompany(String creditCardCompany) {
        this.creditCardCompany = creditCardCompany;
    }

    public Boolean getThreeD() {
        return isThreeD;
    }

    public void setThreeD(Boolean threeD) {
        isThreeD = threeD;
    }

    public Boolean getThreeDInquiryResult() {
        return threeDInquiryResult;
    }

    public void setThreeDInquiryResult(Boolean threeDInquiryResult) {
        this.threeDInquiryResult = threeDInquiryResult;
    }

    public Boolean getPaymentResult() {
        return paymentResult;
    }

    public void setPaymentResult(Boolean paymentResult) {
        this.paymentResult = paymentResult;
    }

    public String getEciCode() {
        return eciCode;
    }

    public void setEciCode(String eciCode) {
        this.eciCode = eciCode;
    }

    public String getCavvCode() {
        return cavvCode;
    }

    public void setCavvCode(String cavvCode) {
        this.cavvCode = cavvCode;
    }

    public String getMdStatus() {
        return mdStatus;
    }

    public void setMdStatus(String mdStatus) {
        this.mdStatus = mdStatus;
    }

    public String getAvsResultCode() {
        return avsResultCode;
    }

    public void setAvsResultCode(String avsResultCode) {
        this.avsResultCode = avsResultCode;
    }

    public String getCvvResultCode() {
        return cvvResultCode;
    }

    public void setCvvResultCode(String cvvResultCode) {
        this.cvvResultCode = cvvResultCode;
    }
}
