package com.yogen.datafeed.model.dto.snapshot;

public class ContactDTO {

    private String contactUID;
    private String contactType;
    private String email;
    private String phone;
    private String accountUrl;

    public String getContactUID() {
        return contactUID;
    }

    public void setContactUID(String contactUID) {
        this.contactUID = contactUID;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAccountUrl() {
        return accountUrl;
    }

    public void setAccountUrl(String accountUrl) {
        this.accountUrl = accountUrl;
    }
}
