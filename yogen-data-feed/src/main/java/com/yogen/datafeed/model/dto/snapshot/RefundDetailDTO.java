package com.yogen.datafeed.model.dto.snapshot;

import java.util.Date;

public class RefundDetailDTO {

    private String refundUID;
    private Boolean isPartial;
    private Date refundAt;
    private Double amount;
    private Integer reasonCode;
    private String reasonDescription;

    public String getRefundUID() {
        return refundUID;
    }

    public void setRefundUID(String refundUID) {
        this.refundUID = refundUID;
    }

    public Boolean getPartial() {
        return isPartial;
    }

    public void setPartial(Boolean partial) {
        isPartial = partial;
    }

    public Date getRefundAt() {
        return refundAt;
    }

    public void setRefundAt(Date refundAt) {
        this.refundAt = refundAt;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(Integer reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }
}
