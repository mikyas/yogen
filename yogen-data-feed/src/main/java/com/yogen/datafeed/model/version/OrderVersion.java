package com.yogen.datafeed.model.version;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.yogen.datafeed.model.AbstractBaseDataFeedEntity;

@Entity
@Table(name = "order_version", catalog = "yogen_datafeed", schema = "dbo")
public class OrderVersion extends AbstractBaseDataFeedEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRANSACTION_VERSION_ID", referencedColumnName = "ID", nullable = false)
    private TransactionVersion transactionVersion;

    @Column(name = "ORDER_UID", nullable = false)
    private String orderUid;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CREATED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "CLOSED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date closedAt;

    @Column(name = "UPDATED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @OneToMany(mappedBy = "orderVersion", fetch = FetchType.LAZY)
    private List<AbstractItemVersion> versionItems = new ArrayList<>();

    @OneToOne(mappedBy = "orderVersion", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private BillingVersion billingVersion;

    @OneToOne(mappedBy = "orderVersion", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private ChargebackDetailVersion chargebackDetailVersion;

    @OneToOne(mappedBy = "orderVersion", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private ContactVersion contactVersion;

    @OneToOne(mappedBy = "orderVersion", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private DeviceVersion deviceVersion;

    @OneToOne(mappedBy = "orderVersion", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private PaymentDetailVersion paymentDetailVersion;

    @OneToOne(mappedBy = "orderVersion", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private PurchaserVersion purchaserVersion;

    @OneToOne(mappedBy = "orderVersion", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private RefundDetailVersion refundDetailVersion;

    @OneToOne(mappedBy = "orderVersion", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private ShipmentVersion shipmentVersion;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public TransactionVersion getTransactionVersion() {
        return transactionVersion;
    }

    public void setTransactionVersion(TransactionVersion transactionVersion) {
        this.transactionVersion = transactionVersion;
    }

    public String getOrderUid() {
        return orderUid;
    }

    public void setOrderUid(String orderUid) {
        this.orderUid = orderUid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getClosedAt() {
        return closedAt;
    }

    public void setClosedAt(Date closedAt) {
        this.closedAt = closedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<AbstractItemVersion> getVersionItems() {
        return versionItems;
    }

    public void setVersionItems(List<AbstractItemVersion> versionItems) {
        this.versionItems = versionItems;
    }

    public BillingVersion getBillingVersion() {
        return billingVersion;
    }

    public void setBillingVersion(BillingVersion billingVersion) {
        this.billingVersion = billingVersion;
    }

    public ChargebackDetailVersion getChargebackDetailVersion() {
        return chargebackDetailVersion;
    }

    public void setChargebackDetailVersion(ChargebackDetailVersion chargebackDetailVersion) {
        this.chargebackDetailVersion = chargebackDetailVersion;
    }

    public ContactVersion getContactVersion() {
        return contactVersion;
    }

    public void setContactVersion(ContactVersion contactVersion) {
        this.contactVersion = contactVersion;
    }

    public DeviceVersion getDeviceVersion() {
        return deviceVersion;
    }

    public void setDeviceVersion(DeviceVersion deviceVersion) {
        this.deviceVersion = deviceVersion;
    }

    public PaymentDetailVersion getPaymentDetailVersion() {
        return paymentDetailVersion;
    }

    public void setPaymentDetailVersion(PaymentDetailVersion paymentDetailVersion) {
        this.paymentDetailVersion = paymentDetailVersion;
    }

    public PurchaserVersion getPurchaserVersion() {
        return purchaserVersion;
    }

    public void setPurchaserVersion(PurchaserVersion purchaserVersion) {
        this.purchaserVersion = purchaserVersion;
    }

    public RefundDetailVersion getRefundDetailVersion() {
        return refundDetailVersion;
    }

    public void setRefundDetailVersion(RefundDetailVersion refundDetailVersion) {
        this.refundDetailVersion = refundDetailVersion;
    }

    public ShipmentVersion getShipmentVersion() {
        return shipmentVersion;
    }

    public void setShipmentVersion(ShipmentVersion shipmentVersion) {
        this.shipmentVersion = shipmentVersion;
    }
}
