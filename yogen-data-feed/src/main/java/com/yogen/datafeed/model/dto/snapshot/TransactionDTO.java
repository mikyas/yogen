package com.yogen.datafeed.model.dto.snapshot;

import java.util.Date;

public class TransactionDTO {

    private String transactionUID;
    private Date transactionDate;
    private Long integratorId;
    private Integer integratorRiskScoreLimit;
    private OrderDTO orderDTO;

    public String getTransactionUID() {
        return transactionUID;
    }

    public void setTransactionUID(String transactionUID) {
        this.transactionUID = transactionUID;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Long getIntegratorId() {
        return integratorId;
    }

    public Integer getIntegratorRiskScoreLimit() {
        return integratorRiskScoreLimit;
    }

    public void setIntegratorRiskScoreLimit(Integer integratorRiskScoreLimit) {
        this.integratorRiskScoreLimit = integratorRiskScoreLimit;
    }

    public void setIntegratorId(Long integratorId) {
        this.integratorId = integratorId;
    }

    public OrderDTO getOrderDTO() {
        return orderDTO;
    }

    public void setOrderDTO(OrderDTO orderDTO) {
        this.orderDTO = orderDTO;
    }

}
