package com.yogen.datafeed.model.snapshot;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "4")
public class DigitalItem extends AbstractItem implements Serializable {

    @Column(name = "SENDER_NAME")
    private String senderName;

    @Column(name = "SENDER_EMAIL")
    private String senderEmail;

    @Column(name = "DISPLAY_NAME")
    private String displayName;

    @Column(name = "PHOTO_UPLOADED")
    private Boolean photoUploaded;

    @Column(name = "PHOTO_URL")
    private String photoUrl;

    @Column(name = "GREETING_PHOTO_URL")
    private String greetingPhotoUrl;

    @Column(name = "MESSAGE")
    private String message;

    @Column(name = "GREETING_MESSAGE")
    private String greetingMessage;

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Boolean getPhotoUploaded() {
        return photoUploaded;
    }

    public void setPhotoUploaded(Boolean photoUploaded) {
        this.photoUploaded = photoUploaded;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getGreetingPhotoUrl() {
        return greetingPhotoUrl;
    }

    public void setGreetingPhotoUrl(String greetingPhotoUrl) {
        this.greetingPhotoUrl = greetingPhotoUrl;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getGreetingMessage() {
        return greetingMessage;
    }

    public void setGreetingMessage(String greetingMessage) {
        this.greetingMessage = greetingMessage;
    }
}
