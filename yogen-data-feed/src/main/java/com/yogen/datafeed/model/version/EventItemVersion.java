package com.yogen.datafeed.model.version;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@DiscriminatorValue(value = "3")
public class EventItemVersion extends AbstractItemVersion implements Serializable {

    @Column(name = "EVENT_NAME")
    private String eventName;

    @Column(name = "EVENT_START_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventStartTime;

    @Column(name = "EVENT_END_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventEndTime;

    @Column(name = "EVENT_CATEGORY")
    private String eventCategory;

    @Column(name = "EVENT_VENUE_NAME")
    private String eventVenueName;

    @Column(name = "EVENT_ADDRESS1")
    private String eventAddress1;

    @Column(name = "EVENT_ADDRESS2")
    private String eventAddress2;

    @Column(name = "EVENT_NEIGHBOURHOOD")
    private String eventNeighbourhood;

    @Column(name = "EVENT_CITY")
    private String eventCity;

    @Column(name = "EVENT_REGION")
    private String eventRegion;

    @Column(name = "EVENT_COUNTRY")
    private String eventCountry;

    @Column(name = "EVENT_PO_BOX_NUMBER")
    private String eventPoBoxNumber;

    @Column(name = "EVENT_POSTAL_CODE")
    private String eventPostalCode;

    @Column(name = "EVENT_LATITUDE")
    private String eventLatitude;

    @Column(name = "EVENT_LONGITUDE")
    private String eventLongitude;

    @Column(name = "EVENT_GEOHASH")
    private String eventGeohash;

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Date getEventStartTime() {
        return eventStartTime;
    }

    public void setEventStartTime(Date eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public Date getEventEndTime() {
        return eventEndTime;
    }

    public void setEventEndTime(Date eventEndTime) {
        this.eventEndTime = eventEndTime;
    }

    public String getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(String eventCategory) {
        this.eventCategory = eventCategory;
    }

    public String getEventVenueName() {
        return eventVenueName;
    }

    public void setEventVenueName(String eventVenueName) {
        this.eventVenueName = eventVenueName;
    }

    public String getEventAddress1() {
        return eventAddress1;
    }

    public void setEventAddress1(String eventAddress1) {
        this.eventAddress1 = eventAddress1;
    }

    public String getEventAddress2() {
        return eventAddress2;
    }

    public void setEventAddress2(String eventAddress2) {
        this.eventAddress2 = eventAddress2;
    }

    public String getEventNeighbourhood() {
        return eventNeighbourhood;
    }

    public void setEventNeighbourhood(String eventNeighbourhood) {
        this.eventNeighbourhood = eventNeighbourhood;
    }

    public String getEventCity() {
        return eventCity;
    }

    public void setEventCity(String eventCity) {
        this.eventCity = eventCity;
    }

    public String getEventRegion() {
        return eventRegion;
    }

    public void setEventRegion(String eventRegion) {
        this.eventRegion = eventRegion;
    }

    public String getEventCountry() {
        return eventCountry;
    }

    public void setEventCountry(String eventCountry) {
        this.eventCountry = eventCountry;
    }

    public String getEventPoBoxNumber() {
        return eventPoBoxNumber;
    }

    public void setEventPoBoxNumber(String eventPoBoxNumber) {
        this.eventPoBoxNumber = eventPoBoxNumber;
    }

    public String getEventPostalCode() {
        return eventPostalCode;
    }

    public void setEventPostalCode(String eventPostalCode) {
        this.eventPostalCode = eventPostalCode;
    }

    public String getEventLatitude() {
        return eventLatitude;
    }

    public void setEventLatitude(String eventLatitude) {
        this.eventLatitude = eventLatitude;
    }

    public String getEventLongitude() {
        return eventLongitude;
    }

    public void setEventLongitude(String eventLongitude) {
        this.eventLongitude = eventLongitude;
    }

    public String getEventGeohash() {
        return eventGeohash;
    }

    public void setEventGeohash(String eventGeohash) {
        this.eventGeohash = eventGeohash;
    }
}
