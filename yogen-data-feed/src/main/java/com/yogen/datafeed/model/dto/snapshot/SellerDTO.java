package com.yogen.datafeed.model.dto.snapshot;

public class SellerDTO {
    private String sellerUID;
    private String name;
    private LocationDTO sellerLocation;

    public String getSellerUID() {
        return sellerUID;
    }

    public void setSellerUID(String sellerUID) {
        this.sellerUID = sellerUID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocationDTO getSellerLocation() {
        return sellerLocation;
    }

    public void setSellerLocation(LocationDTO sellerLocation) {
        this.sellerLocation = sellerLocation;
    }
}
