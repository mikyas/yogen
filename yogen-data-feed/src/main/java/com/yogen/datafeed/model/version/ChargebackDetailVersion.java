package com.yogen.datafeed.model.version;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.yogen.datafeed.model.AbstractBaseDataFeedEntity;

@Entity
@Table(name = "chargeback_detail_version",catalog = "yogen_datafeed", schema = "dbo")
public class ChargebackDetailVersion extends AbstractBaseDataFeedEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_VERSION_ID", referencedColumnName = "ID", nullable = true)
    private OrderVersion orderVersion;

    @Column(name = "CHARGEBACK_UID", nullable = false)
    private String chargebackUId;

    @Column(name = "IS_PARTIAL", nullable = false)
    private Boolean isPartial;

    @Column(name = "CHARGEBACK_AT", nullable = false)
    private Date chargebackAt;

    @Column(name = "AMOUNT",nullable = false)
    private Double amount;

    @Column(name = "REASON_CODE", nullable = false)
    private Integer reasonCode;

    @Column(name = "REASON_DESCRIPTION")
    private String reasonDescription;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "GATEWAY")
    private String gateway;

    @Column(name = "ARN")
    private String arn;

    @Column(name = "CREDIT_CARD_COMPANY")
    private String creditCardCompany;

    @Column(name = "RESPOND_BY")
    private Date respondBy;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public OrderVersion getOrderVersion() {
        return orderVersion;
    }

    public void setOrderVersion(OrderVersion orderVersion) {
        this.orderVersion = orderVersion;
    }

    public String getChargebackUId() {
        return chargebackUId;
    }

    public void setChargebackUId(String chargebackUId) {
        this.chargebackUId = chargebackUId;
    }

    public Boolean getPartial() {
        return isPartial;
    }

    public void setPartial(Boolean partial) {
        isPartial = partial;
    }

    public Date getChargebackAt() {
        return chargebackAt;
    }

    public void setChargebackAt(Date chargebackAt) {
        this.chargebackAt = chargebackAt;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(Integer reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public String getArn() {
        return arn;
    }

    public void setArn(String arn) {
        this.arn = arn;
    }

    public String getCreditCardCompany() {
        return creditCardCompany;
    }

    public void setCreditCardCompany(String creditCardCompany) {
        this.creditCardCompany = creditCardCompany;
    }

    public Date getRespondBy() {
        return respondBy;
    }

    public void setRespondBy(Date respondBy) {
        this.respondBy = respondBy;
    }
}
