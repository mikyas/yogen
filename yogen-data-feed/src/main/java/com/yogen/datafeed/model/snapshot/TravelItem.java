package com.yogen.datafeed.model.snapshot;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@DiscriminatorValue(value = "2")
public class TravelItem extends AbstractItem implements Serializable {

    @Column(name = "LEG_ID")
    private Long legId;

    @Column(name = "TRANSPORT_METHOD")
    private Integer transportMethod;

    @Column(name = "TICKET_CLASS")
    private String ticketClass;

    @Column(name = "DEPARTURE_PORT_CODE")
    private String departurePortCode;

    @Column(name = "DEPARTURE_CITY")
    private String departureCity;

    @Column(name = "DEPARTURE_COUNTRY_CODE")
    private String departureCountryCode;

    @Column(name = "ARRIVAL_PORT_CODE")
    private String arrivalPortCode;

    @Column(name = "ARRIVAL_CITY")
    private String arrivalCity;

    @Column(name = "ARRIVAL_COUNTRY_CODE")
    private String arrivalCountryCode;

    @Column(name = "DEPARTURE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date departureDate;

    @Column(name = "ARRIVAL_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date arrivalDate;

    @Column(name = "CARRIER_NAME")
    private String carrierName;

    @Column(name = "CARRIER_CODE")
    private String carrierCode;

    @Column(name = "ROUTE_INDEX")
    private Integer routeIndex;

    @Column(name = "LEG_INDEX")
    private Integer legIndex;

    public Long getLegId() {
        return legId;
    }

    public void setLegId(Long legId) {
        this.legId = legId;
    }

    public Integer getTransportMethod() {
        return transportMethod;
    }

    public void setTransportMethod(Integer transportMethod) {
        this.transportMethod = transportMethod;
    }

    public String getTicketClass() {
        return ticketClass;
    }

    public void setTicketClass(String ticketClass) {
        this.ticketClass = ticketClass;
    }

    public String getDeparturePortCode() {
        return departurePortCode;
    }

    public void setDeparturePortCode(String departurePortCode) {
        this.departurePortCode = departurePortCode;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public String getDepartureCountryCode() {
        return departureCountryCode;
    }

    public void setDepartureCountryCode(String departureCountryCode) {
        this.departureCountryCode = departureCountryCode;
    }

    public String getArrivalPortCode() {
        return arrivalPortCode;
    }

    public void setArrivalPortCode(String arrivalPortCode) {
        this.arrivalPortCode = arrivalPortCode;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public String getArrivalCountryCode() {
        return arrivalCountryCode;
    }

    public void setArrivalCountryCode(String arrivalCountryCode) {
        this.arrivalCountryCode = arrivalCountryCode;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getCarrierCode() {
        return carrierCode;
    }

    public void setCarrierCode(String carrierCode) {
        this.carrierCode = carrierCode;
    }

    public Integer getRouteIndex() {
        return routeIndex;
    }

    public void setRouteIndex(Integer routeIndex) {
        this.routeIndex = routeIndex;
    }

    public Integer getLegIndex() {
        return legIndex;
    }

    public void setLegIndex(Integer legIndex) {
        this.legIndex = legIndex;
    }
}
