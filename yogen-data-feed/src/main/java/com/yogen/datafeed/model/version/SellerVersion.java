package com.yogen.datafeed.model.version;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.yogen.datafeed.model.AbstractBaseDataFeedEntity;

@Entity
@Table(name = "seller_version",catalog = "yogen_datafeed", schema = "dbo")
public class SellerVersion extends AbstractBaseDataFeedEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ITEM_VERSION_ID", referencedColumnName = "ID", nullable = false)
    private AbstractItemVersion abstractItemVersion;

    @Column(name = "SELLER_UID", nullable = false)
    private String sellerUId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "ADDRESS1")
    private String address1;

    @Column(name = "ADDRESS2")
    private String address2;

    @Column(name = "NEIGHBOURHOOD")
    private String neighbourhood;

    @Column(name = "CITY")
    private String city;

    @Column(name = "REGION")
    private String region;

    @Column(name = "COUNTRY")
    private String country;

    @Column(name = "PO_BOX_NUMBER")
    private Integer poBoxNumber;

    @Column(name = "POSTAL_CODE")
    private String postalCode;

    @Column(name = "LATITUDE")
    private String latitude;

    @Column(name = "LONGITUDE")
    private String longitude;

    @Column(name = "GEOHASH")
    private String geohash;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public AbstractItemVersion getAbstractItemVersion() {
        return abstractItemVersion;
    }

    public void setAbstractItemVersion(AbstractItemVersion abstractItemVersion) {
        this.abstractItemVersion = abstractItemVersion;
    }

    public String getSellerUId() {
        return sellerUId;
    }

    public void setSellerUId(String sellerUId) {
        this.sellerUId = sellerUId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getNeighbourhood() {
        return neighbourhood;
    }

    public void setNeighbourhood(String neighbourhood) {
        this.neighbourhood = neighbourhood;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getPoBoxNumber() {
        return poBoxNumber;
    }

    public void setPoBoxNumber(Integer poBoxNumber) {
        this.poBoxNumber = poBoxNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getGeohash() {
        return geohash;
    }

    public void setGeohash(String geohash) {
        this.geohash = geohash;
    }
}
