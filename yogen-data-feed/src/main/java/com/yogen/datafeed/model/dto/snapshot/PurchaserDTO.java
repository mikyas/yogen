package com.yogen.datafeed.model.dto.snapshot;

import java.util.Date;

public class PurchaserDTO {
    private String purchaserUID;
    private String firstName;
    private String lastName;
    private String mail;
    private String verifiedMail;
    private Date mailVerifiedDate;
    private String phone;
    private String verifiedPhone;
    private Date phoneVerifiedDate;
    private Date firstPurchaseAt;
    private Integer orderCount;
    private String accountType;
    private Date dateOfBirth;
    private String userName;
    private String language;
    private String gender;
    private SocialDetailDTO socialDetailDTO;

    public String getPurchaserUID() {
        return purchaserUID;
    }

    public void setPurchaserUID(String purchaserUID) {
        this.purchaserUID = purchaserUID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getVerifiedMail() {
        return verifiedMail;
    }

    public void setVerifiedMail(String verifiedMail) {
        this.verifiedMail = verifiedMail;
    }

    public Date getMailVerifiedDate() {
        return mailVerifiedDate;
    }

    public void setMailVerifiedDate(Date mailVerifiedDate) {
        this.mailVerifiedDate = mailVerifiedDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getVerifiedPhone() {
        return verifiedPhone;
    }

    public void setVerifiedPhone(String verifiedPhone) {
        this.verifiedPhone = verifiedPhone;
    }

    public Date getPhoneVerifiedDate() {
        return phoneVerifiedDate;
    }

    public void setPhoneVerifiedDate(Date phoneVerifiedDate) {
        this.phoneVerifiedDate = phoneVerifiedDate;
    }

    public Date getFirstPurchaseAt() {
        return firstPurchaseAt;
    }

    public void setFirstPurchaseAt(Date firstPurchaseAt) {
        this.firstPurchaseAt = firstPurchaseAt;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public SocialDetailDTO getSocialDetailDTO() {
        return socialDetailDTO;
    }

    public void setSocialDetailDTO(SocialDetailDTO socialDetailDTO) {
        this.socialDetailDTO = socialDetailDTO;
    }
}
