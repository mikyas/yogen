package com.yogen.datafeed.model.snapshot;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "1")
public class PhysicalItem extends AbstractItem implements Serializable {


    @Column(name = "COLOR", nullable = true)
    private String color;

    @Column(name = "VOLUME", nullable = true)
    private Integer volume;

    @Column(name = "WEIGHT", nullable = true)
    private Integer weight;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }
}
