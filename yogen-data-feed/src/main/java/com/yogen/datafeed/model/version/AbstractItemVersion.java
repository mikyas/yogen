package com.yogen.datafeed.model.version;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.yogen.datafeed.model.AbstractBaseDataFeedEntity;

@Entity
@Table(name = "item_version", catalog = "yogen_datafeed", schema = "dbo")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE", discriminatorType = DiscriminatorType.INTEGER)
public class AbstractItemVersion extends AbstractBaseDataFeedEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "TYPE", insertable = false, updatable = false, nullable = false)
    private Integer type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_VERSION_ID", referencedColumnName = "ID", nullable = false)
    private OrderVersion orderVersion;

    @Column(name = "ITEM_UID", nullable = false)
    private String itemUid;

    @Column(name = "UPC")
    private String upc;

    @Column(name = "SKU")
    private String sku;

    @Column(name = "PRICE")
    private Double price;

    @Column(name = "PAID_PRICE")
    private Double paidPrice;

    @Column(name = "QUANTITY")
    private Integer quantity;

    @Column(name = "BRAND")
    private String brand;

    @Column(name = "MANUFACTURER")
    private String manufacturer;

    @Column(name = "TITLE")
    private String title;

    @OneToOne(mappedBy = "abstractItemVersion", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private SellerVersion sellerVersion;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public OrderVersion getOrderVersion() {
        return orderVersion;
    }

    public void setOrderVersion(OrderVersion orderVersion) {
        this.orderVersion = orderVersion;
    }

    public String getItemUid() {
        return itemUid;
    }

    public void setItemUid(String itemUid) {
        this.itemUid = itemUid;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPaidPrice() {
        return paidPrice;
    }

    public void setPaidPrice(Double paidPrice) {
        this.paidPrice = paidPrice;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SellerVersion getSellerVersion() {
        return sellerVersion;
    }

    public void setSellerVersion(SellerVersion sellerVersion) {
        this.sellerVersion = sellerVersion;
    }
}