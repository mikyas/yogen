package com.yogen.datafeed.model.dto.snapshot;

import java.util.Date;
import com.yogen.util.enumtype.ChargebackState;

public class ChargebackDetailDTO {

    private String chargebackUID;
    private Boolean isPartial;
    private Date chargebackAt;
    private String chargebackCurrency;
    private Double chargebackAmount;
    private Integer reasonCode;
    private String reasonDescription;
    private ChargebackState state;
    private String type;
    private String gateway;
    private String arn;
    private String creditCardCompany;
    private Date respondAt;


    public String getChargebackUID() {
        return chargebackUID;
    }

    public void setChargebackUID(String chargebackUID) {
        this.chargebackUID = chargebackUID;
    }

    public Boolean getPartial() {
        return isPartial;
    }

    public void setPartial(Boolean partial) {
        isPartial = partial;
    }

    public Date getChargebackAt() {
        return chargebackAt;
    }

    public void setChargebackAt(Date chargebackAt) {
        this.chargebackAt = chargebackAt;
    }

    public String getChargebackCurrency() {
        return chargebackCurrency;
    }

    public void setChargebackCurrency(String chargebackCurrency) {
        this.chargebackCurrency = chargebackCurrency;
    }

    public Double getChargebackAmount() {
        return chargebackAmount;
    }

    public void setChargebackAmount(Double chargebackAmount) {
        this.chargebackAmount = chargebackAmount;
    }

    public Integer getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(Integer reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public ChargebackState getState() {
        return state;
    }

    public void setState(ChargebackState state) {
        this.state = state;
    }

    public String getArn() {
        return arn;
    }

    public void setArn(String arn) {
        this.arn = arn;
    }

    public String getCreditCardCompany() {
        return creditCardCompany;
    }

    public void setCreditCardCompany(String creditCardCompany) {
        this.creditCardCompany = creditCardCompany;
    }

    public Date getRespondAt() {
        return respondAt;
    }

    public void setRespondAt(Date respondAt) {
        this.respondAt = respondAt;
    }
}
