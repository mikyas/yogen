package com.yogen.datafeed.model.version;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.yogen.datafeed.model.AbstractBaseDataFeedEntity;

@Entity
@Table(name = "refund_detail_version", catalog = "yogen_datafeed", schema = "dbo")
public class RefundDetailVersion extends AbstractBaseDataFeedEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_VERSION_ID", referencedColumnName = "ID", nullable = true)
    private OrderVersion orderVersion;

    @Column(name = "REFUND_UID", nullable = false)
    private String refundUid;

    @Column(name = "IS_PARTIAL", nullable = false)
    private Boolean isPartial;

    @Column(name = "REFUNDED_AT")
    private Date refundedAt;

    @Column(name = "AMOUNT")
    private Double amount;

    @Column(name = "REASON_CODE")
    private String reasonCode;

    @Column(name = "REASON_DESCRIPTION")
    private String reasonDescription;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public OrderVersion getOrderVersion() {
        return orderVersion;
    }

    public void setOrderVersion(OrderVersion orderVersion) {
        this.orderVersion = orderVersion;
    }

    public String getRefundUid() {
        return refundUid;
    }

    public void setRefundUid(String refundUid) {
        this.refundUid = refundUid;
    }

    public Boolean getPartial() {
        return isPartial;
    }

    public void setPartial(Boolean partial) {
        isPartial = partial;
    }

    public Date getRefundedAt() {
        return refundedAt;
    }

    public void setRefundedAt(Date refundedAt) {
        this.refundedAt = refundedAt;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }
}
