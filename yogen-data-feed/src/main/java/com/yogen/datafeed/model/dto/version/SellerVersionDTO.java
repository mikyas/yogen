package com.yogen.datafeed.model.dto.version;

public class SellerVersionDTO {
    private String sellerUID;
    private String name;
    private LocationVersionDTO sellerLocation;

    public String getSellerUID() {
        return sellerUID;
    }

    public void setSellerUID(String sellerUID) {
        this.sellerUID = sellerUID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocationVersionDTO getSellerLocation() {
        return sellerLocation;
    }

    public void setSellerLocation(LocationVersionDTO sellerLocation) {
        this.sellerLocation = sellerLocation;
    }
}
