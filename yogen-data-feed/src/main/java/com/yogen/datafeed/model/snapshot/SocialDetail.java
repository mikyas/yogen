package com.yogen.datafeed.model.snapshot;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.yogen.datafeed.model.AbstractBaseDataFeedEntity;

@Entity
@Table(name = "social_detail",catalog = "yogen_datafeed", schema = "dbo")
public class SocialDetail extends AbstractBaseDataFeedEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PURCHASER_ID", referencedColumnName = "ID", nullable = true)
    private Purchaser purchaser;

    @Column(name = "SOCIAL_DETAIL_UID", nullable = false)
    private String socialDetailUID;

    @Column(name = "NETWORK")
    private String network;

    @Column(name = "PUBLIC_USERNAME")
    private String publicUsername;

    @Column(name = "COMMUNITY_SCORE")
    private Integer communityScore;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "BIO")
    private String bio;

    @Column(name = "ACCOUNT_URL")
    private String accountUrl;

    @Column(name = "FOLLOWING")
    private Integer following;

    @Column(name = "FOLLOWED")
    private Integer followed;

    @Column(name = "POST_COUNT")     // number of postCount
    private Integer postCount;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Purchaser getPurchaser() {
        return purchaser;
    }

    public void setPurchaser(Purchaser purchaser) {
        this.purchaser = purchaser;
    }

    public String getSocialDetailUID() {
        return socialDetailUID;
    }

    public void setSocialDetailUID(String socialDetailUID) {
        this.socialDetailUID = socialDetailUID;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getPublicUsername() {
        return publicUsername;
    }

    public void setPublicUsername(String publicUsername) {
        this.publicUsername = publicUsername;
    }

    public Integer getCommunityScore() {
        return communityScore;
    }

    public void setCommunityScore(Integer communityScore) {
        this.communityScore = communityScore;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getAccountUrl() {
        return accountUrl;
    }

    public void setAccountUrl(String accountUrl) {
        this.accountUrl = accountUrl;
    }

    public Integer getFollowing() {
        return following;
    }

    public void setFollowing(Integer following) {
        this.following = following;
    }

    public Integer getFollowed() {
        return followed;
    }

    public void setFollowed(Integer followed) {
        this.followed = followed;
    }

    public Integer getPostCount() {
        return postCount;
    }

    public void setPostCount(Integer postCount) {
        this.postCount = postCount;
    }
}
