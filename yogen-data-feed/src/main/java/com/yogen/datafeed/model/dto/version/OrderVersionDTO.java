package com.yogen.datafeed.model.dto.version;

import java.util.Date;
import java.util.List;

public class OrderVersionDTO {

    private String name;
    private String orderUID;
    private Date createdAt;
    private Date closedAt;
    private Date updatedAt;
    private List<ItemVersionDTO> items;
    private PurchaserVersionDTO purchaserDTO;
    private PaymentDetailVersionDTO paymentDetailDTO;
    private BillingVersionDTO billingDTO;
    private ShipmentVersionDTO shipmentDTO;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrderUID() {
        return orderUID;
    }

    public void setOrderUID(String orderUID) {
        this.orderUID = orderUID;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getClosedAt() {
        return closedAt;
    }

    public void setClosedAt(Date closedAt) {
        this.closedAt = closedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<ItemVersionDTO> getItems() {
        return items;
    }

    public void setItems(List<ItemVersionDTO> items) {
        this.items = items;
    }

    public PurchaserVersionDTO getPurchaserDTO() {
        return purchaserDTO;
    }

    public void setPurchaserDTO(PurchaserVersionDTO purchaserDTO) {
        this.purchaserDTO = purchaserDTO;
    }

    public PaymentDetailVersionDTO getPaymentDetailDTO() {
        return paymentDetailDTO;
    }

    public void setPaymentDetailDTO(PaymentDetailVersionDTO paymentDetailDTO) {
        this.paymentDetailDTO = paymentDetailDTO;
    }

    public BillingVersionDTO getBillingDTO() {
        return billingDTO;
    }

    public void setBillingDTO(BillingVersionDTO billingDTO) {
        this.billingDTO = billingDTO;
    }

    public ShipmentVersionDTO getShipmentDTO() {
        return shipmentDTO;
    }

    public void setShipmentDTO(ShipmentVersionDTO shipmentDTO) {
        this.shipmentDTO = shipmentDTO;
    }
}
