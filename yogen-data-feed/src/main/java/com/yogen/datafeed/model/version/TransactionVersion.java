package com.yogen.datafeed.model.version;

import com.yogen.datafeed.model.AbstractBaseDataFeedEntity;
import com.yogen.datafeed.model.snapshot.Transaction;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "transaction_version", catalog = "yogen_datafeed", schema = "dbo")
public class TransactionVersion extends AbstractBaseDataFeedEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRANSACTION_ID", referencedColumnName = "ID", nullable = false)
    private Transaction transaction;

    @Column(name = "TRANSACTION_UID", nullable = false)
    private String transactionUID;

    @OneToOne(mappedBy = "transactionVersion")
    private OrderVersion orderVersion;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public String getTransactionUID() {
        return transactionUID;
    }

    public void setTransactionUID(String transactionUID) {
        this.transactionUID = transactionUID;
    }

    public OrderVersion getOrderVersion() {
        return orderVersion;
    }

    public void setOrderVersion(OrderVersion orderVersion) {
        this.orderVersion = orderVersion;
    }
}
