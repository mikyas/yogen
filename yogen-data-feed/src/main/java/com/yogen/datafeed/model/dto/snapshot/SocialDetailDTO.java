package com.yogen.datafeed.model.dto.snapshot;

public class SocialDetailDTO {

    private String socialDetailUID;
    private String network;
    private String publicUserName;
    private Integer communityScore;
    private String accountUrl;
    private String mail;
    private String bio;
    private Integer following;
    private Integer followed;
    private Integer postCount;

    public String getSocialDetailUID() {
        return socialDetailUID;
    }

    public void setSocialDetailUID(String socialDetailUID) {
        this.socialDetailUID = socialDetailUID;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getPublicUserName() {
        return publicUserName;
    }

    public void setPublicUserName(String publicUserName) {
        this.publicUserName = publicUserName;
    }

    public Integer getCommunityScore() {
        return communityScore;
    }

    public void setCommunityScore(Integer communityScore) {
        this.communityScore = communityScore;
    }

    public String getAccountUrl() {
        return accountUrl;
    }

    public void setAccountUrl(String accountUrl) {
        this.accountUrl = accountUrl;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public Integer getFollowing() {
        return following;
    }

    public void setFollowing(Integer following) {
        this.following = following;
    }

    public Integer getFollowed() {
        return followed;
    }

    public void setFollowed(Integer followed) {
        this.followed = followed;
    }

    public Integer getPostCount() {
        return postCount;
    }

    public void setPostCount(Integer postCount) {
        this.postCount = postCount;
    }
}
