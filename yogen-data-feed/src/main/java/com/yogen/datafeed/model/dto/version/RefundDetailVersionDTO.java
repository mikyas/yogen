package com.yogen.datafeed.model.dto.version;

import java.util.Date;

public class RefundDetailVersionDTO {

    private Long orderId;
    private Long chargebackUID;
    private Date chargebackAt;
    private String chargebackCurrency;
    private Double chargebackAmount;
    private Integer reasonCode;
    private String type;
    private String gateway;
    private String reasonDescription;
    private String mid;
    private String arn;
    private String creditCardCompany;
    private Date respondBy;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getChargebackUID() {
        return chargebackUID;
    }

    public void setChargebackUID(Long chargebackUID) {
        this.chargebackUID = chargebackUID;
    }

    public Date getChargebackAt() {
        return chargebackAt;
    }

    public void setChargebackAt(Date chargebackAt) {
        this.chargebackAt = chargebackAt;
    }

    public String getChargebackCurrency() {
        return chargebackCurrency;
    }

    public void setChargebackCurrency(String chargebackCurrency) {
        this.chargebackCurrency = chargebackCurrency;
    }

    public Double getChargebackAmount() {
        return chargebackAmount;
    }

    public void setChargebackAmount(Double chargebackAmount) {
        this.chargebackAmount = chargebackAmount;
    }

    public Integer getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(Integer reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getArn() {
        return arn;
    }

    public void setArn(String arn) {
        this.arn = arn;
    }

    public String getCreditCardCompany() {
        return creditCardCompany;
    }

    public void setCreditCardCompany(String creditCardCompany) {
        this.creditCardCompany = creditCardCompany;
    }

    public Date getRespondBy() {
        return respondBy;
    }

    public void setRespondBy(Date respondBy) {
        this.respondBy = respondBy;
    }
}
