package com.yogen.datafeed.model.dto.version;

import java.util.Date;

public class DeviceVersionDTO {

    private String deviceUID;
    private String type;
    private String manufacturer;
    private String model;
    private String os;
    private String ipAddress;
    private Date browser;

    public String getDeviceUID() {
        return deviceUID;
    }

    public void setDeviceUID(String deviceUID) {
        this.deviceUID = deviceUID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Date getBrowser() {
        return browser;
    }

    public void setBrowser(Date browser) {
        this.browser = browser;
    }
}
