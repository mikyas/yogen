package com.yogen.datafeed.data.dao;

import com.yogen.datafeed.model.snapshot.ChargebackDetail;

public interface ChargebackDao extends IBaseDataFeedHibernateDao<ChargebackDetail, Long> {

}
