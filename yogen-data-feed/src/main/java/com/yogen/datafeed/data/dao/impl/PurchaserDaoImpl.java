package com.yogen.datafeed.data.dao.impl;

import org.springframework.stereotype.Repository;
import com.yogen.datafeed.data.dao.PurchaserDao;
import com.yogen.datafeed.model.snapshot.Purchaser;

@Repository
public class PurchaserDaoImpl extends BaseDataFeedHibernateDaoSupport<Purchaser, Long> implements PurchaserDao {

}
