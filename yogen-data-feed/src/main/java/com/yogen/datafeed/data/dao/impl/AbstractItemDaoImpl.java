package com.yogen.datafeed.data.dao.impl;

import org.springframework.stereotype.Repository;
import com.yogen.datafeed.data.dao.AbstractItemDao;
import com.yogen.datafeed.model.snapshot.AbstractItem;

@Repository
public class AbstractItemDaoImpl extends BaseDataFeedHibernateDaoSupport<AbstractItem, Long> implements AbstractItemDao {
}
