package com.yogen.datafeed.data.dao.impl;

import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.yogen.datafeed.data.dao.TransactionDao;
import com.yogen.datafeed.model.snapshot.Transaction;

@Repository
@Transactional
public class TransactionDaoImpl extends BaseDataFeedHibernateDaoSupport<Transaction, Long> implements TransactionDao {

    @Override
    public Transaction getTransactionById(Long id) {
        CriteriaBuilder builder = getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<Transaction> criteriaQuery = builder.createQuery(Transaction.class);
        Root<Transaction> root = criteriaQuery.from(Transaction.class);
        criteriaQuery.select(root).where(builder.equal(root.get("ID"), id));
        return getCurrentSession().createQuery(criteriaQuery).getSingleResult();
    }

    @Override
    public List<Transaction> getTransactionListByIntegratorId(Long integratorId) {
        CriteriaBuilder builder = getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<Transaction> criteriaQuery = builder.createQuery(Transaction.class);
        Root<Transaction> root = criteriaQuery.from(Transaction.class);
        criteriaQuery.select(root).where(builder.equal(root.get("integratorId"), integratorId));
        Query<Transaction> query = getCurrentSession().createQuery(criteriaQuery);
        return query.getResultList();
    }
}
