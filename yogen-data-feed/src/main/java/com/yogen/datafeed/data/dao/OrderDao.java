package com.yogen.datafeed.data.dao;

import java.util.List;
import com.yogen.datafeed.model.snapshot.Order;

public interface OrderDao extends IBaseDataFeedHibernateDao<Order, Long> {

    List<Order> getOrderList();

}
