package com.yogen.datafeed.data.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.yogen.datafeed.data.service.DataFeedLogService;
import com.yogen.util.enumtype.LogActor;
import com.yogen.util.enumtype.LogModule;
import com.yogen.util.enumtype.LogPriority;

@Service
@Transactional
public class DataFeedLogServiceImpl implements DataFeedLogService {

    @Override
    public void log(LogPriority priority, LogModule module, String summaryInfo, String detailInfo, Object data, LogActor logActor, Long actorId, String txId) {

    }

    @Override
    public void log(LogPriority priority, LogModule module, String summaryInfo) {

    }

    @Override
    public void log(LogPriority priority, LogModule module, String summaryInfo, String detailInfo) {

    }

    @Override
    public void log(LogPriority priority, LogModule module, String summaryInfo, String detailInfo, Object data) {

    }

    @Override
    public void log(LogPriority priority, LogModule module, String summaryInfo, String detailInfo, Object data, LogActor logActor, Long actorId) {

    }
}
