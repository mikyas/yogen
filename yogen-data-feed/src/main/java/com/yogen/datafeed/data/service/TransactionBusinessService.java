package com.yogen.datafeed.data.service;

import com.yogen.datafeed.model.dto.snapshot.TransactionDTO;

public interface TransactionBusinessService {

    long checkout(TransactionDTO checkoutDTO);

}
