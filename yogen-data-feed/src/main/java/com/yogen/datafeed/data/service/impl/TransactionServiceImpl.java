package com.yogen.datafeed.data.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.yogen.datafeed.data.dao.TransactionDao;
import com.yogen.datafeed.data.service.TransactionService;
import com.yogen.datafeed.model.snapshot.AbstractItem;
import com.yogen.datafeed.model.snapshot.Transaction;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {
    @Autowired
    TransactionDao transactionDao;


    @Override
    public Transaction getById(Long id) {
        return null;
    }

    @Override
    public List<Transaction> getTransactionListByIntegratorId(Long integratorId) {
        List<Transaction> transactions = new ArrayList<>();

        try {
            transactions = transactionDao.getTransactionListByIntegratorId(integratorId);
            transactions.size();
            for (Transaction transaction : transactions) {
                transaction.getOrder().getId();
                for (AbstractItem item : transaction.getOrder().getItems()) {
                    item.getId();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return transactions;

    }
}
