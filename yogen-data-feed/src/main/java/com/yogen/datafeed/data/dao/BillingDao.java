package com.yogen.datafeed.data.dao;

import com.yogen.datafeed.model.snapshot.Billing;

public interface BillingDao extends IBaseDataFeedHibernateDao<Billing, Long> {

}
