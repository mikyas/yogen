package com.yogen.datafeed.data.dao.impl;

import org.springframework.stereotype.Repository;
import com.yogen.datafeed.data.dao.IBaseDataFeedHibernateDao;
import com.yogen.datafeed.model.snapshot.PaymentDetail;

@Repository
public class PaymentDetailDaoImpl extends BaseDataFeedHibernateDaoSupport<PaymentDetail, Long> implements IBaseDataFeedHibernateDao<PaymentDetail, Long> {

}
