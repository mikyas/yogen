package com.yogen.datafeed.data.dao;

import com.yogen.datafeed.model.snapshot.PhysicalItem;

public interface PhysicalItemDao extends IBaseDataFeedHibernateDao<PhysicalItem, Long> {
}
