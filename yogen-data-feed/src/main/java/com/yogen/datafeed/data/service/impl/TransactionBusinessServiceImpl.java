package com.yogen.datafeed.data.service.impl;

import com.yogen.datafeed.data.dao.TransactionDao;
import com.yogen.datafeed.data.dao.TransactionVersionDao;
import com.yogen.datafeed.data.service.TransactionBusinessService;
import com.yogen.datafeed.model.dto.snapshot.*;
import com.yogen.datafeed.model.snapshot.*;
import com.yogen.datafeed.model.version.TransactionVersion;
import com.yogen.util.CollectionsUtil;
import com.yogen.util.enumtype.ItemType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class TransactionBusinessServiceImpl implements TransactionBusinessService {

    @Autowired
    private TransactionDao transactionDao;
    @Autowired
    private TransactionVersionDao transactionVersionDao;

    @Override
    public long checkout(TransactionDTO transactionDTO) {
        //Transaction Settings
        Long transactionId = createTransaction(transactionDTO);
        return createTransactionVersion(transactionId, transactionDTO);
    }

    private Long createTransaction(TransactionDTO transactionDTO) {
        Transaction transaction = new Transaction();
        transaction.setTransactionUID(transactionDTO.getTransactionUID());
        transaction.setIntegratorId(transactionDTO.getIntegratorId());
        transaction.setOrder(initializeOrder(transaction, transactionDTO));
        Long transactionId = null;
        try {
            transactionId = transactionDao.persist(transaction);
            transactionDTO.setTransactionDate(transaction.getCreatedDate());
        } catch (Exception e) {
            System.out.println(e + "");
            e.printStackTrace();
        }
        return transactionId;
    }

    private long createTransactionVersion(Long transactionId, TransactionDTO transactionDTO) {
        TransactionVersion transactionVersion = new TransactionVersion();
        Transaction transaction = new Transaction();
        transaction.setId(transactionId);
        transactionVersion.setTransaction(transaction);
        transactionVersion.setTransactionUID(transactionDTO.getTransactionUID());
//        transactionVersion.setOrderVersion(initializeOrderVersion(transactionDTO));
        return transactionVersionDao.persist(transactionVersion);
    }

//    private OrderVersion initializeOrderVersion(TransactionDTO transactionDTO) {
//        OrderVersion orderVersion = new OrderVersion();
//        return orderVersion;
//    }

    private Order initializeOrder(Transaction transaction, TransactionDTO transactionDTO) {
        OrderDTO orderDTO = transactionDTO.getOrderDTO();
        Order order = new Order();
        order.setTransaction(transaction);
        order.setOrderUID(orderDTO.getOrderUID());
        order.setName(orderDTO.getName());
        order.setUpdatedAt(orderDTO.getUpdatedAt());
        order.setCreatedAt(orderDTO.getCreatedAt());
        order.setClosedAt(orderDTO.getClosedAt());
        //set features
        if (CollectionsUtil.isNotNullAndNotEmpty(orderDTO.getItems())) {
            order.setItems(initializeItems(order, orderDTO.getItems()));
        }
        if (orderDTO.getShipmentDTO() != null) {
            order.setShipment(initializeShipment(order, orderDTO.getShipmentDTO()));
        }
        if (orderDTO.getBillingDTO() != null) {
            order.setBilling(initializeBilling(order, orderDTO.getBillingDTO()));
        }
        if (orderDTO.getPurchaserDTO() != null) {
            order.setPurchaser(initalizePurchaser(order, orderDTO.getPurchaserDTO()));
        }
        if (orderDTO.getPaymentDetailDTO() != null) {
            order.setPaymentDetail(initalizePaymentDetail(order, orderDTO.getPaymentDetailDTO()));
        }
        if (orderDTO.getDeviceDTO() != null) {
            order.setDevice(initalizeDevice(order, orderDTO.getDeviceDTO()));
        }
        if (orderDTO.getContactDTO() != null) {
            order.setContact(initalizeContact(order, orderDTO.getContactDTO()));
        }
        if (orderDTO.getChargebackDetailDTO() != null) {
            order.setChargebackDetail(initalizeChargebackDetail(order, orderDTO.getChargebackDetailDTO()));
        }
        if (orderDTO.getRefundDetailDTO() != null) {
            order.setRefundDetail(initalizeRefundDetail(order, orderDTO.getRefundDetailDTO()));
        }
        return order;

    }

    private List<AbstractItem> initializeItems(Order order, List<ItemDTO> itemDTOs) {
        List<AbstractItem> items = new ArrayList<>();
        for (ItemDTO itemDTO : itemDTOs) {
            //TODO: Castler hatali oluyor buraya tekrar bakilmali
            if (Objects.equals(itemDTO.getType(), ItemType.PHYSICAL.getValue())) {
//                PhysicalItem physicalItem =  initializePhysicalItem(itemDTO, order);
                PhysicalItem physicalItem = (PhysicalItem) initializeItem(itemDTO, order);
                physicalItem.setColor("Beyaz");
                physicalItem.setVolume(100);
                physicalItem.setWeight(400);
                physicalItem.setSeller(initalizeSeller(physicalItem, itemDTO.getSellerDTO()));
                items.add(physicalItem);
            } else if (Objects.equals(itemDTO.getType(), ItemType.TRAVEL.getValue())) {
//                TravelItem travelItem = initializeTravelItem(itemDTO, order);
                TravelItem travelItem = (TravelItem) initializeItem(itemDTO, order);
                travelItem.setSeller(initalizeSeller(travelItem, itemDTO.getSellerDTO()));
                items.add(travelItem);
            } else if (Objects.equals(itemDTO.getType(), ItemType.EVENT.getValue())) {
//                EventItem eventItem = initializeEventItem(itemDTO, order);
                EventItem eventItem = (EventItem) initializeItem(itemDTO, order);
                eventItem.setSeller(initalizeSeller(eventItem, itemDTO.getSellerDTO()));
                items.add(eventItem);
            } else if (Objects.equals(itemDTO.getType(), ItemType.DIGITAL.getValue())) {
//                DigitalItem digitalItem = initializeDigitalItem(itemDTO, order);
                DigitalItem digitalItem = (DigitalItem) initializeItem(itemDTO, order);
                digitalItem.setSeller(initalizeSeller(digitalItem, itemDTO.getSellerDTO()));
                items.add(digitalItem);
            }
        }
        return items;
    }


    private AbstractItem initializeItem(ItemDTO itemDTO, Order order) {
        AbstractItem item = null;
        if (Objects.equals(itemDTO.getType(), ItemType.PHYSICAL.getValue())) {
            item = new PhysicalItem();
        } else if (Objects.equals(itemDTO.getType(), ItemType.TRAVEL.getValue())) {
            item = new TravelItem();
        } else if (Objects.equals(itemDTO.getType(), ItemType.EVENT.getValue())) {
            item = new EventItem();
        } else if (Objects.equals(itemDTO.getType(), ItemType.DIGITAL.getValue())) {
            item = new DigitalItem();
        }

        item.setOrder(order);
        item.setItemUID(itemDTO.getItemUID());
        item.setUpc(itemDTO.getUpc());
        item.setSku(itemDTO.getSku());
        item.setPrice(itemDTO.getPrice());
        item.setPaidPrice(itemDTO.getPaidPrice());
        item.setQuantity(itemDTO.getQuantity());
        item.setBrand(itemDTO.getBrand());
        item.setManufacturer(itemDTO.getManufacturer());
        item.setTitle(itemDTO.getTitle());
        return item;
    }

    private DigitalItem initializeDigitalItem(ItemDTO itemDTO, Order order) {
        DigitalItem item = new DigitalItem();
        item.setOrder(order);
        item.setItemUID(itemDTO.getItemUID());
        item.setUpc(itemDTO.getUpc());
        item.setSku(itemDTO.getSku());
        item.setPrice(itemDTO.getPrice());
        item.setPaidPrice(itemDTO.getPaidPrice());
        item.setQuantity(itemDTO.getQuantity());
        item.setBrand(itemDTO.getBrand());
        item.setManufacturer(itemDTO.getManufacturer());
        item.setTitle(itemDTO.getTitle());
//        item.setSenderName();
//        item.setSenderEmail();
//        item.setDisplayName();
//        item.setGreetingMessage();
//        item.setGreetingPhotoUrl();
//        item.setMessage();
//        item.setPhotoUploaded();
//        item.setPhotoUrl();
        return item;
    }

    private EventItem initializeEventItem(ItemDTO itemDTO, Order order) {
        EventItem item = new EventItem();
        item.setOrder(order);
        item.setItemUID(itemDTO.getItemUID());
        item.setUpc(itemDTO.getUpc());
        item.setSku(itemDTO.getSku());
        item.setPrice(itemDTO.getPrice());
        item.setPaidPrice(itemDTO.getPaidPrice());
        item.setQuantity(itemDTO.getQuantity());
        item.setBrand(itemDTO.getBrand());
        item.setManufacturer(itemDTO.getManufacturer());
        item.setTitle(itemDTO.getTitle());
//        item.setEventName();
//        item.setEventVenueName();
//        item.setEventCategory();
//        item.setEventStartTime();
//        item.setEventEndTime();
//        item.setEventAddress1();
//        item.setEventAddress2();
//        item.setEventNeighbourhood();
//        item.setEventRegion();
//        item.setEventCity();
//        item.setEventCountry();
//        item.setEventPoBoxNumber();
//        item.setEventPostalCode();
//        item.setEventLatitude();
//        item.setEventLongitude();
//        item.setEventGeohash();
        return item;
    }

    private TravelItem initializeTravelItem(ItemDTO itemDTO, Order order) {
        TravelItem item = new TravelItem();
        item.setOrder(order);
        item.setItemUID(itemDTO.getItemUID());
        item.setUpc(itemDTO.getUpc());
        item.setSku(itemDTO.getSku());
        item.setPrice(itemDTO.getPrice());
        item.setPaidPrice(itemDTO.getPaidPrice());
        item.setQuantity(itemDTO.getQuantity());
        item.setBrand(itemDTO.getBrand());
        item.setManufacturer(itemDTO.getManufacturer());
        item.setTitle(itemDTO.getTitle());
//        item.setArrivalCity();
//        item.setArrivalCountryCode();
//        item.setArrivalDate();
//        item.setArrivalPortCode();
//        item.setCarrierCode();
//        item.setCarrierName();
//        item.setDepartureCity();
//        item.setDepartureCountryCode();
//        item.setDepartureDate();
//        item.setDeparturePortCode();
//        item.setLegId();
//        item.setLegIndex();
//        item.setRouteIndex();
//        item.setTicketClass();
//        item.setTransportMethod();
        return item;
    }

    private PhysicalItem initializePhysicalItem(ItemDTO itemDTO, Order order) {
        PhysicalItem item = new PhysicalItem();
        item.setOrder(order);
        item.setItemUID(itemDTO.getItemUID());
        item.setUpc(itemDTO.getUpc());
        item.setSku(itemDTO.getSku());
        item.setPrice(itemDTO.getPrice());
        item.setPaidPrice(itemDTO.getPaidPrice());
        item.setQuantity(itemDTO.getQuantity());
        item.setBrand(itemDTO.getBrand());
        item.setManufacturer(itemDTO.getManufacturer());
        item.setTitle(itemDTO.getTitle());
//        item.setColor();
//        item.setVolume();
//        item.setWeight();
        return item;
    }

    private Shipment initializeShipment(Order order, ShipmentDTO shipmentDTO) {
        Shipment shipment = new Shipment();
        shipment.setOrder(order);
        shipment.setShipmentUID(shipmentDTO.getShipmentUID());
        shipment.setFirstName(shipmentDTO.getFirstName());
        shipment.setLastName(shipmentDTO.getLastName());
        shipment.setCitizenNum(shipmentDTO.getCitizenNum());
        shipment.setTaxNum(shipmentDTO.getTaxNum());
        shipment.setDeliveryCompany(shipmentDTO.getDeliveryCompany());
        shipment.setAddress1(shipmentDTO.getLocation().getAddress1());
        shipment.setAddress2(shipmentDTO.getLocation().getAddress2());
        shipment.setNeighbourhood(shipmentDTO.getLocation().getNeighbourhood());
        shipment.setCity(shipmentDTO.getLocation().getCity());
        shipment.setRegion(shipmentDTO.getLocation().getRegion());
        shipment.setCountry(shipmentDTO.getLocation().getCountry());
        shipment.setPoBoxNumber(shipmentDTO.getLocation().getPoBoxNumber());
        shipment.setPostalCode(shipmentDTO.getLocation().getPostalCode());
        shipment.setLatitude(shipmentDTO.getLocation().getLatitude());
        shipment.setLongitude(shipmentDTO.getLocation().getLongitude());
        shipment.setGeohash(shipmentDTO.getLocation().getGeohash());
        return shipment;
    }

    private Seller initalizeSeller(AbstractItem item, SellerDTO sellerDTO) {
        Seller seller = new Seller();
        seller.setAbstractItem(item);
        seller.setSellerUID(sellerDTO.getSellerUID());
        seller.setName(sellerDTO.getName());
        seller.setAddress1(sellerDTO.getSellerLocation().getAddress1());
        seller.setAddress2(sellerDTO.getSellerLocation().getAddress2());
        seller.setNeighbourhood(sellerDTO.getSellerLocation().getNeighbourhood());
        seller.setCity(sellerDTO.getSellerLocation().getCity());
        seller.setRegion(sellerDTO.getSellerLocation().getRegion());
        seller.setCountry(sellerDTO.getSellerLocation().getCountry());
        seller.setPoBoxNumber(sellerDTO.getSellerLocation().getPoBoxNumber());
        seller.setPostalCode(sellerDTO.getSellerLocation().getPostalCode());
        seller.setLatitude(sellerDTO.getSellerLocation().getLatitude());
        seller.setLongitude(sellerDTO.getSellerLocation().getLongitude());
        seller.setGeohash(sellerDTO.getSellerLocation().getGeohash());
        return seller;
    }

    private Purchaser initalizePurchaser(Order order, PurchaserDTO purchaserDTO) {
        Purchaser purchaser = new Purchaser();
        purchaser.setOrder(order);
        purchaser.setPurchaserUID(purchaserDTO.getPurchaserUID());
        purchaser.setFirstName(purchaserDTO.getFirstName());
        purchaser.setLastName(purchaserDTO.getLastName());
        purchaser.setMail(purchaserDTO.getMail());
        purchaser.setPhone(purchaserDTO.getPhone());
        purchaser.setAccountType(purchaserDTO.getAccountType());
        purchaser.setDateOfBirth(purchaserDTO.getDateOfBirth());
        purchaser.setUserName(purchaserDTO.getUserName());
        purchaser.setLanguage(purchaserDTO.getLanguage());
        purchaser.setGender(purchaserDTO.getGender());
        purchaser.setSocialDetail(initializeSocialDetail(purchaser, purchaserDTO.getSocialDetailDTO()));
        return purchaser;
    }

    private SocialDetail initializeSocialDetail(Purchaser purchaser, SocialDetailDTO socialDetailDTO) {
        SocialDetail socialDetail = new SocialDetail();
        socialDetail.setPurchaser(purchaser);
        socialDetail.setSocialDetailUID(socialDetailDTO.getSocialDetailUID());
        socialDetail.setNetwork(socialDetailDTO.getNetwork());
        socialDetail.setPublicUsername(socialDetailDTO.getPublicUserName());
        socialDetail.setCommunityScore(socialDetailDTO.getCommunityScore());
        socialDetail.setAccountUrl(socialDetailDTO.getAccountUrl());
        socialDetail.setEmail(socialDetailDTO.getMail());
        socialDetail.setBio(socialDetailDTO.getBio());
        socialDetail.setFollowing(socialDetailDTO.getFollowing());
        socialDetail.setFollowed(socialDetailDTO.getFollowed());
        socialDetail.setPostCount(socialDetailDTO.getPostCount());
        return socialDetail;
    }

    private PaymentDetail initalizePaymentDetail(Order order, PaymentDetailDTO paymentDetailDTO) {
        PaymentDetail paymentDetail = new PaymentDetail();
        paymentDetail.setOrder(order);
        paymentDetail.setPaymentDetailUID(paymentDetailDTO.getPaymentDetailUID());
        paymentDetail.setPaymentType(paymentDetailDTO.getPaymentType());
        paymentDetail.setInstallment(paymentDetailDTO.getInstallment());
        paymentDetail.setTotalPrice(paymentDetailDTO.getTotalPrice());
        paymentDetail.setTotalPaidPrice(paymentDetailDTO.getTotalPaidPrice());
        paymentDetail.setCurrency(paymentDetailDTO.getCurrency());
        paymentDetail.setCreditCardBin(paymentDetailDTO.getCreditCardBin());
        paymentDetail.setCreditCardLastFourDigit(paymentDetailDTO.getCreditCardLastFourDigit());
        paymentDetail.setCreditCardCompany(paymentDetailDTO.getCreditCardCompany());
        paymentDetail.setThreeD(paymentDetailDTO.getThreeD());
        paymentDetail.setThreeDInquiryResult(paymentDetailDTO.getThreeDInquiryResult());
        paymentDetail.setPaymentResult(paymentDetailDTO.getPaymentResult());
        paymentDetail.setEciCode(paymentDetailDTO.getEciCode());
        paymentDetail.setCavvCode(paymentDetailDTO.getCavvCode());
        paymentDetail.setMdStatus(paymentDetailDTO.getMdStatus());
        paymentDetail.setAvsResultCode(paymentDetailDTO.getAvsResultCode());
        paymentDetail.setCvvResultCode(paymentDetailDTO.getCvvResultCode());
        return paymentDetail;
    }


    private Device initalizeDevice(Order order, DeviceDTO deviceDTO) {
        Device device = new Device();
        device.setOrder(order);
        device.setDeviceUID(deviceDTO.getDeviceUID());
        device.setType(deviceDTO.getType());
        device.setManufacturer(deviceDTO.getManufacturer());
        device.setModel(deviceDTO.getModel());
        device.setOs(deviceDTO.getOs());
        device.setIpAddress(deviceDTO.getIpAddress());
        device.setBrowser(deviceDTO.getBrowser());
        return device;
    }

    private Contact initalizeContact(Order order, ContactDTO contactDTO) {
        Contact contact = new Contact();
        contact.setOrder(order);
        contact.setContactUID(contactDTO.getContactUID());
        contact.setContactType(contactDTO.getContactType());
        contact.setEmail(contactDTO.getEmail());
        contact.setPhone(contactDTO.getPhone());
        contact.setAccountUrl(contactDTO.getAccountUrl());
        return contact;
    }

    private ChargebackDetail initalizeChargebackDetail(Order order, ChargebackDetailDTO chargebackDetailDTO) {
        ChargebackDetail chargebackDetail = new ChargebackDetail();
        chargebackDetail.setOrder(order);
        chargebackDetail.setChargebackUID(chargebackDetailDTO.getChargebackUID());
        chargebackDetail.setPartial(chargebackDetailDTO.getPartial());
        chargebackDetail.setChargebackAt(chargebackDetailDTO.getChargebackAt());
        chargebackDetail.setAmount(chargebackDetailDTO.getChargebackAmount());
        chargebackDetail.setReasonCode(chargebackDetailDTO.getReasonCode());
        chargebackDetail.setReasonDescription(chargebackDetailDTO.getReasonDescription());
        chargebackDetail.setState(chargebackDetailDTO.getState().getValue());
        chargebackDetail.setGateway(chargebackDetailDTO.getGateway());
        chargebackDetail.setArn(chargebackDetailDTO.getArn());
        chargebackDetail.setCreditCardCompany(chargebackDetailDTO.getCreditCardCompany());
        chargebackDetail.setRespondAt(chargebackDetailDTO.getRespondAt());
        return chargebackDetail;
    }

    private RefundDetail initalizeRefundDetail(Order order, RefundDetailDTO refundDetailDTO) {
        RefundDetail refundDetail = new RefundDetail();
        refundDetail.setOrder(order);
        refundDetail.setRefundUID(refundDetailDTO.getRefundUID());
        refundDetail.setPartial(refundDetailDTO.getPartial());
        refundDetail.setRefundedAt(refundDetailDTO.getRefundAt());
        refundDetail.setAmount(refundDetailDTO.getAmount());
        refundDetail.setReasonCode(refundDetailDTO.getReasonCode());
        refundDetail.setReasonDescription(refundDetailDTO.getReasonDescription());
        return refundDetail;
    }


    private Billing initializeBilling(Order order, BillingDTO billingDTO) {
        Billing billing = new Billing();
        billing.setOrder(order);
        billing.setBillingUID(billingDTO.getBillingUID());
        billing.setFirstName(billingDTO.getFirstName());
        billing.setLastName(billingDTO.getLastName());
        billing.setCitizenNum(billingDTO.getCitizenNum());
        billing.setTaxNum(billingDTO.getTaxNum());
        billing.setSerialNum(billingDTO.getSerialNum());
        billing.setAddress1(billingDTO.getLocation().getAddress1());
        billing.setAddress2(billingDTO.getLocation().getAddress2());
        billing.setNeighbourhood(billingDTO.getLocation().getNeighbourhood());
        billing.setCity(billingDTO.getLocation().getCity());
        billing.setRegion(billingDTO.getLocation().getRegion());
        billing.setCountry(billingDTO.getLocation().getCountry());
        billing.setPoBoxNumber(billingDTO.getLocation().getPoBoxNumber());
        billing.setPostalCode(billingDTO.getLocation().getPostalCode());
        billing.setLatitude(billingDTO.getLocation().getLatitude());
        billing.setLongitude(billingDTO.getLocation().getLongitude());
        billing.setGeohash(billingDTO.getLocation().getGeohash());
        return billing;
    }

}
