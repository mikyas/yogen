package com.yogen.datafeed.data.dao.impl;

import org.springframework.stereotype.Repository;
import com.yogen.datafeed.data.dao.BillingDao;
import com.yogen.datafeed.model.snapshot.Billing;

@Repository
public class BillingDaoImpl extends BaseDataFeedHibernateDaoSupport<Billing, Long> implements BillingDao {

}
