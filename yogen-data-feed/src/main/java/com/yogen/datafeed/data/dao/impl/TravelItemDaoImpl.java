package com.yogen.datafeed.data.dao.impl;

import org.springframework.stereotype.Repository;
import com.yogen.datafeed.data.dao.TravelItemDao;
import com.yogen.datafeed.model.snapshot.TravelItem;

@Repository
public class TravelItemDaoImpl extends BaseDataFeedHibernateDaoSupport<TravelItem, Long> implements TravelItemDao {
}
