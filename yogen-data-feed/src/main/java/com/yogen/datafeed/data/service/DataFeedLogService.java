package com.yogen.datafeed.data.service;


import com.yogen.util.enumtype.LogActor;
import com.yogen.util.enumtype.LogModule;
import com.yogen.util.enumtype.LogPriority;

public interface DataFeedLogService {

    void log(LogPriority priority, LogModule module, String summaryInfo, String detailInfo, Object data, LogActor logActor, Long actorId, String txId);

    void log(LogPriority priority, LogModule module, String summaryInfo);

    void log(LogPriority priority, LogModule module, String summaryInfo, String detailInfo);

    void log(LogPriority priority, LogModule module, String summaryInfo, String detailInfo, Object data);

    void log(LogPriority priority, LogModule module, String summaryInfo, String detailInfo, Object data, LogActor logActor, Long actorId);
}