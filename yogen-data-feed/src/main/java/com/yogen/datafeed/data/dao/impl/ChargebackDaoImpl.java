package com.yogen.datafeed.data.dao.impl;

import org.springframework.stereotype.Repository;
import com.yogen.datafeed.data.dao.ChargebackDao;
import com.yogen.datafeed.model.snapshot.ChargebackDetail;

@Repository
public class ChargebackDaoImpl extends BaseDataFeedHibernateDaoSupport<ChargebackDetail, Long> implements ChargebackDao {

}
