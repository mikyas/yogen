package com.yogen.datafeed.data.dao.impl;

import org.springframework.stereotype.Repository;
import com.yogen.datafeed.data.dao.ShipmentDao;
import com.yogen.datafeed.model.snapshot.Shipment;

@Repository
public class ShipmentDaoImpl extends BaseDataFeedHibernateDaoSupport<Shipment, Long> implements ShipmentDao {
}
