package com.yogen.datafeed.data.dao.impl;

import org.springframework.stereotype.Repository;
import com.yogen.datafeed.data.dao.SellerDao;
import com.yogen.datafeed.model.snapshot.Seller;

@Repository
public class SellerDaoImpl extends BaseDataFeedHibernateDaoSupport<Seller, Long> implements SellerDao {
}
