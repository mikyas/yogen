package com.yogen.datafeed.data.service;

import com.yogen.util.enumtype.MessageCode;

public interface CacheManagementService {

    void testDBConnection();

    Integer controlGivenLanguageAndReturnLanguageId(String language);

    Integer getDefaultLanguageId();

    String getLanguageKeyById(Integer languageId);

    String findMessageLookupByCode(Integer errorCode, Integer languageId);

    String findMessageLookupByCode(MessageCode errorCode, Integer languageId);

    String getSystemError(Integer languageId);

}