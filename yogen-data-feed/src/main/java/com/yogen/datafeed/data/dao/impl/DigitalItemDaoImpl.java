package com.yogen.datafeed.data.dao.impl;

import org.springframework.stereotype.Repository;
import com.yogen.datafeed.data.dao.DigitalItemDao;
import com.yogen.datafeed.model.snapshot.DigitalItem;

@Repository
public class DigitalItemDaoImpl extends BaseDataFeedHibernateDaoSupport<DigitalItem, Long> implements DigitalItemDao {
}
