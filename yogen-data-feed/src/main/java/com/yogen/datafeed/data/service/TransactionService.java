package com.yogen.datafeed.data.service;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import com.yogen.datafeed.model.snapshot.Transaction;

public interface TransactionService {
    Transaction getById(Long id);

    List<Transaction> getTransactionListByIntegratorId(Long integratorId);
}
