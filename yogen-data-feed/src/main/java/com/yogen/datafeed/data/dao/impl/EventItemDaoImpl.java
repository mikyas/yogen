package com.yogen.datafeed.data.dao.impl;

import org.springframework.stereotype.Repository;
import com.yogen.datafeed.data.dao.EventItemDao;
import com.yogen.datafeed.model.snapshot.EventItem;

@Repository
public class EventItemDaoImpl extends BaseDataFeedHibernateDaoSupport<EventItem, Long> implements EventItemDao {
}
