package com.yogen.datafeed.data.dao;

import com.yogen.datafeed.model.snapshot.AbstractItem;

public interface AbstractItemDao extends IBaseDataFeedHibernateDao<AbstractItem, Long> {
}
