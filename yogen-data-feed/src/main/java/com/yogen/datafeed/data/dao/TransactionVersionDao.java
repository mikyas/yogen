package com.yogen.datafeed.data.dao;

import com.yogen.datafeed.model.version.TransactionVersion;

public interface TransactionVersionDao extends IBaseDataFeedHibernateDao<TransactionVersion, Long> {


}
