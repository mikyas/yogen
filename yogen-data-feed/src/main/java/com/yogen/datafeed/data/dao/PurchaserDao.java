package com.yogen.datafeed.data.dao;

import com.yogen.datafeed.model.snapshot.Purchaser;

public interface PurchaserDao extends IBaseDataFeedHibernateDao<Purchaser, Long> {

}
