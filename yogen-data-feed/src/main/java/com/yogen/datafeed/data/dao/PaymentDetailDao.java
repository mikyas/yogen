package com.yogen.datafeed.data.dao;

import com.yogen.datafeed.model.snapshot.PaymentDetail;

public interface PaymentDetailDao extends IBaseDataFeedHibernateDao<PaymentDetail, Long> {
}
