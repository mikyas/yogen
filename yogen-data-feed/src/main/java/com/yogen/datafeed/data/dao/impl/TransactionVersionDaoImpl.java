package com.yogen.datafeed.data.dao.impl;

import com.yogen.datafeed.data.dao.TransactionVersionDao;
import com.yogen.datafeed.model.version.TransactionVersion;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class TransactionVersionDaoImpl extends BaseDataFeedHibernateDaoSupport<TransactionVersion, Long> implements TransactionVersionDao {

}
