package com.yogen.datafeed.data.dao.impl;

import org.springframework.stereotype.Repository;
import com.yogen.datafeed.data.dao.PhysicalItemDao;
import com.yogen.datafeed.model.snapshot.PhysicalItem;

@Repository
public class PhysicalItemDaoImpl extends BaseDataFeedHibernateDaoSupport<PhysicalItem, Long> implements PhysicalItemDao {
}
