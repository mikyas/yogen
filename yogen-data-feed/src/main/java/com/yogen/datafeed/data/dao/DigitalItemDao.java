package com.yogen.datafeed.data.dao;

import com.yogen.datafeed.model.snapshot.DigitalItem;

public interface DigitalItemDao extends IBaseDataFeedHibernateDao<DigitalItem, Long> {
}
