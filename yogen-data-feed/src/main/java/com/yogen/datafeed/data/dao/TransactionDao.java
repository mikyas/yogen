package com.yogen.datafeed.data.dao;

import java.util.List;
import com.yogen.datafeed.model.snapshot.Transaction;

public interface TransactionDao extends IBaseDataFeedHibernateDao<Transaction, Long> {

    Transaction getTransactionById(Long id);

    List getTransactionListByIntegratorId(Long integratorId);

}
