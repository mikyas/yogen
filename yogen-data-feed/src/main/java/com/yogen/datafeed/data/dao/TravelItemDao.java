package com.yogen.datafeed.data.dao;

import com.yogen.datafeed.model.snapshot.TravelItem;

public interface TravelItemDao extends IBaseDataFeedHibernateDao<TravelItem, Long> {
}
