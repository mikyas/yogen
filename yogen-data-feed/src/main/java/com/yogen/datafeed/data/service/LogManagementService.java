package com.yogen.datafeed.data.service;

import java.util.Date;
import com.yogen.util.enumtype.ApiLogModuleAndServiceName;

public interface LogManagementService {

    void logRequest(Long integratorId, ApiLogModuleAndServiceName apiLogModuleAndServiceName, String token, Object request, Date transactionDate, String version, String clientIp, String serverIp, String language);

    void logResponse(Long integratorId, ApiLogModuleAndServiceName apiLogModuleAndServiceName, Object response, byte result, String responseMessage, Integer errorCode, String errorMessage, Date transactionDate);
}
