package com.yogen.datafeed.data.service;

import java.util.List;
import com.yogen.datafeed.model.snapshot.Order;

public interface OrderBusinessService {
    Order getOrderById(Long id);

    List<Order> getOrderListBusiness();

    void createOrder();
}
