package com.yogen.datafeed.data.dao;

import com.yogen.datafeed.model.snapshot.Shipment;

public interface ShipmentDao extends IBaseDataFeedHibernateDao<Shipment, Long> {

}
