package com.yogen.datafeed.data.dao;

import com.yogen.datafeed.model.snapshot.EventItem;

public interface EventItemDao extends IBaseDataFeedHibernateDao<EventItem, Long> {
}
