package com.yogen.datafeed.data.service.impl;

import java.util.Date;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.yogen.datafeed.data.service.LogManagementService;
import com.yogen.util.enumtype.ApiLogModuleAndServiceName;

@Service
@Transactional
public class LogManagementServiceImpl implements LogManagementService {

    @Async
    public void logRequest(Long integratorId, ApiLogModuleAndServiceName apiLogModuleAndServiceName, String token, Object request, Date transactionDate, String version, String clientIp, String serverIp, String language) {
        System.out.println("integratorId  : " + integratorId + ", apiLogModuleAndServiceName: " + apiLogModuleAndServiceName + ", token : " + token + ", transactionDate : " + transactionDate + ", request:" + request.toString());
    }

    @Async
    public void logResponse(Long integratorId, ApiLogModuleAndServiceName apiLogModuleAndServiceName, Object response, byte result, String responseMessage, Integer errorCode, String errorMessage, Date transactionDate) {
        System.out.println("integratorId  : " + integratorId+ ", apiLogModuleAndServiceName: " + apiLogModuleAndServiceName + ", errorCode : " + errorCode + ", result : " + result + ", transactionDate : " + transactionDate + ", response:" + response.toString());
    }

}