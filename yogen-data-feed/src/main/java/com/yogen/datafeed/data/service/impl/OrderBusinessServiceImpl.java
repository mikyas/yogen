package com.yogen.datafeed.data.service.impl;

import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.yogen.datafeed.data.dao.BillingDao;
import com.yogen.datafeed.data.dao.OrderDao;
import com.yogen.datafeed.data.service.OrderBusinessService;
import com.yogen.datafeed.model.snapshot.Billing;
import com.yogen.datafeed.model.snapshot.Order;
import com.yogen.log.data.service.LogService;
import com.yogen.log.model.CommonLog;

@Service
@Transactional
public class OrderBusinessServiceImpl implements OrderBusinessService {

    @Autowired
    OrderDao orderDao;
    @Autowired
    LogService logService;

    @Autowired
    BillingDao billingDao;

    @Override
    public Order getOrderById(Long id) {
        Order user = orderDao.get(id);
        if (user == null) {
            System.out.println(id + " id li kullanıcı bulunamadı!!!");
        }
        return user;
    }

    @Override
    public List<Order> getOrderListBusiness() {
        return orderDao.getOrderList();
    }

    @Override
    public void createOrder() {
        try {

            CommonLog commonLog = new CommonLog();
            commonLog.setData("My Common Log Data");
            logService.fooLog(commonLog);
//            orderDao.persist(order);
            Billing billing = new Billing();
            billing.setBillingUID(UUID.randomUUID().toString());
            billingDao.persist(billing);

        } catch (RuntimeException ex) {
            System.out.println("" + ex);
        } catch (Exception e) {
            System.out.println("" + e);
        }

    }

}
