package com.yogen.datafeed.data.dao;

import com.yogen.datafeed.model.snapshot.Seller;

public interface SellerDao extends IBaseDataFeedHibernateDao<Seller, Long> {
}
