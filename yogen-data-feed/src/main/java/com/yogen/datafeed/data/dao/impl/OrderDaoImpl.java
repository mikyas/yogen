package com.yogen.datafeed.data.dao.impl;

import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import com.yogen.datafeed.data.dao.OrderDao;
import com.yogen.datafeed.model.snapshot.Order;

@Repository
public class OrderDaoImpl extends BaseDataFeedHibernateDaoSupport<Order, Long> implements OrderDao {

    @Override
    public List<Order> getOrderList() {
        CriteriaBuilder builder = getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<Order> criteria = builder.createQuery(Order.class);
        Root<Order> root = criteria.from(Order.class);
        criteria.select(root);
        Query<Order> query = getCurrentSession().createQuery(criteria);
        return query.getResultList();
    }
}
