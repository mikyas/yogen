package com.yogen.datafeed.data.service.impl;

import com.yogen.datafeed.data.service.CacheManagementService;
import com.yogen.util.enumtype.MessageCode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CacheManagementServiceImpl implements CacheManagementService {

    @Override
    public void testDBConnection() {

    }

    @Override
    public Integer controlGivenLanguageAndReturnLanguageId(String language) {
        return 1;
    }

    @Override
    public Integer getDefaultLanguageId() {
        return 1;
    }

    @Override
    public String getLanguageKeyById(Integer languageId) {
        return null;
    }

    @Override
    public String findMessageLookupByCode(Integer errorCode, Integer languageId) {
        return null;
    }

    @Override
    public String findMessageLookupByCode(MessageCode errorCode, Integer languageId) {
        return "languageId : " + languageId + ", HATA :" + errorCode.name();
    }

    @Override
    public String getSystemError(Integer languageId) {
        return null;
    }

}
