package com.yogen.datafeed.util;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import com.yogen.log.util.YogenLogConfigruation;

@Configuration
@ComponentScan("com.yogen.datafeed")
@Import(YogenLogConfigruation.class)
public class YogenDataFeedConfigrutaion {
    public YogenDataFeedConfigrutaion() {
    }
}
