USE [master]
GO
/****** Object:  Database [yogen_datafeed]    Script Date: 2.11.2018 10:04:08 ******/
CREATE DATABASE [yogen_datafeed]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'yogen_datafeed', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\yogen_datafeed.mdf' , SIZE = 94400KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'yogen_datafeed_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\yogen_datafeed_log.ldf' , SIZE = 359424KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [yogen_datafeed] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [yogen_datafeed].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [yogen_datafeed] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [yogen_datafeed] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [yogen_datafeed] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [yogen_datafeed] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [yogen_datafeed] SET ARITHABORT OFF 
GO
ALTER DATABASE [yogen_datafeed] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [yogen_datafeed] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [yogen_datafeed] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [yogen_datafeed] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [yogen_datafeed] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [yogen_datafeed] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [yogen_datafeed] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [yogen_datafeed] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [yogen_datafeed] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [yogen_datafeed] SET  DISABLE_BROKER 
GO
ALTER DATABASE [yogen_datafeed] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [yogen_datafeed] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [yogen_datafeed] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [yogen_datafeed] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [yogen_datafeed] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [yogen_datafeed] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [yogen_datafeed] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [yogen_datafeed] SET RECOVERY FULL 
GO
ALTER DATABASE [yogen_datafeed] SET  MULTI_USER 
GO
ALTER DATABASE [yogen_datafeed] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [yogen_datafeed] SET DB_CHAINING OFF 
GO
ALTER DATABASE [yogen_datafeed] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [yogen_datafeed] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [yogen_datafeed] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'yogen_datafeed', N'ON'
GO
USE [yogen_datafeed]
GO
/****** Object:  Table [dbo].[billing]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[billing](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ORDER_ID] [bigint] NULL,
	[BILLING_UID] [nvarchar](255) NOT NULL,
	[FIRST_NAME] [nvarchar](255) NULL,
	[LAST_NAME] [nvarchar](255) NULL,
	[CITIZEN_NUM] [nvarchar](100) NULL,
	[TAX_NUM] [nvarchar](100) NULL,
	[SERIAL_NUM] [nvarchar](100) NULL,
	[ADDRESS1] [nvarchar](255) NULL,
	[ADDRESS2] [nvarchar](255) NULL,
	[NEIGHBOURHOOD] [nvarchar](255) NULL,
	[CITY] [nvarchar](100) NULL,
	[REGION] [nvarchar](100) NULL,
	[COUNTRY] [nvarchar](100) NULL,
	[PO_BOX_NUMBER] [nvarchar](100) NULL,
	[POSTAL_CODE] [nvarchar](100) NULL,
	[LATITUDE] [nvarchar](255) NULL,
	[LONGITUDE] [nvarchar](255) NULL,
	[GEOHASH] [nvarchar](255) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_billing_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[billing_version]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[billing_version](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ORDER_VERSION_ID] [bigint] NOT NULL,
	[BILLING_UID] [nvarchar](255) NOT NULL,
	[FIRST_NAME] [nvarchar](100) NULL,
	[LAST_NAME] [nvarchar](100) NULL,
	[CITIZEN_NUM] [nvarchar](100) NULL,
	[TAX_NUM] [nvarchar](100) NULL,
	[SERIAL_NUM] [nvarchar](100) NULL,
	[ADDRESS1] [nvarchar](100) NULL,
	[ADDRESS2] [nvarchar](100) NULL,
	[NEIGHBOURHOOD] [nvarchar](100) NULL,
	[CITY] [nvarchar](100) NULL,
	[REGION] [nvarchar](100) NULL,
	[COUNTRY] [nvarchar](100) NULL,
	[PO_BOX_NUMBER] [nvarchar](100) NULL,
	[POSTAL_CODE] [nvarchar](100) NULL,
	[LATITUDE] [nvarchar](255) NULL,
	[LONGITUDE] [nvarchar](255) NULL,
	[GEOHASH] [nvarchar](255) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_billing_version_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[chargeback_detail]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[chargeback_detail](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ORDER_ID] [bigint] NOT NULL,
	[CHARGEBACK_UID] [nvarchar](255) NOT NULL,
	[IS_PARTIAL] [bit] NULL,
	[CHARGEBACK_AT] [datetime2](0) NOT NULL,
	[AMOUNT] [float] NOT NULL,
	[REASON_CODE] [int] NOT NULL,
	[REASON_DESCRIPTION] [nvarchar](255) NULL,
	[STATE] [int] NOT NULL,
	[GATEWAY] [nvarchar](100) NULL,
	[ARN] [nvarchar](100) NULL,
	[CREDIT_CARD_COMPANY] [nvarchar](100) NULL,
	[RESPOND_AT] [datetime2](0) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_chargeback_detail_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[chargeback_detail_version]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[chargeback_detail_version](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ORDER_VERSION_ID] [bigint] NOT NULL,
	[CHARGEBACK_UID] [nvarchar](255) NOT NULL,
	[IS_PARTIAL] [bit] NULL,
	[CHARGEBACK_AT] [datetime2](0) NOT NULL,
	[AMOUNT] [float] NOT NULL,
	[REASON_CODE] [int] NOT NULL,
	[REASON_DESCRIPTION] [nvarchar](255) NULL,
	[STATUS] [int] NOT NULL,
	[GATEWAY] [nvarchar](100) NULL,
	[ARN] [nvarchar](100) NULL,
	[CREDIT_CARD_COMPANY] [nvarchar](100) NULL,
	[RESPOND_BY] [datetime2](0) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_chargeback_detail_version_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[contact]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[contact](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ORDER_ID] [bigint] NOT NULL,
	[CONTACT_UID] [nvarchar](255) NOT NULL,
	[CONTACT_TYPE] [nvarchar](100) NOT NULL,
	[EMAIL] [nvarchar](255) NULL,
	[PHONE] [nvarchar](100) NULL,
	[ACCOUNT_URL] [nvarchar](255) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_contact_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[contact_version]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[contact_version](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ORDER_VERSION_ID] [bigint] NOT NULL,
	[CONTACT_UID] [nvarchar](255) NOT NULL,
	[CONTACT_TYPE] [nvarchar](100) NOT NULL,
	[EMAIL] [nvarchar](255) NULL,
	[PHONE] [nvarchar](100) NULL,
	[ACCOUNT_URL] [nvarchar](255) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_contact_version_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[device]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[device](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ORDER_ID] [bigint] NOT NULL,
	[DEVICE_UID] [nvarchar](255) NULL,
	[TYPE] [nvarchar](100) NULL,
	[MANUFACTURER] [nvarchar](100) NULL,
	[MODEL] [nvarchar](100) NULL,
	[OS] [nvarchar](100) NULL,
	[IP_ADDRESS] [nvarchar](100) NOT NULL,
	[BROWSER] [nvarchar](100) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_device_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[device_version]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[device_version](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ORDER_VERSION_ID] [bigint] NOT NULL,
	[DEVICE_UID] [nvarchar](255) NULL,
	[TYPE] [nvarchar](100) NULL,
	[MANUFACTURER] [nvarchar](100) NULL,
	[MODEL] [nvarchar](100) NULL,
	[OS] [nvarchar](100) NULL,
	[IP_ADDRESS] [nvarchar](100) NOT NULL,
	[BROWSER] [nvarchar](100) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_device_version_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[item]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[item](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ORDER_ID] [bigint] NOT NULL,
	[ITEM_UID] [nvarchar](255) NOT NULL,
	[TYPE] [int] NOT NULL,
	[UPC] [nvarchar](255) NULL,
	[SKU] [nvarchar](255) NULL,
	[PRICE] [float] NULL,
	[PAID_PRICE] [float] NULL,
	[QUANTITY] [int] NULL,
	[BRAND] [nvarchar](255) NULL,
	[MANUFACTURER] [nvarchar](255) NULL,
	[TITLE] [nvarchar](255) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
	[COLOR] [nvarchar](50) NULL,
	[VOLUME] [int] NULL,
	[WEIGHT] [int] NULL,
	[LEG_ID] [nvarchar](255) NULL,
	[TRANSPORT_METHOD] [int] NULL,
	[TICKET_CLASS] [nvarchar](50) NULL,
	[DEPARTURE_PORT_CODE] [nvarchar](25) NULL,
	[DEPARTURE_CITY] [nvarchar](60) NULL,
	[DEPARTURE_COUNTRY_CODE] [nvarchar](2) NULL,
	[ARRIVAL_PORT_CODE] [nvarchar](25) NULL,
	[ARRIVAL_CITY] [nvarchar](60) NULL,
	[ARRIVAL_COUNTRY_CODE] [nvarchar](2) NULL,
	[DEPARTURE_DATE] [datetime2](0) NULL,
	[ARRIVAL_DATE] [datetime2](0) NULL,
	[CARRIER_NAME] [nvarchar](255) NULL,
	[CARRIER_CODE] [nvarchar](25) NULL,
	[ROUTE_INDEX] [int] NULL,
	[LEG_INDEX] [int] NULL,
	[SENDER_NAME] [nvarchar](255) NULL,
	[SENDER_EMAIL] [nvarchar](255) NULL,
	[DISPLAY_NAME] [nvarchar](255) NULL,
	[PHOTO_UPLOADED] [bit] NULL,
	[PHOTO_URL] [nvarchar](255) NULL,
	[GREETING_PHOTO_URL] [nvarchar](255) NULL,
	[MESSAGE] [nvarchar](255) NULL,
	[GREETING_MESSAGE] [nvarchar](255) NULL,
	[EVENT_NAME] [nvarchar](255) NULL,
	[EVENT_START_TIME] [datetime2](0) NULL,
	[EVENT_END_TIME] [datetime2](0) NULL,
	[EVENT_CATEGORY] [nvarchar](255) NULL,
	[EVENT_VENUE_NAME] [nvarchar](255) NULL,
	[EVENT_ADDRESS1] [nvarchar](255) NULL,
	[EVENT_ADDRESS2] [nvarchar](255) NULL,
	[EVENT_NEIGHBOURHOOD] [nvarchar](100) NULL,
	[EVENT_CITY] [nvarchar](100) NULL,
	[EVENT_REGION] [nvarchar](100) NULL,
	[EVENT_COUNTRY] [nvarchar](100) NULL,
	[EVENT_PO_BOX_NUMBER] [nvarchar](100) NULL,
	[EVENT_POSTAL_CODE] [nvarchar](100) NULL,
	[EVENT_LATITUDE] [nvarchar](255) NULL,
	[EVENT_LONGITUDE] [nvarchar](255) NULL,
	[EVENT_GEOHASH] [nvarchar](255) NULL,
 CONSTRAINT [PK_item_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[item_version]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[item_version](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ORDER_VERSION_ID] [bigint] NOT NULL,
	[ITEM_UID] [nvarchar](255) NOT NULL,
	[TYPE] [int] NOT NULL,
	[UPC] [nvarchar](100) NULL,
	[SKU] [nvarchar](100) NULL,
	[PRICE] [float] NULL,
	[PAID_PRICE] [float] NULL,
	[QUANTITY] [int] NULL,
	[BRAND] [nvarchar](255) NULL,
	[MANUFACTURER] [nvarchar](255) NULL,
	[TITLE] [nvarchar](255) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
	[COLOR] [nvarchar](50) NULL,
	[VOLUME] [int] NULL,
	[WEIGHT] [int] NULL,
	[LEG_ID] [nvarchar](255) NULL,
	[TRANSPORT_METHOD] [int] NULL,
	[TICKET_CLASS] [nvarchar](50) NULL,
	[DEPARTURE_PORT_CODE] [nvarchar](25) NULL,
	[DEPARTURE_CITY] [nvarchar](60) NULL,
	[DEPARTURE_COUNTRY_CODE] [nvarchar](2) NULL,
	[ARRIVAL_PORT_CODE] [nvarchar](25) NULL,
	[ARRIVAL_CITY] [nvarchar](60) NULL,
	[ARRIVAL_COUNTRY_CODE] [nvarchar](2) NULL,
	[DEPARTURE_DATE] [datetime2](0) NULL,
	[ARRIVAL_DATE] [datetime2](0) NULL,
	[CARRIER_NAME] [nvarchar](255) NULL,
	[CARRIER_CODE] [nvarchar](25) NULL,
	[ROUTE_INDEX] [int] NULL,
	[LEG_INDEX] [int] NULL,
	[SENDER_NAME] [nvarchar](255) NULL,
	[SENDER_EMAIL] [nvarchar](255) NULL,
	[DISPLAY_NAME] [nvarchar](255) NULL,
	[PHOTO_UPLOADED] [bit] NULL,
	[PHOTO_URL] [nvarchar](255) NULL,
	[GREETING_PHOTO_URL] [nvarchar](255) NULL,
	[MESSAGE] [nvarchar](255) NULL,
	[GREETING_MESSAGE] [nvarchar](255) NULL,
	[EVENT_NAME] [nvarchar](255) NULL,
	[EVENT_START_TIME] [datetime2](0) NULL,
	[EVENT_END_TIME] [datetime2](0) NULL,
	[EVENT_CATEGORY] [nvarchar](255) NULL,
	[EVENT_VENUE_NAME] [nvarchar](255) NULL,
	[EVENT_ADDRESS1] [nvarchar](100) NULL,
	[EVENT_ADDRESS2] [nvarchar](100) NULL,
	[EVENT_NEIGHBOURHOOD] [nvarchar](100) NULL,
	[EVENT_CITY] [nvarchar](100) NULL,
	[EVENT_REGION] [nvarchar](100) NULL,
	[EVENT_COUNTRY] [nvarchar](100) NULL,
	[EVENT_PO_BOX_NUMBER] [nvarchar](100) NULL,
	[EVENT_POSTAL_CODE] [bigint] NULL,
	[EVENT_LATITUDE] [nvarchar](255) NULL,
	[EVENT_LONGITUDE] [nvarchar](255) NULL,
	[EVENT_GEOHASH] [nvarchar](255) NULL,
 CONSTRAINT [PK_item_version_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[order]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[order](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TRANSACTION_ID] [bigint] NOT NULL,
	[ORDER_UID] [nvarchar](255) NOT NULL,
	[NAME] [nvarchar](255) NULL,
	[CREATED_AT] [datetime2](0) NULL,
	[CLOSED_AT] [datetime2](0) NULL,
	[UPDATED_AT] [datetime2](0) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_order_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[order_version]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[order_version](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TRANSACTION_VERSION_ID] [bigint] NOT NULL,
	[ORDER_UID] [nvarchar](255) NOT NULL,
	[NAME] [nvarchar](100) NULL,
	[CREATED_AT] [datetime2](0) NULL,
	[CLOSED_AT] [datetime2](0) NULL,
	[UPDATED_AT] [datetime2](0) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_order_version_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[payment_detail]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[payment_detail](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ORDER_ID] [bigint] NOT NULL,
	[PAYMENT_DETAIL_UID] [nvarchar](255) NOT NULL,
	[PAYMENT_TYPE] [int] NOT NULL,
	[INSTALLMENT] [int] NOT NULL,
	[TOTAL_PRICE] [float] NOT NULL,
	[TOTAL_PAID_PRICE] [float] NOT NULL,
	[CURRENCY] [nvarchar](3) NOT NULL,
	[CREDIT_CARD_BIN] [nvarchar](6) NOT NULL,
	[CREDIT_CARD_LAST_FOUR_DIGIT] [nvarchar](4) NOT NULL,
	[CREDIT_CARD_NO] [nvarchar](19) NULL,
	[CREDIT_CARD_COMPANY] [nvarchar](100) NOT NULL,
	[IS_THREE_D] [bit] NOT NULL,
	[THREE_D_INQUIRY_RESULT] [bit] NOT NULL,
	[PAYMENT_RESULT] [bit] NOT NULL,
	[ECI_CODE] [nvarchar](2) NULL,
	[CAVV_CODE] [nvarchar](1) NULL,
	[MD_STATUS] [nvarchar](1) NULL,
	--- TODO burdaki izin verilen sizelar degisecek
	[AVS_RESULT_CODE] [nvarchar](1) NULL,
	[CVV_RESULT_CODE] [nvarchar](1) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_payment_detail_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[payment_detail_version]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[payment_detail_version](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ORDER_VERSION_ID] [bigint] NOT NULL,
	[PAYMENT_DETAIL_UID] [nvarchar](255) NOT NULL,
	[PAYMENT_TYPE] [int] NOT NULL,
	[INSTALLMENT] [int] NOT NULL,
	[TOTAL_PRICE] [float] NOT NULL,
	[TOTAL_PAID_PRICE] [float] NOT NULL,
	[CURRENCY] [nvarchar](3) NOT NULL,
	[CREDIT_CARD_BIN] [nvarchar](6) NOT NULL,
	[CREDIT_CARD_LAST_FOUR_DIGIT] [nvarchar](4) NOT NULL,
	[CREDIT_CARD_COMPANY] [nvarchar](100) NOT NULL,
	[IS_THREE_D] [bit] NOT NULL,
	[THREE_D_INQUIRY_RESULT] [bit] NOT NULL,
	[PAYMENT_RESULT] [bit] NOT NULL,
	[ECI_CODE] [nvarchar](2) NULL,
	[CAVV_CODE] [nvarchar](1) NULL,
	[MD_STATUS] [nvarchar](1) NULL,
	[AVS_RESULT_CODE] [nvarchar](1) NULL,
	[CVV_RESULT_CODE] [nvarchar](1) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_payment_detail_version_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[purchaser]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[purchaser](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ORDER_ID] [bigint] NOT NULL,
	[PURCHASER_UID] [nvarchar](255) NOT NULL,
	[FIRST_NAME] [nvarchar](255) NULL,
	[LAST_NAME] [nvarchar](255) NULL,
	[MAIL] [nvarchar](255) NULL,
	[PHONE] [nvarchar](100) NULL,
	[VERIFIED_MAIL] [nvarchar](255) NULL,
	[MAIL_VERIFIED_TIME] [datetime2](0) NULL,
	[VERIFIED_PHONE] [nvarchar](100) NULL,
	[PHONE_VERIFIED_TIME] [datetime2](0) NULL,
	[FIRST_PURCHASE_AT] [datetime2](0) NULL,
	[ORDERS_COUNT] [int] NULL,
	[ACCOUNT_TYPE] [nvarchar](100) NULL,
	[DATE_OF_BIRTH] [datetime2](0) NULL,
	[USER_NAME] [nvarchar](100) NULL,
	[LANGUAGE] [nvarchar](100) NULL,
	[GENDER] [nvarchar](1) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_purchaser_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[purchaser_version]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[purchaser_version](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ORDER_VERSION_ID] [bigint] NOT NULL,
	[PURCHASER_UID] [nvarchar](255) NOT NULL,
	[FIRST_NAME] [nvarchar](100) NULL,
	[LAST_NAME] [nvarchar](100) NULL,
	[MAIL] [nvarchar](255) NULL,
	[PHONE] [nvarchar](100) NULL,
	[VERIFIED_MAIL] [nvarchar](255) NULL,
	[MAIL_VERIFIED_TIME] [datetime2](0) NULL,
	[VERIFIED_PHONE] [nvarchar](100) NULL,
	[PHONE_VERIFIED_TIME] [datetime2](0) NULL,
	[FIRST_PURCHASE_AT] [datetime2](0) NULL,
	[ORDERS_COUNT] [int] NULL,
	[ACCOUNT_TYPE] [nvarchar](100) NULL,
	[DATE_OF_BIRTH] [datetime2](0) NULL,
	[USER_NAME] [nvarchar](100) NULL,
	[LANGUAGE] [nvarchar](100) NULL,
	[GENDER] [nvarchar](1) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_purchaser_version_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[refund_detail]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[refund_detail](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ORDER_ID] [bigint] NOT NULL,
	[REFUND_UID] [nvarchar](255) NOT NULL,
	[IS_PARTIAL] [bit] NOT NULL,
	[REFUNDED_AT] [datetime2](0) NOT NULL,
	[AMOUNT] [float] NOT NULL,
	[REASON_CODE] [int] NOT NULL,
	[REASON_DESCRIPTION] [nvarchar](255) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_refund_detail_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[refund_detail_version]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[refund_detail_version](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ORDER_VERSION_ID] [bigint] NOT NULL,
	[REFUND_UID] [nvarchar](255) NOT NULL,
	[IS_PARTIAL] [bit] NOT NULL,
	[REFUNDED_AT] [datetime2](0) NOT NULL,
	[AMOUNT] [float] NOT NULL,
	[REASON_CODE] [int] NOT NULL,
	[REASON_DESCRIPTION] [nvarchar](255) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_refund_detail_version_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[seller]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[seller](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ITEM_ID] [bigint] NOT NULL,
	[SELLER_UID] [nvarchar](255) NOT NULL,
	[NAME] [nvarchar](255) NULL,
	[ADDRESS1] [nvarchar](255) NULL,
	[ADDRESS2] [nvarchar](255) NULL,
	[NEIGHBOURHOOD] [nvarchar](255) NULL,
	[CITY] [nvarchar](100) NULL,
	[REGION] [nvarchar](100) NULL,
	[COUNTRY] [nvarchar](100) NULL,
	[PO_BOX_NUMBER] [nvarchar](100) NULL,
	[POSTAL_CODE] [nvarchar](100) NULL,
	[LATITUDE] [nvarchar](255) NULL,
	[LONGITUDE] [nvarchar](255) NULL,
	[GEOHASH] [nvarchar](255) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_seller_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[seller_version]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[seller_version](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ITEM_VERSION_ID] [bigint] NOT NULL,
	[SELLER_UID] [nvarchar](255) NOT NULL,
	[NAME] [nvarchar](100) NULL,
	[ADDRESS1] [nvarchar](100) NULL,
	[ADDRESS2] [nvarchar](100) NULL,
	[NEIGHBOURHOOD] [nvarchar](100) NULL,
	[CITY] [nvarchar](100) NULL,
	[REGION] [nvarchar](100) NULL,
	[COUNTRY] [nvarchar](100) NULL,
	[PO_BOX_NUMBER] [nvarchar](100) NULL,
	[POSTAL_CODE] [nvarchar](100) NULL,
	[LATITUDE] [nvarchar](255) NULL,
	[LONGITUDE] [nvarchar](255) NULL,
	[GEOHASH] [nvarchar](255) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_seller_version_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[shipment]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[shipment](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ORDER_ID] [bigint] NOT NULL,
	[SHIPMENT_UID] [nvarchar](255) NOT NULL,
	[FIRST_NAME] [nvarchar](100) NULL,
	[LAST_NAME] [nvarchar](100) NULL,
	[CITIZEN_NUM] [nvarchar](100) NULL,
	[TAX_NUM] [nvarchar](100) NULL,
	[DELIVERY_COMPANY] [nvarchar](100) NULL,
	[ADDRESS1] [nvarchar](255) NULL,
	[ADDRESS2] [nvarchar](255) NULL,
	[NEIGHBOURHOOD] [nvarchar](255) NULL,
	[CITY] [nvarchar](100) NULL,
	[REGION] [nvarchar](100) NULL,
	[COUNTRY] [nvarchar](100) NULL,
	[PO_BOX_NUMBER] [nvarchar](100) NULL,
	[POSTAL_CODE] [nvarchar](100) NULL,
	[LATITUDE] [nvarchar](255) NULL,
	[LONGITUDE] [nvarchar](255) NULL,
	[GEOHASH] [nvarchar](255) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_shipment_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[shipment_version]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[shipment_version](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ORDER_VERSION_ID] [bigint] NOT NULL,
	[SHIPMENT_UID] [nvarchar](255) NOT NULL,
	[FIRST_NAME] [nvarchar](100) NULL,
	[LAST_NAME] [nvarchar](100) NULL,
	[CITIZEN_NUM] [nvarchar](100) NULL,
	[TAX_NUM] [nvarchar](100) NULL,
	[DELIVERY_COMPANY] [nvarchar](100) NULL,
	[ADDRESS1] [nvarchar](100) NULL,
	[ADDRESS2] [nvarchar](100) NULL,
	[NEIGHBOURHOOD] [nvarchar](100) NULL,
	[CITY] [nvarchar](100) NULL,
	[REGION] [nvarchar](100) NULL,
	[COUNTRY] [nvarchar](100) NULL,
	[PO_BOX_NUMBER] [nvarchar](100) NULL,
	[POSTAL_CODE] [nvarchar](100) NULL,
	[LATITUDE] [nvarchar](255) NULL,
	[LONGITUDE] [nvarchar](255) NULL,
	[GEOHASH] [nvarchar](255) NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_shipment_version_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[social_detail]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[social_detail](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PURCHASER_ID] [bigint] NOT NULL,
	[SOCIAL_DETAIL_UID] [nvarchar](255) NOT NULL,
	[NETWORK] [nvarchar](100) NULL,
	[PUBLIC_USERNAME] [nvarchar](100) NULL,
	[COMMUNITY_SCORE] [int] NULL,
	[ACCOUNT_URL] [nvarchar](255) NULL,
	[EMAIL] [nvarchar](255) NULL,
	[BIO] [nvarchar](255) NULL,
	[FOLLOWING] [int] NULL,
	[FOLLOWED] [int] NULL,
	[POSTS] [int] NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_social_detail_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[social_detail_version]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[social_detail_version](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PURCHASER_VERSION_ID] [bigint] NOT NULL,
	[SOCIAL_DETAIL_UID] [nvarchar](255) NOT NULL,
	[NETWORK] [nvarchar](100) NULL,
	[PUBLIC_USERNAME] [nvarchar](100) NULL,
	[COMMUNITY_SCORE] [int] NULL,
	[ACCOUNT_URL] [nvarchar](255) NULL,
	[MAIL] [nvarchar](255) NULL,
	[BIO] [nvarchar](255) NULL,
	[FOLLOWING] [int] NULL,
	[FOLLOWED] [int] NULL,
	[POSTS] [int] NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_social_detail_version_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[transaction]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[transaction](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TRANSACTION_UID] [nvarchar](255) NOT NULL,
	[CONSUMER_ID] [bigint] NOT NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_transaction_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[transaction_version]    Script Date: 2.11.2018 10:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[transaction_version](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TRANSACTION_ID] [bigint] NOT NULL,
	[TRANSACTION_UID] [nvarchar](255) NOT NULL,
	[CREATED_DATE] [datetime2](0) NOT NULL,
	[UPDATED_DATE] [datetime2](0) NULL,
 CONSTRAINT [PK_transaction_version_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[payment_detail] ADD  DEFAULT ((1)) FOR [PAYMENT_TYPE]
GO
ALTER TABLE [dbo].[payment_detail] ADD  DEFAULT ((1)) FOR [INSTALLMENT]
GO
ALTER TABLE [dbo].[payment_detail] ADD  DEFAULT ((0)) FOR [IS_THREE_D]
GO
ALTER TABLE [dbo].[payment_detail] ADD  DEFAULT ((0)) FOR [THREE_D_INQUIRY_RESULT]
GO
ALTER TABLE [dbo].[payment_detail] ADD  DEFAULT ((0)) FOR [PAYMENT_RESULT]
GO
ALTER TABLE [dbo].[payment_detail_version] ADD  DEFAULT ((1)) FOR [PAYMENT_TYPE]
GO
ALTER TABLE [dbo].[payment_detail_version] ADD  DEFAULT ((1)) FOR [INSTALLMENT]
GO
ALTER TABLE [dbo].[payment_detail_version] ADD  DEFAULT ((0)) FOR [IS_THREE_D]
GO
ALTER TABLE [dbo].[payment_detail_version] ADD  DEFAULT ((0)) FOR [THREE_D_INQUIRY_RESULT]
GO
ALTER TABLE [dbo].[payment_detail_version] ADD  DEFAULT ((0)) FOR [PAYMENT_RESULT]
GO
USE [master]
GO
ALTER DATABASE [yogen_datafeed] SET  READ_WRITE 
GO
